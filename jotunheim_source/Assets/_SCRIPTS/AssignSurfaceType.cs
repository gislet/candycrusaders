﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssignSurfaceType : MonoBehaviour, ISurface {

	[SerializeField]
	private SurfaceType _surface_type;

	public SurfaceType surface_type {
		get {
			return _surface_type;
		}
	}
}
