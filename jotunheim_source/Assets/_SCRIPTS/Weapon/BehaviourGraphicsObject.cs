﻿/******************************************************************************
*
* BehaviourGraphicsObject.cs
*
* asdf
*
*
* Written by: Gisle Thorsen (August, 2017)
*
******************************************************************************/


using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Weapons.Modules;


public class BehaviourGraphicsObject {


	/* */
	private GameObject _ourself;
	private Weapons.Modules.Graphics _mod_graphics;



	/* */
	public BehaviourGraphicsObject(GameObject p_attack_object, Weapons.Modules.Graphics p_mod_graphics) {
		_mod_graphics = p_mod_graphics;
		initiate(p_attack_object);
	}


	/* */
	//TODO fix the rotations of the graphical object
	private void initiate(GameObject p_attack_object) {
		if(_mod_graphics == null || _mod_graphics.prefab_object.Length == 0)
			return;

		/* Instantiate the graphics-object if it isn't already. */
		if(_ourself == null) {
			GameObject prefab = Resources.Load(_mod_graphics.prefab_object, typeof(GameObject)) as GameObject;
			_ourself = Master.spawnObject(prefab);
		}

		/* Assign the attack-object as its parent and reset its transforms. */
		_ourself.transform.SetParent(p_attack_object.transform);
		_ourself.transform.localPosition = Vector3.zero;
		_ourself.transform.localRotation = Quaternion.identity;

		//Debug.Log("Euler: " + p_object.transform.localEulerAngles);
		/*
		p_object.transform.localScale = Vector3.one;*/
	}


	/* */
	internal void update(Shape p_shape) {
		if(_ourself == null || p_shape == null)
			return;

		_ourself.transform.position = p_shape.position;
		//GlobalMovement.matchDirection(_ourself, p_shape.direction);
	}


	/* */
	internal void terminate<T>(GameObject p_collider = null) where T : class {
		if(_ourself != null) {
			if(typeof(T) == typeof(Weapons.Modules.Time)) {
				expirationBehaviour(_mod_graphics.expiration_time, p_collider);
			} else if(typeof(T) == typeof(Weapons.Modules.AreaOfEffect)) {
				expirationBehaviour(_mod_graphics.expiration_aoe_trigger, p_collider);
			} else if(typeof(T) == typeof(Weapons.Modules.Movement)) {
				expirationBehaviour(_mod_graphics.expiration_environment_trigger, p_collider);
			} else {
				throw new NotImplementedException("Behaviour for terminating a graphics object doesn't recognise using a module of type: " + typeof(T).ToString());
			}
		}
	}


	/* */
	private void expirationBehaviour(ExpirationBehaviour p_behaviour, GameObject p_collider) {
		//TODO, some objects might want to persit a little longer after being called to be removed.
		//Some objects might also spawn something new in its place after being removed.

		switch(p_behaviour) {
		case ExpirationBehaviour.None:
		case ExpirationBehaviour.Stop:
			break;
		case ExpirationBehaviour.Attach:
			throw new NotImplementedException("No Procedure for CollisionReaction.Attach");
		//break;
		case ExpirationBehaviour.Destroy:
			Master.destroyObject(_ourself);
			break;
		default:
			Debug.LogError("The lastGraphicsObjectUpdate() does not have a procedure for the behaviour of type: " + p_behaviour.ToString());
			break;
		}
	}


	#region Getters & Setters
	public GameObject game_object {
		get { return _ourself; }
	}
	#endregion
}
