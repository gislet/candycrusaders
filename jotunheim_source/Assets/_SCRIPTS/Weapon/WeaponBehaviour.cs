﻿/***
 * 
 * WeaponBehaviour.cs
 * 
 * ...
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Weapons {

	public class WeaponBehaviour : IComparable, IIdentifiable {

		/* */
		public Modules.AreaOfEffect mod_areaofeffect = null;
		public Modules.Movement mod_movement = null;
		public Modules.Time mod_time = null;
		public Modules.StatusEffect mod_statuseffect = null;
		public Modules.ExtraBehaviour mod_extrabehaviour = null;
		public Modules.Graphics mod_graphics = null;
		private Weapon _weapon;
		private List<Transform> _transforms_hit;



		/* */
		public WeaponBehaviour() {
			name = "New Behaviour";
			damage_modifier = 100;
			cooldown = Globals.GLOBAL_COOLDOWN;
			standalone = false;
			self_infliction = false;
			repeat_infliction = false;

			do {
				ID = Globals.ID_Creator();
			} while(!Globals.ID_Available(ID, DatabaseHandler.behaviour_database != null ? DatabaseHandler.behaviour_database.list_all : null));
		}


		/* */
		public WeaponBehaviour(WeaponBehaviour p_image) {	

			/* ID Variables. */
			name = p_image.name;
			ID = p_image.ID;

			/* */
			damage_modifier = p_image.damage_modifier;
			cooldown = p_image.cooldown;
			standalone = p_image.standalone;
			self_infliction = p_image.self_infliction;
			repeat_infliction = p_image.repeat_infliction;

			/* Modules. */
			mod_areaofeffect = p_image.mod_areaofeffect != null ? new Modules.AreaOfEffect(p_image.mod_areaofeffect) : null;
			mod_movement = p_image.mod_movement != null ? new Modules.Movement(p_image.mod_movement) : null;
			mod_time = p_image.mod_time != null ? new Modules.Time(p_image.mod_time) : null;
			mod_statuseffect = p_image.mod_statuseffect != null ? new Modules.StatusEffect(p_image.mod_statuseffect) : null;
			mod_extrabehaviour = p_image.mod_extrabehaviour != null ? new Modules.ExtraBehaviour(p_image.mod_extrabehaviour) : null;
			mod_graphics = p_image.mod_graphics != null ? new Modules.Graphics(p_image.mod_graphics) : null;
		}


		/* */
		public WeaponBehaviour(Weapon p_weapon, WeaponBehaviour p_image) : this(p_image) {
			_weapon = p_weapon;
		}


		/* */
		public void attack() {
			Master.monobehaviour.StartCoroutine(update());			
		}


		/* */
		private IEnumerator update() {

			/* */
			_transforms_hit = new List<Transform>();
			Transform owner = _weapon.getOwner().transform;

			/* */
			GameObject attack_visualiser = null;
			GameObject attack_object = null;

			/* Initialisation */
			updateAttackObject(ref attack_object);
			BehaviourGraphicsObject graphics = new BehaviourGraphicsObject(attack_object, mod_graphics);

			/* */
			Shape previous_shape = null;
			bool forced_continuous = false;
			

			do {
				if(mod_time == null || !mod_time.ticking || mod_time.tickUpdate() || (forced_continuous = (mod_movement != null && mod_movement.force_continuous))) {
					float rotational_offset = rotationalOffset();
					float directional_offset = directionalOffset();
					Shape shape = getShape(attack_object.transform, rotational_offset, directional_offset);

					if(mod_areaofeffect != null && !forced_continuous) {
						CombatHandler.Damageable[] damageables = CombatHandler.getDamageables();

						for(int k = 0; k < damageables.Length; k++) {

							/* We then make sure the attack isn't colliding with ourselves, unless we
							 * have self inflicting enabled. */
							if(self_infliction || damageables[k].transform != _weapon.getOwner().transform) {

								/* */
								if(repeat_infliction || (!_transforms_hit.Contains(damageables[k].transform) && CollisionHandler.isColliding(shape, damageables[k].transform.GetComponent<CharacterHandler>().getCharacter))) {
									damageables[k].obj.damage(new AttackOrder(), damage());
									if(!repeat_infliction)
										_transforms_hit.Add(damageables[k].transform);
									//collided = true;

									/* This setting takes presidence over any other life setting, and will immediately cease this weaponbehaviour. */
									if(mod_areaofeffect.ontrigger_stop) {
										graphics.terminate<Modules.AreaOfEffect>(damageables[0].transform.gameObject);
										throw new NotImplementedException();
										//break;
									}
								}
							}
						}

						if(DebugHandler.attack_visualiser && ((mod_time != null ? mod_time.ticking : false) || (mod_time != null && !mod_time.ticking && attack_visualiser == null)))
							attack_visualiser = MeshHandler.create(shape);
						else if(attack_visualiser != null) {
							/* Positioning and Rotation. */
							attack_visualiser.transform.position = shape.position;

							if(shape.GetType() != typeof(Circle)) {
								GlobalMovement.matchDirection(attack_visualiser, shape.direction);

								/*float shape_angle = GlobalMovement.findAngleByDirection(shape.direction);
								float av_angle = GlobalMovement.findAngleByDirection(GlobalMovement.findDirectionByTransform(attack_visualiser.transform));
								float diff = (shape_angle - av_angle);
								attack_visualiser.transform.Rotate(diff * Vector3.up);*/
							}
						}

						/* */
						updateAttackObject(ref attack_object);
						
					}

					/* Check to see if this behaviour is set to disable once it collides with the environment,
					 * and then stop it if it has. */
					if(mod_movement != null && mod_movement.ontrigger_stop && environmentCollision(previous_shape, shape)) {
						graphics.terminate<Modules.Movement>();
						break;
					}

					/* */
					graphics.update(shape);

					/* Finalising Iteration. */
					previous_shape = shape;
					forced_continuous = false;
				}
				
				yield return null;
			} while(!ceaseAttack());


			/* Attempting to terminate the graphics object when the behaviour has expired because of old age or other 'natural' causes. */
			if(ceaseAttack()) {
				graphics.terminate<Modules.Time>();
			}

			finalBehaviour();
		}


		/* */
		private void finalBehaviour() {
		}


		/* */
		private Shape getShape(Transform p_owner, float p_rotational_offset, float p_directional_offset) {
			Shape shape = null;

			if(mod_areaofeffect != null) {
				shape = mod_areaofeffect.getShape(p_owner, p_rotational_offset, p_directional_offset);
			} else {
				shape = new Modules.AreaOfEffect().getShape(p_owner, p_rotational_offset, p_directional_offset);
			}

			return shape;
		}


		/* */
		private float directionalOffset() {
			return (mod_movement != null && mod_time != null) ?
						mod_time.ticking && !mod_movement.force_continuous ? 
							(mod_time.currentTick() - 1) * (float)mod_movement.travel_speed : 
								mod_time.elapsedTime() * (float)mod_movement.travel_speed : 0;
		}


		/* */
		private float rotationalOffset() {
			return (mod_movement != null && mod_time != null) ?
						mod_time.ticking && !mod_movement.force_continuous ? 
							(mod_time.currentTick() - 1) * (float)mod_movement.rotation_speed : 
								mod_time.elapsedTime() * (float)mod_movement.rotation_speed : 0;
		}


		/* */
		private bool environmentCollision(Shape a, Shape b) {
			Debug.Log("Env Col Check");
			if(a == null || b == null)
				return false;

			RaycastHit hit;
			if(Physics.Linecast(a.position, b.position, out hit)) {
				if(hit.transform.gameObject.CompareTag("Untagged")) {
					return true;
				}
			}

			return false;
		}


		/* Updating our attack-object position and rotation. */
		private void updateAttackObject(ref GameObject p_object) {

			/* Every attack has an attack-object. This object is where the collisions are calculated from, allowing us to move it around
			 * if it's a movable attack. */
			if(p_object == null) {
				p_object = new GameObject();
				p_object.name = string.Format("Attack '{0}' by Player '{1}'", name, _weapon.getOwner().transform.name);
				p_object.transform.position = _weapon.getOwner().transform.position;
				float owner_angle = GlobalMovement.findAngleByDirection(GlobalMovement.findDirectionByTransform(_weapon.getOwner().transform));
				p_object.transform.Rotate(owner_angle * Vector3.up);

			/* If the attack is dependant on the owner we update it to correspond with any changes made to the weapons owner. */
			} else if(!standalone) {
				p_object.transform.position = _weapon.getOwner().transform.position;
				float owner_angle = GlobalMovement.findAngleByDirection(GlobalMovement.findDirectionByTransform(_weapon.getOwner().transform));
				float ao_angle = GlobalMovement.findAngleByDirection(GlobalMovement.findDirectionByTransform(p_object.transform));
				float diff = owner_angle - ao_angle;
				p_object.transform.Rotate(diff * Vector3.up);
			}
		}


		/* */
		private bool ceaseAttack() {
			if((_weapon.getOwner() == null || _weapon.getOwner().isDestroyed()) && !standalone) {
				return true;
			}

			if(mod_time != null) {
				if(!mod_time.finished())
					return false;
			}

			return true;
		}
		

		/* */
		public int damage(int p_base_damage = -1) {
			float modifier = damage_modifier > 0 ? (damage_modifier / Globals.PERCENTAGE) : 0;
			float modded = 0;

			if(p_base_damage < 0 && _weapon != null) {
				modded = _weapon.base_damage * modifier;
			} else if(p_base_damage >= 0) {
				modded = p_base_damage * modifier;
			}
            
            return UnityEngine.Mathf.RoundToInt(modded);
        }


		/* */
		public int CompareTo(object p_obj) {
			if(p_obj.GetType() == typeof(Weapon)) {
				if(((Weapon)p_obj).ID.Equals(ID)) {
					return 0;
				}
			}

			return -1;
		}


		#region Getters & Setters
		public string name { get; set; }
		public string ID { get; set; }
		public bool standalone { get; set; }
		public bool self_infliction { get; set; }
		public bool repeat_infliction { get; set; }
		public double cooldown { get; set; }
		public int damage_modifier { get; set; }
		#endregion
	}
}
