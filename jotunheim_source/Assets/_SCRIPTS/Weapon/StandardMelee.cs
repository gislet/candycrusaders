﻿/***
 * 
 * Weapon.cs
 * 
 * This is an entity object that gets placed on a prefab to allow for a custom
 * configuration. *more to be added*
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using Damagable = CombatHandler.Damageable;


/* */
public abstract class AttackMethod {

	protected float life = 0;
	protected float t = .25f;
	//public abstract IEnumerator attack();

	private HashSet<Transform> _transforms_hit = new HashSet<Transform>();

	public AttackMethod(AttackOrder p_order) {
		attack_order = p_order;
		life = attack_order.settings.life;
	}


	/* */
	public virtual IEnumerator attack() {
		//float t = .25f;
		float tick = t / 6;
		float tick_timer = 0;
		int i = 6;

		//FragmentHandler.create(p_order, true);
		AudioManager.playRandom(attack_order.settings.sound_normal, AudioChannel.Effects);

		while(Globals.deltaTimeCountdown(ref t) >= 0 || i >= 0) {
			if(Globals.deltaTimeCountdown(ref tick_timer) <= 0) {
				tick_timer = tick;
				Fragment[] frags = FragmentHandler.create(attack_order, i--);

				if(detectCollisions(frags)) {
					AudioManager.playRandom(attack_order.settings.sound_hit, AudioChannel.Effects);
				}
			}

			yield return null;
		}
	}


	/* */
	protected void setRelativeTransformValues(GameObject p_object) {
		p_object.transform.position = attack_order.character.position;
		p_object.transform.rotation = attack_order.character.rotation;
	}


	/* */
	protected bool detectCollisions(Fragment[] p_fragments, bool p_self_inflicting = false, bool p_unlimited_application = false, bool p_friendly_fire = false) {
		Damagable[] damagables = CombatHandler.getDamageables();
		bool collided = false;

		/* We check each fragment up against every damagable object in the game. */
		for(int i = 0; i < p_fragments.Length; i++) {
			for(int k = 0; k < damagables.Length; k++) {

				/* We want to make sure that we're allowed to damage this target, whether friendly fire is
				 * enabled or if the target is of an alignment that we deem fair game. */
				if(p_friendly_fire || !damagables[k].obj.isFriendly(attack_order.character.team)) {

					/* We make sure before doing any collision detection that the object hasn't already been collided with.
					 * If it has, we have to have unlimited applications enabled. */
					if(p_unlimited_application || !_transforms_hit.Contains(damagables[k].transform)) {

						/* We then make sure the attack isn't colliding with ourselves. Again if is, we
						 * have to have self inflicting enabled. */
						if(p_self_inflicting || damagables[k].transform != attack_order.character.transform) {

							/* Lastly we compute the collision on the fragment itself, and deal out 
							 * damage if appropriate. */
							if(p_fragments[i].collision(damagables[k].transform.position, damagables[k].size)) {
								damagables[k].obj.damage(attack_order, damage);
								_transforms_hit.Add(damagables[k].transform);
								collided = true;
							}
						}
					}
				}
			}
		}

		return collided;
	}


	#region Getters & Setters
	protected virtual int damage { get { return attack_order.settings.damage; } }
	protected AttackOrder attack_order { get; set; }
	#endregion
}


/* */
public class MeleeSwing : AttackMethod {

	public MeleeSwing(AttackOrder p_order) : base(p_order) {
	}

	/*public override IEnumerator attack() {
		float t = .25f;
		float tick = t / 6;
		float tick_timer = 0;
		int i = 6;

		//FragmentHandler.create(p_order, true);


		while(Globals.deltaTimeCountdown(ref t) >= 0 || i >= 0) {
			if(Globals.deltaTimeCountdown(ref tick_timer) <= 0) {
				tick_timer = tick;
				Fragment[] frags = FragmentHandler.create(attack_order, i--);

				detectCollisions(frags);
			}
			
			yield return null;
		}
	}*/
}


/* */
public class MeleeSlam : AttackMethod {

	public MeleeSlam(AttackOrder p_order) : base(p_order) {
	}

	public override IEnumerator attack() {

		AudioManager.playRandom(attack_order.settings.sound_normal, AudioChannel.Effects);

		while(Globals.deltaTimeCountdown(ref life) > 0) {
			yield return null;
		}
		
		Fragment[] frags = FragmentHandler.create(attack_order);
		if(detectCollisions(frags)) {
			AudioManager.playRandom(attack_order.settings.sound_hit, AudioChannel.Effects);
		} else {
			AudioManager.playRandom(attack_order.settings.sound_nohit, AudioChannel.Effects);
		}
	}

}

public class RangedShoot : AttackMethod {

	public RangedShoot(AttackOrder p_order) : base(p_order) {
	}


	public override IEnumerator attack() {

		GameObject go = Master.spawnObject(attack_order.settings.projectile);
		setRelativeTransformValues(go);

		Fragment[] frags = FragmentHandler.create(attack_order, true, go.transform);

		AudioManager.playRandom(attack_order.settings.sound_normal, AudioChannel.Effects);

		while(Globals.deltaTimeCountdown(ref life) > 0) {

			Vector3 dir = Vector3.Normalize(GlobalMovement.findDirectionByTransform(go.transform));
			GlobalMovement.byTransform(go.transform, attack_order.settings.projectile_speed, new Vector2(dir.x, dir.z), false);

			if(detectCollisions(frags)) {
				AudioManager.playRandom(attack_order.settings.sound_hit, AudioChannel.Effects);
				break;
			}

			RaycastHit hit;
			if(Physics.Raycast(go.transform.position, GlobalMovement.findDirectionByTransform(go.transform), out hit, attack_order.settings.radius / 2, attack_order.settings.layer_mask)) {
				if(hit.collider.tag != Globals.TAG_PLAYER)
					break;
			}

			yield return new WaitForFixedUpdate();
		}

		Master.destroyObject(go);
	}
}



