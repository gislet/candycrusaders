﻿/***
 * 
 * Weapon.cs
 * 
 * This is an entity object that gets placed on a prefab to allow for a custom
 * configuration. *more to be added*
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using attackRoutine = WeaponOld.attackRoutine;

public class WeaponOld : MonoBehaviour {

	[Header("Basics", order = 0)]
	[Space(10, order = 1)]
	public new string name = "";
	public Sprite icon;
	public GameObject equipment_left, equipment_right;
	public SlotPosition slotposition_left, slotposition_right;

	[Space(20, order = 2)]
	[Header("Attack Settings", order = 3)]
	[Space(10, order = 4)]
	public AttackSettings primary_attack;
	[Space(10, order = 5)]
	public AttackSettings secondary_attack;



	/* This factory returns the appropriate object for our use. */
	private attackRoutine attackFactory(AttackOrder p_order) {
		AttackMethod obj = null;

		switch(p_order.settings.routine) {
		case UseRoutine.Slam:
			obj = new MeleeSlam(p_order);
			break;
		case UseRoutine.Swing:
			obj = new MeleeSwing(p_order);
			break;
		case UseRoutine.Shoot:
			obj = new RangedShoot(p_order);
			break;
		case UseRoutine.None:
			return null;
		default:
			throw new ArgumentException();
		}
		
		return obj.attack;
	}


	/* Constructs an attackorder based off this weapons configuration, which attack method is going to be used, 
	 * and who or what ordered it. */
	public AttackOrder use(CharacterBase p_character, Use p_use) {
		AttackOrder order = new AttackOrder();

		order.character = p_character;
		order.ID = (p_character.name + System.DateTime.Now).GetHashCode();

		order.settings = p_use == Use.Primary ? primary_attack : secondary_attack;
		order.cooldown = order.settings.attack_cooldown > Globals.GLOBAL_COOLDOWN ? order.settings.attack_cooldown : Globals.GLOBAL_COOLDOWN;

		order.attack = attackFactory(order);

		return order;
	}


	/* A dynamic delegate for the attackRoutine produced by the factory. */
	public delegate IEnumerator attackRoutine();


	/* An enum to specify which use-case was called. */
	public enum Use {
		Primary, Secondary
	}


	/* */
	public enum SlotPosition {
		Arm, Hand
	}


	/* An enum to specify how the weapon behaves. */
	public enum UseRoutine {
		None, Swing, Slam, Shoot
	}
}


/* */
[System.Serializable]
public struct AttackSettings {

	[Range(0f, 60f)]
	public float attack_cooldown;
	public GameObject projectile;
	public AudioClip[] sound_normal;
	public AudioClip[] sound_hit;
	public AudioClip[] sound_nohit;

	[Range(0f, 30f)]
	public float projectile_speed;
	public LayerMask layer_mask;

	[Space(10)]

	public WeaponOld.UseRoutine routine;
	public int damage;
	[Range(0f, 60f)]
	public float life;
	public float radius;
	public float distance;
	[Range(0, 360)]
	public float angle_size;
	

}



/* A struct that acts like a receipt of an attack. It contains vital information about
 * the characteristics of the attack, who or what the attacker is, and a dynamic function
 * of the attack's behaviour. */
public struct AttackOrder {

	public int ID;
	public CharacterBase character;
	public attackRoutine attack;
	public AttackSettings settings;
	
	public float cooldown;
	public GameObject[] projectiles;

}


