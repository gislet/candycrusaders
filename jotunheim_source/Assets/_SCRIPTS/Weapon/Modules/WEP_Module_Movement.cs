﻿/******************************************************************************
*
* WEP_Module_Movement.cs
*
* Description...
*
*
* Written by: Gisle Thorsen (August, 2017)
*
******************************************************************************/


using System;
using UnityEngine;


namespace Weapons {

	/* A collection of modules that can be used with a weapon. Apply as necessary. */
	namespace Modules {

		public class Movement {

			/* Miscellaneous. */
			public bool ontrigger_stop = false;
			public bool force_continuous = false;

			/* Speed Variables. */
			public double travel_speed = 1;
			public double rotation_speed = 0;


			/* Constructors. */
			public Movement() { }
			public Movement(Movement p_image) {

				/* Miscellaneous. */
				ontrigger_stop = p_image.ontrigger_stop;
				force_continuous = p_image.force_continuous;

				/* Speed Variables. */
				travel_speed = p_image.travel_speed;
				rotation_speed = p_image.rotation_speed;
			}
		}
	}
}
