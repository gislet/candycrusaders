﻿/***
 * 
 * WEP_Module_Time.cs
 * 
 * ...
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using System;
using UnityEngine;


namespace Weapons {

	/* A collection of modules that can be used with a weapon. Apply as necessary. */
	namespace Modules {

		public class Time {

			/* */
			public bool ticking = false;
			public int ticks;
			public double tick_interval = 1;
			public double duration = 0;

			/* */
			private double _initiated;
			private int _current_tick;


			/* */
			public Time() { }
			public Time(Time p_image) {

				/* */
				_initiated = UnityEngine.Time.time;
				_current_tick = 0;

				/* */
				ticking = p_image.ticking;
				ticks = p_image.ticks;
				tick_interval = p_image.tick_interval;
				duration = p_image.duration;				
			}


			/* Tries to update the tick count. */
			public bool tickUpdate() {
				int expected_tick_value = Mathf.CeilToInt((float)(elapsedTime() / (tick_interval + 1E-6)));

				if(_current_tick < expected_tick_value) {
					_current_tick++;
					return true;
				}

				return false;
            }


			/* */
			public bool finished() {
				if(ticking) {
					if(_current_tick >= ticks)
						return true;
				} else {
					if(_initiated + duration <= UnityEngine.Time.time)
						return true;
				}

				return false;
			}


			/* */
			public int currentTick() {
				return _current_tick;
			}


			/* */
			public float elapsedTime() {
				return (float)(UnityEngine.Time.time - _initiated);
            }
		}
	}
}