﻿/***
 * 
 * StatusEffect.cs
 * 
 * ...
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using System;
using UnityEngine;


namespace Weapons {

	/* A collection of modules that can be used with a weapon. Apply as necessary. */
	namespace Modules {

		public class ExtraBehaviour {

			/* */
			public ExtraBehaviour() { }
			public ExtraBehaviour(ExtraBehaviour p_image) {
			}

		}
	}
}