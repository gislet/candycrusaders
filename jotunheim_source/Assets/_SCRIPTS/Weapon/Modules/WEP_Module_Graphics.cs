﻿/******************************************************************************
*
* WEP_Module_Graphics.cs
*
* Description...
*
*
* Written by: Gisle Thorsen (July, 2017)
*
******************************************************************************/


using UnityEngine;


namespace Weapons {

	/* A collection of modules that can be used with a weapon. Apply as necessary. */
	namespace Modules {

		public class Graphics {

			/* Miscellaneous. */
			public ExpirationBehaviour expiration_environment_trigger = ExpirationBehaviour.Destroy;
			public ExpirationBehaviour expiration_aoe_trigger = ExpirationBehaviour.Destroy;
			public ExpirationBehaviour expiration_time = ExpirationBehaviour.Destroy;

			/* */
			public string prefab_object = "";

			/* Constructors. */
			public Graphics() {	}
			public Graphics(Graphics p_image) {

				/* Miscellaneous. */
				expiration_environment_trigger = p_image.expiration_environment_trigger;

				/* */
				prefab_object = p_image.prefab_object;
			}
		}


		public enum ExpirationBehaviour {
			None,       //Collision is ignored.
			Stop,       //Stops and stays where it collided.
			Attach,     //Stops and becomes a child of whatever it collided with.
			Destroy     //Stops and is destroyed.
		}
	}
}
