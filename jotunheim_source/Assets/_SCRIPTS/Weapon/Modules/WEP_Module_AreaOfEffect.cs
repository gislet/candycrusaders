﻿/******************************************************************************
*
* WEP_Module_AreaOfEffect.cs
*
* Description...
*
*
* Written by: Gisle Thorsen (August, 2017)
*
******************************************************************************/


using System;
using UnityEngine;


namespace Weapons {

	/* A collection of modules that can be used with a weapon. Apply as necessary. */
	namespace Modules {

		public class AreaOfEffect {

			/* Miscellanous. */
			public ShapeType shape_type = ShapeType.Circle;
			public double distance_offset = 0;
			public bool ontrigger_stop = false;

			/* Circle. */
			public double circle_radius = 1;
			public int circle_degrees = 360;
			public int circle_offset = 0;


			/* Rectangle. */
			public Vec2d[] rect_corners = {
				new Vec2d(-1, -1),
				new Vec2d(1, 1)
			};

			/* Triangle. */
			public Vec2d[] tri_corners = {
				new Vec2d(-1, -1),
				new Vec2d(1, -1),
				new Vec2d(0, 1)
			};



			/* Constructors. */
			public AreaOfEffect() { }
			public AreaOfEffect(AreaOfEffect p_image) {

				/* Miscellanous. */
				shape_type = p_image.shape_type;
				distance_offset = p_image.distance_offset;
				ontrigger_stop = p_image.ontrigger_stop;

				/* Circle. */
				circle_radius = p_image.circle_radius;
				circle_degrees = p_image.circle_degrees;
				circle_offset = p_image.circle_offset;

				/* Rectangle. */
				rect_corners = p_image.rect_corners;

				/* Triangle. */
				tri_corners = p_image.tri_corners;
            }


			/* */
			public Shape getShape(Transform p_owner, float p_rotational_offset, float p_directional_offset, bool p_attach_directly = false) {
				Shape shape = null;
				Vector3 direction;

				switch(shape_type) {
				case ShapeType.Circle:
					if(p_attach_directly)
						shape = new Circle(circle_degrees, (float)circle_radius, p_owner);
					else {
						direction = circle_offset == 0 && p_rotational_offset == 0 ? GlobalMovement.findDirectionByTransform(p_owner) :
											GlobalMovement.findDirectionByAngle(p_owner.position, GlobalMovement.findAngleByDirection(GlobalMovement.findDirectionByTransform(p_owner)) - circle_offset - p_rotational_offset);

						shape = new Circle(circle_degrees, (float)circle_radius, p_owner.position + (direction * (float)distance_offset) + (direction * p_directional_offset), direction);
					}
					break;
				case ShapeType.Triangle:
					if(p_attach_directly) {
						shape = new Triangle(tri_corners, p_owner);
					} else {
						direction = p_rotational_offset == 0 ? GlobalMovement.findDirectionByTransform(p_owner) :
											GlobalMovement.findDirectionByAngle(p_owner.position, GlobalMovement.findAngleByDirection(GlobalMovement.findDirectionByTransform(p_owner)) - p_rotational_offset);

						shape = new Triangle(tri_corners, p_owner.position + (direction * (float)distance_offset) + (direction * p_directional_offset), direction);
					}
					break;
				case ShapeType.Rectangle:
					if(p_attach_directly) {
						shape = new Rectangle(rect_corners, p_owner);
					} else {

						direction = p_rotational_offset == 0 ? GlobalMovement.findDirectionByTransform(p_owner) :
											GlobalMovement.findDirectionByAngle(p_owner.position, GlobalMovement.findAngleByDirection(GlobalMovement.findDirectionByTransform(p_owner)) - p_rotational_offset);

						shape = new Rectangle(rect_corners, p_owner.position + (direction * (float)distance_offset) + (direction * p_directional_offset), direction);
					}
					break;
				}

				return shape;
			}
		}

		public enum ShapeType {
			Circle,
			Rectangle,
			Triangle
		}
	}
}
