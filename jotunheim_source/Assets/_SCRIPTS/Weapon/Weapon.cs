﻿/***
 * 
 * Weapon.cs
 * 
 * ...
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using System;
using UnityEngine;

namespace Weapons {

	[System.Serializable]
	public class Weapon : IComparable, IIdentifiable {


		public string primary_behaviour_ID = "", secondary_behaviour_ID = "";
		public string left_object = "", right_object = "";
		public LimbPosition left_limb = LimbPosition.Hand, right_limb = LimbPosition.Hand;
		private float _cooldown_initiated = 0, _cooldown = 0;
		private ITargetable _owner = null;

		/* */
		public string sprite;
		private Sprite _sprite;



		/* */
		public Weapon() {
			name = "New Weapon";
			base_damage = 1;

			do {
				ID = Globals.ID_Creator(ID);				
            } while(!Globals.ID_Available(ID, DatabaseHandler.weapons_database != null ? DatabaseHandler.weapons_database.list_all : null));
		}


		/* */
		public Weapon(Weapon p_image) {

			/* ID Variables. */
			name = p_image.name;
			ID = p_image.ID;

			/* Miscellanous Variables. */
			base_damage = p_image.base_damage;
			primary_behaviour_ID = p_image.primary_behaviour_ID;
			secondary_behaviour_ID = p_image.secondary_behaviour_ID;
			left_object = p_image.left_object;
			right_object = p_image.right_object;
			left_limb = p_image.left_limb;
			right_limb = p_image.right_limb;

			/* */
			sprite = p_image.sprite;

			/*_left_object = Resources.Load(left_object, typeof(GameObject)) as GameObject;
			_right_object = Resources.Load(right_object, typeof(GameObject)) as GameObject;*/
		}


		/* */
		public Weapon(ITargetable p_owner, Weapon p_image) : this(p_image) {
			_owner = p_owner;
		}


		/* */
		public void equip(HumanoidLimbs p_bodyparts) {
			p_bodyparts.clearBodyParts();

			for(int i = 0; i < 2; i++) {
				string path = (i == 0 ? left_object : right_object);
				if(path.CompareTo("") == 0)
					continue;

				GameObject prefab = Resources.Load(path, typeof(GameObject)) as GameObject;
				if(prefab == null)
					continue;


				Transform t = null;
				int index = (i * 2) + (i == 0 ? (int)left_limb : (int)right_limb);

				switch(index) {
				case 0:
					t = p_bodyparts.left_arm;
					break;
				case 1:
					t = p_bodyparts.left_hand;
					break;
				case 2:
					t = p_bodyparts.right_arm;
					break;
				case 3:
					t = p_bodyparts.right_hand;
					break;
				}

				GameObject go = Master.spawnObject(prefab/*i == 0 ? current_weapon.equipment_left : current_weapon.equipment_right*/);
				go.transform.parent = t;
				go.transform.localPosition = Vector3.zero;
				go.transform.localRotation = Quaternion.identity;
				go.transform.localScale = Vector3.one;
			}
		}


		/* If the attempted behaviour exists, it'll check to see if it's ready */
		private bool isReady(bool p_main) {
			if(isBehaviourNull(p_main))
				return false;

			if(_cooldown_initiated + _cooldown < UnityEngine.Time.time)
				return true;

			return false;
		}


		/* */
		public float cooldownProgress() {
			if(_cooldown <= 0)
				return 1;

			return UnityEngine.Mathf.Clamp01((UnityEngine.Time.time - _cooldown_initiated) / _cooldown);
		}


		/* */
		public float cooldownTimeRemaining() {
			if(_cooldown <= 0)
				return 0;

			return (_cooldown_initiated + _cooldown) - UnityEngine.Time.time;
		}


		public Sprite logo() {
			if(_sprite == null)
				initialse(ref _sprite, sprite);

			return _sprite;
		}


		private void initialse(ref Sprite p_sprite, string p_file_path) {
			Texture2D tex = Load<Texture2D>(p_file_path);

			if(tex != null)
				p_sprite = Sprite.Create(tex, new Rect(0f, 0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100f);
		}


		private T Load<T>(string p_file_path) where T : class {
			T obj = null;

			obj = Resources.Load(p_file_path) as T;

			return obj;
		}


		/* */
		public bool attack(bool p_main) {
			if(isBehaviourNull(p_main))
				return false;

			if(!isReady(p_main))
				return false;

			WeaponBehaviour behaviour = getBehaviour(p_main);

			_cooldown_initiated = UnityEngine.Time.time;
			_cooldown = (float)behaviour.cooldown;
			if(_cooldown < Globals.GLOBAL_COOLDOWN)
				_cooldown = Globals.GLOBAL_COOLDOWN;

			behaviour.attack();

			return true;
        }


		/* */
		public int damage(bool p_main) {
			if(getBehaviour(p_main) == null)
				return 0;

			return getBehaviour(p_main).damage(base_damage);
        }


		/* */
		private bool isBehaviourNull(bool p_main) {
			if(getBehaviour(p_main) == null)
				return true;

			return false;
		}


		/* */
		private WeaponBehaviour getBehaviour(bool p_main) {
			if(p_main)
				return primary_behaviour();
			else
				return secondary_behaviour();
		}


		/* */
		public int CompareTo(object p_obj) {
			if(p_obj.GetType() == typeof(Weapon)) {
				if(((Weapon)p_obj).ID.Equals(ID)) {
					return 0;
				}
			}

			return -1;
		}


		/* */
		public ITargetable getOwner() {
			return _owner;
		}


		#region Getters & Setters
		public string name { get; set; }
		public string ID { get; set; }
		public int base_damage { get; set; }
		public WeaponBehaviour primary_behaviour() {
			WeaponBehaviour behaviour = DatabaseHandler.behaviour_database.find(primary_behaviour_ID);

			if(behaviour != null) {
				return new WeaponBehaviour(this, behaviour);
			}

			return behaviour;
		}
		public WeaponBehaviour secondary_behaviour() {
			WeaponBehaviour behaviour = DatabaseHandler.behaviour_database.find(secondary_behaviour_ID);

			if(behaviour != null) {
				return new WeaponBehaviour(this, behaviour);
			}

			return behaviour;
		}
		#endregion
	}
}
