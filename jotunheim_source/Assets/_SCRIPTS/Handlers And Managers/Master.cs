﻿/***
 * 
 * Master.cs
 * 
 * This is an entity object, which acts as the master of all masters. On awake it
 * calls the awake functions of most static objects.
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Controllers;


public class Master : MonoBehaviour {

	/* This object shows what version the program is, on its entity in the editor, for easy
	 * visibility to the devs. */
	[Header("Game Version", order = 0)]
	[Space(-10, order = 1)]
	[Header(Globals.game_version, order = 2)]
	[Space(10, order = 3)]

	/* Essential ingame variables. */
	public GameObject prefab_player;
	private int _last_num_valid_configs = 0;

	/* Critical variables. */
	private static Master _instance;



	/* The scripts that needs to be triggered before anything else. */
	private void Awake() {
		LanguageHandler.update();
	}


	/* On first frame, the master object wakes up all essential static objects. */
	private void Start () {
		_instance = this;

		coroutines = new Queue<coroutine>();

		/* The master object should never have values different than these, and to make sure this stays
		 * consistent, we automatically set it to these values when it gets called. */
		transform.position = Vector3.zero;
		transform.rotation = Quaternion.identity;
		transform.localScale = Vector3.one;

		importantAwakes();
		generalAwakes();
		finalAwakes();

		if(Controls.main_device != null)
			Controls.main_device.clear();

	}


	/* Anything that is absolutely essential to be initialised/awaked first. */
	private void importantAwakes() {
		DatabaseHandler.awake();
		Configurations.Settings.awake();
	}


	/* */
	private void generalAwakes() {
		GameSettings.awake();

		//TODO These two needs to be phased out.
		AudioPresets.awake();
		GraphicsPresets.awake();
	}


	/* Anything that doesn't have as much, or any, impact on other components and
	 * thus can have a late initialise. */
	private void finalAwakes() {
		Scoring.awake();
		MeshHandler.awake();
		PlayerHandler.awake();
		SpawnHandler.awake();
		TerrainDetector.init();
		SubUIHandler.start();
	}



	/* Called through Monobehaviour every frame. */
	private void Update() {

		/* */
		Time.timeScale = masterControlsTime() ? 1 : Time.timeScale;

		/* Attempts to spawn players when everything is set up. */
		if(LevelManager.stage.CompareTo(IngameStage.Intro) == 0 && PlayerHandler.characters.Count <= 0) {
			spawnPlayers();
		}

		/* If the number of player configs change, we want to update the UI and Cameras to reflect the change. */
		if(PlayerHandler.numValidHumanConfigs() != _last_num_valid_configs && !SubUIHandler.fadeToBlack && (LevelManager.stage.CompareTo(IngameStage.Intro) == 0 || LevelManager.stage.CompareTo(IngameStage.Play) == 0)) {
			_last_num_valid_configs = PlayerHandler.numValidHumanConfigs();
            PlayerUIHandler.start();
			CameraManager.start();
		}
		
		/* Calls the object handling the Controls, making sure everything is cleared up
		 * to avoid a key behind accidentally stuck. */
		Controls.cleanUp();

		/* Calls the main method of the soundmaster. Which handles mostly music. */
		AudioManager.main();

		/* Anything modable will always have their handlers do an update check to see
		 * if anything needs to be reloaded. */
		DatabaseHandler.update();

		/* Miscellanous Updates. */
		MeshHandler.update();

		/* */
		if(coroutines.Count > 0) {
			StartCoroutine(coroutines.Dequeue()());
		}
	}


	/* Called when spawing new players at start. */
	private void spawnPlayers() {
		if(LevelManager.isInGame()) {
			for(int i = 0; i < Globals.MAX_PLAYERS; i++) {
				PlayerConfiguration PC = PlayerHandler.retrieveConfigByIndex(i);

				if(PC != null) {
					SpawnHandler.requestSpawn(prefab_player, SpawnHandler.ObjectType.Character, PC.player_ID, PC.team);
				}
			}
		}
	}


	/* */
	private bool masterControlsTime() {
		if(LevelManager.isInGame() && LevelManager.stage.CompareTo(IngameStage.Play) != 0) {

			if(/*LevelManager.stage.CompareTo(IngameStage.Outro) != 0 &&*/ 
				LevelManager.stage.CompareTo(IngameStage.Intro) != 0 &&
				LevelManager.stage.CompareTo(IngameStage.Cinematic) != 0) {
				return false;
			}
		}

		return true;
	}


	/* */
	public static GameObject spawnObject(GameObject p_prefab) {
		return Instantiate(p_prefab, Vector3.zero, Quaternion.identity) as GameObject;
	}


	/* */
	public static void destroyObject(GameObject p_object) {
		Destroy(p_object);
	}


	#region Getters & Setters
	public static GameObject player_prefab { get { return _instance.prefab_player; } }
	public static Queue<coroutine> coroutines { get; set; }
	public delegate IEnumerator coroutine();
	public static MonoBehaviour monobehaviour { get { return _instance; } }
	#endregion
}
