﻿/***
 * 
 * CollisionHandler.cs
 * 
 * ...
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


public static class MeshHandler {

	/* */
	private static int _circle_resolution = -1;
	private static GameObject _shapes_parent;
	private static List<CollisionHighlight> _shape_objects;
	private static Queue<float> _shape_object_expirations;
	private static float _life_time = 50f;



	/* Makes sure that all lists and other static variables are set to their default values before
	 * being used. */
	public static void awake() {
		_shape_objects = new List<CollisionHighlight>();
		_shape_object_expirations = new Queue<float>();

		_shapes_parent = new GameObject();
		_shapes_parent.transform.position = Vector3.zero;
		_shapes_parent.name = "_Shapes_Parent";
    }


	/* */
	public static void update() {
		while(_shape_object_expirations.Count > 0 && _shape_object_expirations.Peek() < Time.time) {
			_shape_object_expirations.Dequeue();
			GameObject GO = _shape_objects[0].GO;
			_shape_objects.RemoveAt(0);

			Master.destroyObject(GO);
        }

		if(_shape_objects.Count > 0) {
			CombatHandler.Damageable[] damageables = CombatHandler.getDamageables();
			foreach(CollisionHighlight CH in _shape_objects) {
				for(int i = 0; i < damageables.Length; i++) {
					//ITargetable target = damageables[i].transform.GetComponent<ITargetable>();

					//Debug.Log((damageables[i].target_interface == null) + " , " + (CH.shape == null));


					if(CollisionHandler.isColliding(CH.shape, damageables[i].target_interface)) {
						CH.GO.GetComponent<Renderer>().material.color = new Color(0, 1, 0, .5f);
					} else {
						CH.GO.GetComponent<Renderer>().material.color = new Color(1, 0, 0, .5f);
					}
				}
			}
		}
	}


	public struct CollisionHighlight {
		public GameObject GO;
		public Shape shape;
	}


	/* */
	public static GameObject create(Shape p_shape) {
		Mesh mesh = null;
	

		/* Creating a mesh by converting any received shape into a set of triangles. */
		if(p_shape.GetType() == typeof(Circle)) {
			mesh = generateMesh(convertToMeshTriangles(p_shape));
		} else if(p_shape.GetType() == typeof(Rectangle)) {
			mesh = generateMesh(convertToMeshTriangles(p_shape));
		} else if(p_shape.GetType() == typeof(Triangle)) {
			mesh = generateMesh(convertToMeshTriangles(p_shape));
        } else {
			throw new System.ArgumentException(string.Format("The \"{0}\" shape is not yet supported by the isColliding() method.", p_shape.GetType()));
		}


		/* Creating the gameobject and applying necessary component. */
		GameObject go = new GameObject();
		go.transform.parent = p_shape.parent != null ? p_shape.parent : _shapes_parent.transform;
        MeshFilter mf = go.AddComponent<MeshFilter>();
		MeshRenderer mr = go.AddComponent<MeshRenderer>();


		/* Positioning and Rotation. */
		go.transform.position = p_shape.position;
		if(p_shape.GetType() != typeof(Circle)) {
			float diff = GlobalMovement.findAngleByDirection(p_shape.direction);
			go.transform.Rotate(diff * Vector3.up);
		}


		/* Applying the mesh, and setting a material/shader. */
		Shader shader = Shader.Find("Standard");
		mr.material = new Material(shader);
		mr.material.color = new Color(1, 0, 0, .5f);
		mr.receiveShadows = false;
		mr.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
		mf.mesh = mesh;


		/* Adding the object to a queue for removal. */
		CollisionHighlight CH = new CollisionHighlight();
		CH.GO = go;
		CH.shape = p_shape;
        _shape_objects.Add(CH);
		_shape_object_expirations.Enqueue(Time.time + _life_time);

		return go;
    }


	/* Converts a rectangle or circle shape into an array of triangles. */
	private static Triangle[] convertToMeshTriangles(Shape p_shape) {
		Triangle[] tris = null;
		int index = 0;

		if(p_shape.GetType() == typeof(Circle)) {
			Circle circle = (Circle)p_shape;

			/* Finds the number of triangles required to make up the circle. */
			int fragments = Mathf.CeilToInt(circle.degrees / circle_resolution);
			float resolution = circle.degrees;

			/* Finds the start angle of our circle. */
			float start_angle = GlobalMovement.findAngleByDirection(p_shape.direction) - (circle.degrees / 2);
			tris = new Triangle[fragments];

			/* */
			for(int i = 0; i < tris.Length; i++) {
				index = 0;

				Vector3 v = GlobalMovement.findDirectionByAngle(Vector2.zero, start_angle +
																		((fragments > 1 ? circle_resolution : resolution) * (i))) * circle.radius;
				Vector3 v1 = GlobalMovement.findDirectionByAngle(Vector2.zero, start_angle +
																		((fragments > 1 ? circle_resolution : resolution) * (i) +
																		(resolution > circle_resolution ? circle_resolution : resolution))) * circle.radius;

				Vec2d[] corners = new Vec2d[3];
				corners[index++] = new Vec2d();
				corners[index++] = new Vec2d(v.x, v.z);
				corners[index++] = new Vec2d(v1.x, v1.z);

				/*tris[i].corners[index++] = Vector2.zero;
				tris[i].corners[index++] = new Vector2(v.x, v.z);
				tris[i].corners[index++] = new Vector2(v1.x, v1.z);*/

				tris[i] = new Triangle(corners, p_shape.position, p_shape.direction);
				resolution -= circle_resolution;
			}

		} else if(p_shape.GetType() == typeof(Rectangle)) {
			Rectangle rectangle = (Rectangle)p_shape;
			tris = new Triangle[2];

			/* Hardcoded approach to creating both triangles out of the rectangle shape. */
			Vec2d[] tris_corners = new Vec2d[3];
			tris_corners[index++] = new Vec2d(rectangle.corners[0]);
			tris_corners[index++] = new Vec2d(rectangle.corners[3]);
			tris_corners[index++] = new Vec2d(rectangle.corners[1]);
			tris[0] = new Triangle(tris_corners, p_shape.position, p_shape.direction);

			index = 0;

			tris_corners = new Vec2d[3];
			tris_corners[index++] = new Vec2d(rectangle.corners[3]);
			tris_corners[index++] = new Vec2d(rectangle.corners[2]);
			tris_corners[index++] = new Vec2d(rectangle.corners[1]);
			tris[1] = new Triangle(tris_corners, p_shape.position, p_shape.direction);

			/*tris[0] = new Triangle(null, p_shape.position, p_shape.direction);
			tris[0].corners[index++] = new Vector2(rectangle.corners[0].x, rectangle.corners[0].y);
			tris[0].corners[index++] = new Vector2(rectangle.corners[0].x, rectangle.corners[1].y);
			tris[0].corners[index++] = new Vector2(rectangle.corners[1].x, rectangle.corners[0].y);

			index = 0;

			tris[1] = new Triangle(null, p_shape.position, p_shape.direction);
			tris[1].corners[index++] = new Vector2(rectangle.corners[0].x, rectangle.corners[1].y);
			tris[1].corners[index++] = new Vector2(rectangle.corners[1].x, rectangle.corners[1].y);
			tris[1].corners[index++] = new Vector2(rectangle.corners[1].x, rectangle.corners[0].y);*/


		} else if(p_shape.GetType() == typeof(Triangle)) {
			tris = new Triangle[1];

			Vec2d[] tris_corners = new Vec2d[3];
			tris_corners[index++] = new Vec2d(((Triangle)p_shape).corners[0]);
			tris_corners[index++] = new Vec2d(((Triangle)p_shape).corners[2]);
			tris_corners[index++] = new Vec2d(((Triangle)p_shape).corners[1]);
			tris[0] = new Triangle(tris_corners, p_shape.position, p_shape.direction);
		}

		return tris;
	}


	/* */
	private static Mesh generateMesh(Triangle[] p_triangles) {
		Mesh mesh = new Mesh();

		/* We initialise the different sets, arrays and lists. */
		List<int> tris = new List<int>();
		Dictionary<Vector2, int> verts_dict = new Dictionary<Vector2, int>();
		List<Vector3> verts = new List<Vector3>();
		HashSet<Vector2> uv = new HashSet<Vector2>();


		for(int i = 0; i < p_triangles.Length; i++) {

			Vector3[] triangle = new Vector3[3];

			int index = 0;
			triangle[index] = p_triangles[i].corners[index++];
			triangle[index] = p_triangles[i].corners[index++];
			triangle[index] = p_triangles[i].corners[index++];

			foreach(Vector2 v in triangle) {
				float x = ((int)(v.x * 100000)) / 100000f;
				float y = ((int)(v.y * 100000)) / 100000f;

				Vector2 safe_v = new Vector2(x, y);

				if(!verts_dict.ContainsKey(safe_v)) {
					verts_dict.Add(safe_v, verts.Count);
					uv.Add(safe_v);


					Vector3 v3 = new Vector3(x, 0, y);
					verts.Add(v3);
				}

				tris.Add(verts_dict[safe_v]);
			}
		}

		mesh.vertices = verts.ToArray();
		mesh.uv = uv.ToArray();
		mesh.triangles = tris.ToArray();

		mesh.RecalculateNormals();
		mesh.RecalculateBounds();
		;

		return mesh;
	}


	#region Getters & Setters
	public static int circle_resolution {
		get {
			if(_circle_resolution == -1)
				_circle_resolution = 5;

            return _circle_resolution;
		}
		set {
			if(value <= 0)
				value = 1;
			else if(value > 5)
				value = 5;

			_circle_resolution = value;
		}
	}
	#endregion
}
