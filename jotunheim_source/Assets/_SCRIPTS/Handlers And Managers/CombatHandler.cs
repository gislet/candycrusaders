﻿/***
 * 
 * CombatHandler.cs
 * 
 * This object acts as an entity and is tied to a world object. To access the script
 * you treat it as a static object. This object acts as a central to all attacks ordered.
 * Orders supplied have their attacks initiated if possible, and if so their ID's cached
 * while the attack is active. Allowing external objects to monitor specific IDs.
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using attackRoutine = WeaponOld.attackRoutine;

public class CombatHandler : MonoBehaviour {

	private List<AttackID> _IDs;
	private static CombatHandler _instance;

	private const float MIN_DAMAGABLE_RADIUS_SIZE = .1f;


	/* Called before scene load, allowing it to set up vital variables. */
	private void Awake () {
		_instance = this;
		initialise();
	}


	/* Called to initialise variables, and cache necessary objects. */
	private void initialise() {
		_IDs = new List<AttackID>();
    }


	/* */
	private void Update() {
		if(IDs != null) {
			for(int i = 0; i < IDs.Count; i++) {
				if(IDs[i].time < Time.time) {
					IDs.RemoveAt(i);
					i--;
				}
			}
		}
	}


	/* Supplied with an attackorder to be executed as a coroutine. */
	public static int initiateAttack(AttackOrder p_order) {

		if(p_order.attack != null) {
			/* Adding the attack ID to the ID list. Allowing characters or otherwise to
			 * monitor if the attack is still active. */
			IDs.Add(new AttackID(p_order.ID, p_order.cooldown + Time.time));

			_instance.StartCoroutine(p_order.attack());
		}

		/* We only want to return the ID if that which initiated the attack is going to be
		 * locked during the attacks duration. */
		return p_order.ID;
	}


	/* This function finds every transform that can take damage and stores them in an array
	 * of structs that save their respective collision functions and their transforms. */
	public static Damageable[] getDamageables() {
		List<Damageable> damageables = new List<Damageable>();

		/* Aquire each player character that exists. */
		foreach(CharacterBase CB in PlayerHandler.characters) {
			//damageables.Add(new Damageable(CB.transform, CB, CB.radius_size));
			damageables.Add(new Damageable(CB));
		}

		return damageables.ToArray();
	}


	/* A struct allowing an object to monitor a specific transform and its IDamagable interface,
	 * allowing this object to damage or heal it if necessary. */
	public struct Damageable {
		public float size;
		public Transform transform;
		public IDamageable obj;
		public ITargetable target_interface;

		public Damageable(CharacterBase p_object) {
			transform = p_object.transform;
			obj = p_object;
			size = p_object.radius < MIN_DAMAGABLE_RADIUS_SIZE ? MIN_DAMAGABLE_RADIUS_SIZE : p_object.radius;
			target_interface = p_object;
		}
	}


	/* */
	public static bool isIDActive(int p_ID) {
		foreach(AttackID ID in IDs) {
			if(ID.ID == p_ID)
				return true;
		}

		return false;
	}


	/* */
	public static float ID_cooldown_percentage(int p_ID) {

		foreach(AttackID ID in IDs) {
			if(ID.ID == p_ID)
				return ID.cooldown_percentage;
		}

		return 1;
	}


	/* */
	public struct AttackID : IComparable<AttackID> {

		public int ID;
		public float time;
		private float start_time;

		public AttackID(int p_ID, float p_time) {
			ID = p_ID;
			time = p_time;

			start_time = Time.time;
        }

		public float cooldown_percentage {
			get {
				float time_since_start = Time.time - start_time;
				float time_delta = time - start_time;

				return Mathf.Clamp01(time_since_start / time_delta);
			}
		}

		public int CompareTo(AttackID other) {
			if(other.ID == ID)
				return 0;

			return -1;
		}
	}


	#region Getters & Setters
	public static List<AttackID> IDs { get { return _instance._IDs; } }
	#endregion
}
