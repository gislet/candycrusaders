﻿/**
 * 
 * PowerupHandler.cs
 * 
 * asdf
 * 
 * 
 * Written by Gisle Thorsen
 * 
 **/


using UnityEngine;
using System.Collections;


public class PowerupHandler : MonoBehaviour {

	public GameObject[] powerup_prefabs;

	private Transform _spawn_parent;
	private SpawnCluster _cluster;
	private float _last_spawn_attempt = 0;

	[Range(.5f, 10f)]
	public float spawn_frequency = 5f;

	private static PowerupHandler _instance;


	// 
	private void Awake() {
		_instance = this;
	}


	// Runs every frame, attempting to spawn a powerup. 
	private void Update() {

		//Debug.Log("123" + (SpawnHandler.findCluster(SpawnHandler.ObjectType.Powerup) != null));
		if(_cluster == null) {
			_cluster = SpawnHandler.findCluster(SpawnHandler.ObjectType.Powerup);
			preSpawn();
			return;
		}

		// If we've recently attempted to spawn a powerup, we'll wait. 
		if(_last_spawn_attempt + spawn_frequency > Time.time)
			return;

		assessSituation();
		trySpawning();
		
			
	}


	// 
	private void preSpawn() {
		if(_cluster == null)
			return;

		do {
			trySpawning();
			assessSituation();
		} while(_spawn_parent != null);
	}


	// Gets called when there is a spawn-point that needs to be parented. 
	private void trySpawning() {
		if(_spawn_parent == null)
			return;

		if(powerup_prefabs.Length > 0) {
			GameObject powerup = Master.spawnObject(powerup_prefabs[Random.Range(0, powerup_prefabs.Length)]);
			powerup.transform.parent = _spawn_parent;
			powerup.transform.localPosition = Vector3.zero;

        }

		_spawn_parent = null;
	}


	// Assesses the need to spawn a new powerup. 
	private void assessSituation() {

		// Checking for unpopulated spawn positions. 
		foreach(Transform t in _cluster.points) {
			if(t.childCount == 0) {
				_spawn_parent = t;
				break;
            }
        }
	
		// Updating the timer for when the last time we tried to spawn something. 
		_last_spawn_attempt = Time.time;
	}


	 /*
	public static PowerUp findOfParticularModifier(PowerUp[] p_set, PowerUp.ModifierType p_type) {
		foreach(PowerUp p in p_set) {
			if(p.hasModifier(p_type))
				return p;
		}

		return null;
	}*/
}
