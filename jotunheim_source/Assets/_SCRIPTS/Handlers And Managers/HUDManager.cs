﻿/***
 * 
 * CameraFollow.cs
 * 
 * asdf
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using System.Collections;

public class HUDManager : MonoBehaviour {

	public GameObject hud_prefab;
	private int _num_players = 0;

	[Tooltip("Optional - If assigned, it will try to give each hud the layer corrosponding with their assigned player ID.")]
	public LayerMask[] hud_masks;

	
	/* */
	void Update () {
		if(_num_players != PlayerHandler.numValidConfigs()) {
			_num_players = PlayerHandler.numValidConfigs();

			setHUDs();
        }
	}


	/* */
	private void setHUDs() {
		removeChildren();

		for(int i = 0; i < _num_players; i++) {
			GameObject obj = Master.spawnObject(hud_prefab);
			obj.GetComponent<FollowPlayer>().player_of_interest = (PlayerID)i;
			obj.transform.SetParent(transform);
			obj.transform.Rotate(Vector3.right * 45);

			if(hud_masks != null ? hud_masks.Length != 0 : false) {
				if(hud_masks.Length == Globals.MAX_PLAYERS) {
					obj.layer = Mathf.RoundToInt(Mathf.Log(hud_masks[i].value, 2));
				} else {
					Debug.LogError("When assigning layermasks to a HUD manager, you have to include a layer for each possible player. (" + hud_prefab + ")");
				}
			}
		}
	}


	/* */
	private void removeChildren() {
		for(int i = transform.childCount - 1; i >= 0; i--) {
			Destroy(transform.GetChild(i).gameObject);
		}
	}
}
