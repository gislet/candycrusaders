﻿/***
 * 
 * SpawnHandler.cs
 * 
 * This is a static object that can be called to request for the supplied
 * object to be instantiated into the game world. Only one request gets
 * processed each frame.
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;


public static class SpawnHandler {

	/* Vitals! Cached objects. */
	private static Queue<Request> _request_queue = new Queue<Request>();
	private static Request _current_request;

	/* Essentials. */
	private static bool _isProcessing = false;


	/* Is called once per scene. */
	public static void awake() {
		clusters = new List<SpawnCluster>();
		_request_queue = new Queue<Request>();
		_isProcessing = false;
	}


	/* Is called by who and whatever that wants to spawn something. */
	public static void requestSpawn(GameObject p_gameobject, ObjectType p_type = ObjectType.None, int p_special_ID = Globals.INVALID, Team p_ownership = Team.Teamless, bool p_instantiate = true) {
		Request new_request = new Request(p_gameobject, p_type, p_special_ID, p_instantiate, p_ownership);
		_request_queue.Enqueue(new_request);
		tryProcessNext();
	}


	/* Tries to process the next element in our spawn queue. */
	private static void tryProcessNext() {
		if(!_isProcessing && _request_queue.Count > 0) {
			_current_request = _request_queue.Dequeue();
			_isProcessing = true;

			spawnObject();
		}
	}


	/* Handles the spawning of the request currently being processed. */
	private static void spawnObject() {
		GameObject obj = UnityEngine.Object.Instantiate(_current_request.game_object,
														getSpawnPosition(),
														Quaternion.identity) as GameObject;

		//Implement an interface instead!!!
		if(_current_request.type != ObjectType.None) {
			switch(_current_request.type) {
			case ObjectType.Character:
				obj.GetComponent<CharacterHandler>().instantiate(_current_request.special_ID);
				break;
			}
		}


		_isProcessing = false;
		tryProcessNext();
	}


	/* */
	public static SpawnCluster findCluster(ObjectType p_spawns, Team p_ownership = Team.Teamless) {

		foreach(SpawnCluster cluster in clusters) {
			if(cluster.spawns == p_spawns && (p_ownership != Team.Teamless ? cluster.ownership == p_ownership : true)) {
				return cluster;
			}
		}

		return null;
	}


	/* Goes through the various spawn presets and looks for one with our current
	 * game-mode. If it exists we return its spawn-method. */
	/*public static SpawnMethod getSpawnMethod() {
		if(GameSettings.current != null) {
			GameSettings.GameMode mode = GameSettings.current._game_mode;

			if(SpawnPoint.settings_dictionary != null) {
				if(SpawnPoint.settings_dictionary.ContainsKey(mode)) {
					return SpawnPoint.settings_dictionary[mode];
				}
			}
		}

		return SpawnMethod.None;
	}*/


	/* Depending on which spawnmethod we use, we call the appropriate method that
	 * will find us a position in the world to spawn at. */
	private static Vector3 getSpawnPosition() {
		if(_current_request.type == ObjectType.Character) {
			switch(LevelManager.getModeSpawnMethod()) {
			case SpawnMethod.Random:
				return getRandomSpawn();
			case SpawnMethod.Nearest:
				return getNearestSpawn();
			case SpawnMethod.None:
			default:
				return getRandomSpawn(true);
			}
		} else if(_current_request.type == ObjectType.Powerup) {

		}

		return Vector3.zero;
	}


	/* If there are allocated spawnpoints, it will select one of them at random and
	 * return its position. */
	private static Vector3 getRandomSpawn(bool p_force_null = false) {
		SpawnCluster SC;
        if((SC = findCluster(_current_request.type, _current_request.ownership)) != null && !p_force_null) {
			return SC.points[Random.Range(0, SC.points.Length)].position;
		}

		return _current_request.game_object.transform.position;
	}


	/* */
	private static Vector3 getNearestSpawn() {

		SpawnCluster SC;
		if((SC = findCluster(_current_request.type, _current_request.ownership)) == null)
			return _current_request.game_object.transform.position;

		Vector3 nearest = SC.points[0].position;
		Transform[] transforms = objectsOfSameType();
		int index = 0;
		float distance = -1, min_padding = 15f;


		for(int i = 0; i < SC.points.Length; i++) {
			int temp_index = -1;

			for(int g = 0; g < transforms.Length; g++) {
				float temp_dist = Vector3.Distance(SC.points[i].position, transforms[g].position);

				if(temp_dist < min_padding) {
					temp_index = -1;
					break;
				} else if(temp_dist >= min_padding && (temp_dist < distance || distance == -1)) {
					temp_index = i;
					distance = temp_dist;
				}
			}

			if(temp_index >= 0) {
				index = temp_index;
			}
		}

		return SC.points[index].position;
	}


	/* */
	private static Transform[] objectsOfSameType() {
		List<Transform> transforms = new List<Transform>();

		switch(_current_request.type) {
		case ObjectType.Character:
			foreach(CharacterBase CB in PlayerHandler.characters)
				transforms.Add(CB.transform);
			return transforms.ToArray();
		default:
			return null;
		}
	}


	/* Has all the information about the object that wishes to be spawned. */
	public struct Request {

		/* This could be either a prefab (true) or an object in an object-pool (false) depending
		 * on the instantiate bool. */
		public GameObject game_object;
		public bool instantiate;
		public int special_ID;
		public ObjectType type;
		public Team ownership;

		public Request(GameObject p_gameobject, ObjectType p_type, int p_special_ID, bool p_instantiate, Team p_ownership = Team.Teamless) {
			game_object = p_gameobject;
			special_ID = p_special_ID;
			type = p_type;
			instantiate = p_instantiate;
			ownership = p_ownership;
        }
	}


	/* */
	public static List<SpawnCluster> clusters { get; set; }


	/* This enum is used to make sure we look up the appropriate lists and variables for our functions. */
	public enum ObjectType {
		None, Character, Powerup
	}

	/* This enum dictates how an object is spawned into the world. */
	public enum SpawnMethod {
		None = -1, Random, Nearest, Furthest
	}
}
