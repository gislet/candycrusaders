﻿/***
 * 
 * CameraManager.cs
 * 
 * asdf
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class CameraManager : MonoBehaviour {


	public GameObject camera_prefab;
	private List<Camera> _cameras;
	private int[] _starting_indexes = new int[] { 0, 1, 3, 6};
	private Rect[] _rect_positions = new Rect[] {
		new Rect(0, 0, 1, 1),					//0

		new Rect(0, 0, .5f, 1),					//1
		new Rect(.5f, 0, .5f, 1),				//2

		new Rect(0, .5f, .5f, .5f),				//3
		new Rect(.5f, .5f, .5f, .5f),			//4
		new Rect(0, 0, .5f, .5f),				//5

		new Rect(0, .5f, .5f, .5f),				//6
		new Rect(.5f, .5f, .5f, .5f),			//7
		new Rect(0, 0, .5f, .5f),				//8
		new Rect(.5f, 0, .5f, .5f),				//9
	};


	/* */
	private static CameraManager _instance;



	/* */
	void Awake () {
		_instance = this;
		_cameras = new List<Camera>();
		setup();
	}


	/* */
	public static void start() {
		_instance.setup();
	}


	/* Stopping removes all cameras except the main camera. */
	public static void stop() {
		for(int i = _instance._cameras.Count; i > 0; i--) {
			_instance._cameras.RemoveAt(i);
		}

		_instance.setup(false);
	}


	/* Instantiates new cameras if needed, followed by scaling and positioning them correctly
	 * on the screen. */
	private void setup(bool p_allow_instantiate = true) {
		
		int starting_index = p_allow_instantiate ? PlayerHandler.numValidHumanConfigs() - 1 : 0;

		for(int i = 0; i < Globals.MAX_PLAYERS; i++) {

			/* If we allow instantiation we will add camera's that don't exist. */
			if(p_allow_instantiate && (i + 1) > _cameras.Count && _cameras.Count <= i && i < PlayerHandler.numValidHumanConfigs()) {
				GameObject camera = Instantiate(camera_prefab) as GameObject;
				camera.transform.parent = transform;
				camera.tag = "Untagged";

				/* Making sure this camera is following the correct player. */
				camera.GetComponent<CameraFollow>().player_of_interest = (PlayerID)i;

				_cameras.Add(camera.GetComponent<Camera>());

				switch((PlayerID)i) {
				case PlayerID.player0:
					camera.GetComponent<Camera>().cullingMask |= 1 << LayerMask.NameToLayer("Camera_1");
					break;
				case PlayerID.player1:
					camera.GetComponent<Camera>().cullingMask |= 1 << LayerMask.NameToLayer("Camera_2");
					break;
				case PlayerID.player2:
					camera.GetComponent<Camera>().cullingMask |= 1 << LayerMask.NameToLayer("Camera_3");
					break;
				case PlayerID.player3:
					camera.GetComponent<Camera>().cullingMask |= 1 << LayerMask.NameToLayer("Camera_4");
					break;
				}
			}

			if(_cameras.Count > i) {
				//_cameras[i].gameObject.SetActive(false);
				_cameras[i].rect = _rect_positions[_starting_indexes[starting_index] + i];
				_cameras[i].gameObject.SetActive(LevelManager.isInGame());
			}

			
		}
	}


	/* */
	public static Camera main {
		get {
			if(_instance._cameras != null && _instance._cameras.Count > 0)
				return _instance._cameras[0];

			return null;
		}
	}


	#region Getters & Setters
	public static bool isCamerasLocked { get; set; }
	#endregion
}
