﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainHandler : MonoBehaviour {

	/* */
	[SerializeField]
	private TextureSurfaceType[] _texture_surfacetypes;
	private static TerrainHandler _instance;



	/* */
	private void Awake() {
		_instance = this;	
	}


	/* */
	public static SurfaceType getSurfaceType(string p_name) {
		foreach(TextureSurfaceType container in _instance._texture_surfacetypes) {
			if(p_name.CompareTo(container.tex.name) == 0) {
				return container.type;
			}
		}

		Debug.LogError("Couldn't find any associated surfacetype with the texture " + p_name);
		return SurfaceType.UNDEFINED;
	}


	[System.Serializable]
	public struct TextureSurfaceType {
		public Texture2D tex;
		public SurfaceType type;
	}
}
