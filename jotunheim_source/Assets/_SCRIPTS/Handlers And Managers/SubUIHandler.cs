﻿/***
 * 
 * SubUIHandler.cs
 * 
 * asdf
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class SubUIHandler : MonoBehaviour {

	private bool _fade_to_black = false;
	private static SubUIHandler _instance;
	private State _state = State.None;
	private List<SubUIObject> _subs;
	private FadeBlack _fadeblack;
	private SubUIObject _previous_ui;


	/* */
	void Awake() {
		_instance = this;
	}


	/* Called by our master class once this is ready to be initialised. */
	public static void start() {

		_instance.cache();
		_instance.init();

		/* If the coroutine is already running, we want to tell it to restart. */
		if(_instance._state != State.None) {
			_instance._state = State.Restarting;
			return;
		}

		_instance.StartCoroutine(_instance.update());
	}


	/* Caches all Sub-UI objects that exists down the hierarchy. */
	private void cache() {
		_subs = new List<SubUIObject>();

		cacheSubUIs(transform);
	}


	/* Making sure that all UI-Sub objects are turned off at start. */
	private void init() {
		foreach(SubUIObject obj in _subs) {
			obj.group.SetActive(false);
		}
	}


	/* */
	public static void stop() {
		_instance._state = State.None;
	}


	/* */
	private IEnumerator update() {
		_state = State.Running;
		_previous_ui = current_ui;

		/* */
		while(_state == State.Running) {

			/* If our currently cached UI doesn't have a Sub, we want to recache and skip this frame. */
			if(_previous_ui.sub == null) {
				_previous_ui = current_ui;
				yield return null;
				continue;
			}

			/* Runs the fade-to-black process. */
			if((!_previous_ui.sub.isActive && _fade_to_black) || _fadeblack.value > 0) {
				_fadeblack.run();
				_fade_to_black = _fadeblack.isBlack;
			}

			/* Calls the previous UI that was active. */
			callUISub(_previous_ui);

			/* Caches the current UI if the previous one is no longer active. */
			if(!_previous_ui.sub.isActive && !_fade_to_black) {
				_previous_ui = current_ui;
				//callUISub(current_ui);				
			}		
			
			/* Makes sure that only the active and idle UIs are visible. */
			if(!_fade_to_black)
				callIdleUISub();

			yield return null;
		}


		/* */
		if(_state == State.Restarting) {
			_state = State.None;
			start();
		}
	}


	/* */
	private void callUISub(SubUIObject p_ui) {
		if(p_ui.sub == null)
			return;

		bool current = p_ui.sub.isActive;
		p_ui.group.SetActive((current || _fade_to_black));

		if(!p_ui.group.activeSelf || (!_previous_ui.sub.isActive)) {
			return;
		}

		p_ui.sub.actions();
		p_ui.sub.run();
		_fade_to_black = p_ui.sub.fadeToBlack;
	}


	/* Making sure that any UI sub is active at the appropriate time. */
	private void callIdleUISub() {
		foreach(SubUIObject obj in _subs) {
			if(!obj.sub.isActive) {
				
				if(obj.group.activeSelf && !obj.sub.isIdle)
					obj.group.SetActive(false);
				else if(obj.sub.isIdle)
					obj.group.SetActive(true);

				if(obj.group.activeSelf) {
					obj.sub.idle();
				}
			}
		}
	}


	/* A recursive function for finding UI-Subs on ourselves. */
	private void cacheSubUIs(Transform p_transform) {

		/* Looking for the screen-fade-to-black handler object. */
		FadeBlack fb = p_transform.GetComponent<FadeBlack>();
		if(fb != null) {
			_fadeblack = fb;
        }

		/* Looking for Sub-UI objects to cache. */
		IUISub sub = p_transform.GetComponent<IUISub>();
		if(sub != null) {
			SubUIObject obj = new SubUIObject();
			obj.sub = sub;
			obj.group = p_transform.gameObject;

			_subs.Add(obj);
		}


		/* We call each child to go as deep down the rabbit hole as possible, looking
		 * for UI-Subs to cache. */
		for(int i = 0; i < p_transform.childCount; i++) {
			cacheSubUIs(p_transform.GetChild(i));
        }
    }

	
	/* Searching for the Sub-UI object that should currently be in focus. */
	public static SubUIObject current_ui {
		get {

			foreach(SubUIObject obj in _instance._subs) {
				if(obj.sub.isActive) {
					return obj;
				}
			}

			return new SubUIObject();
		}
	}


	/* */
	public static bool fadeToBlack {
		get { return _instance._fade_to_black; }
	}


	/* This enum makes sure that there won't be additional coroutines running at
	 * simultaneously. */
	private enum State {
		None, Running, Restarting
	}
}


/* */
public struct SubUIObject {
	public IUISub sub;
	public GameObject group;
}
