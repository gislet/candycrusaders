﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.Collections.Generic;
using LitJson;
using System.IO;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Core;


public class DebugHandler : MonoBehaviour {

	[Tooltip("Enabling this will allow you to see the area that is affected by an attack made. How" + 
		" long the visualition lasts has to be defined on the FragmentHandler entity.")]
	public bool start_in_debug = true;
	public Text fps_counter, current_stage, set_stage_to;
	public GameObject fps_counter_object;

	private bool _debug_standard = false, _debug_players = false;

	private int _frame_count = 0;
	private float _fps_next_measure_time = 0;
	private float _fps_current;	

	private static DebugHandler _instance;



	List<TestBuffs> _buff_list;


	public GameObject testquadone, testquadtwo;

	private void Awake() {
		_instance = this;

		if(start_in_debug && Application.isEditor)
			_debug_standard = true;




		//BELOW IS TEMP!!!
		//compressTests();
		//loadFBX();
		/*GameObject go;
		_buff_list = JsonMapper.ToObject<List<TestBuffs>>(File.ReadAllText(Application.streamingAssetsPath + "/powerups.json"));*/

		/*if(_buff_list[1].use_prefab) {

			go = Instantiate(Resources.Load(_buff_list[1].prefab_object, typeof(GameObject)) as GameObject);			
			

		} else {
			go = new GameObject();

			MeshFilter mf = go.AddComponent<MeshFilter>();
			MeshRenderer mr = go.AddComponent<MeshRenderer>();

			mf.mesh = (Resources.Load(_buff_list[1].custom_object, typeof(Mesh)) as Mesh);

			Material mat = new Material(Shader.Find("Specular"));
			mat.mainTexture = Resources.Load(_buff_list[1].custom_texture, typeof(Texture2D)) as Texture2D;

			mr.material = mat;
		}

		go.name = _buff_list[1].name + "";
		go.transform.position = new Vector3(-1542.8f, 84.2f, -89.7f);*/
	}


	/*private void loadFBX() {
		if(!LevelManager.isMainMenu())
			return;

		string dir_path = Application.streamingAssetsPath + "/";
		
	}*/


	/*private void compressTests() {
		if(!LevelManager.isMainMenu())
			return;

		string dir_path = Application.streamingAssetsPath + "/CompressTest/";
		DirectoryInfo directory = new DirectoryInfo(dir_path);
		string out_path = dir_path + "/Results/SATAN.result";

		/*FileStream fs_out = File.Create(out_path);
		ZipOutputStream zip_stream = new ZipOutputStream(fs_out);
		int folderOffset = dir_path.Length;// + (dir_path.EndsWith("\\") ? 0 : 1);


		string[] files = Directory.GetFiles(dir_path);
		foreach(string filename in files) {
			FileInfo file = new FileInfo(filename);

			if((File.GetAttributes(file.FullName) &
				   FileAttributes.Hidden) != FileAttributes.Hidden & file.Extension != ".result") {				

				string entryName = filename.Substring(folderOffset); // Makes the name in zip based on the folder
				entryName = ZipEntry.CleanName(entryName);
				ZipEntry newEntry = new ZipEntry(entryName);
				newEntry.DateTime = file.LastWriteTime;
				newEntry.Size = file.Length;

				zip_stream.PutNextEntry(newEntry);
				byte[] buffer = new byte[4096];
				using(FileStream streamReader = File.OpenRead(filename)) {
					StreamUtils.Copy(streamReader, zip_stream, buffer);
				}
				zip_stream.CloseEntry();
			}
		}

		zip_stream.SetLevel(3); //0-9, 9 being the highest level of compression

		zip_stream.IsStreamOwner = true; // Makes the Close also Close the underlying stream
		zip_stream.Close();*/






	/*	int i = 0;


		ZipFile zf = null;
		try {
			FileStream fs = File.OpenRead(out_path);
			zf = new ZipFile(fs);
			foreach(ZipEntry zipEntry in zf) {
				if(!zipEntry.IsFile) {
					continue;           // Ignore directories
				}

				if(i++ < 2) {
					String entryFileName = zipEntry.Name;
					byte[] buffer = new byte[4096];
					Stream zipStream = zf.GetInputStream(zipEntry);

					MemoryStream mem_stream = new MemoryStream();
					StreamUtils.Copy(zipStream, mem_stream, buffer);



					Texture2D tex = new Texture2D(2, 2);
					tex.LoadImage(mem_stream.ToArray());

					if(i == 1) {
						testquadone.GetComponent<MeshRenderer>().material.mainTexture = tex;
					} else {
						testquadtwo.GetComponent<MeshRenderer>().material.mainTexture = tex;
					}
				}

				/*String entryFileName = zipEntry.Name;
				// to remove the folder from the entry:- entryFileName = Path.GetFileName(entryFileName);
				// Optionally match entrynames against a selection list here to skip as desired.
				// The unpacked length is available in the zipEntry.Size property.

				byte[] buffer = new byte[4096];     // 4K is optimum
				Stream zipStream = zf.GetInputStream(zipEntry);

				// Manipulate the output filename here as desired.
				String fullZipToPath = Path.Combine(outFolder, entryFileName);
				string directoryName = Path.GetDirectoryName(fullZipToPath);
				if(directoryName.Length > 0)
					Directory.CreateDirectory(directoryName);

				// Unzip file in buffered chunks. This is just as fast as unpacking to a buffer the full size
				// of the file, but does not waste memory.
				// The "using" will close the stream even if an exception occurs.
				using(FileStream streamWriter = File.Create(fullZipToPath)) {
					StreamUtils.Copy(zipStream, streamWriter, buffer);
				}*/
	/*		}
		} finally {
			if(zf != null) {
				zf.IsStreamOwner = true; // Makes close also shut the underlying stream
				zf.Close(); // Ensure we release resources
			}
		}
	}*/


	private void Update() {
		calculateFPS();

		if(Input.GetKeyDown(KeyCode.F1)) {
			_debug_standard = !_debug_standard;
			_debug_players = false;
        }

		if(Input.GetKeyDown(KeyCode.F2)) {
			_debug_players = !_debug_players;
			_debug_standard = false;
        }

		transform.GetChild(0).gameObject.SetActive(_debug_standard);
		fps_counter_object.SetActive(_debug_standard || GraphicsPresets.show_fps);

		Cursor.visible = _debug_standard || Application.isEditor;

		if(_debug_standard) {
			set_stage_to.text = "Set Stage => " + stage.ToString();
			current_stage.text = LevelManager.stage.current.ToString();
		}

		if(_debug_standard || GraphicsPresets.show_fps) {
			fps_counter.text = currentFPS(false);
		}
	}


	/* */
	private void OnGUI() {
		if(_debug_players)
			debugPlayers();
	}


	/* */
	private void debugPlayers() {
		GUILayout.BeginVertical();
		for(int i = 0; i < PlayerHandler.numValidConfigs(); i++) {
			PlayerConfiguration config = PlayerHandler.retrieveConfigByIndex(i);
			CharacterBase character = PlayerHandler.findAliveCharacter(config.player_ID);

			GUILayout.BeginHorizontal();

			GUILayout.Box(string.Format("Name: {0} ({1})", config.player_name, character != null ? "Alive" : "Dead"));
			if(character != null) {
				GUILayout.BeginVertical();
				GUILayout.BeginHorizontal();

				GUILayout.Box(string.Format("Speed: {0} (Default {1})", character.speed, Globals.DEFAULT_MOVE_SPEED));

				GUILayout.EndHorizontal();
				GUILayout.EndVertical();
			}


			GUILayout.EndHorizontal();
		}
		GUILayout.EndVertical();
	}


	/* Calculates the current fps and stores it in a variable. */
	private void calculateFPS() {
		const float FPS_MEASURE_INTERVAL = 0.5f;

		/* Accumulating frames. */
		_frame_count++;

		/* Once we reach our next measure time, we use the accumulated frames and measurement interval
		 * to calculate the current FPS. */
		if(Time.realtimeSinceStartup > _fps_next_measure_time) {
			_fps_current = (_frame_count / FPS_MEASURE_INTERVAL);
			_frame_count = 0;
			_fps_next_measure_time += FPS_MEASURE_INTERVAL;
		}
	}


	/* Returns a string with just the current FPS or the current FPS formatted. */
	public static string currentFPS(bool p_formatted) {
		const string FORMATTED_DISPLAY = "Current: {0} FPS";
		return string.Format(p_formatted ? FORMATTED_DISPLAY : "{0}", _instance._fps_current.ToString());
	}



	public IngameStage stage {
		get; set;
	}

	public void changeStage(bool forward) {
		stage = (IngameStage)Globals.changeEnumIndex((int)stage, (int)IngameStage.flag_length, forward);

		if(LevelManager.isInGame()) {
			switch((IngameStage)LevelManager.stage.current) {
			case IngameStage.Cinematic:
				CameraManager.isCamerasLocked = true;
				break;
			default:
				CameraManager.isCamerasLocked = false;
				break;
			}
		}
	}

	public void setStageTo() {
		LevelManager.stage.change = stage;
	}

	public void killPlayer(int i) {
		PlayerConfiguration PC = PlayerHandler.retrieveConfigByControllerID(i);
		CharacterBase CB = null;

		foreach(CharacterBase cb in PlayerHandler.characters) {
			if(cb.player_ID == PC.player_ID) {
				CB = cb;
				break;
			}
		}

		if(CB == null)
			return;

		CB.damage(new AttackOrder(), 3);
	}

	public void createConfig(int i) {
		PlayerHandler.createPlayer(i);

		List<Team> allowed_teams = new List<Team>();

		switch(LevelManager.whatGamemodeIsScene()) {
		case GameSettings.GameMode.FreeForAll:
		case GameSettings.GameMode.ResourceRace:
			allowed_teams.Add(Team.Teamless);
			break;
		case GameSettings.GameMode.CaptureTheFlag:
			allowed_teams.Add(Team.Red);
			allowed_teams.Add(Team.Blue);
			break;
		}
		
		PlayerHandler.retrieveConfigByControllerID(i).team = allowed_teams[UnityEngine.Random.Range(0, allowed_teams.Count)];

		/* Maybe overly complicated way of making sure that player 1 and player 2 are always different teams. */
		if(PlayerHandler.numValidConfigs() == 2 && allowed_teams.Count > 1) {
			while(PlayerHandler.retrieveConfigByControllerID(i).team == PlayerHandler.retrieveConfigByIndex(0).team) {
				PlayerHandler.retrieveConfigByControllerID(i).team = allowed_teams[UnityEngine.Random.Range(0, allowed_teams.Count)];
			}
		}

		SpawnHandler.requestSpawn(Master.player_prefab, SpawnHandler.ObjectType.Character, PlayerHandler.retrieveConfigByControllerID(i).player_ID);
		stage = IngameStage.Play;
		setStageTo();
	}

	public static bool attack_visualiser { get; set; }

























}




public class TestBuffs {

	public string name;
	public InteractMethod interact_method;
	public double duration;
	public double tick_interval;
	public bool use_ticks;

	public bool use_prefab;
	public bool modifies_speed, modifies_health, modifies_equipmentslot;

	public string custom_texture, custom_object, prefab_object;

	
	/* Speed Modifying Settings. */
	public int speed_procentage;
	public Buff buff = new Buff();



	public TestBuffs() {
		name = "New Buff";
		interact_method = InteractMethod.Explicit;

		duration = 0;
		tick_interval = 1;
		use_ticks = false;

		use_prefab = true;
		modifies_health = modifies_speed = modifies_equipmentslot = false;
		custom_texture = "";
		custom_object = "";
		prefab_object = "";

		/* Speed */
		speed_procentage = 100;		
	}


	public TestBuffs createCopy() {
		TestBuffs copy = new TestBuffs();

		copy.name = name;
		copy.interact_method = interact_method;

		copy.duration = duration;
		copy.tick_interval = tick_interval;
		copy.use_ticks = use_ticks;

		copy.use_prefab = use_prefab;

		copy.modifies_equipmentslot = modifies_equipmentslot;
		copy.modifies_health = modifies_health;
		copy.modifies_speed = modifies_speed;

		copy.custom_texture = custom_texture;
		copy.custom_object = custom_object;
		copy.prefab_object = prefab_object;

		/* */
		copy.speed_procentage = speed_procentage;

		return copy;
	}


	public GameObject getGameObject() {
		if(use_prefab) {
			return null;
		} else {
			return null;
		}
	}
}


[System.Serializable]
public class Buff {
	public string name = "";
	public int value = 0;
	public bool toggle = false;
}