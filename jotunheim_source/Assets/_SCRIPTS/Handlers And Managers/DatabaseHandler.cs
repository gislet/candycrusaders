﻿/******************************************************************************
*
* DatabaseHandler.cs
*
* asdf
*
*
* Written by: Gisle Thorsen (October, 2017)
*
******************************************************************************/


using System;
using System.Collections.Generic;
using Weapons;
using StatusEffects;
using UnityEngine;


public static class DatabaseHandler {

	/* */
	private static readonly string _weapons_path = "/Data/" + "Weapons.json";
	private static readonly string _behaviour_path = "/Data/" + "WeaponBehaviours.json";
	private static readonly string _statuseffects_path = "/Data/" + "StatusEffects.json";

	/* */
	public static event Action update_event;



	/* */
	public static void awake() {
		update_event = null;

		weapons_database = new HandlerDatabase<Weapon>(_weapons_path, ref update_event);
		behaviour_database = new HandlerDatabase<WeaponBehaviour>(_behaviour_path, ref update_event);
		statuseffects_database = new HandlerDatabase<StatusEffect>(_statuseffects_path, ref update_event);
    }


	/* */
	public static void update() {
		if(update_event != null)
			update_event();
		else
			Debug.Log("Update Event for DatabaseHandler is null.");
	}


	#region Getters & Setters
	public static HandlerDatabase<Weapon> weapons_database { get; private set; }
	public static HandlerDatabase<WeaponBehaviour> behaviour_database { get; private set; }
	public static HandlerDatabase<StatusEffect> statuseffects_database { get; private set; }
	#endregion
}


public class HandlerDatabase<T> where T : class, IIdentifiable, new() {

	/* */
	private static List<T> _list_private = null, _list_public = null, _list_all = null;
	private static System.DateTime _private_writetime = new System.DateTime(), _public_writetime = new System.DateTime(), _updatetime = new System.DateTime();



	/* */
	public HandlerDatabase(string p_path, ref Action p_update) {
		directory_path = p_path;
		p_update += update;
		loadDatabases();
	}


	/* */
	private void update() {
		if(names == null)
			loadDatabases();

		/* Private */
		if(Globals.fileUpdated(DatabasePrivacy.Private, directory_path, _private_writetime)) {
			loadDatabases();

			if(typeof(T) == typeof(Weapon))
				Debug.Log("updated privately");
		}

		/* Public */
		if(Globals.fileUpdated(DatabasePrivacy.Public, directory_path, _public_writetime)) {
			loadDatabases();

			if(typeof(T) == typeof(Weapon))
				Debug.Log("updated publically");
		}
	}


	/* */
	private void loadDatabases() {
		_list_private = Globals.loadDatabase<T>(DatabasePrivacy.Private, directory_path);
		_list_public = Globals.loadDatabase<T>(DatabasePrivacy.Public, directory_path);

		_private_writetime = private_writetime;
		_public_writetime = public_writetime;
		_updatetime = System.DateTime.Now;

		_list_all = new List<T>();
		_list_all.AddRange(_list_private);
		_list_all.AddRange(_list_public);

		names = new string[_list_all.Count];
		for(int i = 0; i < names.Length; i++) {
			names[i] = _list_all[i].name;
		} 
	}


	/* */
	public T find(string p_ID) {
		if(_list_all != null) {
			foreach(T effect in _list_all) {
				if(effect.ID.CompareTo(p_ID) == 0) {
					return effect;
				}
			}
		}

		return null;
	}


	/* */
	public int index(string p_ID) {
		if(_list_all != null) {
			for(int i = 0; i < _list_all.Count; i++) {
				if(_list_all[i].ID.CompareTo(p_ID) == 0) {
					return i;
				}
			}
		}

		return -1;
	}



	#region Getters & Setters
	public string directory_path { get; private set; }
	public string[] names { get; private set; }

#if UNITY_EDITOR
	public System.DateTime private_writetime { get { return Globals.fileWritetime(Globals.pathPrefix(DatabasePrivacy.Private) + directory_path); } }
#else
	public System.DateTime private_writetime { get { return new System.DateTime(); } }
#endif

	public System.DateTime public_writetime { get { return Globals.fileWritetime(Globals.pathPrefix(DatabasePrivacy.Public) + directory_path); } }
	public System.DateTime updatetime { get { return _updatetime; } }

	public List<T> list_private {
		get { return _list_private != null ? _list_private : new List<T>(); }
	}
	public List<T> list_public {
		get { return _list_public != null ? _list_public : new List<T>(); }
	}
	public List<T> list_all {
		get { return _list_all != null ? _list_all : new List<T>(); }
	}
	#endregion

}
