﻿/***
 * 
 * LanguageHandler.cs
 * 
 * asdf
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using System.Collections;
using System.IO;
using LitJson;
using System.Collections.Generic;

public static class LanguageHandler {


	private static Localisation localisation_file;

	/* Miscellaneous. */
	private static string _file_path = "";
	private static string _directory_path = Application.dataPath + "/Resources/Localisations/";
	private static string _default_lang = "en-GB";
	private static string _extension = ".localisation";



	/* */
	public static void reset() {
		localisation_file = null;
    }


	/* */
	public static void update() {
		//loadLocalisationFile();
	}


	/* */
	private static void loadLocalisationFile() {
		_file_path = _directory_path + _default_lang + _extension;

		if(File.Exists(_file_path)) {
			localisation_file = JsonMapper.ToObject<Localisation>(File.ReadAllText(_file_path));
		} else {
			Localisation lang_file = new Localisation();
			JsonWriter writer = new JsonWriter();

			writer.PrettyPrint = true;			
			JsonMapper.ToJson(lang_file, writer);			

			File.WriteAllText(_file_path, writer.ToString());

			loadLocalisationFile();
		}
	}


	/* */
	public static string getValue(string p_key, LocalisationDictionary p_dictionary) {
		if(localisation_file != null) {
			switch(p_dictionary) {
			case LocalisationDictionary.MainMenu:
				return localisation_file.main_menu_dictionary.ContainsKey(p_key) ? localisation_file.main_menu_dictionary[p_key] : "";
			case LocalisationDictionary.Tooltip:
				return localisation_file.tooltip_dictionary.ContainsKey(p_key) ? localisation_file.tooltip_dictionary[p_key] : "";
			case LocalisationDictionary.Custom:
				return localisation_file.custom_dictionary.ContainsKey(p_key) ? localisation_file.custom_dictionary[p_key] : "";
			default:
				return "";
			}
		}

		return "";
	}
}


/* If a localisation is created, it will start off with the default language of this program. In
 * our case that is the localisation of English (United Kingdom, en-GB). */
public class Localisation {

	public string author = "Gisle Thorsen";
	public Dictionary<string, string> main_menu_dictionary = new Dictionary<string, string>();
	public Dictionary<string, string> tooltip_dictionary = new Dictionary<string, string>();
	public Dictionary<string, string> custom_dictionary = new Dictionary<string, string>();


	public Localisation() {

		mainMenuDictionary();
		tooltipDictionary();
		

		/* Options Graphics Keys. */
		/* Options Audio Keys. */
		/* Options Keymapping Keys. */

		/* Confirm Keys. */

	}


	/* */
	private void mainMenuDictionary() {
		/* Main Menu Keys. */
		main_menu_dictionary.Add("main_menu_play", "PLAY");
		main_menu_dictionary.Add("main_menu_options", "OPTIONS");
		main_menu_dictionary.Add("main_menu_credits", "CREDITS");
		main_menu_dictionary.Add("main_menu_quit", "QUIT");

		/* Main Play Keys. */
		main_menu_dictionary.Add("main_play_host", "Start");
		main_menu_dictionary.Add("main_play_find", "Find Server");
		main_menu_dictionary.Add("main_play_quickfind", "Auto Browse");

		/* Main Option Keys. */
		main_menu_dictionary.Add("main_options_graphics", "Graphics");
		main_menu_dictionary.Add("main_options_audio", "Audio");
		main_menu_dictionary.Add("main_options_keymapping", "Controls & Keybinds");
	}


	/* */
	private void tooltipDictionary() {
		/* Main Menu Play Keys. */
		tooltip_dictionary.Add("main_play_host",
						"Quickly set up a <color>local game</color>, or set up your own <color>online lobby</color>.");
		tooltip_dictionary.Add("main_play_find",
						"<color>Browse online servers!</color>\n" +
						"Costumise your filter to find games you want to play. <color> Join with up to 4 friends" +
						"</color> to <color> play big 8 player games</color>.");
		tooltip_dictionary.Add("main_play_quickfind",
						"Sit back and relax while the jotuns find you a battle!");

		/* Main Menu Option Keys. */
		tooltip_dictionary.Add("main_options_graphics",
						"Adjust certain graphical settings and toggle some visual information.\n" +
						"<size>(<color>Note</color>: Currently the menu is more demanding than the game) </size> ");
		tooltip_dictionary.Add("main_options_audio",
						"Adjust the volume and/or mute certain audio channels.");
		tooltip_dictionary.Add("main_options_keymapping",
						"Modify or look up the keymapping for various devices.\n" +
						"<size> (Cannot currently adjust settings, alpha - version and all that) </size> ");
	}

}


public enum LocalisationDictionary {
	Custom, MainMenu, Tooltip
}
