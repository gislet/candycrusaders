﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class FragmentHandler : MonoBehaviour {

	public Material fragment_material;
	[Range(1, 15)]public int angle_resolution = 5;
	[Tooltip("0 will give infinite life to each fragment. If you want to forcefully enable them go to the DebugMaster entity.")]
	[Range(0, 15)]public int frag_lifetime = 5;
	private Queue<float> _frag_timers = new Queue<float>();
	private static FragmentHandler _instance;
	private static List<GameObject> frag_objs = new List<GameObject>();
	private static List<Fragment> frags = new List<Fragment>();
	private static List<MeshRenderer> renderers = new List<MeshRenderer>();

	public void Awake() {
		_instance = this;
	}


	/* */
	public void Update() {
		if(frag_lifetime > 0) {
			while(_frag_timers.Count > 0 ? _frag_timers.Peek() <= Time.time : false) {
				_frag_timers.Dequeue();
				frags.RemoveAt(0);
				renderers.RemoveAt(0);
				Destroy(frag_objs[0]);
				frag_objs.RemoveAt(0);
			}
		}

		for(int i = 0; i < frags.Count; i++) {
			foreach(CharacterBase character in PlayerHandler.characters) {
				if(frags[i] != null ? renderers[i] != null : false) {
					if(frags[i].collision(character.position, 1)) {
						renderers[i].material.color = Color.green;
						break;
					} else {
						renderers[i].material.color = Color.red;
					}
				}
			}
		}
	}


	/* */
	public static void cleanUp() {
		for(int i = frag_objs.Count - 1; i >= 0; i++) {
			Destroy(frag_objs[i]);
		}
	}


	/* */
	public static Fragment[] create(AttackOrder p_order, bool p_instantiate_visuals = false, Transform p_parent = null) {
		return create(p_order, Globals.INVALID, p_instantiate_visuals || DebugHandler.attack_visualiser, p_parent);
	}


	/* */
	public static Fragment[] create(AttackOrder p_order, int p_start_index, bool p_instantiate_visuals = false, Transform p_parent = null) {
		Vector3 origin = p_order.character.transform.position + (Vector3.Normalize(GlobalMovement.findDirectionByTransform(p_order.character.transform)) * p_order.settings.distance);
		float start_angle = GlobalMovement.findAngleByDirection(GlobalMovement.findDirectionByTransform(p_order.character.transform)) - (p_order.settings.angle_size / 2);
		float fragment_size = 15;

		return create(origin, p_order.settings.radius, start_angle, p_order.settings.angle_size, fragment_size, p_start_index, p_instantiate_visuals || DebugHandler.attack_visualiser, p_parent);
	}


	/* Creates fragments that will be used to detect if a user collides with something, like
	 * a melee swing attack or maybe a heal. */
	public static Fragment[] create(Vector3 p_origin, float p_length, float p_start_angle, float p_angle_size, float p_fragment_size, 
									int p_start_index = Globals.INVALID, bool p_instantiate_visuals = false, Transform p_parent = null) {
		List<Fragment> fragments = new List<Fragment>();
		

		int num_frags = Mathf.CeilToInt(p_angle_size / p_fragment_size);

		//Debug.Log("Creating " + num_frags + " Fragment");

		for(int i = p_start_index != Globals.INVALID ? p_start_index : 0; i < num_frags; i++) {
			float angle_start = p_start_angle + (p_fragment_size * i);	//(-(p_angle_size/2)) + (p_fragment_size * i);
            float angle_end = angle_start + p_fragment_size;

			fragments.Add(new Fragment(p_origin, angle_start, angle_end, p_length, p_parent));

			if(i == p_start_index)
				break;
		}

		//Debug.Log("Made " + fragments.Count + " fragments.");

		/* If the user wish to see the fragments created visually in the game. */
		if(p_instantiate_visuals)
			instantiateVisuals(fragments.ToArray(), p_parent);

		return fragments.ToArray();
	}


	/* */
	/*private static Fragment createFragment(Vector3 p_origin, float p_start, float p_end, float p_length) {
		Fragment frag = new Fragment(p_origin, p_start, p_end, p_length);

		

		return frag;
	}*/


	/* Creates meshes of the fragments so that they can be seen in the game. */
	private static void instantiateVisuals(Fragment[] p_fragments, Transform p_parent) {
		//Debug.Log("Visualising Fragment");

		foreach(Fragment fragment in p_fragments) {
			GameObject go = new GameObject();
			MeshFilter mf = go.AddComponent<MeshFilter>();
			MeshRenderer mr = go.AddComponent<MeshRenderer>();
			
			go.transform.position = fragment.origin;
			mr.material = _instance.fragment_material;
			mf.mesh = createMesh(fragment);

			if(p_parent != null) {
				go.transform.parent = p_parent;
			}

			frag_objs.Add(go);
            frags.Add(fragment);
			_instance._frag_timers.Enqueue(_instance.frag_lifetime + Time.time);
			renderers.Add(mr);
		}
	}


	/* */
	private static Mesh createMesh(Fragment p_fragment) {
		Mesh mesh = new Mesh();

		//Debug.Log("Creating Mesh");

		/* We initialise the different sets, arrays and lists. */
		List<int> tris = new List<int>();
		Dictionary<Vector2, int> verts_dict = new Dictionary<Vector2, int>();
		List<Vector3> verts = new List<Vector3>();
		HashSet<Vector2> uv = new HashSet<Vector2>();

		Vector3 origin = Vector3.zero;
		float angle = p_fragment.end - p_fragment.start;
		int fragments = Mathf.RoundToInt(angle / _instance.angle_resolution);


		for(int i = 0; i < fragments; i++) {

			Vector3[] triangle = new Vector3[3];

			triangle[0] = origin;
			triangle[1] = origin + (GlobalMovement.findDirectionByAngle(origin, (p_fragment.start) + (_instance.angle_resolution * (i))) * p_fragment.length);
			triangle[2] = origin + (GlobalMovement.findDirectionByAngle(origin, (p_fragment.start) + (_instance.angle_resolution * (i + 1))) * p_fragment.length);

			foreach(Vector3 v in triangle) {
				float x = ((int)(v.x * 100000)) / 100000f;
				float y = ((int)(v.z * 100000)) / 100000f;

				Vector2 safe_v = new Vector2(x, y);

				if(!verts_dict.ContainsKey(safe_v)) {
					verts_dict.Add(safe_v, verts.Count);
					uv.Add(safe_v);


					Vector3 v3 = new Vector3(x, 0, y);
					verts.Add(v3);
				}

				tris.Add(verts_dict[safe_v]);
			}
		}

		mesh.vertices = verts.ToArray();
		mesh.uv = uv.ToArray();
		mesh.triangles = tris.ToArray();

		mesh.RecalculateNormals();
		mesh.RecalculateBounds();
		;

		return mesh;
	}
}


/* */
public class Fragment {
	private Vector3 _origin;
	private Transform _parent;
	public Vector3 direction;
	public float length;
	public float start, middle, end;

	public Fragment(Vector3 p_origin, float p_start, float p_end, float p_length, Transform p_parent = null) {
		_origin = p_origin;
		_parent = p_parent;
		length = p_length;
		start = p_start;
		middle = p_start + ((p_end - p_start) / 2);
		middle = middle >= 360 ? middle - 360 : middle;
        end = p_end;

		direction = GlobalMovement.findDirectionByAngle(p_origin, p_start + (p_end / 2));
	}


	/* */
	public bool collision(Vector3 p_target_position, float p_target_radius) {
		bool collided = false;
		int index = 0, checklist_size = 5;

		//We should add inn a height collision check as well, for the instances where the target is way below or over the collision area.

		while(!collided && index < checklist_size) {
			switch(index) {
			case 0: /* First we wanna figure out if the target is within length of us. */
				if(!distanceCheck(p_target_position, p_target_radius))
					return false;
				break;
			case 1:	/* Secondly we want to make a raycast vision check. */
				/*if(!raycastCheck(p_target_position))
					return false;*/
                break;
			case 2: /* Then we can check to see if the target's radius is bigger than the distance it is from us, 
					 * which means it will collide no matter what. */
				collided = hugCheck(p_target_position, p_target_radius);
				break;
			case 3: /* Not solved it yet, we check to see if the target is within our field of view. */
				collided = GlobalMovement.isWithinFieldOfView(start, end, GlobalMovement.findAngleByDirection((p_target_position - origin)));
				break;
			case 4: /* If it is outside, we want to check to see if the radius of our target is overlapping our field of view. */
				collided = overlappingFOVCheck(p_target_position, p_target_radius);
				break;
			}

			index++;
		}

		return collided;
	}


	/* Makes sure there's nothing that the raycast collides with between the fragments origin and the
	 * targets position. */
	private bool raycastCheck(Vector3 p_target_position) {
		Vector3 direction = p_target_position - origin;
		float max_distance = Vector3.Distance(origin, p_target_position);

		if(Physics.Raycast(origin, direction, max_distance)) {
			return false;
		}

		return true;
	}


	/* Checks to see whether the target's radius is larger than the distance the target is from
	 * the origin. */
	private bool hugCheck(Vector3 p_target_position, float p_target_radius) {
		float distance = Vector3.Distance(origin, p_target_position);
		if(distance < p_target_radius) {
			return true;
		}

		return false;
	}


	/* Checks to see if the distance the target is from our origin is lesser or equal to
	 * our fragments reach combined with the targets radius. */
	private bool distanceCheck(Vector3 p_target_position, float p_target_radius) {
		float distance = Vector3.Distance(origin, p_target_position);
		if(distance < (p_target_radius + length)) {
			return true;
		}

		return false;
	}


	/* Checks to see if the target position with its radius is somehow overlapping our field of view. */
	private bool overlappingFOVCheck(Vector3 p_target_position, float p_target_radius) {
		float distance = Vector3.Distance(origin, p_target_position);
		Vector3 p1 = origin + (GlobalMovement.findDirectionByAngle(origin, start) * distance);
		Vector3 p2 = origin + (GlobalMovement.findDirectionByAngle(origin, end) * distance);

		if(Vector3.Distance(p1, p_target_position) <= p_target_radius ||
			Vector3.Distance(p2, p_target_position) <= p_target_radius) {
			return true;
		}

		return false;
	}


	/* */
	public Vector3 origin {
		get {
			if(_parent != null) {
				return _parent.position;
			}

			return _origin;
		}
	}
}