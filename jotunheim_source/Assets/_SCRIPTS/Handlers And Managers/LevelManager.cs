﻿/***
 * 
 * LevelManager.cs
 * 
 * This object acts as an entity and is tied to a world object. To access the script
 * you treat it as a static object. This script acts as an intermediate between Unity's
 * own SceneManager and various other objects that wish to load gather info about scenes.
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/

using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using GameMode = GameSettings.GameMode;


public class LevelManager : MonoBehaviour {


	/* */
	private LevelStage _levelStage;
	private CachedScene _tempCachedScene;

	/* Configurable presets of loadable scenes. */
	public SceneObject main_menu, splash_screen;
	public SceneBundle[] scene_bundles;

	/* Enabling static access. */
	private static LevelManager _instance;



	/* This function is called by Monobehaviour. */
	private void Awake() {
		_instance = this;
		_tempCachedScene = new CachedScene(GameMode.None);
		stageSetup();
	}


	/* */
	private void stageSetup() {
		if(isInGame()) {
			_levelStage = new LevelStage(IngameStage.Loading);
		} else if(isMainMenu()) {
			_levelStage = new LevelStage(MenuStage.Menu);
		} else {
			_levelStage = new LevelStage(NullStage.None);
		}
	}


	/* Returns a bool of whether or not our current scene is the Main Menu. */
	public static bool isMainMenu() {
		if(_instance == null)
			return false;


		/* Checks to see if we're in the Main Menu of the game. */
		if(string.Compare(SceneManager.GetActiveScene().name, _instance.main_menu.scene_name) == 0)
			return true;
		return false;
	}


	/* Returns a bool of whether or not our current scene is associated with an ingame scene. */
	public static bool isInGame() {
		if(_instance == null)
			return false;

		/* Checks to see if we're in the main menu, which we don't want. */
		if(string.Compare(SceneManager.GetActiveScene().name, _instance.main_menu.scene_name) == 0)
			return false;

		/* Otherwise it checks to see if we're in the splash-screen, which we also don't want. */
		else if(string.Compare(SceneManager.GetActiveScene().name, _instance.splash_screen.scene_name) == 0)
			return false;

		return true;
	}


	/* Tries to loads the referenced Main Menu Screen. */
	public static void loadMainMenu() {
		LanguageHandler.reset();
		loadScene(_instance.main_menu.scene_name);
	}


	/* Tries to loads the referenced Splash-Screen. */
	public static void loadSplashScreen() {
		LanguageHandler.reset();
		loadScene(_instance.splash_screen.scene_name);
	}


	/* Depending on the currently selected gamemode in our gamesettings, the LevelManager
	 * tries to load a scene associated with it. */
	public static void loadGame() {
		SceneBundle sb = findAssociatedSceneBundle(GameSettings.current._game_mode);

		if(sb.scenes != null ? sb.scenes.Length > 0 : false) {
			LanguageHandler.reset();
			loadScene(sb.scenes[GameSettings.current._level_index].scene_name);
		}
	}


	/* */
	public static void reloadScene() {
		loadScene(SceneManager.GetActiveScene().buildIndex);
	}


	/* */
	private static void loadScene(int level_index) {
		//FragmentHandler.cleanUp();
		SceneManager.LoadScene(level_index);
	}


	/* */
	private static void loadScene(string level_name) {
		//FragmentHandler.cleanUp();
		SceneManager.LoadScene(level_name);
	}


	/* Figures out what gamemode is associated with the current scene, if any. */
	public static GameMode whatGamemodeIsScene() {
		string current_scene = SceneManager.GetActiveScene().name;

		foreach(SceneBundle bundle in _instance.scene_bundles) {
			foreach(SceneObject obj in bundle.scenes) {
				if(obj.scene_name.CompareTo(current_scene) == 0) {
					return bundle.game_mode;
				}
			}
		}

		return GameMode.None;
	}


	/* */
	public static Sprite getWorldPreviewSprite() {
		if(!_instance.cacheCurrentSceneObject())
			return null;

		return _instance._tempCachedScene.scene_object.world_preview;
    }


	/* */
	public static string getWorldName() {
		if(!_instance.cacheCurrentSceneObject())
			return null;

		return _instance._tempCachedScene.scene_object.world_name;
	}


	/* */
	public static string getModeTooltip() {
		SceneBundle sb = findAssociatedSceneBundle(GameSettings.current._game_mode);

		return (sb.scenes != null ? sb.tooltip : "ERROR");
	}


	/* */
	public static SpawnHandler.SpawnMethod getModeSpawnMethod() {
		SceneBundle sb = findAssociatedSceneBundle(GameSettings.current._game_mode);
		return sb.spawn_method;
	}


	/* */
	public static Scoring.ScoringMethod getScoringMethod() {
		SceneBundle sb = findAssociatedSceneBundle(GameSettings.current._game_mode);
		return sb.scoring_method;
	}


	/* */
	public static int getGameGoalScore() {
		SceneBundle sb = findAssociatedSceneBundle(GameSettings.current._game_mode);
		return sb.goal_score;
	}


	/* */
	private bool cacheCurrentSceneObject() {

		/* We first check to see if we already have it cached correctly and return the appropriate bool. */
		if(_tempCachedScene.current_gamemode == GameSettings.current._game_mode) {
			return _instance._tempCachedScene.scene_object != null;
        }		

		/* If something is not right or we haven't cached it yet, we attempt to do so. */
		SceneBundle sb = findAssociatedSceneBundle(GameSettings.current._game_mode);
		_tempCachedScene = new CachedScene(GameSettings.current._game_mode);

		/* Should our SceneBundle be missing scenes, we are going to consider that a failure. */
		if(sb.scenes != null ? sb.scenes.Length <= 0 : true) {
			return false;
		}

		/* Otherwise, everything is good and we apply the correct scene-object to our cached variable and
		 * return success. */
		_tempCachedScene.scene_object = sb.scenes[0];
		return true;
	}


	/* */
	public static SceneBundle findAssociatedSceneBundle(GameMode p_mode) {
		//GameMode game_mode = GameSettings.current._game_mode;

		/* Looks for the correct scenebundle. */
		for(int i = 0; i < _instance.scene_bundles.Length; i++) {
			if(_instance.scene_bundles[i].game_mode == p_mode) {

				/* Once we find the correct scenebundle, we either load a 
				 * specific scene or a scene at random. */
				SceneBundle sb = _instance.scene_bundles[i];
				return sb;
			}
		}

		if(Time.time < 1f)
			Debug.LogError("Scene doesn't belong to a bundle!");
		SceneBundle bundle = new SceneBundle();
		bundle.goal_score = 999;
		return bundle;
	}


	/* */
	public static string levelName {
		get {
			return SceneManager.GetActiveScene().name;
		}
	}


	/* */
	public static LevelStage stage {
		get { return _instance._levelStage; }
	}


	/* */
	private struct CachedScene {
		public GameMode current_gamemode;
		public SceneObject scene_object;

		public CachedScene(GameMode p_mode = GameMode.None) {
			current_gamemode = p_mode;
			scene_object = null;
		}
	}
}

/* A struct containing an array of scenes that are bound to a game-mode. */
[System.Serializable]
public struct SceneBundle {
	public string name;
	public GameMode game_mode;
	public Scoring.ScoringMethod scoring_method;
	public SpawnHandler.SpawnMethod spawn_method;
	public int goal_score;
	[TextArea(15, 15)]
	public string tooltip;
	public SceneObject[] scenes;
	
}

/* This struct stores a scene, and maybe an image for previewing. The scene
 * object is used to aquire its name which is used to be able to load it. */
[System.Serializable]
public class SceneObject {
	public string world_name;
	public Sprite world_preview;
	public string scene_name;
}


public enum IngameStage {
	None,
	Loading, Cinematic,
	Intro, Play, Outro,
	Pause,
	GameOver,
	GameSummary,
	Options, OptionsAudio, OptionsGFX,
	Controllers, Quit,
	flag_length
}


public enum MenuStage {
	None,
	Menu, Play, Options, Credits, Quit,
	HostSettings, CharacterAndTeam,
	Graphics, Audio, KeyMapping,
	HostSettingsSimple
}


public enum NullStage {
	None
}