﻿/***
 * 
 * AudioManager.cs
 * 
 * This script manages everything related to sound and music within the game. Other scripts will call
 * play() with the appropriate arguments and script will proceed with playing the clip. The script will
 * also try to auto-manage the music which happens through calls made in main().
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using System.Collections;
using System;
using Random = UnityEngine.Random;
using Configurations;


public class AudioManager : MonoBehaviour {

	/* */
	public AudioSource ambience_source;
	public AudioSource effects_source;
	public AudioSource music_source;

	public MusicBox_Menu default_menu_musicbox;
	public MusicBox_Game default_game_musicbox;

	/* */
	private MusicState _music_state;
	private bool _dirty = true;

	/* */
	private static AudioManager _instance;
	


	/* Caches a few essentials when the script initialises. */
	private void Awake() {
		_instance = this;
		_music_state = MusicState.Normal;

		//AudioPresets.dirty_settings = true;

		musicbox_game = default_game_musicbox;
		musicbox_menu = default_menu_musicbox;

		musicbox_menu.awake();
		musicbox_game.awake();
    }


	/* Is called by the Master script each frame. */
	public static void main() {

		if(LevelManager.isInGame()) {
			musicbox_game.play();
		} else if(LevelManager.isMainMenu()) {
			musicbox_menu.play();
		}

		/* If the audio settings have been changed, we want to take a pass over each
		 * channel to make sure their settings are up-to-date. */
		if(/*AudioPresets.dirty_settings*/ _instance._dirty) {
			//AudioPresets.dirty_settings = false;
			_instance._dirty = false;
			updateSourceChannels();
        }
	}


	/* Updates the volume of each channel, and if they are supposed to be muted or not. */
	private static void updateSourceChannels() {
		float master_value = float.Parse(Settings.interact("volume_master", ConfigAction.Value)) / 100;
		bool master_enabled = int.Parse(Settings.interact("volume_master_enabled", ConfigAction.Value)) == 1;
		
		for(int i = 0; i < (int)AudioChannel.Length; i++) {
			AudioSource source = getSource((AudioChannel)i);
			//Audio audio = null;
			float other_value = 50;
			bool other_enabled = true;

			if(master_enabled) {
				switch((AudioChannel)i) {
				case AudioChannel.Music:
					other_value = float.Parse(Settings.interact("volume_music", ConfigAction.Value));
					other_enabled = int.Parse(Settings.interact("volume_music_enabled", ConfigAction.Value)) == 1;
					//audio = AudioPresets.music;
					break;
				case AudioChannel.Ambient:
					other_value = float.Parse(Settings.interact("volume_ambient", ConfigAction.Value));
					other_enabled = int.Parse(Settings.interact("volume_ambient_enabled", ConfigAction.Value)) == 1;
					//audio = AudioPresets.ambient;
					break;
				case AudioChannel.Effects:
					other_value = float.Parse(Settings.interact("volume_effects", ConfigAction.Value));
					other_enabled = int.Parse(Settings.interact("volume_effects_enabled", ConfigAction.Value)) == 1;
					//audio = AudioPresets.effects;
					break;
				default:
					Debug.LogError(string.Format("AudioManager couldn't update channel ({0}).", (AudioChannel)i));
					continue;
				}
			}

			if(source != null) {
				source.volume = Mathf.Clamp01(Mathf.Round(other_value * master_value) / 100);
				source.mute = !(other_enabled && master_enabled);
			} else {
				Debug.LogError(string.Format("AudioManager couldn't update source ({0}). Source status is {1} and Audio status is {2}", 
												(AudioChannel)i,
												source != null,
												/*audio != null*/"!obsolete!"));
			}
		}
	}


	/* */
	public static void playRandom(AudioClip[] p_clips, AudioChannel p_channel, float volume_scale = Globals.INVALID) {
		AudioClip clip = (p_clips != null && p_clips.Length > 0) ? p_clips[Random.Range(0, p_clips.Length)] : null;

		play(clip, p_channel, volume_scale);
	}


	/* */
	public static void play(AudioClip p_clip, AudioChannel p_channel, float volume_scale = Globals.INVALID) {
		AudioSource source = getSource(p_channel);

		if(source != null) {
			if(p_channel == AudioChannel.Music) {
				if(_instance._music_state == MusicState.Normal) {
					_instance.switchMusicTrack(p_clip);
                }
			} else if(p_clip != null) {
				if(volume_scale != Globals.INVALID)
					source.PlayOneShot(p_clip, volume_scale);
				else
					source.PlayOneShot(p_clip);
			}
		}
	}


	/* */
	private void switchMusicTrack(AudioClip p_clip) {
		StartCoroutine(switchMusicRoutine(p_clip));
	}


	/* */
	private IEnumerator switchMusicRoutine(AudioClip p_clip) {
		float cached_volume = music_source.volume;
        float volume = cached_volume;

		_music_state = MusicState.Switching;

		while(Globals.deltaTimeCountdown(ref volume) >= 0 && _music_state == MusicState.Switching && music_source.isPlaying) {
			music_source.volume = volume;
			yield return null;
		}

		if(_music_state == MusicState.CancelSwitching) {
			while(Globals.deltaTimeCounter(ref volume) <= cached_volume && _music_state == MusicState.CancelSwitching) {
				music_source.volume = volume;
				yield return null;
			}
		} else {

			music_source.Pause();

			if(p_clip != null) {
				music_source.clip = p_clip;
				music_source.Play();
			}
		}

		music_source.volume = cached_volume;
		_music_state = MusicState.Normal;
	}


	/* */
	public static string currentClip(AudioChannel p_channel) {
		AudioSource source = getSource(p_channel);
		return source != null ? source.clip != null ? source.clip.name : null : null;
	}


	/* */
	public static void resume(AudioChannel p_channel) {
		AudioSource source = getSource(p_channel);

		if(source != null) {
			if(source.clip != null && !source.isPlaying) {
				source.Play();
			} else if(source.clip != null) {
				if(_instance._music_state == MusicState.Switching)
					_instance._music_state = MusicState.CancelSwitching;
            }
		}
	}


	/* */
	public static void setDirty() {
		_instance._dirty = true;
	}


	/* Finds the audio source related to this channel. */
	private static AudioSource getSource(AudioChannel p_channel) {
		switch(p_channel) {
		case AudioChannel.Music:
			return _instance.music_source;
		case AudioChannel.Ambient:
			return _instance.ambience_source;
		case AudioChannel.Effects:
			return _instance.effects_source;
		default:
			return null;
		}
	}


	#region Getters & Setters
	public static MusicBox_Game musicbox_game { get; set; }
	public static MusicBox_Menu musicbox_menu { get; set; }
	#endregion


	private enum MusicState {
		Normal, Switching, CancelSwitching
	}
}


/* An enum of each available audio-channel. Includes a Length-flag
 * to allow for easy enum iteration. */
public enum AudioChannel {
	Music, Ambient,
	Effects, Length
}














	/* */
	/*public static void main() {
		if(Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.M))
			_instance._music_enabled = !_instance._music_enabled;

		_instance.music();
	}*/

	/*[Obsolete]
	public static void nothing() { }

	/* */
	/*private void music() {
		return;
		/*switch(GameMaster.stateCell.game_state) {
		case GameState.mainmenu:
			break;
		case GameState.ingame:
			if(GameMaster.stateCell.ingame_state == IngameState.play) {
				_audio_type = AudioType.Play;
			}
			break;
		}

		playMusic();*/
	//}


	/* */
	/*public void playMusic() {
		AudioClip clip = null;
		bool force_stop = false;

		if(audio_libraries == null) {
			return;
		} else {
			foreach(AudioLibrary al in audio_libraries) {
				if(al.audio_type == _audio_type) {
					clip = al.randomClip;

					if(_music_source.isPlaying) {
						force_stop = !al.hasClip(_music_source.clip);
					}
				}
			}
		}

		if(clip == null)
			return;

		if(force_stop) {
			_music_source.Stop();
		}

		if(_music_source != null && _music_enabled) {
			if(!_music_source.isPlaying) {
				_music_source.clip = clip;
				_music_source.Play();
			}
		} else if(!_music_enabled) {
			if(_music_source.isPlaying) {
				_music_source.Stop();
			}
		}
	}*/


	/* */
	/*public void playSound(AudioClip p_clip) {
		if(p_clip == null)
			return;

		if(_effects_source != null && _sounds_enabled) {
			_effects_source.PlayOneShot(p_clip);
		}
	}*/


	/* */
	/*public void playSound(AudioLibrary[] p_libraries, SoundType p_type) {
		AudioClip clip = null;

		foreach(AudioLibrary library in p_libraries) {
			if(library.sound_type == p_type) {
				clip = library.randomClip;
			}
		}

		playSound(clip);
	}*/


	/* */
	/*public void playSound(AudioLibrary[] p_libraries, SoundType p_type, int p_index) {
		AudioClip clip = getClip(p_libraries, p_type, p_index);

		playSound(clip);
	}*/


	/* */
	/*public AudioClip getClip(AudioLibrary[] p_libraries, SoundType p_type, int p_index) {
		foreach(AudioLibrary library in p_libraries) {
			if(library.sound_type == p_type) {
				if(p_index < library.clips.Length) {
					return library.clips[p_index];
				}
			}
		}

		return null;
	}*/
//}



/* */
/*[System.Serializable]
public class AudioLibrary {


	public string name;
	public AudioType audio_type;
	public SoundType sound_type;
	public AudioClip[] clips;


	
	public AudioClip randomClip {
		get {
			if(clips != null) {
				AudioClip ac = clips[Random.Range(0,clips.Length)];
				return ac;
			}
			return null;
		}
	}

	
	public bool hasClip(AudioClip p_clip) {
		foreach(AudioClip clip in clips) {
			if(clip == p_clip)
				return true;
		}
		return false;
	}
}*/



/* */
/*public enum SoundType {
	None,
	MeleeSwing,
	MeleeHit,
	Countdown,
	HandInFlag,
	BowShoot,
	FootSteps,
	HammerSwing,
	HammerHit
}*/



/* */
/*public enum AudioType {
	None,
	Menu,
	Staging,
	Play
}*/


