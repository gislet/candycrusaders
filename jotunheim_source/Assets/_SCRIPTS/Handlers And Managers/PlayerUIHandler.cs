﻿/***
 * 
 * PlayerUIHandler.cs
 * 
 * asdf
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerUIHandler : MonoBehaviour {


	/* */
	public Sprite player_dead, empty_slot;
	public GameObject player_UI_prefab;
	private State _state = State.None;
	private PlayerUI[] _player_ui;

	/* */
	private static PlayerUIHandler _instance;
	private readonly Vector2 _offset = new Vector2(480, 270);	//The offsets are with a 1920x1080 resolution in mind.
	private readonly Vector2[] _offset_signs = {
		new Vector2(-1, 1),		//Top Left
		new Vector2(1, 1),		//Top Right
        new Vector2(-1, -1),	//Bottom Left
		new Vector2(1, -1)		//Bottom Right
	};



	/* Vital variables are instantiated in this awake function. */
	private void Awake() {
		_instance = this;
	}


	/* */
	public static void start() {
		/* We don't want to setup the player UIs when not appropriate. */
		if(!LevelManager.isInGame())
			return;

		/* If the coroutine is already running, we want to tell it to restart. */
		if(_instance._state != State.None) {
			_instance._state = State.Restarting;
			return;
        }

		/* If nothing went wrong, we setup everything and cache the UI elements
		 * so this script can update it visually. */
		_instance.cache();
		_instance.StartCoroutine(_instance.update());
	}


	/* Called by any script that wish to disable the UI. */
	public static void stop() {
		_instance._state = State.None;
    }


	/* Instantiating, positioning, and caching of all UI elements. */
	private void cache() {

		/* If there are remenents of an old UI, remove it. */
		removeChildren();
		
		_player_ui = new PlayerUI[PlayerHandler.numValidHumanConfigs()];

		/* Instantiating all UI elements. */
		for(int i = 0; i < PlayerHandler.numValidHumanConfigs(); i++) {
			GameObject UI_prefab = Instantiate(player_UI_prefab, Vector3.zero, Quaternion.identity) as GameObject;
			UI_prefab.transform.SetParent(transform);

			/* Caching newly instantiated UI elements to the PlayerUI struct array. */
			_player_ui[i] = new PlayerUI(UI_prefab.transform.GetChild(0));

			/* Then we want to apply team colours and position the UI correctly. */
			modifyElement(UI_prefab.transform, _player_ui[i], i);
		}
	}


	/* Positions and scales the UI according to number of players present. */
	private void modifyElement(Transform p_element, PlayerUI p_ui, int i) {
		p_element.localPosition = new Vector3(PlayerHandler.numValidHumanConfigs() > 1 ? (_offset.x * _offset_signs[i].x) : 0,
												PlayerHandler.numValidHumanConfigs() > 2 ? (_offset.y * _offset_signs[i].y) : 0, 
												0);
		p_element.localScale = new Vector3(PlayerHandler.numValidHumanConfigs() > 1 ? .5f : 1, 
											PlayerHandler.numValidHumanConfigs() > 2 ? .5f : 1, 
											1);


		_player_ui[i].setScales(PlayerHandler.numValidHumanConfigs() == 2 ? 1.4f : 1, PlayerHandler.numValidHumanConfigs() == 2 ? 0.75f : 1);
		p_ui.score.color = GUIGlobals.getTeamColour(PlayerHandler.retrieveConfigByPlayerID(i).team);
	}


	/* Removes all children on this object if any. */
	private void removeChildren() {
		if(transform.childCount > 1) { 
			for(int i = transform.childCount - 1; i > 0; i--) {
				Destroy(transform.GetChild(i).gameObject);
			}
		}
	}


	/* */
	private IEnumerator update() {
		_state = State.Running;


		/* */
		while(_state == State.Running) {

			for(int i = 0; i < _player_ui.Length; i++) {
				updatePlayerUI(i);
            }

			yield return null;
		}


		/* */
		if(_state == State.Restarting) {
			_state = State.None;
			start();
		} else if(_state == State.None) {
			removeChildren();
		}
	}


	/* */
	private void updatePlayerUI(int i) {

		float alpha_min = .65f;

		_player_ui[i].score.text = "" + Scoring.character_score[i];
		bool dead = PlayerHandler.isCharacterDead(i);

		CharacterBase CB = PlayerHandler.findAliveCharacter(i);
		Vector2 alphas = new Vector2(alpha_min, alpha_min);

		if(CB != null)
			alphas = CB.weapon_index == 0 ? new Vector2(1, alpha_min) : new Vector2(alpha_min, 1);
		else
            alphas = new Vector2(1, 0);

		/* */
		for(int g = 0; g < 2; g++) {

			Color color;

			color = _player_ui[i].primary[g].color;
			_player_ui[i].primary[g].color = new Color(color.r, color.g, color.b, alphas.x);

			color = _player_ui[i].secondary[g].color;
			_player_ui[i].secondary[g].color = new Color(color.r, color.g, color.b, alphas.y);

			if(g == 1 && CB != null) {
				_player_ui[i].cooldown_primary.text = CB.cooldownRemaining(0) >= 0.1f ? CB.cooldownRemaining(0).ToString("f1") : "";
				_player_ui[i].cooldown_secondary.text = CB.cooldownRemaining(1) >= 0.1f ? CB.cooldownRemaining(1).ToString("f1") : "";
			}
		}

		/* */
		_player_ui[i].primary[1].sprite = CB != null ?
											CB.weaponLogo(0) != null ?
											CB.weaponLogo(0) : empty_slot : player_dead;
		_player_ui[i].secondary[1].sprite = CB != null ?
											CB.weaponLogo(1) != null ? 
											CB.weaponLogo(1) : empty_slot : null;


		/* We don't want to set these values unecessarily. This should only set it if the player is dead
		 * or the UI is still turned on. */
		if(dead || _player_ui[i].death.enabled) {
			string killer = PlayerHandler.whoDestroyedThisID.ContainsKey(i) ? PlayerHandler.whoDestroyedThisID[i] : "error";

			_player_ui[i].death_background[0].enabled = dead;
			_player_ui[i].death_background[1].enabled = dead;
			_player_ui[i].death.enabled = dead;
			_player_ui[i].death.text = "You got killed by\n" + killer;
		}
	}


	/* A struct which has the score and the player equipment slots cached. */
	private struct PlayerUI {
		public Image[] primary;				//0 is background, 1 is Foreground
		public Image[] secondary;			//0 is background, 1 is Foreground
		public Image[] death_background;	//0 is background, 1 is Foreground
		public Text score, death, cooldown_primary, cooldown_secondary;


		/* The constructor receives the transform which holds the score
		 * and equipment slots. */
		public PlayerUI(Transform p_transform) {
			score = p_transform.GetChild(2).GetComponent<Text>();
			death = p_transform.GetChild(3).GetChild(0).GetChild(0).GetComponent<Text>();
			cooldown_primary = p_transform.GetChild(0).GetChild(0).GetChild(1).GetComponent<Text>();
			cooldown_secondary = p_transform.GetChild(1).GetChild(0).GetChild(1).GetComponent<Text>();

			primary = new Image[2];
			primary[0] = p_transform.GetChild(0).GetComponent<Image>();
			primary[1] = p_transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<Image>();

			secondary = new Image[2];
			secondary[0] = p_transform.GetChild(1).GetComponent<Image>();
			secondary[1] = p_transform.GetChild(1).GetChild(0).GetChild(0).GetComponent<Image>();

			death_background = new Image[2];
			death_background[0] = p_transform.GetChild(3).GetComponent<Image>();
			death_background[1] = p_transform.GetChild(3).GetChild(0).GetComponent<Image>();
		}


		/* */
		public void setScales(float x, float y) {
			primary[0].transform.localScale = new Vector3(x, y, 1);
			secondary[0].transform.localScale = new Vector3(x, y, 1);

			score.transform.localScale = new Vector3(x, y, 1);
		}



		/* Fades the primary or secondary equipment slot depending on
		 * the int received. */
		public void fade(int p_active_slot) {

			/* It doesn't accept values higher than 1 as there are only two equipment slots. */
			if(p_active_slot > 1)
				return;


		}
	}


	/* This enum makes sure that there won't be additional coroutines running at
	 * simultaneously. */
	private enum State {
		None, Running, Restarting
	}
}
