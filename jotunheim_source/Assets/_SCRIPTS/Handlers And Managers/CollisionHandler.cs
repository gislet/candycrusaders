﻿/******************************************************************************
*
* CollisionHandler.cs
*
* Description...
*
*
* Written by: Gisle Thorsen (July, 2017)
*
******************************************************************************/


using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public static class CollisionHandler {

	/* */
	public static bool isColliding(Shape p_shape, ITargetable p_target) {
		if(p_shape == null || p_target == null) {
			Debug.LogError("Cannot supply isColliding with a null shape or target!");
			return false;
		}

		bool collided = false;

		/* Regardless of the collision-area's shape, if the shape's position is literally inside 
		 * of the target it is considered a collision. */
		if(isHugging(p_shape, p_target))
			return true;


		/* The calculations to check whether the target's collision area overlaps the shape's collision area
		 * differs greatly depending on what type the shape is. */
		if(p_shape.GetType() == typeof(Circle)) {
			collided = circleCollision((Circle)p_shape, p_target);
		} else if(p_shape.GetType() == typeof(Rectangle)) {
			collided = rectangleCollision((Rectangle)p_shape, p_target);
		} else if(p_shape.GetType() == typeof(Triangle)) {
			collided = triangleCollision((Triangle)p_shape, p_target);
		} else {
			throw new System.ArgumentException(string.Format("The \"{0}\" shape is not yet supported by the isColliding() method.", p_shape.GetType()));
        }

		/* If a collision has occured, one last check has to be done to make sure the target's position is
		 * actually visible from the shape's position. This should be resolved by a single raycast. */
		if(collided && isVisible(p_shape, p_target))
			return true;

		return false;
	}


	/* Performs the necessary calculations to determine if a collision has occured between
	 * two circles. */
	private static bool circleCollision(Circle p_shape, ITargetable p_target) {

		/* Makes sure the two circles are close enough to intersect eachother. */
		if(!isIntersecting(p_shape, p_target))
			return false;

		/* Not solved it yet, we check to see if the target is within our field of view. */
		if(isWithinFieldOfView(p_shape, p_target))
			return true;

		/* If it is outside, we want to check to see if the radius of our target is overlapping our field of view. */
		if(isOverlappingFieldOfView(p_shape, p_target))
			return true;

		return false;
	}


	/* Performs the necessary calculations to determine if a collision has occured between
	 * a rectangle and a circle. */
	private static bool rectangleCollision(Rectangle p_shape, ITargetable p_target) {

		/* We rotate the rectangle so that it sits neatly in the world space, we then subsequently rotate the difference
		 * on the target as well. This allows us to easily check if the target's position lies within the rectangle's
		 * boundaries. */
		Vector2[] o_points_of_interest;
		if(isResidingWithinEdges(p_shape, p_target, out o_points_of_interest))
			return true;

		/* Should the target not be inside the rectangle's boundaries, then a quick edge overlapping check will be done
		 * using the set of points that caused the boundary check to fail. */
		if((o_points_of_interest != null || o_points_of_interest.Length != 3) && 
			isOverlappingEdge(o_points_of_interest[0], o_points_of_interest[1], o_points_of_interest[2], p_target))
			return true;
		
		return false;
	}


	/* Performs the necessary calculations to determine if a collision has occured between
	 * a triangle and a circle. */
	private static bool triangleCollision(Triangle p_shape, ITargetable p_target) {

		/* We rotate the triangle so that it sits neatly in the world space, we then subsequently rotate the difference
		 * on the target as well. This allows us to easily check if the target's position lies within the triangle's
		 * boundaries. */
		Vector2[] o_points_of_interest;
		if(isResidingWithinEdges(p_shape, p_target, out o_points_of_interest))
			return true;

		/* Should the target not be inside the triangle's boundaries, then a quick edge overlapping check will be done
		 * using the set of points that caused the boundary check to fail. */
		if((o_points_of_interest != null || o_points_of_interest.Length != 3) &&
			isOverlappingEdge(o_points_of_interest[0], o_points_of_interest[1], o_points_of_interest[2], p_target))
			return true;

		return false;
	}


	/* Checks to see if a point lies within a set of boundaries. */
	private static bool isResidingWithinEdges(Edge p_shape, ITargetable p_target, out Vector2[] o_points_of_interest) {
		
		/* First we need both angles to calculate their difference. */
		float shape_angle = GlobalMovement.findAngleByDirection(p_shape.direction);
		float target_angle = GlobalMovement.findAngleByDirection(p_target.position - p_shape.position);
		float relative_angle = (target_angle - shape_angle);

		/* We then find the target's relative direction to the shape's position and the distance between the two objects. */
		Vector3 relative_direction = GlobalMovement.findDirectionByAngle(p_shape.position, relative_angle);
		float distance = Vector3.Distance(p_shape.position, p_target.position);

		/* Finally we can find the target's position relative to the world space origin. */
		Vector2 relative_target_position = new Vector2((relative_direction * distance).x, (relative_direction * distance).z);

		/* We then do a series of checks to see if the target is on the correct side of each of the shape's edges. */
		for(int i = 0; i < p_shape.corners.Length; i++) {
			if(isLeft(p_shape.corners[i], p_shape.corners[i + 1 == p_shape.corners.Length ? 0 : i + 1], relative_target_position) != p_shape.isOriginLeftAligned) {
				o_points_of_interest = new Vector2[] {
					p_shape.corners[i],
					p_shape.corners[i + 1 == p_shape.corners.Length ? 0 : i + 1],
					relative_target_position
				};
				return false;
			}
		}

		o_points_of_interest = null;
		return true;
	}


	/* Checks to see if a target point, given its radius, overlaps any point on a line. */
	private static bool isOverlappingEdge(Vector2 a, Vector2 b, Vector2 c_target, ITargetable p_target) {
		Vector2 line_dir = GlobalMovement.getLineDirectionVec2(a, b).normalized;
		Vector3 tar_pos = new Vector3(c_target.x, 0, c_target.y);
		Vector3 line_origin = new Vector3(a.x, 0, a.y);
		
		/* First we need both angles to calculate their difference. */
		float line_angle = GlobalMovement.findAngleByDirection(new Vector3(line_dir.x, 0, line_dir.y));
		float target_angle = GlobalMovement.findAngleByDirection(tar_pos - line_origin);
		float relative_angle = (target_angle - line_angle);
		
		/* The first line can be greater than 90 degrees, thus if it is we simply do a distance check between
		 * the target position and the first point A to determine whether they overlap or not. */
		if(relative_angle >= 90) {
			return Vector2.Distance(a, c_target) <= p_target.radius;
		}

		float ad_dist = Mathf.Cos((relative_angle * (Mathf.PI / 180))) * Vector2.Distance(a, c_target);
		
		/* If the length of the new line a-d is determined to be longer or equal to the length of the old line a-b
		 * we know b is the closest point the target is to our line and we can perform a simple distance check. */
		if(ad_dist >= Vector2.Distance(a, b)) {
			return Vector2.Distance(b, c_target) <= p_target.radius;
		}
		
		Vector2 d = a + (line_dir * ad_dist);
		
		/* Point d is now the closest point to our target and we simply perform a distance check to see if it overlaps it. */
		if(Vector2.Distance(d, c_target) <= p_target.radius) {
			return true;
		}
		
		return false;
	}
	

	/* Only checks to see if a target is within the circle's field of view without considering
	 * the distance between the objects. */
	private static bool isWithinFieldOfView(Circle p_shape, ITargetable p_target) {
		float start_angle = GlobalMovement.findAngleByDirection(p_shape.direction) - (p_shape.degrees / 2);
		float end_angle = start_angle + p_shape.degrees;
		float target_angle = GlobalMovement.findAngleByDirection(p_target.position - p_shape.position);


		/* We want to make sure that end can't be larger or equal to 360. */
		end_angle = end_angle >= 360 ? end_angle - 360 : end_angle;

		/* We then want to check if start is actually on the negative scale, or if it should be
		 * as is the case when the start angle is somehow larger than the end angle. */
		bool use_negative_scale = start_angle >= end_angle || start_angle < 0;

		/* If start is on the negative scale, we want to subtract 360 from it only if it
		 * isn't already negative. */
		start_angle = (use_negative_scale && start_angle >= 0) ? start_angle - 360 : start_angle;

		/* We want to make sure the target's angle is also on the negative scale, unless it is lesser or equal
		 * to the end angle. */
		target_angle = (use_negative_scale && target_angle > end_angle) ? target_angle - 360 : target_angle;

		/* Everything should be set up correctly that would allow this single line to figure out
		 * if the target is within field of view of the viewer. */
		if(target_angle >= start_angle && target_angle <= end_angle)
			return true;

		return false;
	}


	/* Checks to see if the target position with its radius is somehow overlapping our field of view. */
	private static bool isOverlappingFieldOfView(Circle p_shape, ITargetable p_target) {
		float distance = Vector3.Distance(p_shape.position, p_target.position);
		float start_angle = GlobalMovement.findAngleByDirection(p_shape.direction) - (p_shape.degrees / 2);
		float end_angle = start_angle + p_shape.degrees;

		Vector3 p1 = p_shape.position + (GlobalMovement.findDirectionByAngle(p_shape.position, start_angle) * distance);
		Vector3 p2 = p_shape.position + (GlobalMovement.findDirectionByAngle(p_shape.position, end_angle) * distance);

		if(Vector3.Distance(p1, p_target.position) <= p_target.radius ||
			Vector3.Distance(p2, p_target.position) <= p_target.radius) {
			return true;
		}
		
		return false;
	}


	/* Checks to see if two circles intersect eachother. */
	private static bool isIntersecting(Circle p_shape, ITargetable p_target) {
		float distance = Vector3.Distance(p_shape.position, p_target.position);
		if(distance < (p_target.radius + p_shape.radius)) {
			return true;
		}
		
		return false;
	}


	/* Checks to see if the shape's position is literally inside of the target. */
	private static bool isHugging(Shape p_shape, ITargetable p_target) {
		float distance = Vector3.Distance(p_shape.position, p_target.position);
		if(distance < p_target.radius) {
			return true;
		}

		return false;
	}


	/* Makes sure there's nothing that the raycast collides with between the shape's position and the
	 * target's position. */
	private static bool isVisible(Shape p_shape, ITargetable p_target) {
		Vector3 direction = p_target.position - p_shape.position;
		float raycast_distance = Vector3.Distance(p_shape.position, p_target.position);

		//TODO - Test this as certain collision boxes on the target might interfere
		/*if(Physics.Raycast(p_shape.position, direction, raycast_distance)) {
			return false;
		}*/

		return true;
	}


	/* */
	private static bool isLeft(Vector2 a, Vector2 b, Vector2 c) {
		return ((b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x)) > 0;
	}
}
