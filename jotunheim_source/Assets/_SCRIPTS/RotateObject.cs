﻿using UnityEngine;
using System.Collections;

public class RotateObject : MonoBehaviour {

	public bool x, y, z;
	public bool random;
	public Vector2 random_speed;
	public float speed = 1;


	/* If the speed is set to random, it'll assign the speed variable a fixed value from the random range. */
	void Start() {
		speed = random ? Random.Range(random_speed.x, random_speed.y) : speed;
    }


	/* Rotates the object around the given axis. */
	void Update () {
		transform.Rotate(new Vector3(x ? 1 : 0, y ? 1 : 0, z ? 1 : 0) * Time.deltaTime * speed);
	}
}
