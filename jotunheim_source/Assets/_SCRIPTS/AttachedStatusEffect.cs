﻿/***
 * 
 * AttachedStatusEffect.cs
 * 
 * description.......
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using System.Collections;
using StatusEffects;


[ExecuteInEditMode]
public class AttachedStatusEffect : MonoBehaviour {


	[HideInInspector]public string status_effect_ID = "";
	private string _approved_ID = "", _effect_name = "";
	private System.DateTime _updatetime = new System.DateTime();
	private StatusEffect _cached_status_effect = null;



	/* */
	private void Update () {
#if UNITY_EDITOR
		if(!Application.isPlaying)
			DatabaseHandler.update();
#endif

		if(DatabaseHandler.statuseffects_database == null) {
			DatabaseHandler.awake();
			return;
        } else if(DatabaseHandler.statuseffects_database.list_all.Count == 0 || _updatetime.CompareTo(DatabaseHandler.statuseffects_database.updatetime) != 0) {
			_updatetime = DatabaseHandler.statuseffects_database.updatetime;
			_approved_ID = "";
			DatabaseHandler.update();
            return;
		}

		if(status_effect_ID.Length > 0) {
			if(status_effect_ID.CompareTo(_approved_ID) != 0) {
				if((_cached_status_effect = DatabaseHandler.statuseffects_database.find(status_effect_ID)) != null) {
					_approved_ID = status_effect_ID;
					_effect_name = _cached_status_effect.name;
                } else {
					Debug.LogError(string.Format("Effect ID \"{0}\"{2} on object {1} doesn't exist in the effects-library!",
													status_effect_ID,
													transform.name,
													_effect_name.Length > 0 ? string.Format(", known as {0},", _effect_name) : ""));

#if UNITY_EDITOR
					_approved_ID = "";                    
#else
					status_effect_ID = "";
#endif

				}
			}
        }		
	}


	/* */
	public StatusEffect status_effect() {
		return _cached_status_effect;
	}

}
