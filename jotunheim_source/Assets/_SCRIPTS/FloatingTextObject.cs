﻿using UnityEngine;
using System.Collections;

public class FloatingTextObject : MonoBehaviour {

	private float _float_speed = 2f;
	private float _life_time = 2.5f, _birth;

	void Start() {
		_birth = Time.time;
	}

	// Update is called once per frame
	void Update () {
		transform.position += Vector3.up * Time.deltaTime * _float_speed;

		/*transform.LookAt(Camera.main.transform);
		transform.eulerAngles = new Vector3(-transform.eulerAngles.x,
											transform.eulerAngles.y * 0,
											transform.eulerAngles.z * 0);*/

		if(_birth + _life_time < Time.time) {
			Transform current = transform;
			while(current.parent != null)
				current = current.parent;
			Destroy(current.gameObject);
		}
	}
}
