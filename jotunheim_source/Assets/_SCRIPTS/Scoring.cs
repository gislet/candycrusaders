﻿using UnityEngine;
using System.Collections;

public class Scoring : MonoBehaviour {

	private static Scoring _instance;

	public void Awake() {
		_instance = this;
	}


	/* */
	public static void awake() {
		character_deaths = new int[Globals.MAX_PLAYERS];
		character_score = new int[Globals.MAX_PLAYERS];
		character_kills = new int[Globals.MAX_PLAYERS];
		team_score = new int[Globals.MAX_PLAYERS];
		team_kills = new int[Globals.MAX_PLAYERS];
	}


	/* */
	public static void collect(CharacterBase p_character, ScoringMethod p_method = ScoringMethod.None) {

		if(p_character == null) {
			Debug.Log(string.Format("Failed to collect {0}, character was null. (Level: {1})", p_method.ToString(), LevelManager.levelName));
			return;
		}

		if(p_method == LevelManager.getScoringMethod()) {
			processGoalScore(p_character);
		}


		/* */
		if(p_method == ScoringMethod.Death && p_character != null) {
			processDeathScore(p_character);
		}


		/* */
		if(p_method == ScoringMethod.Kill && p_character != null) {
			processKillScore(p_character);
		}
	}


	/* */
	private static void processGoalScore(CharacterBase p_character) {
		if(p_character.team != Team.Teamless) {
			team_score[(int)p_character.team - 1] += 1;
		}

		character_score[p_character.player_ID] += 1;
	}


	/* */
	private static void processKillScore(CharacterBase p_character) {
		if(p_character.team != Team.Teamless) {
			team_kills[(int)p_character.team - 1] += 1;
		}

		character_kills[p_character.player_ID] += 1;
	}


	/* */
	private static void processDeathScore(CharacterBase p_character) {
		character_deaths[p_character.player_ID] += 1;
	}


	/* */
	public static Color winnerColour() {
		Team team = Team.Teamless;

		for(int i = 0; i < team_score.Length; i++) {
			if((team != Team.Teamless && team_score[i] > team_score[(int)(team) - 1]) ||
				(team == Team.Teamless && team_score[i] > 0)) {
				team = (Team)(i + 1);
			}
		}

		return GUIGlobals.getTeamColour(team);
	}


	/* */
	public static string winnerName() {
		string winner = "Nobody";

		Team team = Team.Teamless;

		for(int i = 0; i < team_score.Length; i++) {
			if((team != Team.Teamless && team_score[i] > team_score[(int)(team) - 1]) ||
				(team == Team.Teamless && team_score[i] > 0)) {
				team = (Team)(i + 1);
			}
		}

		winner = "Team " + team.ToString();

		/* If the team is teamless, we know it's an individual victory and not a team victory. */
		if(team == Team.Teamless) {
			int leader = 0;

			for(int i = 1; i < character_score.Length; i++) {
				if(character_score[leader] < character_score[i])
					leader = i;
            }

			winner = PlayerHandler.retrieveConfigByIndex(leader).player_name;
		}

		return winner;
	}


	/* */
	private static bool doesWinnerExist() {
		for(int i = 0; i < team_score.Length; i++) {
			if(team_score[i] >= goal)
				return true;
		}

		for(int i = 0; i < character_score.Length; i++) {
			if(character_score[i] >= goal)
				return true;
		}

		return false;
	}


	public static int[] character_deaths	{ get; private set; }
	public static int[] character_kills		{ get; private set; }
	public static int[] team_kills			{ get; private set; }
	public static int[] character_score		{ get; private set; }
	public static int[] team_score			{ get; private set; }
	public static int goal					{ get { return GameSettings.current.goal; } }
	public static bool goal_reached			{ get { return doesWinnerExist(); } }


	public enum ScoringMethod {
		None, Kill, Goal, Death, Length
	}
}
