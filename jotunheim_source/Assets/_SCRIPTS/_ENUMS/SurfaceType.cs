﻿
public enum SurfaceType {
	UNDEFINED,
	GRASS,
	STONE,
	WOOD,
	DIRT,
}
