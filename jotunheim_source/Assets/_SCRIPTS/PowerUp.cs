﻿using UnityEngine;
using System.Collections;
using System;
/*
[System.Serializable]
public class PowerUp : IComparable {

	//[Space(10, order = 0)]
	[Header("Basics", order = 1)]
	[Space(10, order = 2)]
	public string name;
	public AudioClip sound_clip;
	public bool is_passively_activated;
	public bool passivly_never_ends = false;

	[Range(0, 15)]
	public float duration = 0;
	public ModifierType[] modifiers;


	[Space(10, order = 3)]
	[Header("Speed Modifiers", order = 4)]
	[Space(10, order = 5)]
	[Range(0, 10)]
	public float speed_modifier = 1;

	[Space(10, order = 6)]
	[Header("Heal Modifiers", order = 7)]
	[Space(10, order = 8)]
	[Range(1,5)]
	public int heal_amount;

	[Space(10, order = 9)]
	[Header("Weapon Modifiers", order = 10)]
	[Space(10, order = 11)]
	public WeaponOld weapon_prefab;


	private float _death_time = -1;


	
	public void renew() {
		_death_time = Time.time + duration;
    }


	
	public bool oneshot(CharacterBase p_character) {
		bool one_shot = false;

		if(hasModifier(ModifierType.Weapon)) {
			p_character.pickupWeapon(weapon_prefab);
			one_shot = true;
		}

		if(hasModifier(ModifierType.Health)) {
			p_character.heal(new AttackOrder(), heal_amount);
			one_shot = true;
		}		

		return one_shot;
	}


	
	public bool isAlive() {
		if(!passivly_never_ends) {
			if(_death_time < 0) {
				renew();
			} else if(_death_time < Time.time) {
				return false;
			}
		}

		return true;
	}


	
	public bool hasModifier(ModifierType p_type) {
		foreach(ModifierType m in modifiers) {
			if(m == p_type)
				return true;
		}

		return false;
	}


	
	public int CompareTo(object obj) {
		if(obj.GetType() == typeof(PowerUp)) {
			return name.CompareTo(((PowerUp)obj).name);
		}

		return -1;
	}

	public enum ModifierType {
		Speed,
		Health,
		Weapon
	}
}*/
