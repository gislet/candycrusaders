﻿using UnityEngine;
using System.Collections;
using System;
/*
public class PowerupShell : MonoBehaviour, IInteractable {

	private const float LOCK_TIME = 2f;

	private float _unlock_time;
	private float _interaction_distance = 1;

	[Range(0, 2)]
	public float sound_effect_scale = 1;
	public PowerUp powerup;


	// 
	void Start () {
		if(gameObject.GetComponent<SphereCollider>() != null) {
			_interaction_distance = GetComponent<SphereCollider>().radius;
		}

		_unlock_time = Time.time + LOCK_TIME;
	}


	public bool interact(CharacterBase p_character, bool p_active_interaction) {

		// We want to make sure that a powerup cannot be picked up immediately after spawning. 
		if(_unlock_time > Time.time) {
			return true;

		// If the character who was interacting with this object has moved too far away, we want to notify that character of that. 
		} else if(Vector3.Distance(p_character.transform.position, transform.position) - (p_character.radius * 2) > _interaction_distance) {
			return false;

		// If the powerup is passively picked up, or the player actively tries to, apply it to the player. 
		} else if(powerup.is_passively_activated || p_active_interaction) {
			pickedUp(p_character);			
			return false;

		// If nothing is happening, we want to return true to let the character know that it can still interact with this object. 
		} else {
			return true;
		}
	}


	// The procedure of picking up the PowerUp. 
	private void pickedUp(CharacterBase p_character) {

		// We want to visually show the player who picked up the PowerUp what they picked up. 
		FloatingText.create(p_character.position, powerup.name, (PlayerID)p_character.player_ID);

		// If the PowerUp has a particle effect attached, play it. 
		//TODO

		// Attempt to play any sound that may be attached to the PowerUp. 
		AudioManager.play(powerup.sound_clip, AudioChannel.Effects, sound_effect_scale);

		// We then want to apply it to the character. 
		//p_character.applyPowerUp(powerup);

		// When everything is done, we end with destroying the PowerUp Shell. 
		Destroy(gameObject);
	}


	// 
	public void forciblyApply(CharacterBase p_character) {
		//p_character.applyPowerUp(powerup);
		Destroy(gameObject);
	}
}*/
