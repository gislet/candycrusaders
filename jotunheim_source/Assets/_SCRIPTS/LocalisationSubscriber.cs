﻿/***
 * 
 * LocalisationSubscriber.cs
 * 
 * asdf
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;


public class LocalisationSubscriber : MonoBehaviour {

	[Tooltip("The <Key> is case-sensitive. When localisation is being applied, " +
			"it will attempt to locate the given key and apply the value.")]
	public string key = "";
	public LocalisationDictionary dictionary;


	/* */
	void Start() {
		//Debug.Log("Local: " + gameObject.name + " > " + key);
		applyLocalisation();
    }


	/* */
	private void applyLocalisation() {
		string value = LanguageHandler.getValue(key, dictionary);		

		if(value.Length > 0)
			GetComponent<Text>().text = value;
	}
}
