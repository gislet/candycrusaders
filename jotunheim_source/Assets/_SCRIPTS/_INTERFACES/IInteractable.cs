﻿using UnityEngine;
using System.Collections;

public interface IInteractable {

	bool interact(CharacterBase p_character, bool p_active_interaction);
	
}
