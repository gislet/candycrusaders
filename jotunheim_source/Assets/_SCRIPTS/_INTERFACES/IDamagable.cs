﻿/***
 * 
 * IDamagable.cs
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using System.Collections;

public interface IDamageable {

	void damage(AttackOrder p_order, int p_value);
	void heal(AttackOrder p_order, int p_value);
	bool isFriendly(Team p_team);
	float radius { get; }
	Transform transform { get; }

}
