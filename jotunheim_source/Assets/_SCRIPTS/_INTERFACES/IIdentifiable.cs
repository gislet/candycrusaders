﻿using UnityEngine;
using System.Collections;

public interface IIdentifiable {

	string name { get; set; }
	string ID { get; set; }

}
