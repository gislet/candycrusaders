﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISurface {
	SurfaceType surface_type { get; }
}
