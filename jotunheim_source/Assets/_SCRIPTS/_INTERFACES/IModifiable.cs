﻿/***
 * 
 * IModifiable.cs
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using System.Collections;
using System;


public interface IModifiable {

	Vector2 position_2d { get; }

}
