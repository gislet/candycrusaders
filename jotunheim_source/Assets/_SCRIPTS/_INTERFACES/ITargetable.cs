﻿/***
 * 
 * ITargetable.cs
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using System.Collections;
using System;

public interface ITargetable {
	
	bool isType(Type p_type);
	bool isDestroyed();
	Transform transform { get; }
	float radius { get; }
	Vector3 position { get; }

}
