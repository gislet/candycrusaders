﻿using UnityEngine;
using System.Collections;
using System;

public interface IUISub {

	void run();
	void actions();
	void idle();
	bool fadeToBlack { get; }
	bool isActive { get; }
	bool isIdle { get; }

	//Enum active_stage { get; }
}
