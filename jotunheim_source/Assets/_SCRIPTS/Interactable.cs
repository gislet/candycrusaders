﻿/***
 * 
 * Interactable.cs
 * 
 * asdf
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Interactable : MonoBehaviour {


	/* */
	//public InteractMethod interact_method;
	//public GameObject particle_prefab;
	//private ParticleSystem _particle_system;

	/* */
	public delegate bool ConditionDelegate();
	public event ConditionDelegate condition_event;

	public delegate void InteractDelegate(CharacterBase p_character);
	public event InteractDelegate interact_event;




	/* */
	private void Awake() {
		//Get variable values from some global class
		interactability_distance = 2;
		/*respawn_time = 5f;
		respawn_init_time -= respawn_time;*/

		_subbed_characters = new List<CharacterBase>();
		/*_particle_object = Instantiate(particle_prefab, transform.position, Quaternion.identity) as GameObject;
		_particle_system = _particle_object.GetComponent<ParticleSystem>();*/
    }


	/* */
	private void FixedUpdate() {
		if(condition_event == null || condition_event()) {
			/*if(_particle_system.isStopped)
				_particle_system.Play();*/
			checkCharacterInteractability();
		}
	}


	/* */
	private void checkCharacterInteractability() {
		if(PlayerHandler.characters != null) {
			foreach(CharacterBase character in PlayerHandler.characters) {
				bool is_within_distance = distanceCheck(character.position_2d);
				bool is_subbed = _subbed_characters.Contains(character);

				if(is_subbed && !is_within_distance) {
					_subbed_characters.Remove(character);
					character.interact_event -= interact;
				} else if(!is_subbed && is_within_distance) {
					_subbed_characters.Add(character);
					character.interact_event += interact;
                }
			}
		}		
	}


	/* */
	private void interact(CharacterBase p_object) {

		/* Fail-safe. */
		if(!distanceCheck(p_object.position_2d)) {
			//Debug.LogError(p_character.name + " tried to interact but failed.");
			return;
		}

		/* If this object is interacted with and is ready to be so, we want to make make sure every character who's
		 * subbed to it are unsubbed and other values are reset/renewed. */
		if(condition_event == null || condition_event()) {
			foreach(CharacterBase character in _subbed_characters) {
				character.interact_event -= interact;
			}

			/* Defaulting Values. */
			_subbed_characters = new List<CharacterBase>();

			/* Calling the subbed interaction events. */
			if(interact_event != null)
				interact_event(p_object);
		}
	}


	/* */
	public bool distanceCheck(Vector2 p_position) {
		Vector2 position = new Vector2(transform.position.x, transform.position.z);
		return (Vector2.Distance(p_position, position) <= interactability_distance);
	}



	#region Getters & Setters
	public float interactability_distance { get; private set; }
	private List<CharacterBase> _subbed_characters { get; set; }
	#endregion
}


/* */
public enum InteractMethod {
	OnCollision, Explicit
}
