﻿/***
 * 
 * Cinematic.cs
 * 
 * sss
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using System.Collections;
using System;


public class Cinematic : MonoBehaviour, IComparable {

	public float speed = 15;
	private int _index = 0;
	private Vector3[] _positions;
	private Quaternion[] _rotations;


	/* */
	private void Start() {
		CinemaHandler.add(this);

		_positions = new Vector3[transform.childCount];
		_rotations = new Quaternion[transform.childCount];

		/* */
		for(int i = 0; i < transform.childCount; i++) {
			_positions[i] = transform.GetChild(i).position;
			_rotations[i] = transform.GetChild(i).rotation;
		}

		/* */
		CameraManager.main.transform.position = _positions[0];
		CameraManager.main.transform.rotation = _rotations[0];
	}


	/* */
	public bool run() {
		if(_index + 1 >= _positions.Length) {
            return true;
		} else {
			if(Vector3.Distance(CameraManager.main.transform.position, _positions[_index + 1]) > Vector3.Distance(_positions[_index], _positions[_index + 1]))
				update(_index - 1);
			else if(update(_index))
				_index++;
		}

		return false;
	}


	/* */
	public bool laterun() {
		if(update(_index - 1, true))
			return true;

		return false;
	}


	/* */
	private bool update(int index, bool p_full_distance = false) {
		float distance = Vector3.Distance(_positions[index], _positions[index + 1]);
		float remaining = Vector3.Distance(CameraManager.main.transform.position, _positions[index + 1]);
		float ratio = /*remaining > distance ? 
					0 : */
					(distance - remaining) / distance;

		/*Vector3 from_pos = remaining > distance ? CameraManager.main.transform.position : _positions[index];
		Quaternion from_rot = remaining > distance ? CameraManager.main.transform.rotation : _rotations[index];*/

		//CameraManager.main.transform.position = Vector3.Lerp(CameraManager.main.transform.position, _positions[_index + 1], Time.deltaTime);
		/*CameraManager.main.transform.position = Vector3.Lerp(from_pos, _positions[index + 1], ratio + (Time.deltaTime / speed));
		CameraManager.main.transform.rotation = Quaternion.Lerp(from_rot, _rotations[index + 1], ratio + (Time.deltaTime / speed));*/

		CameraManager.main.transform.position = Vector3.Lerp(_positions[index], _positions[index + 1], ratio + (Time.deltaTime / speed));
		CameraManager.main.transform.rotation = Quaternion.Lerp(_rotations[index], _rotations[index + 1], ratio + (Time.deltaTime / speed));


		/* */
		if(ratio >= .7f && !p_full_distance)
			return true;
		else if(ratio >= 1)
			return true;
		

		return false;
	}


	/* */
	public int CompareTo(object obj) {
		if(obj.GetType() == typeof(Cinematic)) {
			if(transform == ((Cinematic)obj).transform)
				return 0;
		}

		return -1;
	}

}
