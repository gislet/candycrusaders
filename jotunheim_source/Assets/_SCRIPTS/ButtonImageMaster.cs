﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ButtonImageMaster : MonoBehaviour {

	public Sprite[] keyboard_images;
	public Sprite[] controller_images;

	private static ButtonImageMaster _instance;

	// Use this for initialization
	void Awake () {
		_instance = this;
    }

	public static Sprite getImage(ControllerCode p_code) {
		if(_instance.controller_images == null)
			return null;

		if(_instance.controller_images.Length > (int)p_code)
			return _instance.controller_images[(int)p_code];
		else
			return null;
	}

	public static Sprite getImage(KeyboardCode p_code) {
		if(_instance.keyboard_images == null)
			return null;

		if(_instance.keyboard_images.Length > (int)p_code)
			return _instance.keyboard_images[(int)p_code];
		else
			return null;
	}
}

public enum ControllerCode {
	A, B, X, Y, LeftAnalog, RightAnalog
}

public enum KeyboardCode {
	A, B, C, D, E,
	F, G, H, I, J,
	K, L, M, N, O,
	P, Q, R, S, T,
	U, V, W, X, Y,
	Z, Enter, Escape
}
