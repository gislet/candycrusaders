﻿/***
 * 
 * StatusEffectHandler.cs
 * 
 * description.......
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using System.Collections;
using StatusEffects;
using System.Collections.Generic;
using Weapons;


public static class StatusEffectProcessor {


	/* */
	private const int PERCENTAGE = 100;



	/* */
	public static float processMovementEffects(List<StatusEffect> p_list) {
		float total_difference = PERCENTAGE, positive_difference = PERCENTAGE, negative_difference = PERCENTAGE;
		bool daze_immunity = false;

		for(int i = 0; i < p_list.Count; i++) {
			if(p_list[i].mod_movement != null) {
				float diff = 0;

				/* Calculating negative speed modifiers. */
				if(p_list[i].mod_movement.speed < PERCENTAGE || p_list[i].mod_movement.unavoidable) {
					diff = PERCENTAGE - p_list[i].mod_movement.speed;

					negative_difference -= diff;
					total_difference -= diff;
				}

				/* Calculating positive speed modifiers. */
				if(p_list[i].mod_movement.speed > PERCENTAGE || p_list[i].mod_movement.unavoidable) {
					diff = p_list[i].mod_movement.speed - PERCENTAGE;

					positive_difference += diff;

					/* We don't want to consider an effect twice. */
					if(!p_list[i].mod_movement.unavoidable)
						total_difference += diff;
				}

				/* Checking for anything special. */
				if(p_list[i].mod_movement.daze_immunity)
					daze_immunity = true;
			}
		}

		/* We can't obviously move slower than 0. */
		if(negative_difference < 0)
			negative_difference = 0;

		/* Certain status-effects might make you immune to negative speed effects. */
		if(daze_immunity)
			return positive_difference / PERCENTAGE;

		/* Under normal circumstances, we return the total difference of all status-effects. */
		return total_difference / PERCENTAGE;
	}


	/* */
	public static string processWeapons(List<StatusEffect> p_list, int p_current_index, Weapon[] p_current_weapons) {
		for(int i = 0; i < p_list.Count; i++) {

			/* We only care about the first weapon in the list as it has arrived here first. */
			if(p_list[i].mod_weapon != null) {
				if(p_current_weapons[p_current_index] == null || p_current_weapons[p_current_index].ID.CompareTo(p_list[i].mod_weapon.weapon_ID) != 0) {
					return p_list[i].mod_weapon.weapon_ID;
                }
			}
		}

		return "";
	}


	/* */
	public static int processHealing(List<StatusEffect> p_list) {
		int heal_amount = 0;

		for(int i = 0; i < p_list.Count; i++) {
			if(p_list[i].mod_life != null) {
				heal_amount += p_list[i].mod_life.heal(p_list[i].mod_time != null ? (float)p_list[i].mod_time.duration : 0,
														p_list[i].mod_time != null ? (float)p_list[i].mod_time.time_initiated() : Time.time);
			}
		}

		return heal_amount;
	}


	/* */
	public static bool processImmortality(List<StatusEffect> p_list) {
		for(int i = 0; i < p_list.Count; i++) {
			if(p_list[i].mod_life != null) {
				if(p_list[i].mod_life.immortal)
					return true;
			}
		}

		return false;
	}
}
