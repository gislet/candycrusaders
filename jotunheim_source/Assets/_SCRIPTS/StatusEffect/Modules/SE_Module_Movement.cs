﻿/***
 * 
 * Movement.cs
 * 
 * ...
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using System;
using UnityEngine;


namespace StatusEffects {

	/* A collection of modules that can be used with a status-effect. Apply as necessary. */
	namespace Modules {

		public class Movement {
			public int speed = 100;
			public bool daze_immunity = false;
			public bool unavoidable = false;

			public Movement() { }
			public Movement(Movement p_image) {
				speed = p_image.speed;
				daze_immunity = p_image.daze_immunity;
				unavoidable = p_image.unavoidable;
			}
		}

	}
}
