﻿/***
 * 
 * Graphics.cs
 * 
 * ...
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using System;
using UnityEngine;


namespace StatusEffects {

	/* A collection of modules that can be used with a status-effect. Apply as necessary. */
	namespace Modules {

		public class Graphics {

			public string sprite_primary = "", sprite_logo = "";
			private Sprite _sprite_primary, _sprite_logo;
			private const int SPRITE_SIZE = 256;

			public Graphics() { }
			public Graphics(Graphics p_image) {
				sprite_primary = p_image.sprite_primary;
				sprite_logo = p_image.sprite_logo;
			}

			public Sprite primary() {
				if(_sprite_primary == null)
					initialse(ref _sprite_primary, sprite_primary);

				return _sprite_primary;
			}

			public Sprite logo() {
				return _sprite_logo;
			}


			private void initialse(ref Sprite p_sprite, string p_file_path) {
				Texture2D tex = Load<Texture2D>(p_file_path);

				float pixel_per_unit = tex.width / SPRITE_SIZE;

				if(tex != null)
					p_sprite = Sprite.Create(tex, new Rect(0f, 0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100f * pixel_per_unit);
			}


			private T Load<T>(string p_file_path) where T : class {
				T obj = null;

				obj = Resources.Load(p_file_path) as T;

				return obj;
			}
		}

	}
}
