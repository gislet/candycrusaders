﻿/***
 * 
 * Time.cs
 * 
 * ...
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using System;
using UnityEngine;


namespace StatusEffects {

	/* A collection of modules that can be used with a status-effect. Apply as necessary. */
	namespace Modules {

		public class Time {
			public double duration = 0;
			public bool ignore_refresh = false;
			private double _time_initiated = 0;

			public Time() {
				_time_initiated = UnityEngine.Time.time;
			}

			public Time(Time p_image, ref Action p_refresh_event, ref Func<bool> p_condition_event) : this() {
				duration = p_image.duration;
				p_refresh_event += refresh;
				p_condition_event += conditionCheck;
			}


			/* */
			public void refresh() {
				if(ignore_refresh)
					return;

				_time_initiated = UnityEngine.Time.time;
			}


			/* */
			public double time_initiated() {
				return _time_initiated;
			}


			/* */
			public bool conditionCheck() {
				return (duration - (UnityEngine.Time.time - time_initiated())) >= 0;
			}
		}

	}
}