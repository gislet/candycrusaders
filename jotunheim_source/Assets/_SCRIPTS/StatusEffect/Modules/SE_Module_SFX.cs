﻿/***
 * 
 * Sounds.cs
 * 
 * ...
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using System;
using UnityEngine;


namespace StatusEffects {

	/* A collection of modules that can be used with a status-effect. Apply as necessary. */
	namespace Modules {

		public class Sounds {

			public string sfx_pickup = "", sfx_duration = "";
			private AudioClip _pickup_clip, _duration_clip;

			public Sounds() { }
			public Sounds(Sounds p_image) {
				sfx_pickup = p_image.sfx_pickup;
				sfx_duration = p_image.sfx_duration;
			}

			public AudioClip pickup() {
				if(_pickup_clip == null)
					_pickup_clip = Load<AudioClip>(sfx_pickup);

				return _pickup_clip;
			}

			public AudioClip duration() {
				return _duration_clip;
			}


			private T Load<T>(string p_file_path) where T : class {
				T obj = null;

				obj = Resources.Load(p_file_path) as T;

				return obj;
			}
		}

	}
}