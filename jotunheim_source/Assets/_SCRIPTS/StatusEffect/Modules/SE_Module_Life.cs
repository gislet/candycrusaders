﻿/***
 * 
 * Life.cs
 * 
 * ...
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using System;
using UnityEngine;


namespace StatusEffects {

	/* A collection of modules that can be used with a status-effect. Apply as necessary. */
	namespace Modules {

		public class Life {
			public int heal_amount = 0, tick_amount;
			private int _ticks = 0;
			public bool immortal = false, ignore_refresh = false;

			public Life() { }
			public Life(Life p_image, ref Action p_refresh_event) {
				tick_amount = p_image.tick_amount;
				heal_amount = p_image.heal_amount;
				immortal = p_image.immortal;
				p_refresh_event += refresh;
			}


			/* */
			public void refresh() {
				if(ignore_refresh)
					return;

				_ticks = 0;
			}


			/* */
			public int ticks() {
				return _ticks;
			}


			/* */
			public int heal(float p_full_duration = 0, float p_initiated_time = 0) {
				float tick_rate = p_full_duration / (tick_amount);
				float elapsed_time = (UnityEngine.Time.time - p_initiated_time);
				int reached_tick = tick_rate > 0 ? Mathf.FloorToInt(elapsed_time / tick_rate) : tick_amount;

				if(reached_tick > tick_amount)
					reached_tick = tick_amount;

				if(_ticks < reached_tick) {
					int final_heal_amount = 0;

					while(_ticks < reached_tick) {
						final_heal_amount += heal_amount;
						_ticks++;
					}

					return final_heal_amount;
				}

				return 0;
			}
		}

	}
}
