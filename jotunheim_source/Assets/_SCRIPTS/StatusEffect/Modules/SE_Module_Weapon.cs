﻿/***
 * 
 * Weapon.cs
 * 
 * ...
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using System;
using UnityEngine;


namespace StatusEffects {

	/* A collection of modules that can be used with a status-effect. Apply as necessary. */
	namespace Modules {

		public class Weapon {

			public string weapon_ID = "";

			public Weapon() { }
			public Weapon(Weapon p_image) {
				weapon_ID = p_image.weapon_ID;
			}
		}

	}
}