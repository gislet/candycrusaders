﻿/***
 * 
 * StatusEffectHandler.cs
 * 
 * description.......
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using System.Collections;
using StatusEffects;
using StatusEffects.Modules;
using Time = UnityEngine.Time;
using System;


public class StatusEffectSpawner : MonoBehaviour {

	
	/* */
	public float reset_timer = 2;
	public GameObject particle_object, sprite_object;
	public Sprite default_sprite;

	/* */
	private ParticleSystem _particle_system;
	private SpriteUpdater _sprite_updater;

	/* */
	public Action sprite_stop, sprite_start;
	public Action<Sprite> sprite_change;


	
	/* Makes sure all the required function subbing is done. */
	void Awake () {
		time_used -= reset_timer;

		if(GetComponent<Interactable>() != null) {
			Interactable interactable = GetComponent<Interactable>();

			interactable.interact_event += interact;
			interactable.condition_event += conditionCheck;
		} else {
			Debug.LogError(string.Format("{0} at {1} doesn't have anything to sub its functions to. Consider adding an \"Interactable\" script.", 
											transform.name, transform.position));
		}

		_particle_system = particle_object.GetComponent<ParticleSystem>();
		_sprite_updater = sprite_object.GetComponent<SpriteUpdater>();

		_sprite_updater.linkToSpawner(this);

		_particle_system.Stop();
		_particle_system.Clear();
		sprite_stop();
	}


	/* Processes the interaction that was called on the spawner. */
	private void interact(CharacterBase p_character) {
		if(p_character.applyStatusEffect(new StatusEffect(_cached_effect))) {
			FloatingText.create(p_character.position, _cached_effect.name, (PlayerID)p_character.player_ID);
		}

		/* When the interaction is completed, we reset the spawner. */
		reset();		
	}


	/* Checks the conditions to whether or not an interaction can be made. */
	public bool conditionCheck() {
		if(Time.time > (time_used + reset_timer)) {
			if(_cached_effect == null) {
				initialise();
				return false;
			}

			return true;
		}

		return false;
	}


	/* Resets the spawner, defaulting values and stopping components that might be running. */
	private void reset() {
		time_used = Time.time;
		_cached_effect = null;
		_particle_system.Stop();
		particle_object.SetActive(false);
		sprite_stop();
    }


	/* Initialises the spawner, giving it new values and starting components that should be running. */
	private void initialise() {
		StatusEffect effect = null;
		while(effect == null) {
			effect = DatabaseHandler.statuseffects_database.list_all[UnityEngine.Random.Range(0, DatabaseHandler.statuseffects_database.list_all.Count)];
			if(effect.unique)
				effect = null;
		}

		_cached_effect = effect;
		/*Texture2D custom = _cached_effect.mod_gfx != null ? Resources.Load(_cached_effect.mod_gfx.sprite_spawned_dirpath) as Texture2D  : null;
        Sprite sprite = custom != null ? Sprite.Create(custom, new Rect(0f, 0f, custom.width, custom.height), new Vector2(0.5f, 0.5f), 100f) 
														: default_sprite;*/
		Sprite custom_sprite = _cached_effect.mod_gfx != null ? _cached_effect.mod_gfx.primary() : null;
        sprite_change(custom_sprite != null ? custom_sprite : default_sprite);

		particle_object.SetActive(true);
		_particle_system.Play();
		sprite_start();
    }


	#region Getters & Setters
	public float time_used { get; private set; }
	private StatusEffect _cached_effect { get; set; }
	#endregion
}
