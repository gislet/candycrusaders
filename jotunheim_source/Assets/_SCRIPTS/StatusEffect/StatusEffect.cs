﻿/******************************************************************************
*
* StatusEffect.cs
*
* Status-Effects can either be positive, negative, or even both. These effects are usually applied to
* characters or other creatures, but might even be applied to certain objects that are interactable.
* These effects allow someone's or something's behaviour to change dramatically, or maybe just slightly.
*
*
* Written by: Gisle Thorsen (October, 2017)
*
******************************************************************************/


using System;


namespace StatusEffects {

	[System.Serializable]
	public class StatusEffect : IComparable, IIdentifiable {


		/* Miscellanous Variables. */
		public bool unique = false;

		/* Modules. */
		public Modules.Time mod_time = null;
		public Modules.Movement mod_movement = null;
		public Modules.Life mod_life = null;
		public Modules.Graphics mod_gfx = null;
		public Modules.Sounds mod_sfx = null;
		public Modules.Weapon mod_weapon = null;

		/* Delegates. */
		private Action _refresh_event;
		private Func<bool> _condition_event;



		/* Base constructor. */
		public StatusEffect() {
			name = "New Status Effect";

			do {
				ID = Globals.ID_Creator();
			} while(!Globals.ID_Available(ID, DatabaseHandler.statuseffects_database != null ? DatabaseHandler.statuseffects_database.list_all : null));
		}


		/* This constructor allows you to create a clone of a pre-existing status-effect.
		 * This is required when characters or objects have an effect applied to them to
		 * make sure that no variable on the effect has 'spoiled'. */
		public StatusEffect(StatusEffect p_image) {

			/* ID Variables. */
			name = p_image.name;
			ID = p_image.ID;
			unique = p_image.unique;

			/* Modules. */
			mod_time = p_image.mod_time != null ? new Modules.Time(p_image.mod_time, ref _refresh_event, ref _condition_event) : null;
			mod_movement = p_image.mod_movement != null ? new Modules.Movement(p_image.mod_movement) : null;
			mod_life = p_image.mod_life != null ? new Modules.Life(p_image.mod_life, ref _refresh_event) : null;
			mod_gfx = p_image.mod_gfx != null ? new Modules.Graphics(p_image.mod_gfx) : null;
			mod_sfx = p_image.mod_sfx != null ? new Modules.Sounds(p_image.mod_sfx) : null;
			mod_weapon = p_image.mod_weapon != null ? new Modules.Weapon(p_image.mod_weapon) : null;
		}


		/* */
		public void refresh() {
			if(_refresh_event != null)
				_refresh_event();
		}


		/* */
		public bool isActive() {
			if(_condition_event != null)
				return _condition_event();

			return false;
		}


		/* */
		public int CompareTo(object p_obj) {
			if(p_obj.GetType() == typeof(StatusEffect)) {
				if(((StatusEffect)p_obj).ID.Equals(ID)) {
					return 0;
				}
			}

			return -1;
		}


		#region Getters & Setters
		public string name { get; set; }
		public string ID { get; set; }
		#endregion

	}
}


