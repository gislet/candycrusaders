﻿/******************************************************************************
*
* Settings.cs
*
* asdf
*
*
* Written by: Gisle Thorsen (February, 2017)
*
******************************************************************************/


using System;
using System.Collections;
using System.Collections.Generic;
using Configurations.Modules;


namespace Configurations {

	public static class Settings {


		private static Dictionary<string, IConfiguration> _settings = new Dictionary<string, IConfiguration>();



		/* Should be called by only a single main/master script. Will attempt to 
		 * load in and apply the configuration file. */
		public static void awake() {
			if(_initialised)
				return;

			initialise();
		}


		/* Makes sure that the keys dictionary is set up, and that vital variables are set to their correct values. 
		 * Will attempt to load any pre-existing stored config file, or save the pre-initialised version should
		 * there be none. */
		private static void initialise() {
			Globals.prepareSavePath();
			prepareSettings();

			if(System.IO.File.Exists(FILE_PATH)) {
				load();
			} else {
				save();
			}


			_is_dirty = false;
			_initialised = true;
		}


		/* Will attempt to load all configs from a stored file. If there are differences between the stored file and
		 * the pre-initialised config it will attempt to merge them together. Should there be no stored file, it will
		 * call for an initialisation. */
		public static void load() {

			/* Attempts to read from existing config file on harddrive. */
			if(System.IO.File.Exists(FILE_PATH)) {
				Dictionary<string, string> loaded_settings = Globals.load<Dictionary<string, string>>(FILE_PATH);

				/* If there are major differences between the pre-initialised configs and the stored configs,
				 * we will want to save a mixed version of the two so that the config is up to date. */
				_is_dirty = _settings.Count != loaded_settings.Count;

				foreach(string key in _settings.Keys) {
					/* Attempting to locate and update the value of our config keys. */
					if(loaded_settings.ContainsKey(key)) {
						_settings[key].set(loaded_settings[key]);
						/* Should a key not exist, then we know there is a difference between our configs. */
					} else {
						_is_dirty = true;
					}
				}

				//print(loaded_settings); 

				/* If there were differences, we will now save them. */
				if(_is_dirty)
					save();

				/* If config file does not exist, we will run initialise to create a config. */
			} else {
				initialise();
			}
		}


		/* */
		public static string interact(string p_key, ConfigAction p_action, string p_value = "") {
			if(isValidKey(p_key)) {
				IConfiguration config = _settings[p_key];

				switch(p_action) {
				case ConfigAction.Apply:
					config.apply();
					break;
				case ConfigAction.Next:
					config.next();
					break;
				case ConfigAction.Previous:
					config.previous();
					break;
				case ConfigAction.Set:
					config.set(p_value);
					break;
				case ConfigAction.Toggle:
					config.toggle();
					break;
				case ConfigAction.Value:
					return config.value;
				}
			} else {
				UnityEngine.Debug.LogWarning(string.Format("The settings key \"{0}\" doesn't exist.", p_key));
			}

			return null;
		}


		/* */
		public static bool isValidKey(string p_key, string p_object_name = "") {
			if(_settings != null && _settings.Count > 0) {
				if(!_settings.ContainsKey(p_key)) {
					if(p_object_name != null && p_object_name.Length > 0)
						UnityEngine.Debug.LogError(string.Format("The settings key \"{0}\" is not valid for object ({1})", p_key, p_object_name));
					else
						UnityEngine.Debug.LogError(string.Format("The settings key \"{0}\" is not valid.", p_key));

					return false;
				}

				return true;
			}

			return false;
		}


		/* */
		public static void save() {
			UnityEngine.Debug.Log("Saving Configs");

			Dictionary<string, string> settings = new Dictionary<string, string>();
			foreach(string key in _settings.Keys) {
				settings.Add(key, _settings[key].value);
			}

			Globals.save(settings, FILE_PATH, "");
			_is_dirty = false;
		}


		/* */
		public static void print() {
			try {
				foreach(string key in _settings.Keys) {
					UnityEngine.Debug.Log(string.Format("Key: {0}, Value: {1}", key.ToString(), _settings[key].value));
				}
			} catch(Exception e) {
				UnityEngine.Debug.Log(e);
			}
		}

		public static void print(Dictionary<string, string> p_dict) {
			try {
				foreach(string key in p_dict.Keys) {
					UnityEngine.Debug.Log(string.Format("Key: {0}, Value: {1}", key.ToString(), p_dict[key]));
				}
			} catch(Exception e) {
				UnityEngine.Debug.Log(e);
			}
		}


		/* */
		private static void prepareSettings() {
			new Modules.Audio().add(ref _settings);
			//new Modules.Graphics().add(ref _settings);
		}


		#region Getters & Setters
		private static string FILE_PATH {
			get { return @"" + Globals.SPECIAL_FOLDER_FULL + "/config"; }
		}
		private static bool _initialised { get; set; }
		private static bool _is_dirty { get; set; }
		#endregion
	}


	public enum ConfigAction {
		Set, Next, Previous, Apply, Toggle, Value
	}



	namespace Modules {


		public interface IConfiguration {
			void set(string p_value);
			void next();
			void previous();
			void apply();
			void toggle();
			string value { get; }
		}


		public abstract class Setting {

			public void add(ref Dictionary<string, IConfiguration> p_settings) {
				foreach(string key in getSettings.Keys) {
					if(!p_settings.ContainsKey(key)) {
						p_settings.Add(key.ToLower(), getSettings[key]);
					}
				}
			}

			#region Getter & Setter
			protected abstract Dictionary<string, IConfiguration> getSettings { get; }
			#endregion

		}

	}
}
