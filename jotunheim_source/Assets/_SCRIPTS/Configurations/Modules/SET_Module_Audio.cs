﻿/******************************************************************************
*
* SET_Module_Audio.cs
*
* Description...
*
*
* Written by: Gisle Thorsen (July, 2017)
*
******************************************************************************/


using System;
using System.Collections;
using System.Collections.Generic;


namespace Configurations {

	/* */
	namespace Modules {

		/* */
		public class Audio : Setting {


			private Dictionary<string, IConfiguration> _settings = new Dictionary<string, IConfiguration> {
				{ "volume_master", new Volume() },
				{ "volume_music", new Volume() },
				{ "volume_ambient", new Volume() },
				{ "volume_effects", new Volume() },

				{ "volume_master_enabled", new VolumeState() },
				{ "volume_music_enabled", new VolumeState() },
				{ "volume_ambient_enabled", new VolumeState() },
				{ "volume_effects_enabled", new VolumeState() }
			};



			/* */
			protected override Dictionary<string, IConfiguration> getSettings {
				get {
					return _settings;
				}
			}


			/* */
			public class Volume : IConfiguration {
				public float volume = 50;
				private const int MAX = 100, MIN = 0;

				public void set(string p_value) {
					float new_value = float.Parse(p_value);
					volume = new_value < 0 ? MIN : new_value > MAX ? MAX : new_value;
				}

				public void next() {
					volume = volume + 1 > MAX ? MAX : volume + 1;
				}

				public void previous() {
					volume = volume - 1 < MIN ? MIN : volume - 1;
				}

				/* NOT USED */
				public void apply() {}
				public void toggle() {}

				public string value {
					get { return volume.ToString("f0"); }
				}
			}


			/* */
			public class VolumeState : IConfiguration {
				public bool state = true;

				public void set(string p_value) {
					int new_state = int.Parse(p_value);
					state = new_state >= 1 ? true : false;
				}

				/* NOT USED */
				public void next() {}
				public void previous() {}
				public void apply() { }

				public void toggle() {
					state = !state;
				}

				public string value {
					get { return state ? "1" : "0"; }
				}
			}
		}

	}
}
