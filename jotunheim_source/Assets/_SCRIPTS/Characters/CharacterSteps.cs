﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CharacterSteps : MonoBehaviour {

	/* */
	private int _layer_mask;
	private const float SEARCH_DISTANCE = 10;



	/* */
	private void Awake() {
		_layer_mask = ((1 << (int)LayerMasks.COLLISION_BOXES) | (1 << (int)LayerMasks.GROUND));
	}



	/* */
	public void Step() {
		RaycastHit _hit;
		if(Physics.Raycast(transform.position + (Vector3.up), -Vector3.up, out _hit, SEARCH_DISTANCE, _layer_mask)) {
			switch(_hit.transform.gameObject.layer) {
			case ((int)LayerMasks.COLLISION_BOXES):
				objectStep(_hit);
				break;
			case ((int)LayerMasks.GROUND):
				terrainStep(_hit);
				break;
			}
		} else {
			Debug.LogError(string.Format("Stepped on nothing (Time: {0}, Origin: {1}, Final: {2}). ", Time.time, transform.position, transform.position + (-Vector3.up * SEARCH_DISTANCE)));
		}
	}


	/* */
	private void objectStep(RaycastHit p_hit) {
		try {
			AudioManager.play(getStepSound(p_hit.transform.GetComponent<ISurface>().surface_type), AudioChannel.Effects, .3f);
		} catch {
			Debug.LogError(string.Format("Object doesn't have an ISurface component. (Time: {0}, Position: {1}, Name: {2})", Time.time, p_hit.transform.position, p_hit.transform.name));
		}
	}


	/* */
	private void terrainStep(RaycastHit p_hit) {
		try {
			AudioManager.play(getStepSound(TerrainDetector.getTerrainSurfaceType(p_hit.point)), AudioChannel.Effects, .3f);
		} catch(Exception e) {
			Debug.LogError(e);
		}
	}


	/* */
	public abstract AudioClip getStepSound(SurfaceType p_type);
}
