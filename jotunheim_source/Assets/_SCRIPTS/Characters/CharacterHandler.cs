﻿/***
 * 
 * CharacterHandler.cs
 * 
 * This object acts as an entity and is tied to a world object. The object at instantiation
 * adds itself to a list that holds them cached. The entity object acts as a master object
 * to a linked Character object. It calls the Character object every frame to let it act
 * out, and also handles potential death.
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using System.Collections;
using System;
using Customisation;
using System.Collections.Generic;
using System.Linq;


public class CharacterHandler : MonoBehaviour {

	/* Cached sources. */
	public SkinnedMeshRenderer mesh_renderer;
	public HandlesAnimations my_anim_handler;
	public GameObject my_transform_source;
	public GameObject right_hand_source, right_arm_source;
	public GameObject left_hand_source, left_arm_source;
	public GameObject head_source;

	/* Character related variables. */
	public WeaponOld primary, secondary;
	public Health health;
	private CharacterBase _character = null;
	


	/* Called by an outside script that wish to initialise a new version of a given player. */
	public void instantiate(int p_player_ID = Globals.INVALID) {

		/* Retrieve the config belonging to the given player ID. */
		PlayerConfiguration PC = PlayerHandler.retrieveConfigByPlayerID(p_player_ID);
		if(PC == null) {
			return;
		}

		/* Checking to see what type of player we're instantiating. */
		if(PC.controller_ID != Globals.INVALID) {
			/* Playable-Character! */
			_character = new PlayableCharacter();
		} else {
			/* Non-Playable-Character! */
			_character = new NonPlayableCharacter();
		}

		/* We then apply everything necessary to the new character. */
		if(_character != null) {
			PlayerHandler.applyAvailableConfig(_character, p_player_ID);
			_character.initialise(this);
			applyCharacterCustomisation(PC.customisation);
		}
    }


	/* The function is called externally and applies the given settings that was configured 
	 * at an earlier point. */
	private void applyCharacterCustomisation(CustomisationSettings p_settings) {
		mesh_renderer.material = p_settings.skin_material;
	}


	/* Get's called by Monobehaviour every frame. */
	private void Update() {
		if((LevelManager.stage.CompareTo(IngameStage.Play) != 0 && LevelManager.stage.CompareTo(IngameStage.Outro) != 0) || _character == null || requestRespawn()) 
			return;

		_character.actions();
		_character.passive();
	}


	/* Everything the character does that uses physics is called through this method. */
	private void FixedUpdate() {
		if((LevelManager.stage.CompareTo(IngameStage.Play) != 0 && LevelManager.stage.CompareTo(IngameStage.Outro) != 0) || _character == null || _character.flag_dead)
			return;

		_character.move();
	}

	/* */
	private void LateUpdate() {
		if((LevelManager.stage.CompareTo(IngameStage.Play) != 0 && LevelManager.stage.CompareTo(IngameStage.Outro) != 0) || _character == null || requestRespawn())
			return;

	}


	/* If the character is flaged as dead, we'll initiate the appropriate measures
	 * and disconnect the character script from this playerhandler. This frees it up
	 * and allows us to re-pool it and use it later. */
	private bool requestRespawn() {
		bool flag = _character.flag_dead;

		if(flag) {
			StartCoroutine(characterDeath());
		}

		return flag;
	}


	/* A coroutine that handles the character's death. */
	public IEnumerator characterDeath() {

		/* Set's up the respawn timer, caches the ID of the player, and 
		 * disconnects the character object from this handler. */
		float corpse_timer = 5f;
		int player_ID = _character.player_ID;
		my_anim_handler.killPlayer();
		_character = null;

		/* As long as we're in the play-stage, the corpse-time will tick down. Otherwise it'll wait until we're back in play. */
		while(LevelManager.stage.CompareTo(IngameStage.Play) != 0 || Globals.deltaTimeCountdown(ref corpse_timer) >= 0) {
			yield return null;
		}

		/* Add to character pool and requests new character respawn. */
		SpawnHandler.requestSpawn(Master.player_prefab, SpawnHandler.ObjectType.Character, player_ID, PlayerHandler.retrieveConfigByPlayerID(player_ID).team);
		Destroy(gameObject); //Destroys the object until pool is implemented.
	}


	/* */
	/*public void OnTriggerEnter(Collider p_collider) {
		if(_character != null) {
			if(p_collider.GetComponent<IInteractable>() != null) {
				IInteractable interactable_object = p_collider.GetComponent<IInteractable>();

				foreach(IInteractable interactable in _character.interactables) {
					if(interactable == interactable_object)
						return;
				}

				_character.interactables.Add(interactable_object);
			}
		}
	}*/


	public CharacterBase getCharacter { get { return _character; } }
}
