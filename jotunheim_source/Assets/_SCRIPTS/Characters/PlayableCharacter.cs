﻿/***
 * 
 * PlayableCharacter.cs
 * 
 * This object is instantiated to allow for a human to command the character. The overridden and 
 * inherited functions calls other functions that are specific to human interaction.
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using System.Collections;
using Controllers;
using System;
using Use = WeaponOld.Use;

public class PlayableCharacter : Character {


	/* */
	public override void initialise(CharacterHandler p_handler) {
		base.initialise(p_handler);
	}


	/* Called by an external master object. */
	public override void move() {

		/* Finds the input values for the controller associated with this character. */
		Vector2 input_values = new Vector2();
		//input_values.x = Controls.Axis(Operation.Right, controller_ID) > 0 ? Controls.Axis(Operation.Right, controller_ID) : Controls.Axis(Operation.Left, controller_ID) < 0 ? Controls.Axis(Operation.Left, controller_ID) : 0;
		//input_values.y = Controls.Axis(Operation.Up, controller_ID) > 0 ? Controls.Axis(Operation.Up, controller_ID) : Controls.Axis(Operation.Down, controller_ID) < 0 ? Controls.Axis(Operation.Down, controller_ID) : 0;

		input_values.x = Controls.Axis(Operation.Right, controller_ID) + (controller_ID == 0 ? Controls.Axis(Operation.Left, controller_ID) : 0);
		input_values.y = Controls.Axis(Operation.Up, controller_ID) + (controller_ID == 0 ? Controls.Axis(Operation.Down, controller_ID) : 0);

		/* Moves the character if the input_values aren't zero, and updates the animations accordingly. */
		if(GlobalMovement.byRigidbody(transform, rigidbody, speed, input_values, focused_target == null, controller_ID == 0)) {
			flag_isMoving = true;
			Vector2 anim_values = new Vector2(input_values.x < 0 ? input_values.x * -1 : input_values.x,
												input_values.y < 0 ? input_values.y * -1 : input_values.y);
			float anim_speed = anim_values.x > anim_values.y ? anim_values.x : anim_values.y;
			anim_speed *= (speed / Globals.DEFAULT_MOVE_SPEED);
            _anim_handler.VSpeed(anim_speed, false);
		} else {
			flag_isMoving = false;
			_anim_handler.VSpeed(0, false);
		}
    }


	/* Listens for any input to execute actions. */
	public override void actions() {

		if(actions_CD > Time.time)
			return;

		/* Listening for the attack inputs. */
		if(Controls.Down(Operation.Primary, controller_ID) || Controls.Down(Operation.Secondary, controller_ID)) {
			attack(Controls.Down(Operation.Primary, controller_ID));
		}

		/* */
		if(Controls.Down(Operation.Quaternary, controller_ID)) {
			onInteract();
		}
		/*for(int i = 0; i < interactables.Count; i++) {
			if(!interactables[i].interact(this, Controls.Down(Operation.Quaternary, controller_ID))) {
				interactables.RemoveAt(i);
				i--;
			}				
		}*/

		if(Controls.Down(Operation.Tertiary, controller_ID)) {
			swapEquipment();
		}

		flag_focused = Controls.Axis(Operation.Mod, controller_ID) != 0;

		/* If the player hits the start button, the system will attempt to pause the game and
		 * tell the PlayerHandler object who called it. */
		if((controller_ID == 0 && Controls.Down(Operation.Back, controller_ID)) || (controller_ID != 0 && Controls.Down(Operation.Start, controller_ID))) {
			Controls.main_device.set(controller_ID);
			actions_CD = Time.time + Globals.GLOBAL_COOLDOWN;
            LevelManager.stage.change = IngameStage.Pause;	
		}
	}
}
