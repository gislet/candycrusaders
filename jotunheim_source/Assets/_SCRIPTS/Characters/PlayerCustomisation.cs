﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

namespace Customisation {
	public class PlayerCustomisation : MonoBehaviour {

		public Skins[] skin_materials;
		private Material[] skin_materials_only;
		public Hats[] hat_models;
		private GameObject[] hat_models_only;


		public CustomisationSettings[] default_customisations;

		private static PlayerCustomisation _instance;

		private void Awake() {
			_instance = this;

			skin_materials_only = new Material[skin_materials.Length];
			hat_models_only = new GameObject[hat_models.Length];
            for(int i = 0; i < skin_materials.Length || i < hat_models.Length; i++) {
				if(i < skin_materials.Length)
					skin_materials_only[i] = skin_materials[i].material;
				if(i < hat_models.Length)
					hat_models_only[i] = hat_models[i].prefab;
			}
		}


		/* */
		public static CustomisationSettings defaultPlayerCusomisationOption(PlayerID p_player_id) {
			int index = -1;

			switch(p_player_id) {
			case PlayerID.player0:
				index = 0;
				break;
			case PlayerID.player1:
				index = 1;
				break;
			case PlayerID.player2:
				index = 2;
				break;
			case PlayerID.player3:
				index = 3;
				break;
			}

			if(_instance.default_customisations == null || index == -1 || index >= _instance.default_customisations.Length) {
				Debug.LogError(string.Format("Missing default player customisations, or invalid index ({0})!", index));
				return new CustomisationSettings();
			}

			return _instance.default_customisations[index];
		}



		public static T change<T>(T p_object, CustomisationType p_type, bool p_left_to_right) {
			object[] array = null;

			switch(p_type) {
			case CustomisationType.HeadWear:
				array = hat_models_list;
				break;
			case CustomisationType.SkinMaterial:
				array = skin_materials_list;
				break;
			default:
				Debug.LogError(string.Format(	"Unrecognisable CustomisationType delivered. The array is " +
												"therefore unpopulated and the process cannot continue. {0}", p_type));
				return default(T);
			}

			int index = getIndex(array, p_object);

			index += p_left_to_right ? 1 : -1;
			index = index < 0 ? array.Length - 1 : index >= array.Length ? 0 : index;

			return (T)array[index];
		}


		private static int getIndex(object[] p_array, object p_object) {
			for(int i = 0; i < p_array.Length; i++) {
				if(p_array[i] == p_object) {
					return i;
				}
			}

			return Globals.INVALID;
		}


		public static string getName(object p_object, CustomisationType p_type) {
			string name = "";

			switch(p_type) {
			case CustomisationType.HeadWear:
				name = _instance.hat_models[getIndex(hat_models_list, p_object)].name;
				break;
			case CustomisationType.SkinMaterial:
				name = _instance.skin_materials[getIndex(skin_materials_list, p_object)].name;
				break;
			}

			return name;
		}


		private static Material[] skin_materials_list {
			get {
				return _instance.skin_materials_only;
			}
		}

		private static GameObject[] hat_models_list {
			get {
				return _instance.hat_models_only;
			}
		}
	}

	public enum CustomisationType {
		SkinMaterial, HeadWear
	}

	[System.Serializable]
	public class CustomisationSettings {
		public Material skin_material;
		public GameObject hat_model;
	}

	[System.Serializable]
	public struct Skins {
		public string name;
		public Material material;
	}

	[System.Serializable]
	public struct Hats {
		public string name;
		public GameObject prefab;
	}
}
