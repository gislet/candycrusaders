﻿using UnityEngine;
using System.Collections;
using System;

/* This class is assigned to an object that can take damage and will monitor its health
 * on its behalf. */
public class Health : MonoBehaviour, IDamageable {

	private int _value, _max_value;


	/* */
	public void initialise(int p_max_value = 1, int p_health_value = Globals.INVALID) {
		_value = p_health_value == Globals.INVALID ? p_max_value : p_health_value;
		_max_value = p_max_value;
	}


	/* Returns a bool of whether or not the object's health has depleted or not. */
	public bool isDepleted {
		get { return _value <= 0; }
	}


	/* This method makes sure that the health value gets reduced. */
	public void damage(AttackOrder p_order, int p_value = 1) {
		if(p_value < 0)
			p_value *= -1;

		/* We don't want to reduce the value below 0. */
		_value = _value - p_value < 0 ? 0 : _value - p_value;
	}


	/* This method makes sure that the health value gets replenished. */
	public void heal(AttackOrder p_order, int p_value = 1) {
		if(p_value < 0)
			p_value *= -1;

		/* If we have an assigned max value, we want to make sure that our health cannot be 
		 * at a higher level than it. */
		_value = (_value + p_value > _max_value) && (_max_value > 0) ? _max_value : _value + p_value;
	}


	/* */
	public void modify(int p_value) {
		_value += p_value;

		if(_value < 0)
			_value = 0;

		if(_value > _max_value)
			_value = _max_value;
	}


	/* */
	public bool isFriendly(Team p_team) {
		return false;
	}


	#region Getters & Setters
	public int value { get { return _value; } }
	public float radius {
		get {
			throw new NotImplementedException();
		}
	}
	#endregion
}
