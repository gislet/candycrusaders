﻿/******************************************************************************
*
* HandlesAnimations.cs
*
* asdf
*
*
* Written by: Gisle Thorsen (October, 2017)
*
******************************************************************************/


using UnityEngine;
using System.Collections;


public class HandlesAnimations : MonoBehaviour {

	private Animator _animator;

	void Awake() {
		_animator = GetComponent<Animator>();
	}

	public void VSpeed(float p_value, bool p_ranged = false) {
		_animator.SetFloat("VSpeed", p_value);

		_animator.SetBool("Ranged", p_ranged);
	}

	public void Rolling() {
		StartCoroutine(rollCoroutine());
	}

	public void Attacking(bool p_melee, bool p_ranged) {
		StartCoroutine(attackCoroutine(p_melee, p_ranged));
		//Invoke("StopJumping", 1f);
	}

	public void killPlayer() {
		_animator.applyRootMotion = true;
		_animator.SetBool("isDead", true);
		Invoke("StopDying", .1f);
		_animator.SetFloat("VSpeed", 0);
	}

	private void StopDying() {
		_animator.SetBool("isDead", false);
	}


	/*  */
	/*public void testStep() {
		int layer_mask = ((1 << (int)LayerMasks.COLLISION_BOXES) | (1 << (int)LayerMasks.GROUND));

		RaycastHit _hit;
		if(Physics.Raycast(transform.position + (Vector3.up), -Vector3.up, out _hit, 10, layer_mask)) {
			switch(_hit.transform.gameObject.layer) {
			case ((int)LayerMasks.COLLISION_BOXES):
				Debug.Log("Stepped on a collision box. " + Time.time);
				break;
			case ((int)LayerMasks.GROUND):
				Debug.Log("Stepped on the terrain. " + Time.time);
				break;
			}
		} else {
			Debug.LogError(string.Format("Stepped on nothing (Time: {0}, Origin: {1}, Final: {2}). ", Time.time, transform.position, transform.position + (-Vector3.up * 10)));
		}
	}*/

	IEnumerator rollCoroutine() {
		_animator.SetLayerWeight(0, 1f);
		_animator.SetLayerWeight(1, 0f);
		_animator.SetBool("Rolling", true);

		string name = "Quick_Roll";

		do {
			yield return null;
		}
		while(_animator.GetCurrentAnimatorStateInfo(0).IsName(name) || _animator.IsInTransition(0));

		_animator.SetBool("Rolling", false);
	}

	IEnumerator attackCoroutine(bool p_melee, bool p_ranged) {
		_animator.SetLayerWeight(0, 0f);
		_animator.SetLayerWeight(1, 1f);
		_animator.SetBool("Attacking", true);
		_animator.SetBool("Melee", p_melee);
		_animator.SetBool("Ranged", p_ranged);

		//string attack = "Attacking";
		string name = "standing_melee_combo_attack_ver._2";

		//print(_animator.GetNextAnimatorStateInfo(1).tagHash + ", " + _animator.GetCurrentAnimatorStateInfo(1).tagHash + " - " + attack.GetHashCode());

		//while(_animator.IsInTransition(1) && _animator.GetCurrentAnimatorStateInfo(1).tagHash == "Lol".GetHashCode() || _animator.GetNextAnimatorStateInfo(1).tagHash == attack.GetHashCode()) {

		AnimatorStateInfo currInfo = animator.GetCurrentAnimatorStateInfo(1);

		do {
			yield return null;
		}
		while(_animator.GetCurrentAnimatorStateInfo(1).IsName(name) || _animator.IsInTransition(1));

		_animator.SetBool("Attacking", false);
	}

	public bool isRolling {
		get {
			if(_animator != null)
				return _animator.GetBool("Rolling");
			else
				return false;
		}
	}

	public bool isAttacking {
		get {
			if(_animator != null)
				return _animator.GetBool("Attacking");
			else
				return false;
		}
	}

	public AnimatorStateInfo GetCurrentAnimatorStateInfo(int p_value) {
		return _animator.GetCurrentAnimatorStateInfo(p_value);
	}

	public Animator animator {
		get { return _animator; }
	}

	private void StopJumping() {
		_animator.SetBool("Attacking", false);
	}
}
