﻿/***
 * 
 * Character.cs
 * 
 * This object cannot be instantiated, and inherits functions and variables. The Character
 * handles other more complex functions and variables.
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using System.Collections;
using System;
using Use = WeaponOld.Use;
using System.Collections.Generic;
using StatusEffects;
using Weapons;


public abstract class Character : CharacterBase {

	/* Explaination found in both Characterbase and inherited classes for flavour implementaion. */
	public override void initialise(CharacterHandler p_handler) {
		rigidbody = p_handler.transform.GetComponent<Rigidbody>();
		transform = p_handler.my_transform_source.transform;
		radius = p_handler.my_transform_source.GetComponent<CapsuleCollider>().radius;
		_anim_handler = p_handler.my_anim_handler;

		_health = p_handler.health;
		_health.initialise(Globals.MAX_LIVES);
		_primary = p_handler.primary;
		_secondary = p_handler.secondary;


		bodyparts = new HumanoidLimbs(p_handler.left_hand_source.transform, p_handler.left_arm_source.transform,
										p_handler.right_hand_source.transform, p_handler.right_arm_source.transform);

		_weapons = new Weapon[2];
		weapon_index = 0;
		assignWeapon(new Weapon(this, DatabaseHandler.weapons_database.list_all[0]));

		PlayerHandler.characters.Add(this);
		//interactables = new List<IInteractable>();
		status_effects = new List<StatusEffect>();

		if(PlayerHandler.whoDestroyedThisID.ContainsKey(player_ID))
			PlayerHandler.whoDestroyedThisID.Remove(player_ID);

		//_weapon.equip(bodyparts);
	}


	public override abstract void move();
	public override abstract void actions();


	/* */
	public override void passive() {

		/* If we are focusing a target, check to see if the target is still valid. */
		if(focused_target != null) {
			if(!flag_focused || focused_target.isDestroyed())
				focused_target = null;
		}

		/* If we are attempting to focus, try to find a valid target. */
		if(flag_focused)
			focusTarget();

		/* */
		_health.modify(StatusEffectProcessor.processHealing(status_effects));

		/* */
		string ID = StatusEffectProcessor.processWeapons(status_effects, weapon_index, _weapons);
		if(ID.Length > 0) {
			Weapon weapon = DatabaseHandler.weapons_database.find(ID);

			if(weapon != null) {
				assignWeapon(weapon);
			}
		}


		/* Iterate through our list of current status effects. Remove effects that have expired or
			* that are no longer meeting the required conditions to exist. */
		for(int i = 0; i < status_effects.Count; i++) {
			if(!status_effects[i].isActive()) {
				status_effects.RemoveAt(i);
				i--;
			}
		}
	}


	/* Looking for a valid target. */
	private void acquireTarget() {
		List<ITargetable> targets = new List<ITargetable>();
		float dir_angle = GlobalMovement.findAngleByDirection(GlobalMovement.findDirectionByTransform(transform));
		float angle_half_size = 80;

		/* We go through the list of players alive. */
		foreach(CharacterBase target in PlayerHandler.characters) {

			/* Making sure the current target is not us. */
			if(target.transform != transform && !target.isFriendly(team)) {

				/* Making sure the target is within a certain field of view. */
				if(GlobalMovement.isWithinFieldOfView(dir_angle - (angle_half_size), dir_angle + (angle_half_size), GlobalMovement.findAngleByDirection((target.position - position)))) {
					targets.Add(target);
				}
			}
		}

		/* Making sure the targets are actually in line of sight and not behind some wall (maybe exceptions for really close targets?). */
		foreach(ITargetable target in targets) {
			//if(!Physics.Linecast(target.transform.position, position)) {

				/* If there are several eligible targets, make sure we pick the one nearest us. */
				if((focused_target != null && (Vector3.Distance(target.transform.position, position) < Vector3.Distance(target.transform.position, position)))
					|| focused_target == null) {
					focused_target = target;
                }
			//}
		}
	}


	/* Attempt to focus a valid target. */
	protected void focusTarget() {

		/* If we got no current target, look for one. */
		if(focused_target == null) {
			acquireTarget();
			return;
		}

		GlobalMovement.lookAtTransform(transform, focused_target.transform);

	}

	

	protected void attack(bool p_main) {

		if(_weapon == null)
			return;

		if(!_weapon.attack(p_main))
			return;

		onAttack();

		_anim_handler.Attacking(true, false);
	}

	

	/* Called to handle any attack performed by this character. */
	//protected void attack(Use p_use) {

		/* If we're not flagged for attacking we'll attack with our current weapon. */
		/*if(flag_attacking)
			return;


		if(active_slot == Use.Primary)
			_primary_attack_ID = CombatHandler.initiateAttack(current_weapon.use(this, p_use));
		else
			_secondary_attack_ID = CombatHandler.initiateAttack(current_weapon.use(this, p_use));


		onAttack();*/


		//TODO: Implement a better way to decide what animation to play. Currently only plays a swing animation.
		/*if(flag_attacking) {
			if(current_weapon.primary_attack.routine == WeaponOld.UseRoutine.Shoot)
				_anim_handler.Attacking(false, true);
			else*/
		//_anim_handler.Attacking(true, false);
		//}			
	//}
}
