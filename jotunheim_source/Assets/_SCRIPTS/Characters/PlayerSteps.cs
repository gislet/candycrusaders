﻿using UnityEngine;

public class PlayerSteps : CharacterSteps {

	/* Library of step-sounds. */
	[SerializeField]
	private AudioClip[] _grass_sounds;
	[SerializeField]
	private AudioClip[] _dirt_sounds;
	[SerializeField]
	private AudioClip[] _wood_sounds;
	[SerializeField]
	private AudioClip[] _stone_sounds;



	/* */
	public override AudioClip getStepSound(SurfaceType p_type) {
		switch(p_type) {
		case SurfaceType.DIRT:
			return _dirt_sounds[Random.Range(0, _dirt_sounds.Length)];
		case SurfaceType.STONE:
			return _stone_sounds[Random.Range(0, _stone_sounds.Length)];
		case SurfaceType.WOOD:
			return _wood_sounds[Random.Range(0, _wood_sounds.Length)];
		case SurfaceType.GRASS:
		default:
			return _grass_sounds[Random.Range(0, _grass_sounds.Length)];
		}
	}
}
