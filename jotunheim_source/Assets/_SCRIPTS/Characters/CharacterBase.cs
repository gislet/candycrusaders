﻿/***
 * 
 * CharacterBase.cs
 * 
 * This object cannot be instantiated, and holds any characters most basic functions
 * and variables.
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using System.Collections;
using Customisation;
using System;
using System.Collections.Generic;
using StatusEffects;
using InteractDelegate = Interactable.InteractDelegate;
using Weapons;


public abstract class CharacterBase : IDamageable, ITargetable, IModifiable {

	/* */
	public string name;
	public int player_ID, controller_ID;
	public CustomisationSettings customisation_settings;

	/* */
	protected HandlesAnimations _anim_handler;
	protected SkinnedMeshRenderer _mesh_renderer;
	protected Health _health;
	protected WeaponOld _primary, _secondary;
	protected WeaponOld.Use _current_weapon;

	/* */
	protected int _primary_attack_ID;
	protected int _secondary_attack_ID;

	/* */
	public event InteractDelegate interact_event;
	public event Action attack_event;



	/* This method is called at the very start and makes sure that the character has set up everything
	 * that is essential. */
	public abstract void initialise(CharacterHandler p_handler);


	/* This method is called per-physics-frame and listens for movement commands. */
	public abstract void move();


	/* This method is called per-frame and listens for any special action commands. */
	public abstract void actions();

	/* */
	public abstract void passive();


	/* */
	public void damage(AttackOrder p_order, int p_value) {
		_health.damage(p_order, p_value);
		if(_health.isDepleted) {
			if(StatusEffectProcessor.processImmortality(status_effects)) {
				_health.modify(1);
				return;
			}

			if(p_order.character != null) {
				PlayerHandler.whoDestroyedThisID.Add(player_ID, p_order.character.name);
				Scoring.collect(p_order.character, Scoring.ScoringMethod.Kill);
			}
			Scoring.collect(this, Scoring.ScoringMethod.Death);
		}
	}


	/* */
	public void heal(AttackOrder p_order, int p_value) {
		_health.heal(p_order, p_value);
	}


	/* */
	public bool isFriendly(Team p_team) {
		if(team == Team.Teamless || team != p_team)
			return false;

		return true;
	}


	/* An interface implementation to check if the character is of the same type
	 * as the given parameter. */
	public bool isType(Type p_type) {
		return (p_type == this.GetType());
	}


	/* A check to see if the character is flagged as dead. */
	public bool isDestroyed() {
		return flag_dead;
	}


	/* */
	protected void onInteract() {
		if(can_interact) {
			interact_event(this);
        }
	}


	/* */
	protected void onAttack() {
		if(attack_event != null)
			attack_event();
	}


	/* Allows object to apply status-effects to our player. 
	 * NB! The application process might have to be revised to introduce security measures for online-play. */
	public bool applyStatusEffect(StatusEffect p_effect) {

		/* Looking through our existing list of status-effects for another effect with
		 * the same name.*/
		for(int i = 0; i < status_effects.Count; i++) {

			/* If do we have one already, we either want to refresh our old status-effect, stack
			 * our status effect or ignore it. */
			if(status_effects[i].ID.CompareTo(p_effect.ID) == 0) {
				status_effects[i].refresh();
				//TODO implement stacking ability?
				return false;
			}
		}

		if(p_effect.mod_sfx != null) {
			AudioManager.play(p_effect.mod_sfx.pickup(), AudioChannel.Effects);
		}

		/* Adding the effect to our list. */
		status_effects.Add(p_effect);
		return true;      
	}


	/* Makes sure the speed of this character takes any modifiers into consideration. */
	public float speed {
		get {
			float speed = Globals.DEFAULT_MOVE_SPEED;

			speed *= flag_focused ? .5f : 1;
			speed *= StatusEffectProcessor.processMovementEffects(status_effects);

			return speed;
		}
	}


	/* */
	public void assignWeapon(Weapon p_weapon, int p_weapon_slot = -1) {
		if(p_weapon == null)
			throw new ArgumentNullException("Weapon parameter cannot be null!");

		if(p_weapon_slot < 0 || p_weapon_slot >= _weapons.Length)
			p_weapon_slot = -1;
		
		_weapons[p_weapon_slot != -1 ? p_weapon_slot : weapon_index] = new Weapon(this, p_weapon);

		/* We don't want to equip this assigned weapon if it was assigned to a slot we're not currently 
		 * focused on. */
		if(p_weapon_slot == -1 || p_weapon_slot == weapon_index)
			_weapon.equip(bodyparts);

		//throw new NotImplementedException("Moving away from this weapon assignment.");
		//equipWeaponSlot();
	}


	/* */
	protected void swapEquipment() {
		if(equipment_swap_CD > Time.time && flag_attacking)
			return;

		equipment_swap_CD = Time.time + Globals.GLOBAL_COOLDOWN;


		weapon_index = weapon_index + 1 >= _weapons.Length ? 0 : weapon_index + 1;

		/* We don't want to equip this weapon if the slot is empty. */
		if(_weapon != null)
			_weapon.equip(bodyparts);
		else
			bodyparts.clearBodyParts();



		/*_current_weapon = (Use)Globals.changeEnumIndex((int)_current_weapon, (int)Use.Secondary + 1, true);
		equipWeaponSlot();*/
	}


	/* */
	public void pickupWeapon(WeaponOld p_new_weapon) {
		if(p_new_weapon == null)
			return;

		if(active_slot == WeaponOld.Use.Primary)
			_primary = p_new_weapon;
		else
			_secondary = p_new_weapon;

		//throw new NotImplementedException("Moving away from this weapon pickup.");
		//equipWeaponSlot();
	}


	/* */
	public float cooldownProgress(int p_weapon_slot = 0) {
		if(p_weapon_slot < 0 || p_weapon_slot >= _weapons.Length)
			p_weapon_slot = 0;

		if(_weapons[p_weapon_slot] == null)
			return 0;

		return _weapons[p_weapon_slot].cooldownProgress();
    }


	/* */
	public float cooldownRemaining(int p_weapon_slot) {
		if(p_weapon_slot < 0 || p_weapon_slot >= _weapons.Length)
			p_weapon_slot = 0;

		if(_weapons[p_weapon_slot] == null)
			return 0;

		return _weapons[p_weapon_slot].cooldownTimeRemaining();
	}


	/* */
	public Sprite weaponLogo(int p_slot_index) {
		if(p_slot_index < 0 || p_slot_index >= _weapons.Length)
			p_slot_index = 0;

		return _weapons[p_slot_index] != null ? _weapons[p_slot_index].logo() : null;
    }


	#region Getters & Setters

	public int weapon_index { get; protected set; }
	protected Weapon[] _weapons { get; set; }
	protected Weapon _weapon { get { return _weapons[weapon_index]; } }


	public bool flag_isMoving {						get;																					protected set; }
	public bool flag_attacking {					get { return CombatHandler.isIDActive(_primary_attack_ID) || CombatHandler.isIDActive(_secondary_attack_ID); } }
	public float primary_CD_percentage {			get { return CombatHandler.ID_cooldown_percentage(_primary_attack_ID); } }
	public float secondary_CD_percentage {			get { return CombatHandler.ID_cooldown_percentage(_secondary_attack_ID); } }
	public bool flag_dead {							get { return _health != null ? _health.isDepleted : false; } }
	public bool flag_sprinting {					get;																					protected set; }
	public bool flag_focused {						get;																					protected set; }
	public bool can_interact {						get { return (interact_event != null); } }
	public WeaponOld.Use active_slot {					get { return _current_weapon; } }
	public WeaponOld current_weapon {					get { return active_slot == WeaponOld.Use.Primary ? weapon_primary : weapon_secondary; } }
	public WeaponOld weapon_primary {					get { return _primary; } }
	public WeaponOld weapon_secondary {				get { return _secondary; } }
	public Rigidbody rigidbody {					get;																					protected set; }
	public Vector3 position {						get { return transform.position; }														set { transform.position = value; } }
	public Vector2 position_2d {					get { return new Vector2(transform.position.x, transform.position.z); } }
	public Quaternion rotation {					get { return transform.rotation; } }
	public Team team {								get;																					set; }
	public Transform transform {					get;																					protected set; }

	public HumanoidLimbs bodyparts {				get;																					protected set; }

	public float radius {							get;																					protected set; }
	protected List<StatusEffect> status_effects {	get;																					set; }
	protected float equipment_swap_CD {				get;																					set; }
	protected float actions_CD {					get;																					set; }
	protected ITargetable focused_target {			get;																					set; }
	#endregion
}



public class HumanoidLimbs : BodyParts {


	public HumanoidLimbs(Transform p_LH, Transform p_LA, Transform p_RH, Transform p_RA) {
		left_hand = p_LH;
		left_arm = p_LA;
		right_arm = p_RA;
		right_hand = p_RH;
    }


	public override void clearBodyParts() {
		for(int i = 0; i < 4; i++) {
			Transform t = null;

			switch(i) {
			case 0:
				t = left_arm;
				break;
			case 1:
				t = left_hand;
				break;
			case 2:
				t = right_arm;
				break;
			case 3:
				t = right_hand;
				break;
			}

			for(int g = t.childCount; g > 0; g--) {
				Master.destroyObject(t.GetChild(g - 1).gameObject);
			}
		}
	}

	public Transform left_hand { get; protected set; }
	public Transform left_arm { get; protected set; }
	public Transform right_hand { get; protected set; }
	public Transform right_arm { get; protected set; }
}


public abstract class BodyParts {

	public abstract void clearBodyParts();

}


public enum LimbPosition {
	Arm, Hand
}
