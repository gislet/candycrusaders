﻿/***
 * 
 * PlayerHandler.cs
 * 
 * This is a static object that when called by an external master object sets up
 * an array that can hold all possible player configurations. The object can create
 * new players and remove them. It also holds a list of them to be accessed by outside
 * sources, and a way to find how many active players there are.
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Customisation;


/* Handles player creation and player config distrubution. */
public static class PlayerHandler {

	/* Cached characters and configurations. */
	public static List<CharacterBase> _characters;
	private static List<PlayerConfiguration> _player_configs;



	/* Called by some master script, making sure this script is up and running. */
	public static void awake(bool p_reset = false) {

		/* We want the list of characters to always be reset when this function gets called. */
		_characters = new List<CharacterBase>();

		/* We don't want to add more empty player configs unless there are none. */
		if(p_reset || _player_configs == null || _player_configs.Count < Globals.MAX_PLAYERS) {
			_player_configs = new List<PlayerConfiguration>();
		} else {
			return;
		}

		/* Adding in some empty player config shells to be used later. */
		for(int i = 0; i < Globals.MAX_PLAYERS; i++) {
			_player_configs.Add(new PlayerConfiguration());
		}

		whoDestroyedThisID = new Dictionary<int, string>();
	}


	/* This function simply calls awake, but exists to make things easier. */
	public static void reset() {
		awake(true);
	}


	/* If the received controller ID is Invalid, we assume that the created player is 
	 * going to be an AI. Unless the online bool is true, in which case the controller ID
	 * represents its online ownership. */
	public static void createPlayer(int p_controller_ID = Globals.INVALID, bool p_online = false) {

		/* Before anything, we want to make sure the controller ID isn't already
		 * assigned to a player config. */
		if(p_controller_ID != Globals.INVALID && retrieveConfigByControllerID(p_controller_ID) != null) {
			return;
		}

		for(int i = 0; i < _player_configs.Count; i++) {

			/* We won't assign a player to a config unless there's an available
			 * config. */
			if(_player_configs[i].available()) {

				/* The position in the list is the player's ID. */
				_player_configs[i].assignOwnership(i, p_controller_ID, p_online);
				_player_configs[i].customisation = PlayerCustomisation.defaultPlayerCusomisationOption((PlayerID)i);
				break;
			}
		}
	}


	/* Making a player config available to be repopulated. */
	public static void removePlayerByIndex(int p_index) {
		if(!_player_configs[p_index].available()) {
			_player_configs[p_index] = new PlayerConfiguration();
        }
	}


	/* Removes an existing configuration identified by its controller ID,
	 * allowing it to be recreated. */
	public static void removePlayerByControllerID(int p_controller_ID) {
		PlayerConfiguration PC = retrieveConfigByControllerID(p_controller_ID);
		removePlayerByIndex(PC.player_ID);
    }


	/* Looks up the player config for the given player ID, and calls the given method with the relevant information. */
	public static void applyAvailableConfig(this CharacterBase p_character, int p_player_ID) {
		if(p_character == null)
			return;

		PlayerConfiguration PC = retrieveConfigByPlayerID(p_player_ID);
		if(PC != null) {
			p_character.player_ID = PC.player_ID;
			p_character.controller_ID = PC.controller_ID;
			p_character.name = PC.player_name;
			p_character.team = PC.team;
		}
	}


	/* Attempts to find an existing configuration based on the Player ID. Function
	 * might be redundant. */
	public static PlayerConfiguration retrieveConfigByPlayerID(int p_player_ID) {

		if(p_player_ID != Globals.INVALID) {
			for(int i = 0; i < _player_configs.Count; i++) {
				PlayerConfiguration PC = _player_configs[i];

				if(PC.player_ID == p_player_ID) {
					return PC;
				}
			}
		}

		return null;
	}


	/* Attempts to find an existing configuration based on the Controller ID. */
	public static PlayerConfiguration retrieveConfigByControllerID(int p_controller_ID) {
		for(int i = 0; i < _player_configs.Count; i++) {
			PlayerConfiguration PC = _player_configs[i];

			if(PC.controller_ID == p_controller_ID) {
				return PC.player_ID != Globals.INVALID ? PC : null;
			}
		}

		return null;
	}


	/* Attempts to return an existing configuration based on its index position. */
	public static PlayerConfiguration retrieveConfigByIndex(int p_index) {
		if(_player_configs == null ? true : p_index < 0 || p_index >= _player_configs.Count)
			return null;

		return _player_configs[p_index].player_ID != Globals.INVALID ? _player_configs[p_index] : null;
	}


	/* Finds all active player configs. */
	public static int numValidConfigs() {
		int num = 0;

		for(int i = 0; i < Globals.MAX_PLAYERS; i++) {
			PlayerConfiguration PC = retrieveConfigByIndex(i);

			if(PC != null)
				num++;
		}

		return num;
	}


	/* Finds all active player configs of local human players only. */
	public static int numValidHumanConfigs() {
		int num = 0;

		for(int i = 0; i < Globals.MAX_PLAYERS; i++) {
			PlayerConfiguration PC = retrieveConfigByIndex(i);

			if(PC != null ? PC.controller_ID >= 0 : false) {
				num++;
			}
		}

		return num;
	}


	/* Returns a list of all alive characters if possible. */
	public static List<CharacterBase> characters {
		get {
			if(_characters != null) {
				for(int i = 0; i < _characters.Count; i++) {
					CharacterBase character = _characters[i];

					/* If a character is dead we want to remove it from the list. */
					if(character.flag_dead)
						_characters.RemoveAt(i--);
				}
			}

			return _characters;
		}
	}


	/* */
	public static bool isCharacterDead(int i) {
		foreach(CharacterBase CB in characters) {
			if(CB.player_ID == i) {
				return CB.flag_dead;
			}
		}

		return true;
	}


	/* */
	public static CharacterBase findAliveCharacter(int i) {
		foreach(CharacterBase CB in characters) {
			if(CB.player_ID == i) {
				return CB;
			}
		}

		return null;
	}


	/* */
	public static Dictionary<int, string> whoDestroyedThisID { get; set; }


	/* */
	/*public static int paused_Controller_ID {
		get; set;
	}*/
}


/* Stores all essential player configuration, such as customisables, names, IDs 
 * and more. */
public class PlayerConfiguration {

	/* Essentials. */
	//private bool _online; //?????????


	public PlayerConfiguration() {
		team = Team.Teamless;
		player_ID = Globals.INVALID;
		controller_ID = Globals.INVALID;
		player_name = "Invalid";
		//_online = false;
	}


	/* Checks to see if the particular config is available to be populated. */
	public bool available() {
		if(player_ID == Globals.INVALID) {
			return true;
		}

		return false;
	}


	/* Assigns the given ID to the player config, claiming ownership. */
	public void assignOwnership(int p_player_ID, int p_controller_ID, bool p_online) {
		if(!available())
			return;

		player_ID = p_player_ID;
		controller_ID = p_controller_ID;
		//_online = p_online;

		if(player_name == "Invalid") {
			player_name = "Player " + (player_ID + 1);
        }
    }


	#region Getters & Setters
	public int player_ID						{ get; private set; }
	public int controller_ID					{ get; private set; }
	public string player_name					{ get; set; }
	public Team team						{ get; set; }
	public CustomisationSettings customisation	{ get; set; }
	#endregion
}


public enum PlayerID {
	unspecified = -1,
	player0,
	player1,
	player2,
	player3
}
