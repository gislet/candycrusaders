﻿/***
 * 
 * NonPlayableCharacter.cs
 * 
 * This object is allowed to be instantiated, though the complexity is low to allow for basic
 * AI control. This object is mostly inherited to specialise the AI.
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/

using UnityEngine;
using System.Collections;

public class NonPlayableCharacter : Character {

	/* */
	public override void initialise(CharacterHandler p_handler) {

	}


	/* */
	public override void move() {

	}


	/* */
	public override void actions() {

	}

}
