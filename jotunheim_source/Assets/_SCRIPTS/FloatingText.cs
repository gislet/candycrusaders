﻿/***
 * 
 * OutroSub.cs
 * 
 * Handles handles the UI subcategory of the Outro stage. 
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class FloatingText : MonoBehaviour {


	public GameObject prefab_object;
	private static FloatingText _instance;
	public LayerMask[] hud_masks;


	/* */
	private void Start() {
		_instance = this;
    }


	/* */
	public static void create(Vector3 p_pos, string p_msg, PlayerID p_id) {
		GameObject floater = Instantiate(_instance.prefab_object, p_pos, Quaternion.identity) as GameObject;
		floater.transform.Rotate(Vector3.right * 45);

		GameObject canvas = floater.transform.GetChild(0).gameObject;
		if((int)p_id != Globals.INVALID) {
			switch(p_id) {
			case PlayerID.player0:
				canvas.layer = Mathf.RoundToInt(Mathf.Log(masks[0].value, 2));
				break;
			case PlayerID.player1:
				canvas.layer = Mathf.RoundToInt(Mathf.Log(masks[1].value, 2));
				break;
			case PlayerID.player2:
				canvas.layer = Mathf.RoundToInt(Mathf.Log(masks[2].value, 2));
				break;
			case PlayerID.player3:
				canvas.layer = Mathf.RoundToInt(Mathf.Log(masks[3].value, 2));
				break;
			}
		}

		floater.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<Text>().text = p_msg;
	}


	private static LayerMask[] masks { get { return _instance.hud_masks; } }
}
