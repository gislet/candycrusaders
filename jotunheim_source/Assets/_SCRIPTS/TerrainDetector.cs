﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TerrainDetector {

	/* */
	private static TerrainData _terrain_data;
	private static int _alphamap_width;
	private static int _alphamap_height;
	private static float[,,] _splatmap_data;
	private static int _num_textures;


	/* */
	public static void init() {
		if(Terrain.activeTerrain == null)
			return;

		_terrain_data = Terrain.activeTerrain.terrainData;
		_alphamap_width = _terrain_data.alphamapWidth;
		_alphamap_height = _terrain_data.alphamapHeight;

		_splatmap_data = _terrain_data.GetAlphamaps(0, 0, _alphamap_width, _alphamap_height);
		_num_textures = _splatmap_data.Length / (_alphamap_width * _alphamap_height);
	}


	/* */
	private static Vector3 ConvertToSplatMapCoordinate(Vector3 p_world_position) {
		Vector3 splat_position = new Vector3();
		Terrain terrain = Terrain.activeTerrain;
		Vector3 terrain_position = terrain.transform.position;
		splat_position.x = ((p_world_position.x - terrain_position.x) / terrain.terrainData.size.x) * terrain.terrainData.alphamapWidth;
		splat_position.z = ((p_world_position.z - terrain_position.z) / terrain.terrainData.size.z) * terrain.terrainData.alphamapHeight;
		return splat_position;
	}


	/* */
	public static SurfaceType getTerrainSurfaceType(Vector3 p_position) {
		Vector3 terrain_coordinates = ConvertToSplatMapCoordinate(p_position);
		int active_terrain_index = 0;
		float largest_opacity = 0f;

		for(int i = 0; i < _num_textures; i++) {
			if(largest_opacity < _splatmap_data[(int)terrain_coordinates.z, (int)terrain_coordinates.x, i]) {
				active_terrain_index = i;
				largest_opacity = _splatmap_data[(int)terrain_coordinates.z, (int)terrain_coordinates.x, i];
			}
		}

		return TerrainHandler.getSurfaceType(_terrain_data.splatPrototypes[active_terrain_index].texture.name);
	}
}
