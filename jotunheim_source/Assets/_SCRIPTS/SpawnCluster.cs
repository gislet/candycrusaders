﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnCluster : MonoBehaviour {


	[Tooltip("What this cluster spawns, and nothing else.")]
	public SpawnHandler.ObjectType spawns;
	public Team ownership;

	
	/* */
	private void Start() {
		cache();
		StartCoroutine(attach());
	}


	/* */
	private void cache() {
		List<Transform> children = new List<Transform>();
		for(int i = 0; i < transform.childCount; i++) {
			children.Add(transform.GetChild(i));
		}

		points = children.ToArray();
	}


	/* */
	private IEnumerator attach() {

		while(SpawnHandler.clusters == null) {
			yield return null;
		}

		SpawnHandler.clusters.Add(this);
	}


	#region Getters & Setters
	public Transform[] points {	get; private set; }
	#endregion
}
