﻿/***
 * 
 * AudioPresets.cs
 * 
 * Handles the loading and saving of the user's sound/music settings. Makes sure that a change made by the user
 * is set and taken into effect immediately.
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using System.Collections;
using System;


public static class AudioPresets {


	/* Once this static class wakes up, we call the load-graphics-settings method. */
	static AudioPresets() {
		music = new Audio(100, false);
		effects = new Audio(100, false);
		ambient = new Audio(100, false);

		loadSettings();
	}


	/* Called by anyscript to make sure that this static class is awake. */
	public static void awake() { }


	/* Saves our current graphical settings to a file. */
	public static void saveSettings() {
		Globals.prepareSavePath();

		using(System.IO.StreamWriter sw = new System.IO.StreamWriter(Globals.AUDIO_SETTINGS_FILE)) {
			sw.WriteLine(music.volume);
			sw.WriteLine(music.mute);
			sw.WriteLine(effects.volume);
			sw.WriteLine(effects.mute);
			sw.WriteLine(ambient.volume);
			sw.WriteLine(ambient.mute);

			sw.Close();
		}
	}


	/* If any graphical settings exists, we set our game to those. */
	private static void loadSettings() {
		if(System.IO.File.Exists(Globals.AUDIO_SETTINGS_FILE)) {
			try {
				int expected_length = 6;
				int index = 0;
				string s;
				
				using(System.IO.StreamReader sr = new System.IO.StreamReader(Globals.AUDIO_SETTINGS_FILE)) {
					while((s = sr.ReadLine()) != null) {
						switch(index) {
						case 0: //Music
							music.volume = float.Parse(s);
                            break;
						case 1: //Music
							music.mute = bool.Parse(s);
							break;
						case 2: //Effects
							effects.volume = float.Parse(s);
							break;
						case 3: //Effects
							effects.mute = bool.Parse(s);
							break;
						case 4: //Ambient
							ambient.volume = float.Parse(s);
							break;
						case 5: //Ambient
							ambient.mute = bool.Parse(s);
							break;
						default:
							index--;
							break;
						}
						index++;
					}

					sr.Close();
				}

				if(index != expected_length) {
					System.IO.File.Delete(Globals.AUDIO_SETTINGS_FILE);
				}
			} catch(Exception e) {
				Debug.LogError(e);
				System.IO.File.Delete(Globals.AUDIO_SETTINGS_FILE);
			}
		}
	}


	#region Getters & Setters
	public static Audio music { get; set; }
	public static Audio ambient { get; set; }
	public static Audio effects { get; set; }
	public static bool dirty_settings { get; set; }
	#endregion
}


/* */
public class Audio {
	private float _volume;
	private bool _mute;

	public Audio(int p_volume, bool p_mute) {
		_volume = p_volume;
		_mute = p_mute;
    }

	public float volume {
		get { return _volume; }
		set {
			float updated_value = value;
            _volume = updated_value > 100 ? 100 :
						updated_value < 0 ? 0 : 
						updated_value;
			AudioPresets.dirty_settings = true;
		}
	}

	public bool mute {
		get { return _mute; }
		set {
			_mute = value;
			AudioPresets.dirty_settings = true;
		}
	}
}
