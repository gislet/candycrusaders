﻿using UnityEngine;
using System.Collections;

public class HUD_Life : MonoBehaviour {

	float something = 0;

	public Texture[] health;
	public Health health_script;

	void Update () {
		something += (90 * Time.unscaledDeltaTime);
		something = (something > 359) ? something - 360 : something;

		transform.rotation = Quaternion.identity;
		transform.Rotate(Vector3.right * 90);
		transform.Rotate(Vector3.forward * something);

		changeDisplay(health_script.value);
    }


	public void changeDisplay(int p_life) {
		int index = (Globals.MAX_LIVES - p_life);

		if(index < Globals.MAX_LIVES && index >= 0)
			transform.GetComponent<Renderer>().material.mainTexture = health[index];
		/*else if(index >= Globals.max_players_lives) {
			transform.GetComponent<Renderer>().material.mainTexture = null;
        }*/

		transform.GetComponent<Renderer>().enabled = index < Globals.MAX_LIVES;
    }
}
