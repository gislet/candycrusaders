﻿using UnityEngine;
using System.Collections;

public static class GameSettings {

	private static GameSetting _current;

	public static void awake() {
		createGameSetting();
	}

	public static void createGameSetting(bool p_force = false) {
		if(_current == null || p_force) {
			_current = new GameSetting();
        }
	}

	public static void destroySettings() {
		_current = null;
	}


	public static GameSetting current {
		get { return _current; }
	}


	public static string changeLobbyType(bool p_left_to_right = true, bool p_getcurrent = false) {
		LobbyType current = _current._lobby_type;
		int max = (int)LobbyType.Length;

		if(!p_getcurrent)
			_current._lobby_type = (LobbyType)newEnumPosition((int)current, max, p_left_to_right);
		return _current._lobby_type.ToString().ToUpper();
    }


	public static string changeAIDifficulty(bool p_left_to_right = true, bool p_getcurrent = false) {
		AIDifficulty current = _current._ai_difficulty;
		int max = (int)AIDifficulty.Length;

		if(!p_getcurrent)
			_current._ai_difficulty = (AIDifficulty)newEnumPosition((int)current, max, p_left_to_right);
		return _current._ai_difficulty.ToString().ToUpper();
	}


	public static string changeContinuousMode(bool p_left_to_right = true, bool p_getcurrent = false) {
		ContinuousMode current = _current._continuous_mode;
		int max = (int)ContinuousMode.Length;

		if(!p_getcurrent)
			_current._continuous_mode = (ContinuousMode)newEnumPosition((int)current, max, p_left_to_right);
		return _current._continuous_mode.ToString().ToUpper();
	}


	/* */
	public static string changeLevel(bool p_left_to_right = true, bool p_getcurrent = false) {
		

		if(p_getcurrent) {
			return LevelManager.findAssociatedSceneBundle(_current._game_mode).scenes[_current._level_index].world_name;
		}

		Debug.Log("Changing level from " + _current._level_index);
		_current._level_index += p_left_to_right ? 1 : -1;

		if(_current._level_index < 0)
			_current._level_index = LevelManager.findAssociatedSceneBundle(_current._game_mode).scenes.Length - 1;
		else if(_current._level_index >= LevelManager.findAssociatedSceneBundle(_current._game_mode).scenes.Length)
			_current._level_index = 0;

		Debug.Log("to " + _current._level_index);

		return LevelManager.findAssociatedSceneBundle(_current._game_mode).scenes[_current._level_index].world_name;
	}


	public static string changeGameMode(bool p_left_to_right = true, bool p_getcurrent = false) {
		GameMode current = _current._game_mode;
		int max = (int)GameMode.Length;

		if(!p_getcurrent)
			_current._game_mode = (GameMode)newEnumPosition((int)current, max, p_left_to_right);

		string mode_name = "";
		switch(_current._game_mode) {
		case GameMode.CaptureTheFlag:
			mode_name = "Capture The Flag";
			break;
		case GameMode.FreeForAll:
			mode_name = "Free For All";
			break;
		case GameMode.ResourceRace:
			mode_name = "Resource Race";
			break;
		default:
			mode_name = "ERROR";
            break;
		}

		_current._level_index = 0;
        return mode_name.ToUpper();
	}
	


	public enum LobbyType {
		Offline, Public, Private,
		Length
	}

	public enum AIDifficulty {
		Off, Easy, Normal, Difficult,
		Length
	}

	public enum ContinuousMode {
		Off, Random, Repeat,
		Length
	}

	public enum GameMode {
		None = -1,
		CaptureTheFlag, ResourceRace, FreeForAll,
		Length
	}

	private static int newEnumPosition(int current, int max, bool p_left_to_right) {
		if(p_left_to_right)
			current = current + 1 >= max ? 0 : current + 1;
		else
			current = current - 1 < 0 ? max - 1 : current - 1;

		return current;
	}
}


public class GameSetting {

	public GameSettings.LobbyType _lobby_type;
	public GameSettings.AIDifficulty _ai_difficulty;
	public GameSettings.ContinuousMode _continuous_mode;
	public GameSettings.GameMode _game_mode;
	public int _level_index;
	public float _round_time;
	//private GameSettings.RoundTime _round_time;
	//private GameSettings.RespawnTime _respawn_time;

	public GameSetting() {
		_lobby_type = GameSettings.LobbyType.Offline;
		_ai_difficulty = GameSettings.AIDifficulty.Easy;
		_continuous_mode = GameSettings.ContinuousMode.Off;
		_round_time = Globals.SECONDS * 5;
		_level_index = 0;
		setGameMode();

		//_game_mode = GameSettings.GameMode.CaptureTheFlag;
		//_round_time = GameSettings.RoundTime.TwoMinutes;
		//_respawn_time = GameSettings.RespawnTime.ThreeSeconds;
	}

	public int goal { get { return LevelManager.getGameGoalScore(); } }
	public float time_limit { get { return _round_time; } }


	private void setGameMode() {
		if(LevelManager.isInGame()) {
			_game_mode = LevelManager.whatGamemodeIsScene();
		} else {
			_game_mode = GameSettings.GameMode.CaptureTheFlag;
		}
	}
}
