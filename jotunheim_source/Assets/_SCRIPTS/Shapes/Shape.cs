﻿/******************************************************************************
*
* Shape.cs
*
* Description...
*
*
* Written by: Gisle Thorsen (July, 2017)
*
******************************************************************************/


using UnityEngine;


public abstract class Shape {

	/* */
	protected Vector3 _position;
	protected Vector3 _direction;
	protected Transform _parent;
	protected Vector3 _relative_origin;


	/* */
	public Shape(Transform p_parent) {
		if(p_parent == null)
			throw new System.NullReferenceException();

		_parent = p_parent;
	}

	/* */
	public Shape(Vector3 p_position, Vector3 p_direction) {
		_position = p_position;
		_direction = p_direction;
	}


	#region Getters & Setters
	public Vector3 relative_origin {
		get {
			return _relative_origin;
		}
	}
	public Vector3 position {
		get {
			if(_parent) {
				return _parent.position;
			}

			return _position;
		}
	}
	public Vector3 direction {
		get {
			if(_parent) {
				return GlobalMovement.findDirectionByTransform(_parent);
			}

			return _direction;
		}
	}
	public Transform parent {
		get { return _parent; }
	}
	#endregion
}
