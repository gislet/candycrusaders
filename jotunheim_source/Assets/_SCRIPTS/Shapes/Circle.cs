﻿/******************************************************************************
*
* Circle.cs
*
* Description...
*
*
* Written by: Gisle Thorsen (July, 2017)
*
******************************************************************************/


using UnityEngine;


public class Circle : Shape {

	/* */
	public float radius = 1;
	public float degrees = 360;



	/* */
	public Circle(float p_degrees, float p_radius, Transform p_parent) : base(p_parent) {
		degrees = p_degrees;
		radius = p_radius;

		validateDegrees();
	}


	/* */
	public Circle(float p_degrees, float p_radius, Vector3 p_position, Vector3 p_direction) : base(p_position, p_direction) {
		degrees = p_degrees;
		radius = p_radius;

		validateDegrees();
	}


	/* */
	private void validateDegrees() {
		degrees = degrees < 0 ? 0 : degrees > 360 ? 360 : degrees;
	}
}
