﻿/******************************************************************************
*
* Edge.cs
*
* Description...
*
*
* Written by: Gisle Thorsen (July, 2017)
*
******************************************************************************/


using UnityEngine;


public abstract class Edge : Shape {

	/* */
	protected Vector2[] _corners;
	private bool _is_left;



	/* */
	public Edge(Vec2d[] p_vectors, Transform p_parent) : base(p_parent) {
		findOrigin(p_vectors);
	}


	/* */
	public Edge(Vec2d[] p_vectors, Vector3 p_position, Vector3 p_direction) : base(p_position, p_direction) {
		findOrigin(p_vectors);
	}


	/* Finds the objects relative origin point given the mean value of all the received vector points. */
	private void findOrigin(Vec2d[] p_vectors) {
		if(p_vectors == null) {
			Debug.LogError("Vector array cannot be null.");
			return;
		}

		double x = 0;
		double y = 0;

		for(int i = 0; i < p_vectors.Length; i++) {
			x += p_vectors[i].x;
			y += p_vectors[i].y;
		}

		x /= p_vectors.Length;
		y /= p_vectors.Length;

		_relative_origin = new Vector3((float)x, 0, (float)y);
	}


	/* Initialises the corners array and populates it with the received vector parameters. */
	protected virtual void setupCorners(Vec2d[] p_vectors) {
		if(p_vectors == null) {
			Debug.LogError("Vector array cannot be null.");
			return;
		}

		_corners = new Vector2[p_vectors.Length];

		for(int i = 0; i < _corners.Length; i++) {
			_corners[i] = p_vectors[i].toVector2();
		}
	}


	/* Figures out whether the origin point is located to the left or the right in relation
	 * to a line. The line considered is the line from the first to the second corner of this
	 * edge shape. */
	protected void establishOriginAlignment() {
		if(corners == null || corners.Length < 2) {
			Debug.LogError("This 'Shape' object requires more than 2 corners.");
			return;
		}

		Vector2 a = new Vector2(corners[0].x, corners[0].y);
		Vector2 b = new Vector2(corners[1].x, corners[1].y);
		Vector2 c = new Vector2(_relative_origin.x, _relative_origin.y);

		_is_left = ((b.x - a.x) * (c.y - a.y) - 
					(b.y - a.y) * (c.x - a.x)) > 0;
	}



	#region Getters & Setters
	public Vector2[] corners {
		get { return _corners; }
	}
	public bool isOriginLeftAligned {
		get { return _is_left; }
	}
	#endregion
}
