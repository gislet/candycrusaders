﻿/******************************************************************************
*
* Triangle.cs
*
* Description...
*
*
* Written by: Gisle Thorsen (July, 2017)
*
******************************************************************************/


using UnityEngine;


public class Triangle : Edge {


	/* */
	public Triangle(Vec2d[] p_vectors, Transform p_parent) : base(p_vectors, p_parent) {
		setupCorners(p_vectors);
		establishOriginAlignment();
	}


	/* */
	public Triangle(Vec2d[] p_vectors, Vector3 p_position, Vector3 p_direction) : base(p_vectors, p_position, p_direction) {
		setupCorners(p_vectors);
		establishOriginAlignment();
	}
}
