﻿/******************************************************************************
*
* Rectangle.cs
*
* Description...
*
*
* Written by: Gisle Thorsen (July, 2017)
*
******************************************************************************/


using UnityEngine;


public class Rectangle : Edge {


	/* */
	public Rectangle(Vec2d[] p_vectors, Transform p_parent) : base(p_vectors, p_parent) {
		setupCorners(p_vectors);
		establishOriginAlignment();

		//_direction = GlobalMovement.findDirectionByAngle(_position, (GlobalMovement.findAngleByDirection(_direction) - 90));
	}


	/* */
	public Rectangle(Vec2d[] p_vectors, Vector3 p_position, Vector3 p_direction) : base(p_vectors, p_position, p_direction) {
		setupCorners(p_vectors);
		establishOriginAlignment();

		//_direction = GlobalMovement.findDirectionByAngle(_position, (GlobalMovement.findAngleByDirection(_direction) - 90));
	}


	/* */
	protected override void setupCorners(Vec2d[] p_vectors) {
		if(p_vectors == null) {
			Debug.LogError("Vector array cannot be null.");
			return;
		}

		_corners = new Vector2[p_vectors.Length*2];

		_corners[0] = new Vector2(p_vectors[0].toVector2().x, p_vectors[0].toVector2().y);
		_corners[1] = new Vector2(p_vectors[1].toVector2().x, p_vectors[0].toVector2().y);
		_corners[2] = new Vector2(p_vectors[1].toVector2().x, p_vectors[1].toVector2().y);
		_corners[3] = new Vector2(p_vectors[0].toVector2().x, p_vectors[1].toVector2().y);
	}
}
