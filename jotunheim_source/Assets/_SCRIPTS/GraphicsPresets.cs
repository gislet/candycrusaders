﻿/***
 * 
 * GraphicsPresets.cs
 * 
 * Handles the loading and saving of the user's graphical settings. Makes sure that a change made by the user
 * is set and taken into effect immediately. Also returns the name of a setting back to the object that
 * called for its change.
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using System;
using System.Collections;


public static class GraphicsPresets {

	/* Once this static class wakes up, we call the load-graphics-settings method. */
	static GraphicsPresets() {
		loadGFXSettings();
	}


	/* */
	private static bool _load_lock = false;


	/* Cached graphical values. */
	static AntiAliasing _anti_aliasing;
	static int _AA_level = 0;
	static int _VSync_enabled = 0;
	static bool _dirty = false;

	

	/* Called by anyscript to make sure that this static class is awake. */
	public static void awake() { }


	/* */
	public static string changeSetting(GraphicSetting p_type, bool p_increment, bool p_only_update = false) {
		string setting_text = "";

		switch(p_type) {
		case GraphicSetting.Preset:

			Preset preset = (Preset)QualitySettings.GetQualityLevel();
			if(!p_only_update)
				preset = (Preset)Globals.changeEnumIndex((int)preset, (int)Preset.Length, p_increment);
			setPreset(preset);

			switch(preset) {
			case Preset.Low:
				setting_text = "Low";
				break;
			case Preset.LowPluss:
				setting_text = "Low+";
				break;
			case Preset.Medium:
				setting_text = "Medium";
				break;
			case Preset.High:
				setting_text = "High";
				break;
			}

			break;

		case GraphicSetting.Vsync:

			if(!p_only_update)
				_VSync_enabled = Globals.changeEnumIndex(_VSync_enabled, (int)VSync.Length, p_increment);

			switch(_VSync_enabled) {
			case 0:
				setting_text = "Off";
				break;
			case 1:
				setting_text = "On";
				break;
			case 2:
				setting_text = "Double";
				break;
			}

			break;

		case GraphicSetting.Antialiasing:

			if(!p_only_update)
				_anti_aliasing = (AntiAliasing)Globals.changeEnumIndex((int)_anti_aliasing, (int)AntiAliasing.Length, p_increment);

			switch(_anti_aliasing) {
			case AntiAliasing.Off:
				setAntialiasing(_AA_level = 0);			
				setting_text = "Off";
				break;
			case AntiAliasing.MSAAx2:
				setAntialiasing(_AA_level = 2);
				setting_text = "MSAA 2x";
				break;
			case AntiAliasing.MSAAx4:
				setAntialiasing(_AA_level = 4);
				setting_text = "MSAA 4x";
				break;
			case AntiAliasing.MSAAx8:
				setAntialiasing(_AA_level = 8);
				setting_text = "MSAA 8x";
				break;
            }

			break;

		case GraphicSetting.FullScreen:

			if(!p_only_update)
				Screen.fullScreen = !Screen.fullScreen;

			setting_text = Screen.fullScreen ? "On" : "Off";

			break;

		case GraphicSetting.Resolution:

			/*if(!p_only_update)
				Screen.fullScreen = !Screen.fullScreen;*/

			setting_text = string.Format("{0} <size=25>x</size> {1}", Screen.width, Screen.height);

			break;

		}

		return setting_text;
	}


	/* Sets the shadow quality by modifying what graphical preset we're using. */
	private static void setPreset(Preset p_setting) {
		if((int)p_setting == QualitySettings.GetQualityLevel())
			return;

		QualitySettings.SetQualityLevel((int)p_setting, true);
		setGraphics();
	}


	/* Modifies the inbuilt unity AA level. */
	private static void setAntialiasing(int p_value) {
		if(QualitySettings.antiAliasing == p_value)
			return;

		QualitySettings.antiAliasing = p_value;
		setGraphics();
    }


	/* Sets the VSync level to the value given. */
	private static void setVSync(VSync p_setting) {
		if(_VSync_enabled == (int)p_setting)
			return;

		_VSync_enabled = (int)p_setting;
		QualitySettings.vSyncCount = _VSync_enabled;
		setGraphics();
    }


	/* Sets the resolution to the value given. */
	private static void setResolution(Resolution p_resolution) {
		if(Screen.width != p_resolution.width || Screen.height != p_resolution.height) {
			Screen.SetResolution(p_resolution.width, p_resolution.height, Screen.fullScreen);
		}
	}


	/* Makes sure that all the secondary graphical values are applied on top of any
	 * potential quality-preset change. */
	public static void setGraphics() {
		QualitySettings.antiAliasing = _AA_level;
		QualitySettings.vSyncCount = _VSync_enabled;

		if(!_load_lock)
			saveGFXSettings();
	}


	/* Saves our current graphical settings to a file. */
	private static void saveGFXSettings() {
		Globals.prepareSavePath();

		using(System.IO.StreamWriter sw = new System.IO.StreamWriter(Globals.GFX_SETTINGS_FILE)) {
			sw.WriteLine(Screen.fullScreen);
			sw.WriteLine(QualitySettings.vSyncCount);
			sw.WriteLine(QualitySettings.GetQualityLevel());
			sw.WriteLine(QualitySettings.antiAliasing);
			sw.WriteLine(Screen.width);
			sw.WriteLine(Screen.height);
			sw.WriteLine(show_fps);
			sw.WriteLine(DebugHandler.attack_visualiser);

			sw.Close();
		}
	}


	/* If any graphical settings exists, we set our game to those. */
	private static void loadGFXSettings() {

		if(_load_lock)
			return;

		_load_lock = true;

		
		if(System.IO.File.Exists(Globals.GFX_SETTINGS_FILE)) {

			try {
				int expected_length = 8;
				int index = 0;
				string s;
				

				using(System.IO.StreamReader sr = new System.IO.StreamReader(Globals.GFX_SETTINGS_FILE)) {

					Resolution resolution = new Resolution();
					Preset loaded_preset = Preset.Length;

					while((s = sr.ReadLine()) != null) {

						switch(index) {
						case 0: //FullScreen Toggle
							bool fullscreen = bool.Parse(s);
							if(Screen.fullScreen != fullscreen)
								Screen.fullScreen = fullscreen;
							break;
						case 1: //VSync Level
							setVSync((VSync)int.Parse(s));
							break;
						case 2: // ShadowQuality Level
							loaded_preset = (Preset)int.Parse(s);
							break;
						case 3: //AA Level
							int parsed_int = int.Parse(s);
							setAntialiasing(parsed_int);
							switch(parsed_int) {
							case 0:
								_anti_aliasing = AntiAliasing.Off;
								break;
                            case 2:
								_anti_aliasing = AntiAliasing.MSAAx2;
								break;
							case 4:
								_anti_aliasing = AntiAliasing.MSAAx4;
								break;
							case 8:
								_anti_aliasing = AntiAliasing.MSAAx8;
								break;
							}
							break;
						case 4: //Screen Width
							resolution.width = int.Parse(s);
                            break;
						case 5: //Screen Height
							resolution.height = int.Parse(s);
							break;
						case 6: //Show Fps
							show_fps = bool.Parse(s);
							break;
						case 7: //Show attack_visualiser
							DebugHandler.attack_visualiser = bool.Parse(s);
							break;
						default:
							index--;
							break;
						}
						index++;
					}
					
					sr.Close();

					setResolution(resolution);
					if(loaded_preset != Preset.Length)
						setPreset(loaded_preset);
				}
				
				if(index != expected_length) {
					System.IO.File.Delete(Globals.GFX_SETTINGS_FILE);
				}
			} catch(Exception e) {
				Debug.Log("Loading GFX Error: " + e);
				System.IO.File.Delete(Globals.GFX_SETTINGS_FILE);
			}
		}

		_load_lock = false;
    }


	/* Enum for easy VSync modifications. 
	 * 0 is off
	 * 1 is 60fps
	 * 2 is 30 fps */
	public enum VSync {
		Off = 0,
		EveryVBlank = 1,
		EverySecondVBlank = 2,
		Length
	}


	/* Enum for easy AntiAliasing modifications. */
	public enum AntiAliasing {
		Off,
		MSAAx2,
		MSAAx4,
		MSAAx8,
		Length
	}


	/* */
	public enum Preset {
		Low,
		LowPluss,
		Medium,
		High,
		Length
	}


	/* */
	public enum GraphicSetting {
		Preset,
		Resolution,
		Vsync,
		FullScreen,
		Antialiasing
	}


	public static bool show_fps { get; set; }
}
