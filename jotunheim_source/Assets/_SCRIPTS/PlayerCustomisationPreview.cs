﻿using UnityEngine;
using System.Collections;
using Customisation;

public class PlayerCustomisationPreview : MonoBehaviour {

	public PlayerID player_to_review;

	private SkinnedMeshRenderer _mesh_renderer;

	private void Awake() {
		_mesh_renderer = GetComponent<SkinnedMeshRenderer>();
    }

	private void Update() {
		PlayerConfiguration PC = PlayerHandler.retrieveConfigByPlayerID((int)player_to_review);

		if(PC != null)
			preview(PC.customisation);
	}


	private void preview(CustomisationSettings p_settings) {
		if(_mesh_renderer.material != p_settings.skin_material)
			_mesh_renderer.material = p_settings.skin_material;
    }
}
