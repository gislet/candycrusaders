﻿using UnityEngine;
using System.Collections;
using StatusEffects;


public class SpriteUpdater : MonoBehaviour {


	private float _speed = 1;
	private SpriteRenderer _sprite_renderer;
	private Status _status;


	/* */
	private void Awake() {
		_status = Status.Disabled;
		_sprite_renderer = GetComponent<SpriteRenderer>();
    }


	/* */
	private void Update() {
		float alpha_change = 0;

		if(_status == Status.Disabled && _sprite_renderer.color.a > 0) {
			alpha_change -= Time.deltaTime * _speed;
        } else if(_status == Status.Enabled && _sprite_renderer.color.a < .9f) {
			alpha_change += Time.deltaTime * _speed;
		}

		_sprite_renderer.color += new Color(0, 0, 0, alpha_change);
    }
	

	/* */
	public void linkToSpawner(StatusEffectSpawner p_spawner) {
		p_spawner.sprite_stop += stop;
		p_spawner.sprite_start += start;
		p_spawner.sprite_change += change;
    }


	/* */
	private void stop() {
		_speed = 5f;
		_status = Status.Disabled;
	}


	/* */
	private void start() {
		_speed = .33f;
		_status = Status.Enabled;
	}


	/* */
	private void change(Sprite p_sprite) {
		_sprite_renderer.sprite = p_sprite;
	}


	private enum Status {
		Disabled, Enabled
	}
}
