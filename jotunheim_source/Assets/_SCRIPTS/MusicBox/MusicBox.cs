﻿/***
 * 
 * MusicBox.cs
 * 
 * sss
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using Random = UnityEngine.Random;


/* */
public abstract class MusicBox<T> : MonoBehaviour {
	

	private Enum _previous_stage;


	/* */
	public void play() {
		updateTrackList();

		string name = AudioManager.currentClip(AudioChannel.Music);
		if(name == null || !isThisAllowed(name)) {
			AudioManager.play(random(), AudioChannel.Music);
		} else {
			AudioManager.resume(AudioChannel.Music);
		}
    }


	/* */
	protected virtual void updateTrackList() {

		if(LevelManager.stage.CompareTo(_previous_stage) != 0) {
			_previous_stage = LevelManager.stage.current;
		} else {
			return;
		}

		List<AudioClip> clips = new List<AudioClip>();

		foreach(TrackInfo<T> track in _collection) {
			if(track.isAllowed())
				clips.Add(track.clip);
		}

		available_tracks = clips.ToArray();
	}


	/* */
	private AudioClip random() {
		AudioClip clip = null;

		if(available_tracks != null ? available_tracks.Length > 0 : false)
			clip = available_tracks[Random.Range(0, available_tracks.Length)];

		return clip;
	}


	/* */
	private bool isThisAllowed(string p_name) {
		foreach(AudioClip c in available_tracks) {
			if(c.name.CompareTo(p_name) == 0) {
				return true;
			}
		}

		return false;
	}
	

	public AudioClip[] available_tracks { get; set; }
	protected TrackInfo<T>[] _collection { get; set; }
}


/* */
[System.Serializable]
public class TrackInfo_Menu : TrackInfo<MenuStage> {
}


/* */
[System.Serializable]
public class TrackInfo_Game : TrackInfo<IngameStage> {
}


/* */
public class TrackInfo<T> {
	public AudioClip clip;
	public bool all_stages;
	public T[] active_stages;

	public bool isAllowed() {
		if(all_stages)
			return true;

		foreach(T stage in active_stages) {
			if(LevelManager.stage.CompareTo(stage) == 0)
				return true;
		}

		return false;
	}
}
