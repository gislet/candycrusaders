﻿/***
 * 
 * MusicBox_Game.cs
 * 
 * sss
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using System.Collections;
using System;


/* */
public class MusicBox_Menu : MusicBox<MenuStage> {

	[Space(10, order = 0)]
	[Header("This is a >>Menu<< Music Box!", order = 1)]
	[Space(20, order = 2)]

	public TrackInfo_Menu[] collection;


	public void awake() {
		_collection = collection;
	}


	/* */
	protected override void updateTrackList() {
		if(_collection != null)
			base.updateTrackList();
	}
}

