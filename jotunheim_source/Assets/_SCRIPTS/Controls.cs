﻿/***
 * 
 * Controls.cs
 * 
 * As long as some script is continuously calling Controls.cleanUp() every frame, this script should
 * safely handle any gameplay input by keyboard or a controller. It tries to make the job easier for
 * other programmers by allowing simple calls to achieve common use-cases such as having a
 * onButtonDown operation. Although that already exists in Unity, what makes this different is that
 * you can map certain keys to certain operations. These operations could be "Start", "Select", "MoveUp",
 * "MoveLeft", and more. 
 *
 * Controls.Down(Operation.Left) listen for the left-key or -axis of any device being used unless a master device is set. 
 * Controls.Down(Operation.Left, 1) would listen specifically for the left-axis of a controller (0 = keyboard, 1-4 is Controllers). 
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text.RegularExpressions;


namespace Controllers {
	public static class Controls {


		public static device main_device { get; private set; }
		

		private static PreviousOperation[] _down_operation;
		private static PreviousOperation[] _interval_operation;
		private static float _previous_operation_time;
		private const float MINIMUM_OPERATION_COOLDOWN = .33f;
		private static int _previous_operation_frame;
		private static float _sensitivity = .66f;


		/* */
		private static KeyMap _default_keyboard = new KeyMap() {
			left = KeyCode.A,
			right = KeyCode.D,
			up = KeyCode.W,
			down = KeyCode.S,

			start = KeyCode.Return,
			select = KeyCode.Return,
			back = KeyCode.Escape,

			primary = KeyCode.J,
			secondary = KeyCode.K,
			tertiary = KeyCode.L,
			quaternary = KeyCode.P,

			mod = KeyCode.LeftShift,
		};


		/* */
		private static KeyMap _default_controller = new KeyMap() {
			vertical = "JoyAnyVert",
			horizontal = "JoyAnyHori",
			trigger_right = "JoyAny_3rd",

			start = KeyCode.JoystickButton7,
			select = KeyCode.JoystickButton0,
			back = KeyCode.JoystickButton1,

			primary = KeyCode.JoystickButton0,
			secondary = KeyCode.JoystickButton1,
			tertiary = KeyCode.JoystickButton3,
			quaternary = KeyCode.JoystickButton2,
		};


		/* */
		private static void awake() {
			_down_operation = new PreviousOperation[Globals.MAX_DEVICES];
			_interval_operation = new PreviousOperation[Globals.MAX_DEVICES];
			main_device = new device();
		}


		/* This function needs to be called by some master object, otherwise we won't free up our down keys once they are released. */
		public static void cleanUp() {

			/* If this is our first time calling and activating Controls.cs, we want to call its Awake function. */
			if(_down_operation == null)
				awake();

			for(int i = 0; i < Globals.MAX_DEVICES; i++) {
				PreviousOperation PO_Down = _down_operation[i];
				PreviousOperation PO_Interval = _interval_operation[i];

				if(PO_Down.operation != Operation.None) {
					_down_operation[i].operation = Hold(PO_Down.operation, i) ? PO_Down.operation : Operation.None;
				}

				if(PO_Interval.operation != Operation.None) {
					_interval_operation[i].operation = Hold(PO_Interval.operation, i) ? PO_Interval.operation : Operation.None;
				}
			}
		}


		/* */
		/*public static float AxisOnce(Operation p_operation, int p_device_id = Globals.INVALID) {
			return Axis(p_operation, p_device_id, true);
		}*/


		/* */
		public static float Axis(Operation p_operation, int p_device_id = Globals.INVALID) {
			int device_id = getDeviceID(p_device_id);
			KeyMap keymap;
			
			if(device_id == Globals.INVALID) {
				device_id = getActiveDeviceID();
				if(device_id == Globals.INVALID)
					return 0;
			}

			if(device_id > 0)
				keymap = _default_controller;
			else
				keymap = _default_keyboard;
			
			float axis_value = 0;
			switch(p_operation) {
			case Operation.Up:
				axis_value = device_id > 0 ? Input.GetAxis(getAxisName(keymap.vertical, device_id)) :
							device_id == 0 && Input.GetKey(keymap.up) ? 1 : 0;
				break;
			case Operation.Down:
				axis_value = device_id > 0 ? Input.GetAxis(getAxisName(keymap.vertical, device_id)) :
							device_id == 0 && Input.GetKey(keymap.down) ? -1 : 0;
				break;
			case Operation.Left:
				axis_value = device_id > 0 ? Input.GetAxis(getAxisName(keymap.horizontal, device_id)) :
							device_id == 0 && Input.GetKey(keymap.left) ? -1 : 0;
				break;
			case Operation.Right:
				axis_value = device_id > 0 ? Input.GetAxis(getAxisName(keymap.horizontal, device_id)) :
							device_id == 0 && Input.GetKey(keymap.right) ? 1 : 0;
				break;
			case Operation.Mod:
				axis_value = device_id > 0 ? Input.GetAxis(getAxisName(keymap.trigger_right, device_id)) :
							device_id == 0 && Input.GetKey(keymap.mod) ? 1 : 0;
				break;
			}

			if(Vector2.Distance(new Vector2(axis_value, 0), new Vector2(0, 0)) < .2f)
				axis_value = 0;
			/*if(axis_value != 0 && p_once) {
				if(p_operation == _down_operation[device_id].operation &&
						((int)(_down_operation[device_id].time * 1000)) != ((int)(Time.time * 1000))) {
					axis_value = 0;
				} else {
					_down_operation[device_id] = new PreviousOperation(p_operation, Time.time);
				}
			}*/

			return axis_value;
		}


		/* Returns a bool on whether the given operation is currently being active. */
		public static bool Hold(Operation p_operation, int p_device_id = Globals.INVALID, bool p_force_lock = false) {
			ListenBehaviour listen_behaviour = ListenBehaviour.Hold;

			return isOperationActive(p_operation, listen_behaviour, getDeviceID(p_device_id), false, p_force_lock);
		}


		/* Returns a bool on whether the given operation was pressed on this frame. */
		public static bool Down(Operation p_operation, int p_device_id = Globals.INVALID, bool p_force_lock = false) {
			ListenBehaviour listen_behaviour = ListenBehaviour.Down;

			return isOperationActive(p_operation, listen_behaviour, getDeviceID(p_device_id), false, p_force_lock);
		}


		/* On call, returns a bool at an interval on whether the given operation is being pressed/active. */
		public static bool Interval(Operation p_operation, int p_device_id = Globals.INVALID, bool p_force_lock = false) {
			ListenBehaviour listen_behaviour = ListenBehaviour.Interval;

			return isOperationActive(p_operation, listen_behaviour, getDeviceID(p_device_id), false, p_force_lock);
		}


		/* */
		public static void SimulateButtonDown(Operation p_operation, int p_device_id) {
			ListenBehaviour listen_behaviour = ListenBehaviour.Down;
			isOperationActive(p_operation, listen_behaviour, p_device_id, true);
		}


		/* */
		private static bool isOperationActive(Operation p_operation, ListenBehaviour p_listen_behaviour, int p_device_id, bool p_simulated = false, bool p_force_lock = false) {

			/* If this is our first time calling and activating Controls.cs, we want to call its Awake function. */
			if(_down_operation == null)
				awake();

			/* We begin with checking to see if a controller or the keyboard is being used. */
			int device_id = p_device_id;
			KeyMap keymap;

			if(device_id == Globals.INVALID) {
				device_id = getActiveDeviceID();
				if(device_id == Globals.INVALID)
					return false;
			}

			if(device_id > 0)
				keymap = _default_controller;
			else
				keymap = _default_keyboard;

			/* Retrieves the axis value and/or bool for the operation we're looking up. */
			bool is_ready = false;
			float axis_value = 0;
			switch(p_operation) {
			case Operation.Up:
				axis_value = device_id > 0 ? Input.GetAxis(getAxisName(keymap.vertical, device_id)) : 0;
				is_ready = device_id > 0 ? axis_value >= _sensitivity : Input.GetKey(keymap.up);
				break;
			case Operation.Down:
				axis_value = device_id > 0 ? Input.GetAxis(getAxisName(keymap.vertical, device_id)) : 0;
				is_ready = device_id > 0 ? axis_value <= -_sensitivity : Input.GetKey(keymap.down);
				break;
			case Operation.Left:
				axis_value = device_id > 0 ? Input.GetAxis(getAxisName(keymap.horizontal, device_id)) : 0;
				is_ready = device_id > 0 ? axis_value <= -_sensitivity : Input.GetKey(keymap.left);
				break;
			case Operation.Right:
				axis_value = device_id > 0 ? Input.GetAxis(getAxisName(keymap.horizontal, device_id)) : 0;
				is_ready = device_id > 0 ? axis_value >= _sensitivity : Input.GetKey(keymap.right);
				break;
			case Operation.Select:
				is_ready = Input.GetKey(device_id > 0 ? getKeyCode(keymap.select.ToString(), device_id) : keymap.select);
				break;
			case Operation.Start:
				is_ready = Input.GetKey(device_id > 0 ? getKeyCode(keymap.start.ToString(), device_id) : keymap.start);
				break;
			case Operation.Back:
				is_ready = Input.GetKey(device_id > 0 ? getKeyCode(keymap.back.ToString(), device_id) : keymap.back);
				break;
			case Operation.Primary:
				is_ready = Input.GetKey(device_id > 0 ? getKeyCode(keymap.primary.ToString(), device_id) : keymap.primary);
				break;
			case Operation.Secondary:
				is_ready = Input.GetKey(device_id > 0 ? getKeyCode(keymap.secondary.ToString(), device_id) : keymap.secondary);
				break;
			case Operation.Tertiary:
				is_ready = Input.GetKey(device_id > 0 ? getKeyCode(keymap.tertiary.ToString(), device_id) : keymap.tertiary);
				break;
			case Operation.Quaternary:
				is_ready = Input.GetKey(device_id > 0 ? getKeyCode(keymap.quaternary.ToString(), device_id) : keymap.quaternary);
				break;
			default:
				Debug.LogWarning("Controls operation is missing!");
				break;
			}

			/* Even if the bool is true, we might be using a listen-behaviour that will 
			 * change the bool's value back to false. */
			if(is_ready || p_simulated) {
				if(p_listen_behaviour == ListenBehaviour.Interval) {
					if(p_operation == _interval_operation[device_id].operation) {
						/* Once we've waited out the cooldown, we'll reset the time. */
						if((Time.unscaledTime - _interval_operation[device_id].time) >= MINIMUM_OPERATION_COOLDOWN) {
							_interval_operation[device_id].time = Time.unscaledTime;

						/* If the time is the same, we can assume we're still in the same frame. */
						} else if(((int)(_interval_operation[device_id].time * 1000)) != ((int)(Time.unscaledTime * 1000))) {
							is_ready = false;
						}
					} else {
						_interval_operation[device_id] = new PreviousOperation(p_operation, Time.unscaledTime);
					}

					/*if(p_operation == _previous_operation) {
						if((Time.time - _previous_operation_time) >= MINIMUM_OPERATION_COOLDOWN) {
							_previous_operation_time = Time.time;
							_previous_operation_frame = Time.frameCount;
						} else if(_previous_operation_frame != Time.frameCount) {
							is_ready = false;
						}
					} else {
						_previous_operation = p_operation;
						_previous_operation_time = Time.time;
						_previous_operation_frame = Time.frameCount;
					}*/
				} else if(p_listen_behaviour == ListenBehaviour.Down) {
					if(p_operation == _down_operation[device_id].operation &&
						((int)(_down_operation[device_id].time * 1000)) != ((int)(Time.unscaledTime * 1000))) {
						is_ready = false;
					} else {
						_down_operation[device_id] = new PreviousOperation(p_operation, Time.unscaledTime);
					}
				}
			}


			return is_ready;
		}


		/* Given a string and a device ID, returns the keycode representing it. (For Controllers/Joysticks) */
		private static KeyCode getKeyCode(string p_code, int p_device_id) {
			KeyCode code;

			int position = p_code.IndexOf("Button");
			code = (KeyCode)Enum.Parse(typeof(KeyCode), "Joystick" + p_device_id + p_code.Substring(position));

			return code;
		}


		/* Given a string and a device ID, returns the "button name" for the axis we're after. (For Controllers/Joysticks) */
		private static string getAxisName(string p_code, int p_device_id) {
			string code = "";

			string substring_any = "Any";
			int position = p_code.IndexOf(substring_any);

			code = "Joy" + p_device_id + p_code.Substring(position + substring_any.Length);

			return code;
		}


		/* Returns the main devices ID if current ID is invalid. */
		private static int getDeviceID(int p_device_id = Globals.INVALID) {
			if(main_device != null)
				if(p_device_id == Globals.INVALID && main_device.ID != Globals.INVALID)
					return main_device.ID;

			return p_device_id;
		}


		/* Returns the ID of a currently active device. */
		public static int getActiveDeviceID() {
			int id = Globals.INVALID;

			if((id = activeControllerIndex()) == Globals.INVALID && Input.anyKey)
				id = 0;

			/*if(id == Globals.INVALID)
				_previous_operation = Operation.None;*/

			return id;
		}


		/* */
		private static int activeControllerIndex() {
			int max_controller_buttons = 20;

			for(int i = 1; i <= 4; i++) {
				for(int count = 0; count < max_controller_buttons; count++) {
					KeyCode KC;
					KC = (KeyCode)Enum.Parse(typeof(KeyCode), "Joystick" + i + "Button" + count);

					if(Input.GetKey(KC)) {
						return i;
					}

					string vert_axis = "Joy" + i + "Vert";
					string hori_axis = "Joy" + i + "Hori";

					if(Input.GetAxis(vert_axis) != 0 || Input.GetAxis(hori_axis) != 0) {
						return i;
					}
				}
			}

			return Globals.INVALID;
		}

		private enum ListenBehaviour {
			Hold, Down, Interval
		}


		private struct PreviousOperation {
			public Operation operation;
			public float time;

			public PreviousOperation(Operation p_operation, float p_time = Globals.INVALID) {
				operation = p_operation;
				time = p_time;
			}
		}
	}


	/* */
	public class device {

		private int _ID;

		public device() {
			_ID = Globals.INVALID;
		}

		public void set(int p_ID) {
			if(p_ID > Globals.INVALID && p_ID < Globals.MAX_DEVICES)
				_ID = p_ID;
		}

		public void clear() {
			_ID = Globals.INVALID;
		}


		public int ID {
			get { return _ID; }
		}

	}


	/* */
	public enum Operation {
		None,
		Up, Down, Left, Right,
		Start, Select, Back,
		Primary, Secondary, Tertiary, Quaternary,
		Mod
	}


	/* */
	public struct KeyMap {

		public string vertical, horizontal, trigger_left, trigger_right;
		public KeyCode left, right, up, down;
		public KeyCode primary, secondary, tertiary, quaternary;
		public KeyCode start, select, back;
		public KeyCode mod;

	}
}
