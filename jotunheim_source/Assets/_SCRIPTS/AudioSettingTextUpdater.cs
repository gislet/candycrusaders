﻿/******************************************************************************
*
* AudioSettingTextUpdater.cs
* 
* Simply updates the UI text to show what the current setting for this particular audiochannel is set to. 
*
*
* Written by: Gisle Thorsen (October, 2017)
*
******************************************************************************/


using UnityEngine;
using UnityEngine.UI;
using Configurations;


public class AudioSettingTextUpdater : MonoBehaviour {

	/* */
	[SerializeField] private string setable_setting_key;
	[SerializeField] private string togglable_setting_key;

	public AudioChannel channel;
	private Text _display;
	private string _muted = "<color=#FF4400FF>muted ({0})</color>";



	/* Caches the text component. */
	private void Awake() {
		_display = GetComponent<Text>();

		Settings.isValidKey(setable_setting_key, gameObject.name);
		Settings.isValidKey(togglable_setting_key, gameObject.name);
	}


	/* Updates the text component. */
	private void Update() {
		_display.text = status();
	}


	/* Finds the status of our currently selected channel. */
	private string status() {
		string str_enabled = Settings.interact(togglable_setting_key, ConfigAction.Value);
		string str_volume = Settings.interact(setable_setting_key, ConfigAction.Value);

		if(str_enabled != null && str_volume != null) {
			return int.Parse(str_enabled) == 0 ?
						string.Format(_muted, Mathf.RoundToInt(float.Parse(str_volume))) :
						"" + Mathf.RoundToInt(float.Parse(str_volume));
		}

		return "<color=#FF4400FF>ERROR!</color>";
	}
}


