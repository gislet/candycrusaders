﻿using UnityEngine;
using System.Collections;

public class UIBobbing : MonoBehaviour {

	public float speed = 2;
	public bool fixed_size = true;
	public float world_unit_size = 2;
	public AnimationCurve curve;
	private Vector3 pos;

	// Use this for initialization
	void Start () {
		pos = transform.position;
	
		Sprite sprite = GetComponent<SpriteRenderer>().sprite;

		float relative_size = (sprite.texture.width/world_unit_size)/sprite.pixelsPerUnit;

		transform.localScale = new Vector3(1 / relative_size, 1 / relative_size, 1);
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = new Vector3(pos.x, pos.y + curCurvePos(), pos.z);
    }

	public float curCurvePos() {
		float value = 0;

		value = curve.Evaluate((Time.time % speed)/speed);

		return value;
	}
}
