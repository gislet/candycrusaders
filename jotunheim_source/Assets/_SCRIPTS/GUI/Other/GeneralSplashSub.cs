﻿/***
 * 
 * GeneralSplashSub.cs
 * 
 * sss. 
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using UnityEngine.UI;
using Controllers;
using System;

public class GeneralSplashSub : MenuSubUI {


	public AnimationCurve fade_in, fade_out;
	public float linger_time;
	public GameObject splash_group;
	private Image[] _splash_images;
	private int _splash_index = 0;
	private float _linger_time;
	private SplashStage _stage;


	/* */
	public override bool isActive {
		get {
			return (!LevelManager.isInGame() && !LevelManager.isMainMenu());
		}
	}


	/* */
	public override void actions() {
		if(Input.anyKey)
			nextScene();
	}


	/* */
	public override void run() {
		base.run();

		/* */
		if(_splash_index >= _splash_images.Length) {
			nextScene();
			return;
		}

		if(_stage == SplashStage.init) {
			if(Time.time >= 1) {
				linger_time = Time.time;
				_stage = SplashStage.intro;
            }
			return;
		}

		float alpha = 0;

		switch(_stage) {
		case SplashStage.intro:
			alpha = fade_in.Evaluate((Time.time - linger_time));
			if((Time.time - linger_time) >= 1) {
				_stage = SplashStage.hold;
				linger_time = _linger_time;
			}
            break;
		case SplashStage.hold:
			alpha = 1;
			if(Globals.deltaTimeCountdown(ref linger_time) <= 0) {
				_stage = SplashStage.outro;
				linger_time = Time.time;
			}
			break;
		case SplashStage.outro:
			alpha = fade_out.Evaluate((Time.time - linger_time));
			break;
		}

		_splash_images[_splash_index].color = new Color(_splash_images[_splash_index].color.r,
                                                        _splash_images[_splash_index].color.g,
														_splash_images[_splash_index].color.b,
														alpha);

		if((Time.time - linger_time) >= 1 && _stage == SplashStage.outro) {
			_stage = SplashStage.intro;
			linger_time = Time.time;
			_splash_index++;
        }
	}


	/* */
	private void nextScene() {
		LevelManager.loadMainMenu();
	}


	/* */
	protected override void onEnable() {
		_splash_images = new Image[splash_group.transform.childCount];

		for(int i = 0; i < _splash_images.Length; i++) {
			_splash_images[i] = splash_group.transform.GetChild(i).GetComponent<Image>();
		}

		foreach(Image image in _splash_images) {
			image.color = image.color * new Color(1, 1, 1, 0);
		}

		_linger_time = linger_time;
		_stage = SplashStage.init;
    }


	private enum SplashStage {
		init, intro, hold, outro
	}
}
