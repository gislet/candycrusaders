﻿/***
 * 
 * OutroSub.cs
 * 
 * Handles handles the UI subcategory of the Outro stage. 
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using UnityEngine.UI;
using Controllers;
using System;

public class GameoverSub : GameSubUI {

	public GameObject names_group, teams_group, goals_group, deaths_group, kills_group;
	public Text winner_is;

	private Text[] _names, _teams, _goals, _deaths, _kills;

	private float _wait_timer = Globals.INVALID;


	/* */
	public override void actions() {
		base.actions();
	}


	/* */
	protected override void interact() {
		/* We want to force the players to view the score-screen a little, to avoid them accidentally continuing. */
		if(_wait_timer > Time.unscaledTime && _wait_timer != Globals.INVALID)
			return;

		switch(index) {
		case 0: //Continue
			Debug.LogWarning("Continue Mechanic is not implemented!");
			retreat();
			break;
		case 1: //Retry
			LevelManager.reloadScene();
			break;
		case 2: //Exit to Menu
			retreat();
			break;
		}
	}


	/* */
	protected override void retreat() {
		PlayerHandler.awake(true);
		LevelManager.loadMainMenu();
	}


	/* */
	public override void run() {
	}


	/* */
	protected override void cache() {
		base.cache();

		_names = new Text[4];
		_kills = new Text[4];
		_deaths = new Text[4];
		_goals = new Text[4];
		_teams = new Text[4];


		for(int i = 0; i < Globals.MAX_PLAYERS; i++) {
			_names[i] = names_group.transform.GetChild(i).GetComponent<Text>();
			_kills[i] = kills_group.transform.GetChild(i).GetComponent<Text>();
			_deaths[i] = deaths_group.transform.GetChild(i).GetComponent<Text>();
			_goals[i] = goals_group.transform.GetChild(i).GetComponent<Text>();
			_teams[i] = teams_group.transform.GetChild(i).GetComponent<Text>();
		}

		_wait_timer = Time.unscaledTime + 2f;
		setupScore();
	}


	/* */
	private void setupScore() {
		for(int i = 0; i < _names.Length; i++) {
			if(i >= PlayerHandler.numValidConfigs()) {
				_kills[i].text = _deaths[i].text = _goals[i].text = _teams[i].text = _names[i].text = "";
			} else {
				_names[i].text = PlayerHandler.retrieveConfigByIndex(i).player_name;
				_kills[i].text = "" + Scoring.character_kills[i];
				_deaths[i].text = "" + Scoring.character_deaths[i];
				_goals[i].text = "" + Scoring.character_score[i];
				_teams[i].text = PlayerHandler.retrieveConfigByIndex(i).team.ToString();

				Color team_color = GUIGlobals.getTeamColour(PlayerHandler.retrieveConfigByIndex(i).team);
				_teams[i].color = team_color;
			}
		}


		Color winner_colour = Scoring.winnerColour();
		string winner_name = Scoring.winnerName();


		winner_is.text = string.Format("the winner is    <color=#{0}>{1}</color>", ColorUtility.ToHtmlStringRGBA(winner_colour), winner_name);
	}
}
