﻿/***
 * 
 * PlaySub.cs
 * 
 * Handles handles the UI subcategory of the Play stage. 
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using UnityEngine.UI;
using Controllers;
using System;

public class PlaySub : GameSubUI {


	public GameObject time_group, goal_group;
	public Text time, goal;

	private float _played_time = 0;


	/* Should only process player commands. */
	public override void actions() {
	}


	/* Should only process automated code. */
	public override void run() {

		/* If we happen to have the round timer enabled. */
		if(GameSettings.current.time_limit != Globals.INVALID) {

			/* We count the time elapsed during the play stage. */
			_played_time = (_played_time + Time.deltaTime >= GameSettings.current.time_limit) ? GameSettings.current.time_limit : _played_time + Time.deltaTime;

			/* Once played time surpasses the time limit. */
			if(_played_time >= GameSettings.current.time_limit) {

				/* We switch to the outro stage and end the game. */
				LevelManager.stage.change = IngameStage.Outro;
			}
		}

		/* We also want to check to see if anyone reached the goal. */
		if(Scoring.goal_reached) {

			/* If so, we switch to the outro stage and end the game. */
			LevelManager.stage.change = IngameStage.Outro;
		}

		/* We always end with updating the UI. */
		update();
	}


	/* Updates the UI and the counts the time elapsed. */
	private void update() {
		float time_left = GameSettings.current.time_limit - Mathf.Floor(_played_time);

		/* Update the current time. */
		if(GameSettings.current.time_limit != Globals.INVALID)
			time.text = string.Format("{0} : {1}", Mathf.Floor((time_left / Globals.SECONDS)).ToString("00"), Mathf.Ceil(time_left % Globals.SECONDS).ToString("00"));
		else {
			time_group.SetActive(false);
			goal_group.transform.localPosition = Vector3.zero;
		}

		goal.text = "" + Scoring.goal;
	}
}
