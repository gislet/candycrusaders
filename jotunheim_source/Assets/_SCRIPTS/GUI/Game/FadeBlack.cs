﻿/***
 * 
 * FadeBlack.cs
 * 
 * Handles handles the UI subcategory of the Play stage. 
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using UnityEngine.UI;
using Controllers;


public class FadeBlack : MonoBehaviour {

	public Image overlay;
	public AnimationCurve fade_curve;
	private float _value_threshold = .95f, _value = 0;


	/* */
	public void run() {
		_value = _value >= 1 ? 0 : _value + Time.unscaledDeltaTime;
		overlay.color = new Color(overlay.color.r, overlay.color.g, overlay.color.b, fade_curve.Evaluate(_value));
	}


	/* */
	public float value {
		get {
			return _value;
		}
	}


	/* */
	public bool isBlack {
		get {
			return value < .5f;
		}
	}
}
