﻿/***
 * 
 * IntroSub.cs
 * 
 * Handles handles the UI subcategory of the Intro stage. 
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using UnityEngine.UI;
using Controllers;
using System;

public class IntroSub : GameSubUI {

	public Text counter;
	public Image background;

	public AudioClip countdown, start;

	public AnimationCurve flash_curve;
	private int _previous_timer;
	private float _timer = 5f;


	/* */
	public override void actions() {
		base.actions();	
	}


	/* */
	protected override void cache() {
		base.cache();
		PlayerUIHandler.start();
		CameraManager.start();
	}


	/* */
	public override void run() {
		if(_previous_timer != Mathf.CeilToInt(_timer)) {
			_previous_timer = Mathf.CeilToInt(_timer);

			if(_previous_timer != 0)
				AudioManager.play(countdown, AudioChannel.Effects);
			else
				AudioManager.play(start, AudioChannel.Effects);
		}

		if(_timer > 0) {
			counter.text = Mathf.Ceil(_timer) + "";
			counter.color = new Color(counter.color.r, counter.color.g, counter.color.b, flash_curve.Evaluate(1 - (_timer % 1)));
		} else {
			counter.text = "START";
			counter.color = new Color(counter.color.r, counter.color.g, counter.color.b, flash_curve.Evaluate(((_timer % 1) * -1)));
			background.color = new Color(background.color.r, background.color.g, background.color.b, flash_curve.Evaluate(((_timer % 1) * -1)));
		}


		if(Globals.deltaTimeCountdown(ref _timer) <= -1)
			LevelManager.stage.change = IngameStage.Play;
	}
}
