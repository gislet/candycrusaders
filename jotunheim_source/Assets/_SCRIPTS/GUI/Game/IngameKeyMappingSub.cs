﻿/***
 * 
 * IngameKeyMappingSub.cs
 * 
 * Handles handles the UI subcategory of the Outro stage. 
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using UnityEngine.UI;
using Controllers;
using System;


public class IngameKeyMappingSub : GameSubUI {


	public GameObject xbox_controller, keyboard;


	/* */
	public override void actions() {
		base.actions();
	}


	/* */
	public override void run() {
		xbox_controller.SetActive(index == 0);
		keyboard.SetActive(index == 1);
	}
}
