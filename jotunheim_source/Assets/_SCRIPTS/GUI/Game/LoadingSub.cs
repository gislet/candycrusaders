﻿/***
 * 
 * LoadingSub.cs
 * 
 * Handles handles the UI subcategory of the Loading stage. Will display gameplay tips and hints, show
 * a fake loading bar that allows the users to read these tips and hints at their own pace before
 * starting a game.
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using UnityEngine.UI;
using Controllers;
using System;
using System.Collections;

public class LoadingSub : GameSubUI {

	/* Loading Bar */
	public Text loading_finished, tips_n_hints;
	public Image loadingbar, loadingbar_container;
	public AnimationCurve ready_flash_curve;
	public float fill_timer = 5;
	private float _fade_value = -.5f;


	/* */
	public override void actions() {
		base.actions();

		if(loadingbar.fillAmount == 1) {
			if(Controls.getActiveDeviceID() != -1) {
				LevelManager.stage.change = IngameStage.Cinematic;
			}
		}
	}


	/* */
	public override void run() {
		base.run();

		float loadingbar_fade_threshold = .05f;

		/* Grow the loading bar. */
		if(loadingbar.fillAmount < 1) {
			float growth_rate = (1 / fill_timer) * Time.unscaledDeltaTime;

			loadingbar.fillAmount += growth_rate;

			if(loadingbar.fillAmount > 1) {
				loadingbar.fillAmount = 1;
			}

		/* Fade the loading bar. */
		} else if(loadingbar.fillAmount == 1 && loadingbar_container.color.a > loadingbar_fade_threshold) {

			loadingbar.color = Color.Lerp(loadingbar.color, loadingbar.color * new Color(1, 1, 1, 0), Time.unscaledDeltaTime * 2);
			loadingbar_container.color = Color.Lerp(loadingbar.color, loadingbar.color * new Color(1, 1, 1, 0), Time.unscaledDeltaTime * 2);

			if(loadingbar_container.color.a < loadingbar_fade_threshold) {
				loadingbar.color *= new Color(1, 1, 1, 0);
				loadingbar_container.color *= new Color(1, 1, 1, 0);
			}

		/* */
		} else {

			loading_finished.color = new Color(loading_finished.color.r, loading_finished.color.g, loading_finished.color.b, ready_flash_curve.Evaluate(_fade_value));
            _fade_value = _fade_value >= 1 ? 0 : _fade_value + (Time.unscaledDeltaTime / 2);
		}
	}


	/* */
	protected override void onEnable() {
		Time.timeScale = 0;
		tips_n_hints.text = "";
		StartCoroutine(attachTip());
    }


	/* */
	private IEnumerator attachTip() {
		while(LevelManager.stage.CompareTo(active_stage) == 0 && tips_n_hints.text.CompareTo("") == 0) {
			tips_n_hints.text = LoadingScreenTipManager.getTip;
			yield return null;
		}
	}
}
