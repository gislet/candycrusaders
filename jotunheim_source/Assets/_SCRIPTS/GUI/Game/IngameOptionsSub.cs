﻿/***
 * 
 * IngameOptionsSub.cs
 * 
 * Handles handles the UI subcategory of the Outro stage. 
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using UnityEngine.UI;
using Controllers;
using System;


public class IngameOptionsSub : GameSubUI {
	

	/* */
	public override void actions() {
		base.actions();
	}


	/* */
	protected override void interact() {
		switch(index) {
		case 0:
			LevelManager.stage.change = IngameStage.OptionsGFX;
			break;
		case 1:
			LevelManager.stage.change = IngameStage.OptionsAudio;
			break;
		}	
	}
}
