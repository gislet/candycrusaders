﻿/***
 * 
 * OutroSub.cs
 * 
 * Handles handles the UI subcategory of the Outro stage. 
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using UnityEngine.UI;
using Controllers;
using System;

public class OutroSub : GameSubUI {


	public Image background;
	public Text game_over;
	public int final_font_size;
	public AudioClip finish_jingle, overlap_sound;
	private float _transition_time = 3f;
	private float _padded_wait_time = 2f;


	/* */
	public override void actions() {

	}


	/* */
	public override void run() {
		base.run();

		background.color = new Color(background.color.r, background.color.g, background.color.b, 1 - Time.timeScale);
		game_over.color = new Color(game_over.color.r, game_over.color.g, game_over.color.b, 1 - Time.timeScale);

		game_over.fontSize = Mathf.RoundToInt(final_font_size * (1 - (Time.timeScale + .001f)));

		if(Time.timeScale > 0) {
			Time.timeScale = Time.timeScale - (Time.unscaledDeltaTime / _transition_time) < 0 ? 0 : Time.timeScale - (Time.unscaledDeltaTime / _transition_time);
            return;
		}

		if(Globals.deltaTimeCountdown(ref _padded_wait_time) <= 0) {
			LevelManager.stage.change = IngameStage.GameOver;
			PlayerUIHandler.stop();
		}
	}


	/* */
	protected override void onEnable() {
		AudioManager.play(finish_jingle, AudioChannel.Effects);
		AudioManager.play(overlap_sound, AudioChannel.Effects);
	}
}
