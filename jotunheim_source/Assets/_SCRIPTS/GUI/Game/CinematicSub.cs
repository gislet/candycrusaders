﻿/***
 * 
 * CinematicSub.cs
 * 
 * Handles handles the UI subcategory of the Cinematic stage. 
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using UnityEngine.UI;
using Controllers;
using System;
using System.Collections.Generic;

public class CinematicSub : GameSubUI {


	public List<object> array;
	private float _time_elapsed = 0;



	/* */
	public override void actions() {
		if(Globals.deltaTimeCounter(ref _time_elapsed) > .5f) {
			if(Controls.getActiveDeviceID() != -1) {
				changeState();
			}
		}
	}


	/* */
	private void changeState() {
		CameraManager.isCamerasLocked = false;
		LevelManager.stage.change = IngameStage.Intro;
	}


	/* */
	public override void run() {
		base.run();

		if(!CinemaHandler.isPlaying) {
			changeState();
		}
	}


	/* */
	protected override void onEnable() {
		CameraManager.isCamerasLocked = true;
		CinemaHandler.play();
	}
}
