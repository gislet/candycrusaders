﻿/***
 * 
 * PlaySub.cs
 * 
 * Handles handles the UI subcategory of the Play stage. 
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using UnityEngine.UI;
using Controllers;
using System;

public class SplitscreenSub : GameSubUI {


	public GameObject vertical, horizontal, forth_player_block, errorscreen;
	private int _num_players = -1;


	/* Should only process player commands. */
	public override void actions() {
	}


	/* Should only process automated code. */
	public override void idle() {

		if(_num_players != PlayerHandler.numValidConfigs())
			update();
		
	}

	public override void run() {
	}


	/* Updates the UI. */
	private void update() {
		_num_players = PlayerHandler.numValidConfigs();

		errorscreen.SetActive(_num_players == 0);
        vertical.SetActive(_num_players > 1);
		horizontal.SetActive(_num_players > 2);
		forth_player_block.SetActive(_num_players == 3);

	}
}