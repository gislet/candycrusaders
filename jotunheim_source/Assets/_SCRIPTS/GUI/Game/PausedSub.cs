﻿/***
 * 
 * OutroSub.cs
 * 
 * Handles handles the UI subcategory of the Outro stage. 
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using UnityEngine.UI;
using Controllers;
using System;
using System.Collections.Generic;

public class PausedSub : GameSubUI {

	public Text player_who_paused;
	public GameObject leaderboard;

	/* */
	public override void actions() {
		PlayerUIHandler.stop();
		base.actions();
	}


	/* */
	protected override void interact() {
		switch(index) {
		case 0: //resume
			retreat();
			break;
		case 1: //options
			LevelManager.stage.change = IngameStage.Options;
			break;
		case 2: //controllers
			LevelManager.stage.change = IngameStage.Controllers;
			break;
		case 3: //quit
			LevelManager.stage.change = IngameStage.Quit;
			break;
		}
	}


	/* */
	protected override void retreat() {
		base.retreat();
		PlayerUIHandler.start();
		Controls.main_device.clear();
	}


	/* */
	public override void run() {
		base.run();

		if(!isActive || Controls.main_device == null)
			return;

		player_who_paused.text = PlayerHandler.retrieveConfigByControllerID(Controls.main_device.ID).player_name + " has paused";
		updateLeaderboard();
	}


	/* */
	private void updateLeaderboard() {
		List<PlayerConfiguration> podium_occupiers = new List<PlayerConfiguration>();

		for(int i = 0; i < Globals.MAX_PLAYERS; i++) {
			PlayerConfiguration PC = PlayerHandler.retrieveConfigByPlayerID(i);

			if(PC != null) {
				for(int g = 0; g <= podium_occupiers.Count; g++) {

					if(g == podium_occupiers.Count) {
						podium_occupiers.Add(PC);
						break;
					} else {

						int score = PC.team == Team.Teamless ? Scoring.character_score[i] : Scoring.team_score[(int)PC.team - 1];
						int other_score = podium_occupiers[g].team == Team.Teamless ? Scoring.character_score[podium_occupiers[g].player_ID] : Scoring.character_score[(int)podium_occupiers[g].team - 1];


						if(score > other_score) {
							podium_occupiers.Insert(g, PC);
							break;
						}
					}
				}
			}
		}

		for(int i = 0; i < 3; i++) {

			/* We don't want to display more leaderboard positions than there are players. */
			leaderboard.transform.GetChild(i).gameObject.SetActive(i < PlayerHandler.numValidConfigs());
			if(!leaderboard.transform.GetChild(i).gameObject.activeSelf)
				continue;

			/* Aquiring the text component and then formatting it with a players details. */
			Text t = leaderboard.transform.GetChild(i).GetChild(0).GetComponent<Text>();
			t.text = string.Format("<color=#{0}>{1}</color> ({2}/{3})", 
									ColorUtility.ToHtmlStringRGBA(GUIGlobals.getTeamColour(podium_occupiers[i].team)),
									podium_occupiers[i].player_name,
									podium_occupiers[i].team == Team.Teamless ? Scoring.character_score[podium_occupiers[i].player_ID] : Scoring.team_score[(int)podium_occupiers[i].team - 1], 
									GameSettings.current.goal);

        }
	}


	/* */
	protected override void onDisable() {
		leaderboard.SetActive(false);
	}


	/* */
	protected override void onEnable() {
		Time.timeScale = 0;
		leaderboard.SetActive(true);
	}
}