﻿/***
 * 
 * LoadingScreenTipManager.cs
 * 
 * Loads tips in from a text file and allows various systems to retrieve them
 * for whatever use.
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using System.Collections;


public class LoadingScreenTipManager : MonoBehaviour {


	public string[] tips;
	private static LoadingScreenTipManager _instance;


	/* Caches the static instance of the tip manager. */
	void Start () {
		_instance = this;
    }
	
	
	/* Retrieves a stored tip for the caller. */
	public static string getTip {
		get {
			if(_instance == null || _instance.tips == null)
				return null;

			return _instance.tips[Random.Range(0, _instance.tips.Length)];
		}
	}
}
