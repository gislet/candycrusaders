﻿/***
 * 
 * PlayHostSimpleSub.cs
 * 
 * Handles handles the UI subcategory of the Outro stage. 
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using UnityEngine.UI;
using Controllers;
using System;

public class PlayHostSimpleSub : MenuSubUI {


	public Image world_preview;
	public Text mode_tooltip, world_name;
	private int _num_settings = 2;


	/* */
	public override void actions() {
		base.actions();

		if(Controls.Down(Operation.Select))
			interact();

		if(index < _num_settings) {
			if(Controls.Down(Operation.Left))
				interact(false);
			else if(Controls.Down(Operation.Right))
				interact(true);
		}
	}


	/* */
	protected void interact(bool p_increment = true, int i = Globals.INVALID) {
		Text setting = null;
		int intermediate_index = i != Globals.INVALID ? i : index;

		if(intermediate_index < _num_settings) {
			AudioManager.play(GUIGlobals.ui_move, AudioChannel.Effects, _volume_scale);
			setting = button_group.transform.GetChild(intermediate_index).GetChild(0).GetComponent<Text>();
		}

		switch(intermediate_index) {
		case 0: //GameMode
			setting.text = GameSettings.changeGameMode(p_increment, i != Globals.INVALID);
			break;
		case 1: //Map
			setting.text = GameSettings.changeLevel(p_increment, i != Globals.INVALID); //LevelManager.getWorldName();
			break;
		case 2: //Advanced Settings
			break;
		case 3: //Continue to Character Selection
			LevelManager.stage.change = MenuStage.CharacterAndTeam;
			break;
		}
	}


	/* */
	public override void run() {
		base.run();

		/* Setting the World Preview Sprite. */
		//Debug.Log((world_preview.sprite != LevelManager.getWorldPreviewSprite()) + ", " + world_preview.sprite.name + " and " + LevelManager.getWorldPreviewSprite().name);

		if(world_preview.sprite != LevelManager.getWorldPreviewSprite()) {
			world_preview.sprite = LevelManager.getWorldPreviewSprite();
        }

		/* Setting the World Name. */
		/*if(world_name.text.CompareTo(LevelManager.getWorldName()) != 0) {
			world_name.text = LevelManager.getWorldName();
		}*/

		/* Setting the GameMode Tooltip. */
		if(mode_tooltip.text.CompareTo(LevelManager.getModeTooltip()) != 0) {
			mode_tooltip.text = LevelManager.getModeTooltip();
        }
	}


	/* */
	protected override void onEnable() {
		if(GameSettings.current == null)
			GameSettings.createGameSetting();

		for(int i = 0; i < _num_settings; i++)
			interact(false, i);

		index = 3;
	}
}