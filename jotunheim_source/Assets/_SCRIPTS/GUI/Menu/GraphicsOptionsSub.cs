﻿/***
 * 
 * OptionsSub.cs
 * 
 * sss
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using UnityEngine.UI;
using Controllers;
using System;
using System.Collections.Generic;

public class GraphicsOptionsSub : MenuSubUI {


	public GameObject tooltips;
	private int _num_settings = 7;
	private bool _previously_active = false;



	/* */
	public override void actions() {
		base.actions();

		if(Controls.Down(Operation.Select))
			interact();

		if(index < _num_settings) {
			if(Controls.Down(Operation.Left)) {
				AudioManager.play(GUIGlobals.ui_move, AudioChannel.Effects, _volume_scale);
				interact(false);
			} else if(Controls.Down(Operation.Right)) {
				AudioManager.play(GUIGlobals.ui_move, AudioChannel.Effects, _volume_scale);
				interact(true);
			}
		}
	}


	/* */
	protected void interact(bool p_increment = true, int i = Globals.INVALID) {
		Text setting = null;
		bool only_update = i != Globals.INVALID;
		int intermediate_index = only_update ? i : index;

		if(intermediate_index < _num_settings)
			setting = button_group.transform.GetChild(intermediate_index).GetChild(0).GetComponent<Text>();

		switch(intermediate_index) {
		case 0: //Preset
			setting.text = GraphicsPresets.changeSetting(GraphicsPresets.GraphicSetting.Preset, p_increment, only_update);
			break;
		case 1: //Resolution
			setting.text = GraphicsPresets.changeSetting(GraphicsPresets.GraphicSetting.Resolution, p_increment, only_update);
			break;
		case 2: //AA
			setting.text = GraphicsPresets.changeSetting(GraphicsPresets.GraphicSetting.Antialiasing, p_increment, only_update);
			break;
		case 3: //VSync
			setting.text = GraphicsPresets.changeSetting(GraphicsPresets.GraphicSetting.Vsync, p_increment, only_update);
			break;
		case 4: //Fullscreen
			setting.text = GraphicsPresets.changeSetting(GraphicsPresets.GraphicSetting.FullScreen, p_increment, only_update);
			break;
		case 5: //Showfps
			if(!only_update)
				GraphicsPresets.show_fps = !GraphicsPresets.show_fps;

			setting.text = GraphicsPresets.show_fps ? "On" : "Off";
			break;
		case 6: //attack visuals
			if(!only_update)
				DebugHandler.attack_visualiser = !DebugHandler.attack_visualiser;

			setting.text = DebugHandler.attack_visualiser ? "On" : "Off";
			break;
		}
	}


	/* */
	private void tooltipHandler() {
		for(int i = 0; i < tooltips.transform.childCount; i++)
			tooltips.transform.GetChild(i).gameObject.SetActive(index == i);
	}


	/* */
	public override void run() {
		base.run();

		tooltipHandler();
	}


	/* */
	protected override void onEnable() {
		for(int i = 0; i < _num_settings; i++)
			interact(false, i);
	}


	/* */
	protected override void onDisable() {
		GraphicsPresets.setGraphics();
	}
}