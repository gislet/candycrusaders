﻿/***
 * 
 * QuitSub.cs
 * 
 * sss
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using UnityEngine.UI;
using Controllers;
using System;
using System.Collections.Generic;


public class VersionSub : MenuSubUI {


	/* */
	public override void idle() {
		if(button_texts == null)
			cache();
		
		button_texts[0].text = string.Format("{0}\n{1}", Globals.game_version, Globals.version_release_date);
	}


	/* */
	public override bool isIdle {
		get { return LevelManager.isMainMenu();	}
	}
}