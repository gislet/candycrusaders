﻿/***
 * 
 * NavAssistanceSub.cs
 * 
 * This sub-ui is only and always idle during the menu-screen. It'll show the navigational buttons
 * for the currently active controller, or the current master-controller.
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using UnityEngine.UI;
using Controllers;
using System;
using System.Collections.Generic;


public class NavAssistanceSub : MenuSubUI {

	public GameObject offset_center;
	public Image confirm, cancel;
	public Image[] nav;
	private bool _isController = true;
	private bool _initialised = false;


	/* */
	public override void idle() {

		if(!_initialised) {
			_initialised = true;
			updateNavAssistance();
		}

		if(Controls.main_device.ID == Globals.INVALID) {
			if(Controls.getActiveDeviceID() > 0 && !_isController) {
				_isController = true;
				updateNavAssistance();
			} else if(Controls.getActiveDeviceID() == 0 && _isController) {
				_isController = false;
				updateNavAssistance();
			}
		} else {
			if(Controls.main_device.ID > 0 && !_isController) {
				_isController = true;
				updateNavAssistance();
			} else if(Controls.main_device.ID == 0 && _isController) {
				_isController = false;
				updateNavAssistance();
			}
		}

	}


	/* */
	private void updateNavAssistance() {

		if(_isController) {

			//offset_center.transform.localPosition = Vector3.zero;

			confirm.sprite = ButtonImageMaster.getImage(ControllerCode.A);
			cancel.sprite = ButtonImageMaster.getImage(ControllerCode.B);
			nav[0].sprite = ButtonImageMaster.getImage(ControllerCode.LeftAnalog);

			for(int i = 1; i < nav.Length; i++) {
				nav[i].color = nav[i].color * new Color(1, 1, 1, 0);
            }

		} else {

			//offset_center.transform.localPosition = Vector3.zero + (Vector3.right * 75);

			confirm.sprite = ButtonImageMaster.getImage(KeyboardCode.Enter);
			cancel.sprite = ButtonImageMaster.getImage(KeyboardCode.Escape);
			nav[0].sprite = ButtonImageMaster.getImage(KeyboardCode.D);

			for(int i = 1; i < nav.Length; i++) {
				nav[i].color = nav[i].color + new Color(0, 0, 0, .687f);
			}

		}

	}


	/* */
	public override bool isIdle {
		get { return LevelManager.isMainMenu() && LevelManager.stage.CompareTo(MenuStage.Credits) != 0;	}
	}
}