﻿/***
 * 
 * OutroSub.cs
 * 
 * Handles handles the UI subcategory of the Outro stage. 
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using UnityEngine.UI;
using Controllers;
using System;

public class MainplaySub : MenuSubUI {


	public Transform tooltips;


	/* */
	public override void actions() {
		base.actions();
	}


	/* */
	protected override void interact() {
		switch(index) {
		case 0: //HostSimple
			LevelManager.stage.change = MenuStage.HostSettingsSimple;
			break;
		case 1: //Find Game
			break;
		case 2: //QuickFind Game
			break;
		}
	}


	/* */
	public override void run() {
		base.run();

		for(int i = 0; i < tooltips.childCount; i++) {
			tooltips.GetChild(i).gameObject.SetActive(index == i);
		}
	}


	/* */
	protected override void onEnable() {
		GameSettings.destroySettings();
		PlayerHandler.reset();
	}
}
