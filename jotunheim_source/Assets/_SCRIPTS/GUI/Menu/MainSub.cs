﻿/***
 * 
 * OutroSub.cs
 * 
 * Handles handles the UI subcategory of the Outro stage. 
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using UnityEngine.UI;
using Controllers;
using System;

public class MainSub : MenuSubUI {


	/* */
	public override void actions() {
		base.actions();
	}


	/* */
	protected override void interact() {
		switch(index) {
		case 0: //Play
			Controls.main_device.set(Controls.getActiveDeviceID());
			LevelManager.stage.change = MenuStage.Play;
			break;
		case 1: //Options
			Controls.main_device.set(Controls.getActiveDeviceID());
			LevelManager.stage.change = MenuStage.Options;
			break;
		case 2: //Credits
			LevelManager.stage.change = MenuStage.Credits;
			break;
		case 3: //Quit
			LevelManager.stage.change = MenuStage.Quit;
			break;
		}
	}


	/* */
	protected override void onEnable() {
		Controls.main_device.clear();
	}
}
