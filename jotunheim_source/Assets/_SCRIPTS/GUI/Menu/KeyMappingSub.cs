﻿/***
 * 
 * OptionsSub.cs
 * 
 * ddd
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using UnityEngine.UI;
using Controllers;
using System;
using System.Collections.Generic;


public class KeyMappingSub : MenuSubUI {


	public GameObject xbox_controller, keyboard;


	/* */
	public override void actions() {
		base.actions();
	}


	/* */
	public override void run() {
		xbox_controller.SetActive(index == 0);
		keyboard.SetActive(index == 1);
	}
}