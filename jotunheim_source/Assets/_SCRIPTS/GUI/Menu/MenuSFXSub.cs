﻿/******************************************************************************
*
* MenuSFXSub.cs
*
* asdf
*
*
* Written by: Gisle Thorsen (October, 2017)
*
******************************************************************************/


using UnityEngine;
using Controllers;
using Configurations;


public class MenuSFXSub : MenuSubUI {


	/* */
	private string[] _setable_settings = {
		"volume_master",
		"volume_music",
		"volume_ambient",
		"volume_effects",
	};

	private string[] _togglable_settings = {
		"volume_master_enabled",
		"volume_music_enabled",
		"volume_ambient_enabled",
		"volume_effects_enabled",
	};



	/* */
	public override void actions() {
		base.actions();
		
		if(Controls.Hold(Operation.Left))
			interact(-1);
		else if(Controls.Hold(Operation.Right))
			interact(1);
	}


	/* */
	protected void interact(int p_dir) {
		float speed = 50f;
		
		if(_setable_settings.Length > index) {
			string str_value = Settings.interact(_setable_settings[index], ConfigAction.Value);

			if(str_value != null) {
				Settings.interact(_setable_settings[index],
										ConfigAction.Set,
										(float.Parse(str_value) + (Time.unscaledDeltaTime * speed * p_dir)).ToString());
				AudioManager.setDirty();
			}
		}
	}


	/* */
	protected override void interact() {
		if(_togglable_settings.Length > index) {
			Settings.interact(_togglable_settings[index], ConfigAction.Toggle);
			AudioManager.setDirty();
		}
	}
	
	
	/* */
	protected override void onDisable() {
		Settings.save();
		AudioManager.setDirty();
	}
}