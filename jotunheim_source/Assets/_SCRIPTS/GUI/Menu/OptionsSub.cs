﻿/***
 * 
 * OptionsSub.cs
 * 
 * sss
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using UnityEngine.UI;
using Controllers;
using System;
using System.Collections.Generic;

public class OptionsSub : MenuSubUI {


	public Transform tooltips;


	/* */
	public override void actions() {
		base.actions();
	}


	/* */
	protected override void interact() {
		switch(index) {
		case 0: //Graphics
			LevelManager.stage.change = MenuStage.Graphics;
			break;
		case 1: //Audio
			LevelManager.stage.change = MenuStage.Audio;
			break;
		case 2: //Controls
			LevelManager.stage.change = MenuStage.KeyMapping;
			break;
		}
	}


	/* */
	public override void run() {
		base.run();

		for(int i = 0; i < tooltips.childCount; i++) {
			tooltips.GetChild(i).gameObject.SetActive(index == i);
		}
	}
}