﻿/***
 * 
 * CharacterSelectSub.cs
 * 
 * Hss. 
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using UnityEngine.UI;
using Controllers;
using System.Collections.Generic;
using System.Collections;
using Customisation;

public class CharacterSelectSub : MenuSubUI {

	public Text status;
	private bool[] _ready;
	private bool refresh_settings = false;
    private StartGameStage _start_stage;
	private Text[][] _buttons;
	private Image[] _team_ribbions;
	private GameObject[] _viewport;
	private GameObject[] _viewport_inactive;
	private int[] _indexes;


	/* */
	public override void actions() {
		/*if(_buttons == null) {
			cache();
		}*/

		if(PlayerHandler.numValidConfigs() <= 0)
			return;

		for(int i = 0; i < _buttons.Length; i++) {

			int ctrl_id;
            PlayerConfiguration PC = PlayerHandler.retrieveConfigByIndex(i);

			if(PC == null) {
				_indexes[i] = 0;
				continue;
			} else {
				ctrl_id = PC.controller_ID;
			}


			/* Allows the player to interact with the current button they're hovering over. */
			if(Controls.Down(Operation.Select, ctrl_id))
				interact(ctrl_id, i);			

			/* Allowing each individual player navigate their own customisation field. */
			if(!_ready[i]) {
				/* Attempts to backtrack to the previous page. */
				if(Controls.Down(Operation.Back, ctrl_id))
					retreat(ctrl_id, i);

				if(Controls.Down(Operation.Up, ctrl_id)) {
					AudioManager.play(GUIGlobals.ui_move, AudioChannel.Effects, _volume_scale);
					_indexes[i] = Globals.changeEnumIndex(_indexes[i], _buttons[i].Length, false);
				}

				if(Controls.Down(Operation.Down, ctrl_id)) {
					AudioManager.play(GUIGlobals.ui_move, AudioChannel.Effects, _volume_scale);
					_indexes[i] = Globals.changeEnumIndex(_indexes[i], _buttons[i].Length, true);
				}

				/* We only want to allow players to ready up by hitting the select key. */
				if(_indexes[i] != _buttons[i].Length - 1) {
					if(Controls.Down(Operation.Left, ctrl_id))
						interact(ctrl_id, i, false);
					if(Controls.Down(Operation.Right, ctrl_id))
						interact(ctrl_id, i, true);
				}
			}
		}
	}


	/* */
	/*protected void cache() {
		_buttons = new Text[Globals.MAX_PLAYERS][];
		_indexes = new int[Globals.MAX_PLAYERS];
		_viewport = new GameObject[Globals.MAX_PLAYERS];
		_viewport_inactive = new GameObject[Globals.MAX_PLAYERS];
		_ready = new bool[Globals.MAX_PLAYERS];
		_team_ribbions = new Image[Globals.MAX_PLAYERS];
        Globals.setBoolArray(_ready, false);

		for(int i = 0; i < _buttons.Length; i++) {
			Transform trans = button_group.transform.GetChild(i).GetChild(0).GetChild(0);
			_buttons[i] = new Text[4];

			_team_ribbions[i] = button_group.transform.GetChild(i).GetChild(0).GetChild(1).GetChild(0).GetComponent<Image>();
			_viewport[i] = button_group.transform.GetChild(i).GetChild(0).gameObject;
			_viewport_inactive[i] = button_group.transform.GetChild(i).GetChild(3).gameObject;


			for(int g = 0; g < _buttons[i].Length; g++) {
				_buttons[i][g] = trans.GetChild(g).GetComponent<Text>();
			}
		}
	}*/


	/* */
	protected void interact(int p_device_id, int i, bool p_increment = true) {

		if(_indexes[i] < 3) {
			AudioManager.play(GUIGlobals.ui_move, AudioChannel.Effects, _volume_scale);
		} else {
			if(!_ready[i]) {
				AudioManager.play(GUIGlobals.ui_confirm_special, AudioChannel.Effects, _volume_scale);
			} else {
				AudioManager.play(GUIGlobals.ui_cancel, AudioChannel.Effects, _volume_scale);
			}
		}

		switch(_indexes[i]) {
		case 0: //Team Selection
			changeTeam(i, p_increment);
            _buttons[i][_indexes[i]].text = getTeamName(PlayerHandler.retrieveConfigByIndex(i).team);
			_team_ribbions[i].color = GUIGlobals.getTeamColour(PlayerHandler.retrieveConfigByIndex(i).team);
			break;
		case 1: //Skin Selection
			_buttons[i][_indexes[i]].text = changeSkin(i, p_increment);
			break;
		case 2: //Hat Selection
			break;
		case 3: //Ready
			_ready[i] = !_ready[i];
			break;
		}

	}


	/* */
	protected void retreat(int p_device_id, int i) {
		if(p_device_id == Controls.main_device.ID && !_ready[i]) {
			AudioManager.play(GUIGlobals.ui_cancel, AudioChannel.Effects, _volume_scale);
			LevelManager.stage.change = MenuStage.HostSettingsSimple;
			refresh_settings = true;
		}
	}


	/* */
	public override void run() {
		base.run();

		if(_start_stage == StartGameStage.Starting) {
			LevelManager.loadGame();
			return;
		} else if(_start_stage == StartGameStage.Stopping) {
			_start_stage = StartGameStage.None;
			return;
		}

		/*if(PlayerHandler.numValidConfigs() <= 0 || (refresh_settings && LevelManager.stage.CompareTo(active_stage) == 0)) {
			updateCustomisations(Controls.main_device.ID, true);
			refresh_settings = false;
        }*/

		if(listenForDevices())
			return;
		statusText();

		for(int i = 0; i < _buttons.Length; i++) {
			PlayerConfiguration PC = PlayerHandler.retrieveConfigByIndex(i);

			if(PC == null) {
				_viewport[i].SetActive(false);
				_viewport_inactive[i].SetActive(true);
			} else {
				_viewport[i].SetActive(true);
				_viewport_inactive[i].SetActive(false);
			}

			for(int g = 0; g < _buttons[i].Length; g++) {
				if(g == _indexes[i]) {
					if((g == _buttons[i].Length - 1) && _ready[i])
						GUIGlobals.changeTextColour(ref _buttons[i][g], GUIGlobals.CachedColor.Idle, GUIGlobals.CachedColor.Ready, animation_behaviour, animation_speed);
					else
						GUIGlobals.changeTextColour(ref _buttons[i][g], GUIGlobals.CachedColor.Idle, GUIGlobals.CachedColor.Hover, animation_behaviour, animation_speed);
				} else {
					if((g == _buttons[i].Length - 1) && _ready[i])
						GUIGlobals.changeTextColour(ref _buttons[i][g], GUIGlobals.CachedColor.Ready, GUIGlobals.CachedColor.Ready, GUIGlobals.ChangeColor.None, 1);
					else
						GUIGlobals.changeTextColour(ref _buttons[i][g], GUIGlobals.CachedColor.Hover, GUIGlobals.CachedColor.Idle, GUIGlobals.ChangeColor.None, 1);
				}
			}

		}

		
	}


	/* */
	protected override void onEnable() {

		/* Caching all the buttons, viewports and other vital objects. */
		_buttons = new Text[Globals.MAX_PLAYERS][];
		_indexes = new int[Globals.MAX_PLAYERS];
		_viewport = new GameObject[Globals.MAX_PLAYERS];
		_viewport_inactive = new GameObject[Globals.MAX_PLAYERS];
		_ready = new bool[Globals.MAX_PLAYERS];
		_team_ribbions = new Image[Globals.MAX_PLAYERS];
		Globals.setBoolArray(_ready, false);

		/* Making sure that the play configs are following gamemode rules. */
		updateCustomisations(Controls.main_device.ID, true);

		for(int i = 0; i < _buttons.Length; i++) {
			Transform trans = button_group.transform.GetChild(i).GetChild(0).GetChild(0);
			_buttons[i] = new Text[4];
			
			_team_ribbions[i] = button_group.transform.GetChild(i).GetChild(0).GetChild(1).GetChild(0).GetComponent<Image>();
			_viewport[i] = button_group.transform.GetChild(i).GetChild(0).gameObject;
			_viewport_inactive[i] = button_group.transform.GetChild(i).GetChild(3).gameObject;

			for(int g = 0; g < _buttons[i].Length; g++) {
				_buttons[i][g] = trans.GetChild(g).GetComponent<Text>();
			}

			if(PlayerHandler.retrieveConfigByIndex(i) != null) {
				PlayerConfiguration PC = PlayerHandler.retrieveConfigByIndex(i);

				_team_ribbions[i].color = GUIGlobals.getTeamColour(PC.team);
				_buttons[i][0].text = "TEAM: " + getTeamName(PC.team).ToUpper();
			}
		}

		for(int i = 0; i < _indexes.Length; i++) {
			_indexes[i] = 3;
		}
	}


	/* */
	private string changeSkin(int p_player_index, bool p_left_to_right, bool p_getcurrent = false) {
		PlayerConfiguration PC = PlayerHandler.retrieveConfigByIndex(p_player_index);

		if(!p_getcurrent) {
			PC.customisation.skin_material =
				PlayerCustomisation.change(PC.customisation.skin_material, CustomisationType.SkinMaterial, p_left_to_right);
		}

		return "SKIN: " + PlayerCustomisation.getName(PC.customisation.skin_material, CustomisationType.SkinMaterial).ToUpper();
	}


	/* */
	private void changeTeam(int p_player_index, bool p_left_to_right, bool p_getcurrent = false) {
		PlayerConfiguration PC = PlayerHandler.retrieveConfigByIndex(p_player_index);
		
		List<Team> banned_teams = new List<Team>();

		Team team = PC.team;


		switch(GameSettings.current._game_mode) {
		case GameSettings.GameMode.CaptureTheFlag:
			banned_teams = new List<Team>() { Team.Yellow, Team.Green, Team.Teamless };
			break;
		case GameSettings.GameMode.FreeForAll:
			banned_teams = new List<Team>() { Team.Yellow, Team.Green, Team.Blue, Team.Red };
			break;
		case GameSettings.GameMode.ResourceRace:
			banned_teams = new List<Team>() { Team.Yellow, Team.Green, Team.Blue, Team.Red };
			break;
		}


		if(!p_getcurrent || banned_teams.Contains(team)) {
			for(int i = 0; i <= (int)Team.length; i++) {
				team = (Team)Globals.changeEnumIndex((int)team, (int)Team.length, p_left_to_right);
				if(!banned_teams.Contains(team))
					break;
			}
		}

		PC.team = team;
	}


	public string getTeamName(Team p_team) {
		switch(p_team) {
		case Team.Blue:
			return "Blue";
		case Team.Red:
			return "Red";
		case Team.Yellow:
			return "Yellow";
		case Team.Green:
			return "Green";
		case Team.Teamless:
			return "Lone Wolf";
		default:
			return "<ERROR: TEAM NAME>";
		}
	}


	/* Listening for devices that are trying to join the game. */
	private bool listenForDevices() {
		int device_id = Globals.INVALID;
		if((device_id = Controls.getActiveDeviceID()) > Globals.INVALID) {
			PlayerConfiguration PC = PlayerHandler.retrieveConfigByControllerID(device_id);

			/* If the config with the given device ID doesn't exist, we try to create the player. */
			if(PC == null) {

				/* We will only create a new config if the key pressed was not the back key, which
				 * is usually associated with deleting a config. */
				if(!Controls.Hold(Operation.Back, device_id)) {
					updateCustomisations(device_id);
					Controls.SimulateButtonDown(Operation.Select, device_id);
					return true;
				}

			/* If the config already exists, then maybe there was an attempt at deleting the config? Or maybe
			 * just un-readying themselves. */
			} else {

				/* If the player hasn't marked themselves as ready, and the key pressed was the back key. The config associated
				 * which this device is removed/deleted. */
				if(!_ready[PC.player_ID] && device_id != Controls.main_device.ID && Controls.Down(Operation.Back, device_id)) {
					PlayerHandler.removePlayerByIndex(PC.player_ID);
					return true;

				/* If the player was marked ready, and pressed the back key. The player is simply marked as not ready. */
				} else if(_ready[PC.player_ID] && Controls.Down(Operation.Back, device_id)) {
					_ready[PC.player_ID] = false;
					return true;
				}
			}
		}

		return false;
	}


	/* */
	private void updateCustomisations(int p_device_id, bool p_update_all = false) {
		PlayerConfiguration PC = PlayerHandler.retrieveConfigByControllerID(p_device_id);

		if(PC == null)
			PlayerHandler.createPlayer(p_device_id);

		PC = PlayerHandler.retrieveConfigByControllerID(p_device_id);
		changeTeam(PC.player_ID, true, true);

		if(p_update_all) {
			for(int i = 0; i < Globals.MAX_PLAYERS; i++) {
				if(p_device_id != i && PlayerHandler.retrieveConfigByIndex(i) != null) {
					updateCustomisations(PlayerHandler.retrieveConfigByIndex(i).controller_ID);
				}
			}
		}
	}


	/* */
	private bool everyActivePlayerReady() {
		for(int i = 0; i < Globals.MAX_PLAYERS; i++) {
			if(PlayerHandler.retrieveConfigByIndex(i) != null) {
				if(!_ready[i])
					return false;
			}
		}

		return true;
	}


	/* */
	private void statusText() {
		List<string> players = new List<string>();
		string not_ready = "";


		for(int i = 0; i < Globals.MAX_PLAYERS; i++) {
			PlayerConfiguration PC = PlayerHandler.retrieveConfigByIndex(i);

			if(PC != null) {
				if(!_ready[i])
					players.Add(PC.player_name);
			}
		}

		if(players.Count > 0) {
			for(int i = 0; i < players.Count; i++) {
				not_ready += players[i];

				if(i + 1 >= players.Count) {
					continue;
				} else if(i + 2 >= players.Count) {
					not_ready += ", and ";
				} else if(i + 3 >= players.Count) {
					not_ready += ", ";
				}
			}

			status.text = string.Format("Waiting for {0} to ready up...", not_ready);
			status.text.ToUpper();
		} else {
			if(_start_stage == StartGameStage.None)
				StartCoroutine(startGame());
		}
	}


	/* */
	private IEnumerator startGame() {
		_start_stage = StartGameStage.Counting;
		float countdown = 5;

		while(true) {
			countdown -= Time.deltaTime;
			if(countdown < 0)
				countdown = 0;

			if(!everyActivePlayerReady()) {
				_start_stage = StartGameStage.Stopping;
			}

			status.text = string.Format("GAME STARTS IN ... {0}", countdown.ToString("0.0"));
			status.text.ToUpper();

			if(countdown == 0 || _start_stage == StartGameStage.Stopping) {
				break;
			}
			yield return null;
		}

		if(_start_stage == StartGameStage.Counting)
			_start_stage = StartGameStage.Starting;
	}


	/* */
	private enum StartGameStage {
		None, Stopping, Counting, Starting
	}
}
