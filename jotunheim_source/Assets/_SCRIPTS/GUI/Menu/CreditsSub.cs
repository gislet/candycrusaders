﻿/***
 * 
 * CreditsSub.cs
 * 
 * sss
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using UnityEngine.UI;
using Controllers;
using System;
using System.Collections.Generic;

public class CreditsSub : MenuSubUI {


	public GameObject credits, stop_object, mask_object;
	public float y_start_value;
	public float scroll_speed = 50f;

	private bool _previously_active = false;


	/* */
	public override void actions() {
		base.actions();
	}


	/* */
	protected override void interact() {
	}


	/* */
	public override void run() {
		base.run();

		if(stop_object.transform.position.y < transform.position.y)
			credits.transform.localPosition += Vector3.up * scroll_speed * Time.deltaTime;
    }
	

	/* */
	protected override void onEnable() {
		mask_object.GetComponent<Mask>().enabled = true;
		credits.transform.localPosition = new Vector3(0, y_start_value, 0);
	}
}