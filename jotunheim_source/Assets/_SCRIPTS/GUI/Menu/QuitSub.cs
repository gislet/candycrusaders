﻿/***
 * 
 * QuitSub.cs
 * 
 * sss
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using UnityEngine.UI;
using Controllers;
using System;
using System.Collections.Generic;

public class QuitSub : MenuSubUI {


	/* */
	public override void actions() {
		base.actions();
	}


	/* */
	protected override void interact() {
		switch(index) {
		case 0: //Cancel
			retreat();
			break;
		case 1: //Quit
			Application.Quit();
			break;
		}
	}


	/* */
	protected override void retreat() {
		base.retreat();
		index = 0;
	}
}
