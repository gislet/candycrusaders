﻿/***
 * 
 * SubUI.cs
 * 
 * This abstract class serves to set some ambigious enum variables that we want to overwrite later,
 * it also adds in all the interface functions that can't be used until the enum variables have been
 * assigned a particular enum type.
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using System;
using Controllers;
using UnityEngine.UI;

public abstract class SubUI : MonoBehaviour, IUISub {


	/* Setting a couple of ambigious enums that requires to be overwritten down the line. */
	public Enum active_stage, return_stage;
	public Enum[] idle_stages;

	public Navigation navigation;
	public bool suppress_navigation_sounds = false;


	[Tooltip("All animations takes a second to complete, increasing the speed makes the animation go faster.")]
	[Range(.25f,10)]
	public float animation_speed = 1;
	public GUIGlobals.ChangeColor animation_behaviour;


	[Tooltip("If true, will case the screen to fade to black when transitioning away from this UI to another.")]
	public bool fade_to_black = false;
	public GameObject button_group;
	protected Text[] button_texts;


	protected float _volume_scale = .25f;



	/// <summary>
	/// Base actions will auto cache the button group. It will allow you to navigate the buttons as directed (horizontally/vertically),
	/// <para/>it will allow you to interact with the buttons when pressing the designated select key. Lastly it will also call the retreat() function
	/// <para/>which usually sets the game-state to the designated return state. Remember to include base.actions() if you wish to keep this functionality
	/// <para/>when overriding.
	/// </summary>
	public virtual void actions() {

		if(button_group != null) {
			if(button_texts == null) {
				cache();
			}

			if(Controls.Down(navigation == Navigation.Horizontal ? Operation.Left : Operation.Up)) {
				if(!suppress_navigation_sounds)
					AudioManager.play(GUIGlobals.ui_move, AudioChannel.Effects, _volume_scale);
				index = Globals.changeEnumIndex(index, button_group.transform.childCount, false);
			}

			if(Controls.Down(navigation == Navigation.Horizontal ? Operation.Right : Operation.Down)) {
				if(!suppress_navigation_sounds)
					AudioManager.play(GUIGlobals.ui_move, AudioChannel.Effects, _volume_scale);
				index = Globals.changeEnumIndex(index, button_group.transform.childCount, true);
			}

			animations();
		}

		if(Controls.Down(Operation.Select)) {
			if(!suppress_navigation_sounds)
				AudioManager.play(GUIGlobals.ui_confirm, AudioChannel.Effects, _volume_scale);
			interact();
		}

		if(Controls.Down(Operation.Back)) {
			if(!suppress_navigation_sounds)
				AudioManager.play(GUIGlobals.ui_cancel, AudioChannel.Effects, _volume_scale);
			retreat();
		}

	}


	/* */
	protected virtual void cache() {
		button_texts = new Text[button_group.transform.childCount];

		for(int i = 0; i < button_texts.Length; i++) {
			button_texts[i] = button_group.transform.GetChild(i).GetComponent<Text>();
		}
	}


	/* */
	private void animations() {
		for(int i = 0; i < button_texts.Length; i++) {
			if(index == i) {
				GUIGlobals.changeTextColour(ref button_texts[i],
											GUIGlobals.CachedColor.Idle, GUIGlobals.CachedColor.Hover,
											animation_behaviour, animation_speed);
			} else {
				GUIGlobals.changeTextColour(ref button_texts[i],
											GUIGlobals.CachedColor.Hover, GUIGlobals.CachedColor.Idle,
											GUIGlobals.ChangeColor.None, animation_speed);
			}
		}
	}


	/* Keeping the run() method abstract to force an implementation at the end. */
	public abstract void run();
	public virtual void idle() { }
	protected virtual void interact() { }
	protected virtual void retreat() { }
	protected abstract void onDisable();
	protected abstract void onEnable();


	/* */
	/*protected bool hasActiveStage(object p_stages) {
		if(p_stages != null) {
			//foreach(object stage in p_stages) {
				if(LevelManager.stage.CompareTo(p_stages) == 0)
					return true;
			//}
		}

		return false;
	}*/



	#region Getters & Setters
	protected int index { get; set;	}
	public bool fadeToBlack { get { return fade_to_black; } }
	public abstract bool isActive { get; }
	public abstract bool isIdle { get; }
	#endregion


	public enum Navigation {
		Horizontal, Vertical
	}
}
