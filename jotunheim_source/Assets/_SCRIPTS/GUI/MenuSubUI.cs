﻿/***
 * 
 * MenuSubUI.cs
 * 
 * This intermediate abstract class serves to set the ambigious enum variables to a certain type,
 * and to avoid having general methods such as isActive and isIdle rewritten several places.
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/

using System;
using UnityEngine;

public abstract class MenuSubUI : SubUI {


	/* Setting the ambigious enums to a certain type. */
	public new MenuStage active_stage, return_stage;
	public new MenuStage[] idle_stages;


	private bool _active_last_frame = false;



	/* */
	protected override void retreat() {
		if((int)return_stage > 0)
			LevelManager.stage.change = return_stage;
	}


	/* */
	public override void run() {
		if(!_active_last_frame) {
			_active_last_frame = true;
			onEnable();
		} else if(!isActive) {
			_active_last_frame = false;
			onDisable();
		}
	}


	/* Returns true if the current stage in LevelManager is the same as our active stage. */
	public override bool isActive {
		get {
			return (LevelManager.stage.CompareTo(active_stage) == 0);
		}
	}


	/* Returns true if the current stage in LevelManager is not the same as our active stage and
	 * one of our idle stages is. */
	public override bool isIdle {
		get {
			if(!isActive) {
				foreach(MenuStage stage in idle_stages) {
					if(LevelManager.stage.CompareTo(stage) == 0)
						return true;
				}
			}

			return false;
		}
	}


	/* */
	protected override void onDisable() { }


	/* */
	protected override void onEnable() { }
}