﻿/***
 * 
 * LevelStage.cs
 * 
 * Handles the stage of our current level. Allows the system to change it directly and compare it with
 * other enum values. 
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using System;


public class LevelStage : IComparable {

	public Enum _enum;


	/* Instantiates the Stage. */
	public LevelStage(Enum p_enum) {
		if(p_enum == null)
			throw new ArgumentNullException();

		_enum = p_enum;
	}


	/* Receives an enum value that we change to, if of the same type as our current enum. */
	public Enum change {
		set {
			if(value.GetType() != _enum.GetType())
				throw new InvalidCastException();

			_enum = value;
		}
	}


	/* Returns the current value of our enum. */
	public Enum current {
		get {
			return _enum;
		}
	}


	/* Compares the received enum to our current enum. If of the same type and of the same
	 * value, we return 0. */
	public int CompareTo(object obj) {

		if(obj != null) {
			if(obj.GetType() == _enum.GetType()) {
				if((int)obj == Convert.ToInt32(current)) {
					return 0;
				}
			}
		}

		return -1;
	}
}
