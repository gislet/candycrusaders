﻿/***
 * 
 * CameraFollow.cs
 * 
 * asdf
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public abstract class FollowPlayer : MonoBehaviour {


	/* */
	public PlayerID player_of_interest;


	/* */
	public FollowPlayer() {
		target = null;
		onEnable();
	}


	/* */
	private void FixedUpdate() {
		updateTarget();
		follow();
		updateDisplay();
	}


	/* */
	public void Update() {
		if(target == null) {
			transform.position = Vector3.up * -1000f;
		}
	}


	/* Finds a valid target to lock on to. */
	private void updateTarget() {
		if(target != null ? target.isDestroyed() : false) {
			onDestroyedTarget();
			target = null;
			character = null;
        }

		if(player_of_interest != PlayerID.unspecified) {
			if(target != null ? target.isType(typeof(CharacterBase)) : false) {
				return;
			} else {
				if(PlayerHandler.characters != null) {
					foreach(CharacterBase CB in PlayerHandler.characters) {
						if(CB.player_ID == (int)player_of_interest) {
							character = CB;
                            target = character;
							onNewTarget();
						}
					}
				}
			}
		}
	}


	/* If we've got a cached target, we follow it. */
	private void follow() {
		if(target != null) {
			transform.position = target.transform.position;
		} else {
			transform.position = Vector3.up * -1000f;
		}
	}


	/* */
	private void updateDisplay() {
		if(target != null) {
			onUpdate();
		}
	}


	/* */
	protected abstract void onDestroyedTarget();
	protected abstract void onNewTarget();
	protected abstract void onEnable();
	protected abstract void onUpdate();


	#region Getters & Setters
	protected ITargetable target { get; private set; }
	protected CharacterBase character { get; private set; }
	#endregion
}
