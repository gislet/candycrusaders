﻿/***
 * 
 * CameraFollow.cs
 * 
 * asdf
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/
 
 
using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;


public class HealthHUDFollow : FollowPlayer {

	public Sprite active, inactive;
	
	private int _previous_health_value = Globals.INVALID;


	/* */
	protected override void onDestroyedTarget() {
		_previous_health_value = Globals.INVALID;
	}


	/* */
	protected override void onEnable() {
		_previous_health_value = Globals.INVALID;
	}


	/* */
	protected override void onNewTarget() {
		_health = target.transform.GetComponent<Health>();
	}


	/* */
	protected override void onUpdate() {
		if(_previous_health_value != _health.value) {
			_previous_health_value = _health.value;

			Transform health_hud = transform.GetChild(0);

			for(int i = 0; i < Globals.MAX_LIVES; i++) {
				if(_previous_health_value < i + 1) {
					health_hud.GetChild(i).GetComponent<Image>().sprite = inactive;
				} else {
					health_hud.GetChild(i).GetComponent<Image>().sprite = active;
				}
			}
		}
	}


	#region Getters & Setters
	private Health _health { get; set; }
	#endregion
}
