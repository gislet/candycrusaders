﻿/***
 * 
 * CaptureTheFlag.cs
 * 
 * sss. 
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using StatusEffects;


[RequireComponent(typeof(AttachedStatusEffect))]
public class CaptureTheFlag : MonoBehaviour {


	//public bool use_carrier_effect = false;
	//public PowerUp carrier_effect;
	public AudioClip captured, stolen;
	public GameObject flag_blue, flag_red, flag_yellow, flag_green;
	private GameFlag[] _game_flags;
	private static CaptureTheFlag _instance;


	/* */
	private void Awake() {
		setupFlags();
		_instance = this;		
    }


	/* Takes the given flag gameobjects and stores them in their class containers. */
	private void setupFlags() {
		List<GameFlag> list_flags = new List<GameFlag>();

		for(int i = 1; i < (int)Team.length; i++) {

			GameObject flag_object = null;

			switch((Team)i) {
			case Team.Blue:
				flag_object = flag_blue != null ? flag_blue : null;
                break;
			case Team.Green:
				flag_object = flag_green != null ? flag_green : null;
				break;
			case Team.Red:
				flag_object = flag_red != null ? flag_red : null;
				break;
			case Team.Yellow:
				flag_object = flag_yellow != null ? flag_yellow : null;
				break;
			default:
				Debug.Log("Team not implemented for this gamemode. Don't have a flag to cache.");
				break;
			}

			if(flag_object != null)
				list_flags.Add(new GameFlag(flag_object.transform, (Team)i, GetComponent<AttachedStatusEffect>().status_effect));

		}

		_game_flags = list_flags.ToArray();

		/* If a scene is actually using this object, we want to have flags to populate it with. */
		if(_game_flags.Length <= 0)
			throw new UnassignedReferenceException();
	}


	/* */
	private void LateUpdate() {
		if(_game_flags != null && _game_flags.Length > 0) {
			foreach(GameFlag flag in _game_flags) {

				flag.evaluate();

				/* No need to compare this flag against other flags if this flag is not at its base. */
				if(flag.isHome) {
					foreach(GameFlag other in _game_flags) {

						/* We want to do heaps of checks, but we're not interested in doing these if the
						 * two flags are the same flag. */
						if(flag.transform != other.transform) {

							flag.capture(other);

						}
					}
				}
			}
		}
	}


	/* Flag Container. */
	private class GameFlag : GoalObject {

		/* */
		public GameFlag(Transform p_transform, Team p_team, Func<StatusEffect> p_carrier_effect = null) : base(p_transform, p_team, p_carrier_effect) {

		}


		protected override void interact(CharacterBase p_character) {

			if(!distanceCheck(p_character.transform)) {
				return;
			}

			if(carrier == null) {				

				/* A friendly interaction returns the flag to the base. */
				if(p_character.team == team) {
					returnToBase();

				/* Any other character is set as the flags carrier. */
				} else {
					if(isHome)
						AudioManager.play(sfx_stolen, AudioChannel.Effects);

					carrier = p_character;
					p_character.attack_event += onAttack;
					isHome = false;
				}

				/* Unsubs every character that was previously subbed to the interact delegate. */
				foreach(CharacterBase character in _subbed_characters) {
					character.interact_event -= interact;
				}

				_subbed_characters = new List<CharacterBase>();
			}
		}


		/* */
		public void capture(GameFlag p_other) {
			if(!p_other.isHome) {
				if(distanceCheck(p_other.transform)) {
					AudioManager.play(sfx_captured, AudioChannel.Effects);

					Scoring.collect(p_other.carrier, Scoring.ScoringMethod.Goal);
					p_other.returnToBase();
                }
			}
		}		
	}


	#region Getters & Setters
	public static AudioClip sfx_captured { get { return _instance.captured; } }
	public static AudioClip sfx_stolen { get { return _instance.stolen; } }
	#endregion
}


/* */
public abstract class GoalObject {

	public Transform transform;

	public Vector3 home_position;
	public Quaternion home_rotation;
	protected Func<StatusEffect> _carrier_effect;
	
	
	/* */
	public GoalObject(Transform p_transform, Team p_team, Func<StatusEffect> p_carrier_effect = null) {
		transform = p_transform;

		home_position = transform.position;
		home_rotation = transform.rotation;

		team = p_team;
		isHome = true;

		interaction_distance = 2.5f;

		_carrier_effect = p_carrier_effect;
		carrier_attacking_allowed = false;
		sameteam_carrier_allowed = false;
		use_lerp_follow = false;
		lerp_dynamic_speed = true;
		lerp_follow_speed = 1;

		_subbed_characters = new List<CharacterBase>();
    }


	/* */
	public virtual void evaluate() {

		/* If we have a carrier, we want to make sure that we're both following this character and
		 * that the character is alive. */
		if(carrier != null) {

			if(carrier.isDestroyed()) {
				carrierDetachment();
			} else {
				if(_carrier_effect != null)
					carrier.applyStatusEffect(_carrier_effect());

				if(use_lerp_follow) {
					float speed = lerp_dynamic_speed ? Mathf.Pow(1.25f, Vector3.Distance(carrier.transform.position, transform.position)) : lerp_follow_speed;

					if(!distanceCheck(carrier.transform, lerp_distance_commence))
						transform.position = Vector3.Lerp(transform.position, carrier.position, speed * Time.deltaTime);

					transform.LookAt(carrier.transform);
				} else {
					transform.position = carrier.position;
					transform.rotation = carrier.rotation;
				}
			}

			/* If we don't have a carrier, we want to add this flag as an interactable to all eligible characters. */
		} else {
			if(PlayerHandler.characters != null) {
				foreach(CharacterBase character in PlayerHandler.characters) {
					bool is_within_distance = distanceCheck(character.transform);
					bool is_subbed = _subbed_characters.Contains(character);

					/* Removes subbed characters that aren't within distance anymore. */
					if(is_subbed && !is_within_distance) {
						_subbed_characters.Remove(character);
						character.interact_event -= interact;

					/* Subs characters that aren't already and are within distance. */
					} else if(!is_subbed && is_within_distance) {
						if(sameteam_carrier_allowed || character.team != team || !isHome) {
							_subbed_characters.Add(character);
							character.interact_event += interact;
						}
					}
				}
			}
		}
	}


	/* */
	protected void onAttack() {
		if(carrier_attacking_allowed)
			return;

		carrierDetachment();
	}


	/* */
	protected abstract void interact(CharacterBase p_character);


	/* */
	protected bool distanceCheck(Transform p_other, float distance = Globals.INVALID) {
		if(distance == Globals.INVALID)
			distance = interaction_distance;

		if(Vector2.Distance(new Vector2(transform.position.x, transform.position.z), new Vector2(p_other.position.x, p_other.position.z)) <= distance) {
			return true;
		}

		return false;
	}


	/* */
	public void returnToBase() {
		if(carrier != null)
			carrierDetachment();

		transform.position = home_position;
		transform.rotation = home_rotation;

		isHome = true;
	}


	/* */
	protected void carrierDetachment() {
		carrier.attack_event -= onAttack;
		carrier = null;
	}


	#region Getters & Setters
	public bool isHome { get; protected set; }
	public Team team { get; protected set; }
	public CharacterBase carrier { get; protected set; }
	protected float interaction_distance { get; set; }
	protected float lerp_follow_speed { get; set; }
	protected float lerp_distance_commence { get; set; }
	protected bool carrier_attacking_allowed { get; set; }
	protected bool sameteam_carrier_allowed { get; set; }
	protected bool use_lerp_follow { get; set; }
	protected bool lerp_dynamic_speed { get; set; }
	protected List<CharacterBase> _subbed_characters { get; set; }
	#endregion
}
