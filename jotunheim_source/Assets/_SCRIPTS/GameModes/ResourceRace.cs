﻿/***
 * 
 * CaptureTheFlag.cs
 * 
 * sss. 
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using StatusEffects;


public class ResourceRace : MonoBehaviour {


	public GameObject object_of_interest;
//	public PowerUp carrier_effect;
	private ResourceObject _resource_object;


	/* */
	private void Awake() {
		setupObjectOfInterest();
	}


	/* Takes the given flag gameobjects and stores them in their class containers. */
	private void setupObjectOfInterest() {
		if(object_of_interest != null)
			_resource_object = new ResourceObject(object_of_interest.transform, Team.Teamless, null);
		else
			throw new UnassignedReferenceException();
	}


	/* */
	private void FixedUpdate() {
		if(LevelManager.stage.CompareTo(IngameStage.Play) != 0)
			return;

		_resource_object.evaluate();
	}


	/* */
	private class ResourceObject : GoalObject {


		float timer = 0;


		/* */
		public ResourceObject(Transform p_transform, Team p_team, Func<StatusEffect> p_carrier_effect = null) : base(p_transform, p_team, p_carrier_effect) {
			carrier_attacking_allowed = true;
			sameteam_carrier_allowed = true;

			lerp_distance_commence = 1.5f;
			use_lerp_follow = true;
		}


		/* */
		public override void evaluate() {
			base.evaluate();

			if(carrier != null) {
				if(timer > (Time.time % 1)) {
					Scoring.collect(carrier, Scoring.ScoringMethod.Goal);
				}

				timer = (Time.time % 1);
            }
        }

		protected override void interact(CharacterBase p_character) {
			throw new NotImplementedException();
		}




		/* */
		/*public override bool interact(CharacterBase p_character, bool p_active_interaction) {
			if(carrier == null && p_character != null) {
				if(distanceCheck(p_character.transform)) {
					if(p_active_interaction) {

						carrier = p_character;

						return false;
					}

					return true;
				}
			}

			return false;
		}*/

	}
}
