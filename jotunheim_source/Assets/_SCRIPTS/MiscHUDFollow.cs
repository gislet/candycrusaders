﻿/***
 * 
 * CameraFollow.cs
 * 
 * asdf
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/
 
 
using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;


public class MiscHUDFollow : FollowPlayer {


	public Sprite controller, keyboard;
	public Image interactable_display;

	public float flashing_speed = 2f;
    public AnimationCurve flashing_curve;



	/* */
	protected override void onDestroyedTarget() {
		interactable_display.color = interactable_display.color * new Color(1, 1, 1, 0);
    }


	/* */
	protected override void onEnable() { }


	/* */
	protected override void onNewTarget() {
		if(character.controller_ID <= 0) {
			interactable_display.sprite = keyboard;
		} else {
			interactable_display.sprite = controller;
		}
	}


	/* */
	protected override void onUpdate() {
		if(character.can_interact) {
			float alpha_value = flashing_curve.Evaluate((Time.time * flashing_speed) % 1);

			interactable_display.color = new Color(interactable_display.color.r,
													interactable_display.color.g,
													interactable_display.color.b,
													alpha_value);
		} else {
			interactable_display.color = interactable_display.color * new Color(1, 1, 1, 0);
		}
	}


	#region Getters & Setters
	//private Image _interactable_image { get; set; }
	#endregion
}
