﻿/***
 * 
 * CinemaHandler.cs
 * 
 * sss
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CinemaHandler : MonoBehaviour {


	private List<Cinematic> _objects;
	private static CinemaHandler _instance;


	/* */
	private void Awake() {
		_instance = this;
		_objects = new List<Cinematic>();
	}


	/* */
	public static void play() {
		Debug.LogWarning("CinemaHandler is disabled.");

		//TODO IMPLEMENT
		/*if(_instance._objects == null || _instance._objects.Count <= 0)
			return;

		isPlaying = true;
		Master.coroutines.Enqueue(_instance.update);*/
	}


	/* */
	public IEnumerator update() {
		int index = 0;

		/* We want to allow cinematics to be used outside of ingame scenes as well, though if we are ingame we only want it to
		 * be working while we're in the cinematic stage. */
		while(index < _objects.Count && (LevelManager.isInGame() ? LevelManager.stage.CompareTo(IngameStage.Cinematic) == 0 : true)) {
			if(_objects[index].run())
				index++;
			yield return null;
		}

		stop();

		while(!_objects[index - 1].laterun()) {
			yield return null;
		}
	}


	/* */
	public static void stop() {
		isPlaying = false;
    }


	/* */
	public static void add(Cinematic p_object) {
		if(!_instance._objects.Contains(p_object))
			_instance._objects.Add(p_object);
    }


	#region Getters & Setters
	public static bool isPlaying { get; private set; }
	#endregion
}
