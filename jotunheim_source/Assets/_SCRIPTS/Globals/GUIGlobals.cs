﻿/***
 * 
 * GUIGlobals.cs
 * 
 * asdf. 
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;


public class GUIGlobals : MonoBehaviour {


	/* Colours */
	[Space(10, order = 0)]
	[Header("Colours", order = 1)]
	[Space(10, order = 2)]
	public Color Hover;
	public Color Idle;
	public Color Selected;
	public Color Ready;
	[Space(10, order = 2)]
	public Color team_neutral;
	public Color team_red;
	public Color team_blue;
	public Color team_green;
	public Color team_yellow;


	/* Animation Curves */
	[Space(20, order = 3)]
	[Header("Animation Curves", order = 4)]
	[Space(10, order = 5)]
	public AnimationCurve pulse_curve;
	public AnimationCurve fade_curve;


	/* Sound Effects */
	[Space(20, order = 6)]
	[Header("Sound Effects", order = 7)]
	[Space(10, order = 8)]
	public AudioClip confirm;
	public AudioClip confirm_special;
	public AudioClip cancel;
	public AudioClip move;


	/* */
	private Dictionary<Text, ElementCache> _text_element_dic = new Dictionary<Text, ElementCache>();
	private static GUIGlobals _instance;



	/* */
	void Awake () {
		_instance = this;
    }


	/* */
	public static void changeTextColour(ref Text p_text, CachedColor p_from, CachedColor p_to, ChangeColor p_behaviour, float p_speed = 1) {
		Color from = getColour(p_from);
		Color to = getColour(p_to);

		changeTextColour(ref p_text, from, to, p_behaviour, p_speed);
	}


	/* */
	public static void changeTextColour(ref Text p_text, Color p_from, Color p_to, ChangeColor p_behaviour, float p_speed = 1) {
		ElementCache ec = getElementCache(p_text, p_to);

		switch(p_behaviour) {
		case ChangeColor.Pulse:
			p_text.color = Color.Lerp(p_from , p_to, _instance.pulse_curve.Evaluate((((Time.unscaledTime - ec.start_time) * p_speed) % 1)));
			break;
		case ChangeColor.Fade:
			p_text.color = Color.Lerp(p_from, p_to, _instance.fade_curve.Evaluate(Mathf.Clamp01(Time.unscaledTime - ec.start_time) * p_speed));
			break;
		case ChangeColor.None:
			p_text.color = p_to;
			break;
        }
	}


	/* Based on an enum, we can easily find the sought for colour. */
	private static Color getColour(CachedColor p_colour) {
		switch(p_colour) {
		case CachedColor.Hover:
			return _instance.Hover;
		case CachedColor.Idle:
			return _instance.Idle;
		case CachedColor.Selected:
			return _instance.Selected;
		case CachedColor.Ready:
			return _instance.Ready;
		default:
			return Color.white;
		}
	}


	/* Retrieves the ElementCache associated with the given Text object. If it doesn't exist
	 * already we create a new one.*/
	private static ElementCache getElementCache(Text p_text, Color p_to) {
		if(_instance._text_element_dic.ContainsKey(p_text)) {

			/* If there happened a change, we want to reset the cache's start time. */
			if(_instance._text_element_dic[p_text].change_to != p_to) {
				_instance._text_element_dic[p_text].change_to = p_to;
				_instance._text_element_dic[p_text].start_time = Time.unscaledTime;
            }

			return _instance._text_element_dic[p_text];
		} else {
			ElementCache ec = new ElementCache();

			ec.change_to = p_to;
			ec.start_time = Time.unscaledTime;

			_instance._text_element_dic.Add(p_text, ec);

			return ec;
        }
	}


	/* */
	public static Color getTeamColour(Team p_team) {
		switch(p_team) {
		case Team.Teamless:
			return _instance.team_neutral;
		case Team.Blue:
			return _instance.team_blue;
		case Team.Red:
			return _instance.team_red;
		case Team.Yellow:
			return _instance.team_yellow;
		case Team.Green:
			return _instance.team_green;
		default:
			return _instance.team_neutral;
		}
	}


	/* Removes an ElementCache from the text-ElementCahce dictionary. Not extremely important to be called
	 * unless the scene has an incredible amount of various UI elements that use various functions inside of this
	 * GUI-Global class as the cached elements are otherwise only cleaned up between scenes. */
	public static void free(Text p_text) {
		
	}


	#region Getters & Setters
	public static AudioClip ui_move { get { return _instance.move; } }
	public static AudioClip ui_confirm { get { return _instance.confirm; } }
	public static AudioClip ui_confirm_special { get { return _instance.confirm_special; } }
	public static AudioClip ui_cancel { get { return _instance.cancel; } }
	#endregion


	/* A struct of a cache that keeps track of a start-time and a state identifier which is represented as
	 * a colour. */
	class ElementCache {
		public float start_time;
		public Color change_to;
	}
	

	public enum CachedColor {
		Hover, Idle, Selected, Ready
	}

	public enum ChangeColor {
		None,
		Fade,
		Pulse
	}
}
