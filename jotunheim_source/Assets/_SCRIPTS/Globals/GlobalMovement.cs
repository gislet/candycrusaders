﻿using UnityEngine;
using System.Collections;

/* */
public static class GlobalMovement {


	/* */
	public static bool byTransform(Transform p_transform, float p_speed, Vector2 p_input, bool p_lookat_direction, bool p_normalize = true) {
		Vector3 direction = Vector3.zero;

		direction = new Vector3(p_input.x, 0, p_input.y);
		direction = p_normalize ? direction.normalized : direction;

		p_transform.position += direction * Time.fixedDeltaTime * p_speed;


		if(p_input != Vector2.zero && p_lookat_direction) {
			float angle = Mathf.Atan2(-direction.z, direction.x) * (180 / Mathf.PI);
			p_transform.eulerAngles = new Vector3(0, angle, 0);
		}

		return direction != Vector3.zero;	
	}


	/* */
	public static bool byRigidbody(Transform p_transform, Rigidbody p_rigidbody, float p_speed, Vector2 p_input, bool p_lookat_direction, bool p_normalize = true) {
		Vector3 direction = Vector3.zero;

		direction = new Vector3(p_input.x, 0, p_input.y);
		direction = p_normalize ? direction.normalized : direction;

		p_rigidbody.MovePosition(p_rigidbody.position + (direction * Time.fixedDeltaTime * p_speed));

		//p_transform.rigidbody.position += direction * Time.fixedDeltaTime * p_speed;


		if(p_input != Vector2.zero && p_lookat_direction) {
			float angle = Mathf.Atan2(-direction.z, direction.x) * (180 / Mathf.PI);
			p_transform.eulerAngles = new Vector3(0, angle, 0);
		}

		return direction != Vector3.zero;
	}


	/* */
	public static void matchDirection(GameObject p_object, Vector3 p_direction) {
		float ob_angle = findAngleByDirection(findDirectionByTransform(p_object.transform));
		float tar_angle = findAngleByDirection(p_direction);
		float diff = tar_angle - ob_angle;

		p_object.transform.Rotate(diff * Vector3.up);
	}


	/* */
	public static void lookAtTransform(Transform p_transform, Transform p_target, bool p_prediction = false) {
		//p_prediction = true;
		Vector3 look_at = Vector3.zero;

		if(!p_prediction) {
			look_at = Vector3.Lerp(findDirectionByTransform(p_transform), (p_target.position - p_transform.position), Time.deltaTime * 180f);			
		} else {

			//TODO: Make it possible to predict collision in the future.
			//This is on the right track, but not quite right.

			Vector3 prediction = ((p_target.position + findDirectionByTransform(p_target) * Globals.DEFAULT_MOVE_SPEED));
			prediction = (prediction) * (Vector3.Distance(prediction, p_transform.position) / 20);
			prediction -= p_transform.position;
            look_at = Vector3.Lerp(findDirectionByTransform(p_transform), prediction, Time.deltaTime * 180f);
		}

		float angle = Mathf.Atan2(-look_at.z, look_at.x) * (180 / Mathf.PI);
		p_transform.eulerAngles = new Vector3(0, angle, 0);

		/*float angle_to_target = findAngleByDirection((p_transform.position - p_target.position));
		float current_angle = findAngleByDirection(findDirectionByTransform(p_transform));
		float rotate_speed = 180f;
		float new_angle = angle_to_target;*/

		/* If the target is to my left. */
		/*if(isWithinFieldOfView(current_angle, current_angle + 180, angle_to_target)) {
			new_angle += rotate_speed * Time.deltaTime;
		} else {
			new_angle -= rotate_speed * Time.deltaTime;
		}

		if(isWithinFieldOfView(new_angle - 2.5f, new_angle + 2.5f, angle_to_target)) {


			p_transform.eulerAngles = new Vector3(0, -new_angle, 0);
		}*/
	}


	/* */
	public static Vector2 getLineDirectionVec2(Vector2 a, Vector2 b) {
		return new Vector2(b.x - a.x, b.y - a.y);
	}


	/* */
	public static Vector3 findDirectionByTransform(Transform p_transform) {
		float angle = -((p_transform.localEulerAngles.y) * (Mathf.PI/180));
		Vector3 direction = new Vector3(Mathf.Cos(angle), 0, Mathf.Sin(angle));

		return direction;
		//return (p_transform.position + direction);
	}


	/* */
	public static Vector3 findDirectionByAngle(Vector3 p_origin, float p_angle) {
		float angle = -(p_angle * (Mathf.PI / 180));
		Vector3 direction = new Vector3(Mathf.Cos(angle), 0, Mathf.Sin(angle));

		return direction;
	}


	/* */
	public static Vector3 adjustDirectionByVectors(Vector3 p_direction, float p_angular_offset = 0) {
		//float angle = findAngleByVectors(p_origin, p_direction);
		float angle = findAngleByDirection(p_direction);
		angle = -((angle + p_angular_offset) * (Mathf.PI / 180));

		Vector3 adjusted_direction = new Vector3(Mathf.Cos(angle), 0, Mathf.Sin(angle)) * 5;
		return (/*p_origin +*/ adjusted_direction);
	}


	/* */
	public static float findAngleByDirection(Vector3 p_origin, Vector3 p_dir) {
		Vector3 direction = new Vector3(p_dir.x - p_origin.x, 0, p_dir.z - p_origin.z);
		return findAngleByDirection(direction);
	}


	/* */
	public static float findAngleByDirection(Vector3 p_dir) {
		/*float deltaY = p_end.z - p_start.z;
		float deltaX = p_end.x - p_start.x;*/
		float deltaY = p_dir.z;
		float deltaX = p_dir.x;

		float angle = (Mathf.Atan2(-deltaY, deltaX) * (180 / Mathf.PI));

		return angle < 0 ? angle + 360 : angle;
	}


	/* */
	/*public static bool isAngleBetweenAngles(float p_start, float p_end, float p_angle) {
		// I don't like this code, but it is successfully completing the task when
		// the angle should be within the given angles.
		float start = p_start > p_end ? p_start - 360 : p_start;

		if(p_angle >= start && p_angle <= p_end) {
			return true;
		} else {
			p_angle = (p_angle + 180) % 360 - 180;

			if(p_angle >= p_start && p_angle <= p_end) {
				return true;
			}
		}


		return false;
	}*/


	/* Checking to see if a target is within a field of view. */
	public static bool isWithinFieldOfView(float p_min, float p_max, float p_target) {

		/* We want to make sure that max can't be larger or equal to 360. */
		p_max = p_max >= 360 ? p_max - 360 : p_max;

		/* We then want to check if min is actually on the negative scale, or if it should be
		 * as is the case when min is somehow larger than max. */
		bool use_negative_scale = p_min > p_max || p_min < 0;

		/* If min is on the negative scale, we want to subtract 360 from it only if it
		 * isn't already negative. */
		p_min = (use_negative_scale && p_min >= 0) ? p_min - 360 : p_min;

		/* We want to make sure target is also on the negative scale, unless it is lesser or equal
		 * to max. */
		p_target = (use_negative_scale && p_target > p_max) ? p_target - 360 : p_target;

		/* Everything should be set up correctly that would allow this single line to figure out
		 * if the target is within field of view of the viewer. */
		if(p_target >= p_min && p_target <= p_max)
			return true;

		return false;
	}


	/* */
	/*public static bool isAngleBetweenAngles(Vector3 p_origin, Vector3 p_target, Vector3 p_direction, float p_angle) {
		float angle_current = findAngleByDirection(p_direction);
		int angle_start = (int)(angle_current - (p_angle / 2));
		int angle_end = (int)(angle_current + (p_angle / 2));
		int angle_target = (int)(findAngleByDirection(p_target));


		return point_in_closed_interval(angle_target, angle_start, angle_end);
		//return isAngleBetweenAngles(angle_start, angle_end, angle_target);
	}


	static bool point_in_closed_interval(int x, int a, int b) {
		return modN(x - a) <= modN(b - a);
	}

	static int modN(int x) {
		const int N = 360;
		int m = x % N;
		if(m < 0) {
			m += N;
		}
		return m;
	}*/


	public static bool moveInDirection(Transform p_trans, float p_speed,
								float p_up, float p_down, float p_left, float p_right, bool p_normalize = true) {
		if(p_up == 0 && p_down == 0 && p_left == 0 && p_right == 0)
			return false;

		Vector3 moveDir = Vector3.zero;

		if(p_normalize)
			moveDir = new Vector3(p_left + p_right, 0, p_up + p_down).normalized;
		else
			moveDir = new Vector3(p_left + p_right, 0, p_up + p_down);

		p_trans.position += moveDir * Time.fixedDeltaTime * p_speed;

		if(moveDir != Vector3.zero) {
			return true;
		} else {
			return false;
		}
	}

	public static Vector3 direction(Vector3 p_pos, float p_angle, float p_length = 5.0f) {
		Vector3 lookDir = p_pos;

		float radians = (p_angle * Mathf.PI/180);

		lookDir += new Vector3((float)Mathf.Sin(radians), 0, (float)Mathf.Cos(radians)) * p_length;

		return lookDir;
	}

	public static Vector3 direction(float p_up, float p_down, float p_left, float p_right) {
		return new Vector3(p_left + p_right, 0, p_up + p_down).normalized;
	}
}