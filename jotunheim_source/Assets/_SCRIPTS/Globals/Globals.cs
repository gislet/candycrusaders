﻿/***
 * 
 * Globals.cs
 * 
 * Globals contains some basic functions that might be useful in very random scripts. It also contains useful generic game-information
 * such as max amount of players that can exist, default ability cooldowns, and default character move speeds to name a few. It also
 * stores vital information such as save/load destinations.
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using LitJson;
using System.Security.Cryptography;
using System.Text;


public static class Globals {

	public const string game_version = "Alpha-0.4.0";
    public const string version_release_date = "May 23rd, 2016"; //ex.: July 15th, 2020


	/* The corner stone of my program. It deserves the throne position in Globals. :) */
	public const int INVALID = -1;

	/* MAX VALUES */
	public const int MAX_PLAYERS = 4;
	public const int MAX_DEVICES = 5; //0 is Keyboard, 1-4 is Joysticks (Consider changing logic in the future to allow many many users to play simultaneously.)
	public static int MAX_LIVES = 5;

	/* Default Values. */
	public const float GLOBAL_COOLDOWN = 0.5f;
	public static readonly float DEFAULT_MOVE_SPEED = 10f;

	/* Miscellanous Values */
    public const float SECONDS = 60;
	public const float PERCENTAGE = 100;
	public const string TAG_PLAYER = "Player";


	/* Prepares the save-path for various items/settings to the client's specialfolder. */
	public static void prepareSavePath() {
		if(!System.IO.Directory.Exists(SPECIAL_FOLDER_GAMES))
			System.IO.Directory.CreateDirectory(SPECIAL_FOLDER_GAMES);

		if(!System.IO.Directory.Exists(SPECIAL_FOLDER_FULL))
			System.IO.Directory.CreateDirectory(SPECIAL_FOLDER_FULL);
	}


	/* Sets all items inside the referenced boolean array to the passed value. */
	public static void setBoolArray(this bool[] p_array, bool p_status) {
		for(int i = 0; i < p_array.Length; i++)
			p_array[i] = p_status;
	}


	/* Allows for easy incrementations or opposite of an enumerator which has generic values.
	 * (0 ... 1 ... 2 ... 3 ... etc ...)*/
	public static int changeEnumIndex(int p_current, int p_max, bool p_positive) {
		int changed_index = 0;

		if(p_positive) {
			changed_index = p_current + 1 >= p_max ? 0 : p_current + 1;
		} else {
			changed_index = p_current - 1 < 0 ? p_max - 1 : p_current - 1;
		}

		return changed_index;
	}


	/* Allows you to pass in a referenced float and decrease its value by time. It uses the
	 * unscaledDeltaTime so that it will not be affected by time-scaling. */
	public static float deltaTimeCountdown(ref float p_timer) {
		return (p_timer -= Time.unscaledDeltaTime);
    }


	/* Allows you to pass in a referenced float and increase its value by time. It uses the
	 * unscaledDeltaTime so that it will not be affected by time-scaling. */
	public static float deltaTimeCounter(ref float p_timer) {
		return (p_timer += Time.unscaledDeltaTime);
	}


	/* */
	public static void openFile(string p_path, DatabasePrivacy p_privacy = DatabasePrivacy.Private) {
		string path = pathPrefix(p_privacy) + p_path;

		if(File.Exists(path)) {
			System.Diagnostics.Process.Start(@path);
		}
	}


	/* Opens and creates a list of given type from a single or multiple stored JSON files. */
	public static List<T> loadDatabase<T>(DatabasePrivacy p_privacy, string p_path) where T : class, new() {
		List<T> list = new List<T>();
		int start = p_privacy == DatabasePrivacy.Public ? 1 : 0;
		int max = p_privacy == DatabasePrivacy.Private ? 1 : 2;

		while(start < max) {
			string path = "";

			switch(start) {
			case 0: //Private
				path = p_path.Substring(1, p_path.Length - 1);
				path = path.Replace(".json", "");
				break;
			case 1: //Public
				path = STREAMING_PATH + p_path;
				break;
			}

			if(File.Exists(path) && p_privacy == DatabasePrivacy.Public) {
				list.AddRange(JsonMapper.ToObject<List<T>>(File.ReadAllText(path)));
			} else if(Resources.Load(path) != null) {
				//TODO: Figure out why Resources.Load have a loading issue in the editor.
#if UNITY_EDITOR
				path = RESOURCE_PATH + p_path;
				list.AddRange(JsonMapper.ToObject<List<T>>(File.ReadAllText(path)));
#else
				list.AddRange(JsonMapper.ToObject<List<T>>(Resources.Load<TextAsset>(path).text));
#endif
			} else {
				List<T> objects = new List<T>();
				list.AddRange(objects);
				saveDatabase(objects, path);
			}

			start++;
		}

		return list;
	}


	/* Saves a list of a generic type at the given parameter path. */
	public static void saveDatabase<T>(DatabasePrivacy p_privacy, List<T> p_list, string p_path) {
		if(p_privacy == DatabasePrivacy.Both)
			throw new ArgumentException("DatabasePrivacy enum cannot in this instance be set to \"Both\".");

		string path = pathPrefix(p_privacy) + p_path;

		saveDatabase(p_list, path);
	}


	/* Saves a list of a generic type at the given parameter path. */
	private static void saveDatabase<T>(List<T> p_list, string p_path) {
		JsonWriter writer = new JsonWriter();
		writer.PrettyPrint = true;

		JsonMapper.ToJson(p_list, writer);
		File.WriteAllText(p_path, writer.ToString());
    }


	/* Checks to see if the file at the parameter path has a writetime that is different from the parameter
	 * writetime. */
	public static bool fileUpdated(DatabasePrivacy p_privacy, string p_path, System.DateTime p_writetime) {
		if(p_privacy == DatabasePrivacy.Both)
			throw new ArgumentException("DatabasePrivacy enum cannot in this instance be set to \"Both\".");

		string path = pathPrefix(p_privacy) + p_path;

#if UNITY_EDITOR
		if((new FileInfo(path).LastWriteTime.CompareTo(p_writetime) != 0)) {
			return true;
		}
#else
		if(p_privacy == DatabasePrivacy.Public) {
			if((new FileInfo(path).LastWriteTime.CompareTo(p_writetime) != 0)) {
				return true;
			}
		}
#endif

		return false;
	}


	/* Returns the current writetime of a file. */
	public static DateTime fileWritetime(string p_path) {
		if(File.Exists(p_path)) {
			return new FileInfo(p_path).LastWriteTime;
		}

		return new DateTime();
	}


	/* Returns the path-prefixes that are used for jsonfiles under their various privacy settings. */
	public static string pathPrefix(DatabasePrivacy p_privacy) {
		switch(p_privacy) {
		case DatabasePrivacy.Private:
			return RESOURCE_PATH;
		case DatabasePrivacy.Public:
			return STREAMING_PATH;
		default:
			return "";
		}
	}


	/* Creates a random 8 characters long hash by first creating a 32 long one and then selecting a snippit of it. */
	public static string ID_Creator(string p_random_noise = "") {
		MD5 hasher = MD5.Create();
		byte[] data = hasher.ComputeHash(Encoding.UTF8.GetBytes(System.DateTime.Now.ToString() + p_random_noise));
		StringBuilder sBuilder = new StringBuilder();

		for(int i = 0; i < data.Length; i++) {
			sBuilder.Append(data[i].ToString("x2"));
		}

		return sBuilder.ToString().Substring((new System.Random()).Next(24), 8).ToUpper();
	}


	/* Iterates through an existing list checking to see if any of its members currently hold
	 * the parameter ID. */
	public static bool ID_Available<T>(string p_ID, List<T> p_list) where T : class, IIdentifiable {
		if(p_list != null) {
			foreach(T item in p_list) {
				if(p_ID.CompareTo(item.ID) == 0)
					return false;
			}
		}

		return true;
	}








	/* */
	public static string serialize<T>(T p_object, bool p_pretty_print = false) {
		JsonWriter writer = new JsonWriter();
		writer.PrettyPrint = p_pretty_print;
		JsonMapper.ToJson(p_object, writer);
		return writer.ToString();
	}


	public static string serializeList<T>(List<T> p_list, bool p_pretty_print = false) {
		JsonWriter writer = new JsonWriter();
		writer.PrettyPrint = p_pretty_print;
		JsonMapper.ToJson(p_list, writer);
		return writer.ToString();
	}


	/* */
	public static T deserialize<T>(string p_data) {
		JsonReader reader = new JsonReader(p_data);
		return JsonMapper.ToObject<T>(reader);
	}


	/* */
	public static List<T> deserializeList<T>(string p_data) {
		JsonReader reader = new JsonReader(p_data);
		return JsonMapper.ToObject<List<T>>(reader);
	}


	/* Saves a list of a generic type at the given parameter path. */
	public static void saveList<T>(List<T> p_list, string p_path, string p_extension) {
		string content = serializeList(p_list, true);
		writeFile(content, p_path, p_extension);
	}


	/* Loads a list of a generic type from the given parameter path. */
	public static List<T> loadList<T>(string p_path, bool p_resource = true) {
		string content;
		if((content = readFile(p_path, p_resource)) != null) {
			List<T> list = JsonMapper.ToObject<List<T>>(content);
			return list;
		}
		return null;
	}


	/* Saves an object of a generic type at the given parameter path. */
	public static void save<T>(T p_object, string p_path, string p_extension) {
		string content = serialize(p_object, true);
		writeFile(content, p_path, p_extension);
	}


	/* Loads an object of a generic type from the given parameter path. */
	public static T load<T>(string p_path, bool p_resource = true) {
		string content;
		if((content = readFile(p_path, p_resource)) != null) {
			return JsonMapper.ToObject<T>(content);
		}

		return default(T);
	}


	/* */
	private static void writeFile(string p_content, string p_path, string p_extension) {
		if(!Directory.Exists(SPECIAL_FOLDER_GAMES) || !Directory.Exists(SPECIAL_FOLDER_FULL))
			prepareSavePath();

		File.WriteAllText(p_path + p_extension, p_content);
	}


	/* */
	private static string readFile(string p_path, bool p_resource) {
		if(p_resource && Resources.Load(p_path) != null) {
			return Resources.Load<TextAsset>(p_path).text;
		} else {
			return File.ReadAllText(p_path);
		}
	}















	#region Getters & Setters
	public static string RESOURCE_PATH {
		get { return Application.dataPath + "/Resources"; }
	}
	public static string STREAMING_PATH	{
		get { return Application.dataPath + "/StreamingAssets"; }
	}
	public static string GFX_SETTINGS_FILE {
		get { return @"" + SPECIAL_FOLDER_FULL + "/gfx_settings.txt"; }
	}
	public static string AUDIO_SETTINGS_FILE {
		get { return @"" + SPECIAL_FOLDER_FULL + "/audio_settings.txt"; }
	}
	public static string LOCAL_SETTINGS_FILE {
		get { return @"" + SPECIAL_FOLDER_FULL + "/local_settings.txt"; }
	}
	public static string SPECIAL_FOLDER_FULL {
		get { return SPECIAL_FOLDER_GAMES + "/Jotunheim Brawlers"; }
	}
	public static string SPECIAL_FOLDER_GAMES {
		get { return SPECIAL_FOLDER + "/my games"; }
	}
	public static string SPECIAL_FOLDER {
		get { return System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments); }
	}
	#endregion
}


/* */
public enum DatabasePrivacy {
	Both, Private, Public
}


/* Enum of each allowed Team in the game. */
public enum Team {
	Teamless,
	Blue,
	Red,
	Green,
	Yellow,
	length
}


public struct Vec2d {
	public double x;
	public double y;

	public Vec2d(double p_x = 0, double p_y = 0) {
		x = p_x;
		y = p_y;
	}


	public Vec2d(Vector2 p_vector2) {
		x = p_vector2.x;
		y = p_vector2.y;
	}


	public Vector2 toVector2() {
		return new Vector2((float)x, (float)y);
	}
}