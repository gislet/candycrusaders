﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;


namespace Pathfinding {
	using Collection;
	

	public class PathRequestManager : MonoBehaviour {

		/* Vitals! Cached objects. */
		private Queue<PathRequest> _path_request_queue = new Queue<PathRequest>();
		private static PathRequestManager _instance;
		private PathRequest _current_path_request;
		private Pathfinder _pathfinder;


		/* Essentials. */
		private bool _isProcessing;



		/* Once this script gets initialised, we cache certain vitals. */
		void Awake() {
			_instance = this;
			_pathfinder = GetComponent<Pathfinder>();
		}
		

		/* Easy way for other objects to figure if their position is currently on a walkable node. */
		public static bool isWalkable(Vector3 p_pos) {
			return _instance._pathfinder.grid.BestNodeFromWorldPoint(p_pos).walkable;
		}

		/* Easy way for other objects to find the position of their current node. */
		public static Vector3 isWalkablePosition(Vector3 p_pos) {
			return _instance._pathfinder.grid.BestNodeFromWorldPoint(p_pos).world_position;
		}

		public static Vector3 isWalkableBestPosition(Vector3 p_pos) {
			return _instance._pathfinder.grid.BestNodeFromWorldPoint(p_pos).world_position;
		}

		public static Vector3 nearestOnGridNode(Vector3 p_pos) {
			Node node = _instance._pathfinder.grid.bestWorstNode(p_pos);

			if(node != null)
				return node.world_position;
			else
				return Vector3.zero;
		}


		/* Called by objects if they wish to find a path from one point to another. If there are already existing
		 * requests, the manager simply put this one into a queue for processing later. */
		public static void requestPath(Transform p_path_origin, Transform p_path_target, Action<Vector3[], bool> p_callback) {
			PathRequest new_request = new PathRequest(p_path_origin, p_path_target, p_callback);
			_instance._path_request_queue.Enqueue(new_request);
			_instance.tryProcessNext();
		}


		/* Attempts to initiate a new pathfinding process. */
		void tryProcessNext() {
			if(!_isProcessing && _path_request_queue.Count > 0) {
				_isProcessing = true;
				_current_path_request = _path_request_queue.Dequeue();

				if(_current_path_request.path_origin != null && _current_path_request.path_target != null) {
					_pathfinder.startFindPath(_current_path_request.path_origin.position, _current_path_request.path_target.position);
				} else {
					finishedProcessingPath(null, false);
				}
			}
		}


		/* Once a pathfinding request is processed, it will notify that nothing is being processed, and it will
		 * also automatically check to see if there is anything else in need of processing. */
		public void finishedProcessingPath(Vector3[] p_path, bool p_success) {
			_current_path_request.callback(p_path, p_success);
			_isProcessing = false;
			tryProcessNext();
		}


		/* A struct that holds all the necessary information reguarding a pathfinding request. */
		struct PathRequest {
			public Transform path_origin;
			public Transform path_target;

			public Action<Vector3[], bool> callback;

			/* The structs constructor. */
			public PathRequest(Transform p_origin, Transform p_target, Action<Vector3[], bool> p_callback) {
				path_origin = p_origin;
				path_target = p_target;
				callback = p_callback;
			}
		}
	}
}
