﻿#pragma warning disable

using UnityEngine;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using Pathfinding;
using Pathfinding.Collection;

[RequireComponent(typeof(PathRequestManager))]
[RequireComponent(typeof(Pathfinder))]
public class Grid : MonoBehaviour {

	/* Debug */
	[Header("Gizmo Debugging", order=0)]
	public bool debugging_level = false;
	public DisplayGridInfo display_grid_info = DisplayGridInfo.None;
	public bool inAirBuffer = false;


	/* World Path Generation */
	[Space(8, order=1)]
	[Header("Grid Generation Settings", order=2)]

	[Tooltip("Assign each node's neighbour immediately after grid has been created. (Will be more taxing initially)")]
	public bool immediate_neighbouring = true;

	[Tooltip("Size of each node - Locked at 1 until certain problems have been ironed out.")]
	[Range(1,1)]public float node_diameter = 1;

	[Tooltip("Determines how low a passage can be with nodes generating there.")]
	public float character_height = 1;

	[Tooltip("Max climb angle - Locked at 35 until certain problems have been ironed out.")]
	[Range(35, 35)] public float max_climb_angle = 35;

	[Tooltip("Specify the area where the nodes are going to be generated in.")]
	public Vector3 grid_world_size = Vector3.one;

	[Tooltip("Specify ONE layer mask that will force nodes to not generate around it.")]
	public LayerMask unwalkable_mask;


	public LayerMask obstructive_masks;
	public TerrainType[] walkable_regions;
	LayerMask walkable_mask;
	Dictionary<int, int> walkable_regions_dictionary = new Dictionary<int, int>();


	/* Essentials */
	private Node[,,] _grid;
	private float _node_radius;
	private int _grid_size_x, _grid_size_y, _grid_size_z;



	/* Once the script gets initiated, it will immediately execute the following. */
	public void Awake() {
		/* Caches a few handy variables. */
		_node_radius = node_diameter / 2;

		_grid_size_x = Mathf.RoundToInt(grid_world_size.x / node_diameter);
		_grid_size_y = Mathf.RoundToInt(grid_world_size.y / node_diameter);
		_grid_size_z = Mathf.RoundToInt(grid_world_size.z / node_diameter);


		foreach(TerrainType region in walkable_regions) {
			walkable_mask.value |= region.terrain_mask.value;
			walkable_regions_dictionary.Add((int)Mathf.Log(region.terrain_mask.value, 2), region.terrain_penalty);
		}


		/* Once the initial stuff is done, we create our grid. */
		create();
	}



	public void create() {
		createGrid();
		if(immediate_neighbouring)
			findNeighbours();
	}



	//TEMPORARY
	/*public void OnGUI() {
		if(Input.GetKeyDown(KeyCode.F)) {
			Stopwatch sw = new Stopwatch();
			sw.Start();
			createGrid();
			if(inAirBuffer)
				findNeighbours();
			sw.Stop();
			UnityEngine.Debug.Log("Time: " + sw.ElapsedMilliseconds + "ms");
		}
	}*/


	/* Goes through each node in our grid and tries to find neighbours for them. */
	private void findNeighbours() {
		if(_grid != null) {
			foreach(Node n in _grid) {
				if(n != null) {
					if(n.neighbours == null) {
						n.neighbours = getNeighbours(n);
					}
				}
			}
		}
	}


	/* Returns the max amount of nodes our grid might possibly have. */
	public int max_size {
		get {
			return _grid_size_x * _grid_size_y * _grid_size_z;
		}
	}


	/* Looks around the given node for any other nodes. Which it then checks to see
	 * if it can connect to and have as neighbours. */
	public List<Node> getNeighbours(Node node) {
		List<Node> neighbours = new List<Node>();


		for(int x = -1; x <= 1; x++) {
			for(int z = -1; z <= 1; z++) {
				bool is_y_inAir = false;

				for(int y = -1; y <= 1; y++) {

					/* If all coordinates are at 0, we're looking at our focus
					 * node, which means we can just fall through this iteration. */
					if(x == 0 && y == 0 && z == 0) {
						continue;
					} else {

						/* We create temporary coordinates to use for our tests. */
						int checkX = node.gridX + x;
						int checkY = node.gridY + y;
						int checkZ = node.gridZ + z;

						/* First test is to make sure the node is within our grid boundaries. */
						if(checkX >= 0 && checkX < _grid_size_x && 
							checkY >= 0 && checkY < _grid_size_y &&
							checkZ >= 0 && checkZ < _grid_size_z) {

							/* Attempting to create a buffer-zone from edges where there is nodes in the middle of the air. 
							 * It only cares about this if both the lower AND same height y-coordinate ends up
							 * being in this state. */
							if((y == -1 || y == 0) && inAirBuffer) {
								if(_grid[checkX, checkY, checkZ].inAir) {
									if(!is_y_inAir) {
										is_y_inAir = true;
									} else {

										/* If both had a positive result, then we end up saying that this node just ins't walkable
										 * and return its neighbours as an uninitiated list. */
										node.walkable = false;
										return null;
									}
								}
							}

							/* Checks the difference in height between our focus node and our current iteration node. 
							 * If it happens to be too far away from each other, the climb is too tall and we just
							 * simply don't add it to our neighbours list. */
							float y_diff = _grid[checkX, checkY, checkZ].world_position.y - node.world_position.y;

							//NEEDS TO BE CLEANED UP, REMOVE THE MAGIC SHIT
							if(y_diff < .71f && y_diff > -.71f) {

								neighbours.Add(_grid[checkX, checkY, checkZ]);
							}
						}
					}
				}
			}
		}

		/* When all is done, we should have a nicely populated list for our node to store. */
		return neighbours;
	}


	/* Simply only looks to find the node at the given world-position. */
	public Node NodeFromWorldPoint(Vector3 p_point) {

		/* We begin by finding the indexes it must have on our grid. */
		float percentX = (p_point.x - transform.position.x + grid_world_size.x/2) / grid_world_size.x;
		float percentY = (p_point.y - transform.position.y) / grid_world_size.y;
		float percentZ = (p_point.z - transform.position.z + grid_world_size.z/2) / grid_world_size.z;

		percentX = Mathf.Clamp01(percentX);
		percentY = Mathf.Clamp01(percentY);
		percentZ = Mathf.Clamp01(percentZ);

		int x = Mathf.RoundToInt((_grid_size_x-1) * percentX);
		int y = Mathf.RoundToInt((_grid_size_y-1) * percentY);
		int z = Mathf.RoundToInt((_grid_size_z-1) * percentZ);

		/* And then return the node at those indexes. */
		return _grid[x, y, z];
	}


	/* Using a vector3 in the world, we attempt at finding the best node available to it. */
	public Node BestNodeFromWorldPoint(Vector3 p_point) {

		/* We begin by finding the indexes it must have on our grid. */
		float percentX = (p_point.x - transform.position.x + grid_world_size.x/2) / grid_world_size.x;
		float percentY = (p_point.y - transform.position.y) / grid_world_size.y;
		float percentZ = (p_point.z - transform.position.z + grid_world_size.z/2) / grid_world_size.z;

		percentX = Mathf.Clamp01(percentX);
		percentY = Mathf.Clamp01(percentY);
		percentZ = Mathf.Clamp01(percentZ);

		int x = Mathf.RoundToInt((_grid_size_x-1) * percentX);
		int y = Mathf.RoundToInt((_grid_size_y-1) * percentY);
		int z = Mathf.RoundToInt((_grid_size_z-1) * percentZ);

		/* If the node we found at those indexes happens to be unwalkable,
		 * we'll try and look for the best-worst node. */
		if(!_grid[x, y, z].walkable) {
			if(y > 0) {
				for(int i = y; i >= 0; i--) {
					if(_grid[x, i, z].walkable) {
						y = i;
						break;
                    }
				}
			}

				/*Node best_node = null;
				if((best_node = bestWorstNode(_grid[x, y, z], p_point)) != null) {
					return best_node;
				}*/
		}

		/* If everything went smoothly we should be able to return the node at the location of
		 * our indexes. */
		return _grid[x, y, z];
	}


	/* Looks around the given node to find the best-worst node that is 
	 * walkable. */
	public Node bestWorstNode(/*Node p_node, */Vector3 p_point) {
		/* Once they have been found, we check to see if it is walkable. If not, we try to make
		 * the best out of the situation. */
		//Node p_node = BestNodeFromWorldPoint(p_point);
		Node p_node = NodeFromWorldPoint(p_point);
        Node best_node = null;


		/* We the nodes around our current focus node, and try to look for one that is walkable. */
		int searchArea = 3;
		for(int x2 = -searchArea; x2 <= searchArea; x2++) {
			for(int y2 = 0; y2 <= 0; y2++) {
				for(int z2 = -searchArea; z2 <= searchArea; z2++) {
					if(x2 == 0 && y2 == 0 && z2 == 0) {
						continue;
					} else {

						int checkX = p_node.gridX + x2;
						int checkY = p_node.gridY + y2;
						int checkZ = p_node.gridZ + z2;

						if(checkX >= 0 && checkX < _grid_size_x && 
							checkY >= 0 && checkY < _grid_size_y &&
							checkZ >= 0 && checkZ < _grid_size_z) {

							/* Every time we find a node that is walkable, we try and pick the one that is the closest to us. */
							if(_grid[checkX, checkY, checkZ].walkable) {
								if(best_node == null) {
									best_node = _grid[checkX, checkY, checkZ];
								} else if(Vector3.Distance(best_node.world_position, p_point) > Vector3.Distance(_grid[checkX, checkY, checkZ].world_position, p_point)) {
									best_node = _grid[checkX, checkY, checkZ];
								}
							}
						}
					}
				}
			}
		}

		/* Once we've found our node, we return it. */
		return best_node;
	}


	/* Generates the grid, determining if a node is walkable or not, and whether it is in the air or not. 
	 * At this point, no nodes are connected. That happens either at runtime (during pathfinding) or after
	 * the grid has been generated. */
	private void createGrid() {

		/* Instantiates the grid. */
		_grid = new Node[_grid_size_x, _grid_size_y, _grid_size_z];
		Vector3 world_bottom_left = transform.position - (Vector3.right * grid_world_size.x/2) - (Vector3.forward * grid_world_size.z/2);

		/* Cycles through the length and width of the grid. */
		for(int x = 0; x < _grid_size_x; x++) {
			for(int z = 0; z < _grid_size_z; z++) {

				/* Sets this node's world position (except its y-coordinate if it is larger than 0,
				 * that get's set later. */
				Vector3 world_point = world_bottom_left + 
									(Vector3.right * (x  * node_diameter + _node_radius)) + 
									(Vector3.forward * (z  * node_diameter + _node_radius));
				float elevation = 0;

				/* Casting a raycast from the grid ceiling and straight down. */
				RaycastHit hit;
				bool walkable = false;
				if(Physics.Raycast(world_point + Vector3.up * grid_world_size.y, Vector3.down, out hit)) {

					/* Depending on the node-size and y-coordinate of this cast, we can figure out how many sphere-collisions 
					 * it will take to figure out where to place nodes. */
					int y_height = Mathf.CeilToInt(hit.point.y/node_diameter);

					/* Going through the different y-levels. */
					for(int y = 0; y < _grid_size_y; y++) {

						/* We assume that we're not in the air. */
						bool inAir = false;
						elevation = y * node_diameter;

						/* If the previous node at our location was walkable, then this node cannot be walkable. */
						if(!walkable) {
							if(y <= y_height) {
								bool unwalkable = false;
								if(Physics.Raycast(world_point + new Vector3(0, _node_radius * 1f, 0) + new Vector3(0, y, 0) * node_diameter, Vector3.down, out hit)) {
									elevation = hit.point.y;
								}

								/* Checking to see if the sphere-collision detected our unwalkable, which in that case we want to make sure 
								 * our node is exactly that - unwalkable. */
								unwalkable = (Physics.CheckSphere(world_point  + new Vector3(0, _node_radius * character_height, 0) + new Vector3(0, y, 0) * node_diameter,
																_node_radius * .95f * character_height * 1.5f,
																unwalkable_mask));

								/* Otherwise we'll perform our standard check to see if there is anything that would obstruct us. */
								if(!unwalkable) {
									float node_breathing_room = 2f;
									//foreach(LayerMask lm in obstructive_masks) {
										walkable = !(Physics.CheckSphere(world_point + new Vector3(0, _node_radius * character_height * node_breathing_room, 0) + new Vector3(0, y, 0) * node_diameter,
																	_node_radius * .95f * character_height * node_breathing_room, obstructive_masks));

										/*if(!walkable)
											break;
									}*/
								}


						/* Catching anything that falls through these simple checks and sets them to some
						 * default values. */
							} else {
								walkable = false;
								inAir = true;
							}
						} else {
							walkable = false;
							inAir = true;
						}

						int movement_penalty = 0;

						if(walkable) {
							Ray ray = new Ray(world_point + Vector3.up * character_height, Vector3.down);
							RaycastHit hit2;

							if(Physics.Raycast(ray, out hit2, 100, walkable_mask)) {
								walkable_regions_dictionary.TryGetValue(hit.collider.gameObject.layer, out movement_penalty);
							}

						}

						/* When all is done, create a new node. */
						_grid[x, y, z] = new Node(walkable, inAir, new Vector3(world_point.x, world_point.y + elevation, world_point.z), x, y, z, movement_penalty);
					}
				}
			}
		}
	}


	/* Drawing Gizmos on the screen. Used only for debugging and thus is a free space for very
	 * messy code. Please make sure that anything here won't accidentally trip off
	 * some kind of error during normal gameplay. */
	void OnDrawGizmos() {
		if(!Application.isPlaying)
			Gizmos.DrawWireCube(transform.position + Vector3.up * grid_world_size.y/2, new Vector3(grid_world_size.x, grid_world_size.y, grid_world_size.z));

		//Node vn = null, bn = null;
		/*if(debugging_level) {
			if(Application.isPlaying) {
				//Vector3 v = new Vector3(-4.5f, 1, -7.5f), b = new Vector3(-3.5f, 1, -7.5f);
				Vector3 v = new Vector3(15.5f, 1, -7.5f), b = new Vector3(14.5f, 1, -7.5f);

				vn = BestNodeFromWorldPoint(v); bn = BestNodeFromWorldPoint(b);
				Gizmos.color = (vn.walkable) ? Color.red : Color.red;
				Gizmos.DrawCube(vn.world_position, Vector3.one * (node_diameter - (node_diameter * .05f)));
				Gizmos.color = (bn.walkable) ? Color.blue : Color.red;
				Gizmos.DrawCube(bn.world_position, Vector3.one * (node_diameter - (node_diameter * .05f)));

				Vector3 t = new Vector3(0, /*vn.world_position.y*/// 0, 0), tt = new Vector3(node_diameter, /*bn.world_position.y*/ .71f, 0);
				/*float radians = Mathf.Atan2(bn.world_position.y, node_diameter) - Mathf.Atan2(vn.world_position.y, 0);
				float degrees = (180/Mathf.PI) * radians;

				float deltaX = t.x - tt.x;
				float deltaY = t.y - tt.y;
				float angleInDegrees = Mathf.Atan(deltaY / deltaX) * 180 / Mathf.PI;*/


				//UnityEngine.Debug.LogFormat("Test Angle: {0}", angleInDegrees);



				/*Vector3 t = new Vector3(0, vn.world_position.y, 0), tt = new Vector3(node_diameter, bn.world_position.y, 0);
				UnityEngine.Debug.LogFormat("Test Angle: {0}, {3} - {1}, {2}", Vector3.Angle(vn.world_position, bn.world_position), vn.world_position, bn.world_position, Vector3.Angle(t, tt));*/
		/*	}
		}*/

		if(_grid != null) {
			switch(display_grid_info) {
			case DisplayGridInfo.Nodes:
				displayGridNodes();
				break;
			case DisplayGridInfo.Weights:
				displayGridWeights();
				break;
			}
			
		}
	}

	private void displayGridNodes() {
		foreach(Node n in _grid) {
			if(n == null)
				continue;

			if((!n.walkable /*&& !n.inAir*/) /*|| n == vn || n == bn*/)
				continue;

			Gizmos.color = (n.walkable) ? Color.white : Color.red;
			Gizmos.DrawCube(n.world_position, Vector3.one * (node_diameter - (node_diameter * .05f)));
		}
	}

	private void displayGridWeights() {
		foreach(Node n in _grid) {
			if(n == null)
				continue;

			if((!n.walkable /*&& !n.inAir*/) /*|| n == vn || n == bn*/)
				continue;

			GUIStyle style = new GUIStyle();
			style.normal.textColor = Color.green;

			if(n.movement_penalty >= 10)
				style.normal.textColor = Color.red;
			else if(n.movement_penalty >= 5)
				style.normal.textColor = Color.yellow;

			//Handles.color = Color.red;
			//Handles.Label(n.world_position, n.movement_penalty + "", style);
			//Gizmos.color = (n.walkable) ? Color.white : Color.red;
			//Gizmos.DrawCube(n.world_position, Vector3.one * (node_diameter - (node_diameter * .05f)));
		}
	}

	[System.Serializable]
	public class TerrainType {
		public LayerMask terrain_mask;
		public int terrain_penalty;
	}

	public enum DisplayGridInfo {
		None,
		Nodes,
		Weights
	}
}
