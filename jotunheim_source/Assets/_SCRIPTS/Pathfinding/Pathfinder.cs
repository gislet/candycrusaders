﻿using UnityEngine;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using System;


namespace Pathfinding {
	namespace Collection {
		public class Pathfinder : MonoBehaviour {

			/* Vitals! Cached objects. */
			private Grid _grid;
			private PathRequestManager _request_manager;

			
			void Awake() {
				_grid = GetComponent<Grid>();
				_request_manager = GetComponent<PathRequestManager>();
			}


			/* A way for others to access the grid object through the pathfinding. */
			public Grid grid {
				get { return _grid; }
			}


			/* Gets called by the request manager when it needs to find a path. (Should never be
			 * called by any other script!) */
			public void startFindPath(Vector3 p_start_pos, Vector3 p_target_pos) {
				StartCoroutine(findPath(p_start_pos, p_target_pos));
			}


			/* The co-routine that attempts to find a path to our target, or at worst, the nearest point
			 * to our target. */
			IEnumerator findPath(Vector3 p_start_pos, Vector3 p_target_pos) {

				/* Setting up the essential variables for all of this to succeed. */
				Vector3[] waypoints = new Vector3[0];
				bool path_success = false;
				Node start_node = _grid.BestNodeFromWorldPoint(p_start_pos);
				Node target_node = _grid.BestNodeFromWorldPoint(p_target_pos);
				Node best_node = null;

				/* Making sure that our starting locating is actually on a walkable node,
				 * otherwise there is no way to find a path. */
				if(start_node.walkable) {

					/* Initialising the open- and closed-set. */
					Heap<Node> open_set = new Heap<Node>(_grid.max_size);
					HashSet<Node> closed_set = new HashSet<Node>();

					/* Adding our start node to the open-set. */
					open_set.Add(start_node);

					/* For as long as the open-set is populated, unless the loop breaks under some internal condition,
					 * it will continue to loop. */
					while(open_set.Count > 0) {

						/* We pop a node off our open-set and assign it to a temporary 'current-node' object. */
						Node current_node = open_set.pop();
						closed_set.Add(current_node);

						/* We check to see if this current-node is actually our target-node. */
						if(current_node == target_node) {

							/* If that is true, amazing! Our pathfinding was successul and we break out of 
							 * our while-loop. */
							path_success = true;
							break;
						}

						/* We want to make sure this node has neighbours. If not we try and find them. */
						if(current_node.neighbours == null)
							current_node.neighbours = _grid.getNeighbours(current_node);

						/* We also want to make sure this node is walkable (a status which might have changed after our
						 * neighbour checking process), and if it is, also make sure it isn't a deadend for whatever reason. */
						if(current_node.walkable && current_node.neighbours != null) {

							/* We iterate through all of our node's neighbours. */
							foreach(Node neighbour in current_node.neighbours) {

								/* We make sure our neighbour is indeed walkable and that it doesn't
								 * already exist in our closed-set. */
								if(!neighbour.walkable || closed_set.Contains(neighbour)) {
									continue;
								}

								/* We initiate a temporary variable that holds this nodes possible g-cost. */
								int new_movement_cost_to_neighbour = current_node.gCost + getDistance(current_node, neighbour) + neighbour.movement_penalty;

								/* If the neighbour had already been discovered and exists in our open-set, then we check to see if
								 * our temporary g-cost variable is less than its current. If that is true, or the object just simply doesn't
								 * exist in our set yet, we add it and assign this g-cost to it. */
								if(new_movement_cost_to_neighbour < neighbour.gCost || !open_set.Contains(neighbour)) {
									neighbour.gCost = new_movement_cost_to_neighbour;

									/* We also assign our current node as this node's parent, and
									 * we assign it an h-cost. */
									neighbour.hCost = getDistance(neighbour, target_node);
									neighbour.parent = current_node;

									/* We then either add it to our open-set, or find the existing
									 * node and update it. */
									if(!open_set.Contains(neighbour)) {
										open_set.Add(neighbour);
									} else {
										open_set.updateItem(neighbour);
									}
								}
							}

							/* For every iterating, we also assign our current-node as our best-node,
							 * that is if our current-node has the best possible h-cost in comparrison to our
							 * already cached best-node. We do this in case we never find a path, which means
							 * whatever node is saved as our best-node will be our new target to move towards. */
							if(best_node == null) {
								if(current_node != start_node) {
									best_node = current_node;
								}
							} else if(best_node.hCost > current_node.hCost) {
								best_node = current_node;
							}
						}
					}

					/* If we weren't successful at finding a path, we will assign the path we found
					 * for our best-node to our waypoints array. */
					if(!path_success && best_node != null) {
						waypoints = retracePath(start_node, best_node);
					}
				} 

				yield return null;

				/* If we did find a path, we assign that path our our waypoint array. */
				if(path_success) {
					waypoints = retracePath(start_node, target_node);
				}

				/* Whateve the results are, we send them back to our request-manager, which sends it
				 * to the correct object that asked for it. */
				_request_manager.finishedProcessingPath(waypoints, path_success);
			}
			

			/* We retrace the path we ended up with. */
			Vector3[] retracePath(Node p_start_node, Node p_end_node) {

				List<Node> path = new List<Node>();
				Node current_node = p_end_node;

				while(current_node != p_start_node) {
					path.Add(current_node);
					current_node = current_node.parent;
				}

				/* We simplify our path, so that it doesn't have as many nodes to move towards. */
				Vector3[] waypoints = simplyPath(path);

				/* We then reverse the direction of our path, as it is currently set to move from
				 * end to start. */
				Array.Reverse(waypoints);

				/* We then return it as an array. */
				return waypoints;
			}


			/* This method simplifies a path so that it contains less nodes. It also turns it into an array
			 * instead of a list. */
			Vector3[] simplyPath(List<Node> p_path) {

				List<Vector3> waypoints = new List<Vector3>();
				Vector3 direction_old = Vector3.zero;

				for(int i = 1; i < p_path.Count; i++) {
					Vector3 direction_new = new Vector3(p_path[i-1].gridX - p_path[i].gridX, 
														p_path[i-1].gridY - p_path[i].gridY,
														p_path[i-1].gridZ - p_path[i].gridZ);

					if(direction_new != direction_old) {
						waypoints.Add(p_path[i].world_position);
					}
					direction_old = direction_new;
				}

				/* We then return it as an array instead of a list. */
				return waypoints.ToArray();
			}


			/* Retrieves a manhatten heuristical value. */
			int getDistance(Node p_nodeA, Node p_nodeB) {
				int dstX = Mathf.Abs(p_nodeA.gridX - p_nodeB.gridX);
				int dstZ = Mathf.Abs(p_nodeA.gridZ - p_nodeB.gridZ);

				if(dstX > dstZ)
					return 14 * dstZ + 10 * (dstX - dstZ);

				return 14 * dstX + 10 * (dstZ - dstX);
			}
		}
	}
}
