﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


namespace Pathfinding {
	namespace Collection {

		/* An object storing information regarding pathfinding nodes. */
		public class Node : IHeapItem<Node> {

			public bool walkable, inAir;
			public Vector3 world_position;
			public int gridX, gridY, gridZ;
			public int movement_penalty;

			public Node parent;
			private List<Node> _neighbours;

			public int gCost, hCost;
			private int _heapIndex;


			/* The constructor, setting initial values for the nodes accessability, position, and grid indexes. */
			public Node(bool p_walkable, bool p_inAir, Vector3 p_world_pos, int p_gridX, int p_gridY, int p_gridZ, int p_penalty) {

				inAir = p_inAir;
				walkable = p_walkable;
				world_position = p_world_pos;

				gridX = p_gridX;
				gridY = p_gridY;
				gridZ = p_gridZ;

				movement_penalty = p_penalty;
			}


			/* Sums the total cost this node's saved g- and h-cost. */
			public int fCost {
				get {
					return gCost + hCost;
				}
			}


			/* This node's index on the heap. */
			public int heapIndex {
				get {
					return _heapIndex;
				}
				set {
					_heapIndex = value;
				}
			}


			/* A list of this node's neighbours. */
			public List<Node> neighbours {
				get {
					return _neighbours;
				}
				set {
					_neighbours = value;
				}
			}


			/* A way for this node to compare itself to another - by assessing the values of
			 * this and the other node's f- or h-cost. */
			public int CompareTo(Node node_to_compare) {

				int compare = fCost.CompareTo(node_to_compare.fCost);

				if(compare == 0) {
					compare = hCost.CompareTo(node_to_compare.hCost);
				}
				return -compare;
			}
		}
	}
}
