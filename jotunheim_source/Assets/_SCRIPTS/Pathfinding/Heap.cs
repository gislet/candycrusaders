﻿using UnityEngine;
using System.Collections;
using System;

namespace Pathfinding {
	namespace Collection {

		public class Heap<T> where T : IHeapItem<T> {

			private T[] _items;
			private int _current_item_count;


			/* */
			public Heap(int p_max_heap_size) {
				_items = new T[p_max_heap_size];
			}


			/* */
			public void Add(T p_item) {
				p_item.heapIndex = _current_item_count;
				_items[_current_item_count] = p_item;
				sortUp(p_item);
				_current_item_count++;
			}


			/* */
			public T pop() {
				T first_item = _items[0];
				_current_item_count--;

				_items[0] = _items[_current_item_count];
				_items[0].heapIndex = 0;

				sortDown(_items[0]);
				return first_item;

			}


			/* */
			public void updateItem(T p_item) {
				sortUp(p_item);
			}


			/* */
			public int Count {
				get {
					return _current_item_count;
				}
			}


			/* */
			public bool Contains(T p_item) {
				return Equals(_items[p_item.heapIndex], p_item);
			}


			/* */
			void sortDown(T p_item) {

				while(true) {

					int child_index_left = p_item.heapIndex*2 + 1;
					int child_index_right = p_item.heapIndex*2 + 2;
					int swap_index = 0;

					if(child_index_left < _current_item_count) {
						swap_index = child_index_left;

						if(child_index_right < _current_item_count) {
							if(_items[child_index_left].CompareTo(_items[child_index_right]) < 0) {
								swap_index = child_index_right;
							}
						}

						if(p_item.CompareTo(_items[swap_index]) < 0) {
							swap(p_item, _items[swap_index]);
						} else {
							return;
						}
					} else {
						return;
					}
				}
			}


			/* */
			void sortUp(T p_item) {
				int parent_index = (p_item.heapIndex-1)/2;

				while(true) {
					T parent_item = _items[parent_index];

					if(p_item.CompareTo(parent_item) > 0) {
						swap(p_item, parent_item);
					} else {
						break;
					}

					parent_index = (p_item.heapIndex-1)/2;
				}
			}


			/* */
			void swap(T p_itemA, T p_itemB) {
				_items[p_itemA.heapIndex] = p_itemB;
				_items[p_itemB.heapIndex] = p_itemA;

				int item_index = p_itemA.heapIndex;
				p_itemA.heapIndex = p_itemB.heapIndex;
				p_itemB.heapIndex = item_index;
			}
		}


		/* */
		public interface IHeapItem<T> : IComparable<T> {
			int heapIndex {
				get;
				set;
			}
		}
	}
}