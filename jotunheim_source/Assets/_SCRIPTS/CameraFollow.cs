﻿/***
 * 
 * CameraFollow.cs
 * 
 * asdf
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/

 //pos 12 , 25, 0
 //rot 66 , xx, 0
 //??????????????????????

using UnityEngine;
using System.Collections;
using System;

public class CameraFollow : MonoBehaviour {


	public PlayerID player_of_interest;

	[Header("Leaving this value at 0 defaults it to 25.")]
	[Range(0, 30)]
	public float distance_from_target;

	[Header("Leaving this value at 0 defaults it to 60.")]
	[Range(0, 90)]
	public float angle_to_target;
	private float _old_angle_to_target = -1;

	private ITargetable _target;


	/* */
	private void Awake() {
		_target = null;
		angle_to_target = angle_to_target == 0 ? 60 : angle_to_target;
        distance_from_target = distance_from_target == 0 ? 25 : distance_from_target;
    }


	// Update is called once per frame
	private void FixedUpdate () {

		if(CameraManager.isCamerasLocked)
			return;

		updateTarget();
		follow();

	}


	/* Finds a valid target to lock on to. */
	private void updateTarget() {
		if(_target != null ? _target.isDestroyed() : false) {
			_target = null;
		}

		if(player_of_interest != PlayerID.unspecified) {
			if(_target != null ? _target.isType(typeof(CharacterBase)) : false) {
				return;
			} else {
				if(PlayerHandler.characters != null) {
					foreach(Character character in PlayerHandler.characters) {
						if(character.player_ID == (int)player_of_interest) {
							_target = character;
							break;
						}
					}
				}
			}
		}
	}


	/* If we've got a cached target, we follow it. */
	private void follow() {
		if(_target != null) {
			Vector3 dir = GlobalMovement.findDirectionByAngle(Vector3.zero, angle_to_target);

			if(_old_angle_to_target != angle_to_target) {
				_old_angle_to_target = angle_to_target;
                transform.rotation = Quaternion.identity;
				transform.Rotate(Vector3.right * _old_angle_to_target);
			}

			transform.position = _target.transform.position - (new Vector3(0, dir.z, dir.x) * distance_from_target);
		}
	}
}
