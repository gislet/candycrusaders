*******************************************************************************
****************************** README (PowerUps) ******************************
*******************************************************************************



By editing the <powerups.json> file, you can change the behaviour of every 
character modifying object in the game. You can also add and remove as you
please. This document is here to help you understand what each value means
and what boundaries they operate within.




*************** BASICS ***************


name: 					This is name of the object as it appears ingame.
interact_method:		How a character interacts with the object.
						0 (OnCollision), 1 (Explicit)


use_prefab:				***

prefab_object:			***
			
custom_object:			A mesh object that uses the <.obj> extensions. Should
						be supplied with a texture. (Not Implemented)
						
custom_texture: 		A texture object that uses the <.jpg, .png> extensions.
						Won't be used unless there's a custom_object. (Not 
						Implemented)




*************** SPEED ***************


modifies_speed:			Toggles whether the PowerUp object will use the various
						available speed modifications. Ignored if false.
						



*************** HEALTH ***************


modifies_health:		Toggles whether the PowerUp object will use the various
						available health modifications. Ignored if false.
						



*************** EQUIPMENTSLOT ***************

modifies_equipmentslot:	Toggles whether the PowerUp object will use the various
						available equipmentslot modifications. Ignored if false.














