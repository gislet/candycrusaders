﻿/***
 * 
 * AttachedStatusEffectInspector.cs
 * 
 * description.......
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using UnityEditor;
using System.Collections;


[CustomEditor(typeof(AttachedStatusEffect))]
public class AttachedStatusEffectInspector : Editor {


	private int _effects_index = -1;
	private string _effects_ID = "";
	private bool _forced_change = false;



	/* */
	public override void OnInspectorGUI() {

		base.OnInspectorGUI();

		if(DatabaseHandler.statuseffects_database.names != null && DatabaseHandler.statuseffects_database.names.Length > 0) {

			/* Attempt to try and find ID previously attached to our object - if the ID isn't empty. */
			if(_effects_index == -1) {
				if(((AttachedStatusEffect)target).status_effect_ID != null && ((AttachedStatusEffect)target).status_effect_ID.Length > 0) {
					int new_index = 0;

					if((new_index = DatabaseHandler.statuseffects_database.index(((AttachedStatusEffect)target).status_effect_ID)) != -1) {
						_effects_index = new_index;
						_forced_change = false;
					}
				}

				if(_effects_index == -1)
					_effects_index = 0;

				_effects_ID = ((AttachedStatusEffect)target).status_effect_ID = DatabaseHandler.statuseffects_database.list_all[_effects_index].ID;
			}

			/* If we're out of bounds, we need to reset the index value to the default value and warn about
			 * a forced change. */
			if(_effects_index >= DatabaseHandler.statuseffects_database.names.Length) {
				_effects_index = 0;
				_forced_change = true;
			}

			/* Should the effects list change, we want to make sure our attached effect doesn't. However the our effect
			 * no longer exists we want to reset our index value and warned about a forced change. Should the old effect
			 * suddenly reappear during the forced change warning, we will remove the warning and restore order. */
			if(DatabaseHandler.statuseffects_database.list_all[_effects_index].ID.CompareTo(_effects_ID) != 0 || _forced_change) {
				int new_index = 0;

				if((new_index = DatabaseHandler.statuseffects_database.index(_effects_ID)) != -1) {
					_effects_index = new_index;
					_forced_change = false;
				} else if(!_forced_change) {
					_effects_index = 0;
					_forced_change = true;
				}
			}

			/* Allows to you select an available status-effect. */
			_effects_index = EditorGUILayout.Popup(_effects_index, DatabaseHandler.statuseffects_database.names);

			/* Displays the currently selected status-effect's ID. */
            EditorGUILayout.SelectableLabel(string.Format("ID: \"{0}\"{1}", 
											((AttachedStatusEffect)target).status_effect_ID,
											_forced_change ? " -> \"" + DatabaseHandler.statuseffects_database.list_all[_effects_index].ID + "\"" : ""));

			
			if(_forced_change) {
				/* Requires the user to confirm a new change to suppress the forced change warning. */
				GUI.color = _forced_change ? Color.red : Color.white;
				if(GUILayout.Button("Accept Change.")) {
					_forced_change = false;
				}
				GUI.color = Color.white;
			} 

			if(!_forced_change) { 
				/* Won't allow an ID change to occur until the force change warning is suppressed. */
				_effects_ID = ((AttachedStatusEffect)target).status_effect_ID = DatabaseHandler.statuseffects_database.list_all[_effects_index].ID;
			}
		} else {
			GUILayout.Label("No Effects Exists.");
		}
	}
}
