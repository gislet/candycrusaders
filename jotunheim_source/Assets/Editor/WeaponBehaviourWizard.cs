﻿/******************************************************************************
*
* WeaponWizard.cs
*
* This script is an editor tool that allows designers to create and modify objects 
* we call Weapons. These objects allow the players to damage other players or 
* objects in the world. The tool uses Json files to store the created objects, 
* which also allows for external editing.
*
*
* Written by: Gisle Thorsen (August, 2017)
*
******************************************************************************/


using UnityEditor;
using UnityEngine;
using System.Collections;
using System.IO;
using LitJson;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System;
using Weapons;
using System.Reflection;


public class WeaponBehaviourWizard : EditorWindow {


	/* Caches. */
	private GlobalEditorStruct<WeaponBehaviour> _struct;

	/* Miscellaneous. */
	private System.DateTime _updatetime;
	private List<bool> _foldout_menu;
	private int _foldout_index;
	private Editor _editor_graphics_gameobject = null;


	/* */
	[MenuItem("Window/Wizards/WeaponBehaviourWizard")]
	public static void ShowWindow() {
		EditorWindow window = GetWindow(typeof(WeaponBehaviourWizard));
		window.minSize = new Vector2(950, 600);
	}


	/* */
	void OnEnable() {
		if(DatabaseHandler.behaviour_database == null)
			DatabaseHandler.awake();

		_struct = new GlobalEditorStruct<WeaponBehaviour>("Behaviour", DatabaseHandler.behaviour_database.directory_path, createNew, deleteEntry, selectEntry, saveDatabase, basicSettings, toggleModules, editModules);
		_foldout_menu = new List<bool>();
	}


	/* */
	void OnGUI() {
		if(DatabaseHandler.behaviour_database == null)
			DatabaseHandler.awake();

		if(DatabaseHandler.behaviour_database.private_writetime.CompareTo(_updatetime) != 0) {
			DatabaseHandler.update();
			_updatetime = DatabaseHandler.behaviour_database.private_writetime;
			selectEntry();
		}

		try {
			EditorGUILayout.BeginHorizontal(GUILayout.ExpandWidth(true));
			GlobalEditor.displayListArea(ref _struct, DatabaseHandler.behaviour_database.list_private);
			GlobalEditor.displayEditArea(ref _struct, DatabaseHandler.behaviour_database.list_private);
			EditorGUILayout.EndHorizontal();
		} catch {
			Repaint();
		}
	}


	/* */
	private void saveDatabase() {
		Globals.saveDatabase(DatabasePrivacy.Private, DatabaseHandler.behaviour_database.list_private, DatabaseHandler.behaviour_database.directory_path);
	}


	/* */
	private void createNew() {
		DatabaseHandler.behaviour_database.list_private.Add(new WeaponBehaviour());
		saveDatabase();
	}


	/* */
	private void deleteEntry() {
		saveDatabase();
	}


	/* */
	private void selectEntry() {
		if(DatabaseHandler.behaviour_database.list_private != null && DatabaseHandler.behaviour_database.list_private.Count > _struct.index && _struct.index >= 0)
			_struct.item = new WeaponBehaviour(DatabaseHandler.behaviour_database.list_private[_struct.index]);
	}


	/* */
	private void basicSettings() {
		GlobalEditor.label("Basics Settings", EditorStyles.boldLabel);
		GUILayout.Space(20);


		EditorGUILayout.BeginHorizontal();
		GUILayout.Label("Name: ", GUILayout.MinWidth(GlobalEditor.LABEL_MIN_WIDTH));
		_struct.item.name = GUILayout.TextField(_struct.item.name, GUILayout.MaxWidth(GlobalEditor.OPTIONS_MAX_WIDTH));
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		GUILayout.Label("Effect ID: ", GUILayout.MinWidth(GlobalEditor.LABEL_MIN_WIDTH));
		EditorGUILayout.SelectableLabel(_struct.item.ID, GUILayout.MaxWidth(GlobalEditor.OPTIONS_MAX_WIDTH));
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		GUILayout.Label("Cooldown (On Usage): ", GUILayout.MinWidth(GlobalEditor.LABEL_MIN_WIDTH));
		_struct.item.cooldown = EditorGUILayout.Slider((float)_struct.item.cooldown, Globals.GLOBAL_COOLDOWN, 60, GUILayout.MaxWidth(GlobalEditor.OPTIONS_MAX_WIDTH));
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		GUILayout.Label("Damage Modifier (Percentage of Base): ", GUILayout.MinWidth(GlobalEditor.LABEL_MIN_WIDTH));
		_struct.item.damage_modifier = EditorGUILayout.IntSlider(_struct.item.damage_modifier, 0, 1000, GUILayout.MaxWidth(GlobalEditor.OPTIONS_MAX_WIDTH));
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		GUILayout.Label("Standalone (Attack Functions Without Owner): ", GUILayout.MinWidth(GlobalEditor.LABEL_MIN_WIDTH));
		_struct.item.standalone = EditorGUILayout.Toggle(_struct.item.standalone, GUILayout.MaxWidth(GlobalEditor.OPTIONS_MAX_WIDTH));
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		GUILayout.Label("Repeat Inflictions (On Same Target): ", GUILayout.MinWidth(GlobalEditor.LABEL_MIN_WIDTH));
		_struct.item.repeat_infliction = EditorGUILayout.Toggle(_struct.item.repeat_infliction, GUILayout.MaxWidth(GlobalEditor.OPTIONS_MAX_WIDTH));
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		GUILayout.Label("Self Inflictions (Can Inflict On Owner): ", GUILayout.MinWidth(GlobalEditor.LABEL_MIN_WIDTH));
		_struct.item.self_infliction = EditorGUILayout.Toggle(_struct.item.self_infliction, GUILayout.MaxWidth(GlobalEditor.OPTIONS_MAX_WIDTH));
		EditorGUILayout.EndHorizontal();



		GUILayout.Space(20);
	}


	/* */
	private void toggleModules() {
		moduleToggle(ref _struct.item.mod_time, DatabaseHandler.behaviour_database.list_private[_struct.index].mod_time, "Time");
		moduleToggle(ref _struct.item.mod_areaofeffect, DatabaseHandler.behaviour_database.list_private[_struct.index].mod_areaofeffect, "AreaOfEffect");
		moduleToggle(ref _struct.item.mod_movement, DatabaseHandler.behaviour_database.list_private[_struct.index].mod_movement, "Movement");
		moduleToggle(ref _struct.item.mod_statuseffect, DatabaseHandler.behaviour_database.list_private[_struct.index].mod_statuseffect, "Status Effect");
		moduleToggle(ref _struct.item.mod_extrabehaviour, DatabaseHandler.behaviour_database.list_private[_struct.index].mod_extrabehaviour, "Extra Behaviour");
		moduleToggle(ref _struct.item.mod_graphics, DatabaseHandler.behaviour_database.list_private[_struct.index].mod_graphics, "Graphics");
	}


	/* */
	private void editModules() {
		_foldout_index = 0;

		/* Displays the Time modifying GUI if available. */
		if(_struct.item.mod_time != null) {
			moduleTime(ref _struct.item.mod_time);
			if(_foldout_menu[_foldout_index - 1])
				GUILayout.Space(20);
		}

		/* Displays the AreaOfEffect modifying GUI if available. */
		if(_struct.item.mod_areaofeffect != null) {
			moduleAOE(ref _struct.item.mod_areaofeffect);
			if(_foldout_menu[_foldout_index - 1])
				GUILayout.Space(20);
		}

		/* Displays the Movement modifying GUI if available. */
		if(_struct.item.mod_movement != null) {
			moduleMovement(ref _struct.item.mod_movement);
			if(_foldout_menu[_foldout_index - 1])
				GUILayout.Space(20);
		}

		/* Displays the Movement modifying GUI if available. */
		if(_struct.item.mod_statuseffect != null) {
			moduleStatusEffect(ref _struct.item.mod_statuseffect);
			if(_foldout_menu[_foldout_index - 1])
				GUILayout.Space(20);
		}

		/* Displays the Movement modifying GUI if available. */
		if(_struct.item.mod_extrabehaviour != null) {
			moduleExtraBehaviour(ref _struct.item.mod_extrabehaviour);
			if(_foldout_menu[_foldout_index - 1])
				GUILayout.Space(20);
		}

		/* Displays the Movement modifying GUI if available. */
		if(_struct.item.mod_graphics != null) {
			moduleGraphics(ref _struct.item.mod_graphics);
			if(_foldout_menu[_foldout_index - 1])
				GUILayout.Space(20);
		}
	}


	/* */
	private void moduleAOE(ref Weapons.Modules.AreaOfEffect p_module) {
		if(!isFoldedOut("Area-Of-Effect Module Settings"))
			return;

		/* Basic Settings */
		EditorGUILayout.BeginHorizontal();
		GUILayout.Label("OnTrigger (Stop): ", GUILayout.MinWidth(GlobalEditor.LABEL_MIN_WIDTH));
		p_module.ontrigger_stop = EditorGUILayout.Toggle(p_module.ontrigger_stop, GUILayout.MaxWidth(GlobalEditor.OPTIONS_MAX_WIDTH));
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		GUILayout.Label("Initial Distance From Origin: ", GUILayout.MinWidth(GlobalEditor.LABEL_MIN_WIDTH));
		p_module.distance_offset = EditorGUILayout.Slider((float)p_module.distance_offset, 0, 30, GUILayout.MaxWidth(GlobalEditor.OPTIONS_MAX_WIDTH));
		EditorGUILayout.EndHorizontal();
		GUILayout.Space(20);


		/* Collision Shape Settings */
		EditorGUILayout.BeginHorizontal();
		GUILayout.Label("Type of Collision Shape: ", GUILayout.MinWidth(GlobalEditor.LABEL_MIN_WIDTH));
		p_module.shape_type = (Weapons.Modules.ShapeType)EditorGUILayout.EnumPopup(p_module.shape_type, GUILayout.MaxWidth(GlobalEditor.OPTIONS_MAX_WIDTH));
		EditorGUILayout.EndHorizontal();		

		switch(p_module.shape_type) {
		case Weapons.Modules.ShapeType.Circle:
			moduleAOE_Circle(ref p_module);
            break;
		case Weapons.Modules.ShapeType.Rectangle:
			moduleAOE_Rectangle(ref p_module);
			break;
		case Weapons.Modules.ShapeType.Triangle:
			moduleAOE_Triangle(ref p_module);
			break;
		}
	}


	/* */
	private void moduleAOE_Circle(ref Weapons.Modules.AreaOfEffect p_module) {
		EditorGUILayout.BeginHorizontal();
		GUILayout.Label("Radius: ", GUILayout.MinWidth(GlobalEditor.LABEL_MIN_WIDTH));
		p_module.circle_radius = EditorGUILayout.Slider((float)p_module.circle_radius, 0, 30, GUILayout.MaxWidth(GlobalEditor.OPTIONS_MAX_WIDTH));
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		GUILayout.Label("Degrees (Halfway is always forward): ", GUILayout.MinWidth(GlobalEditor.LABEL_MIN_WIDTH));
		p_module.circle_degrees = EditorGUILayout.IntSlider(p_module.circle_degrees, 1, 360, GUILayout.MaxWidth(GlobalEditor.OPTIONS_MAX_WIDTH));
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		GUILayout.Label("Offset (Pushes the halfway point): ", GUILayout.MinWidth(GlobalEditor.LABEL_MIN_WIDTH));
		p_module.circle_offset = EditorGUILayout.IntSlider(p_module.circle_offset, -180, 180, GUILayout.MaxWidth(GlobalEditor.OPTIONS_MAX_WIDTH));
		EditorGUILayout.EndHorizontal();
	}


	/* */
	private void moduleAOE_Rectangle(ref Weapons.Modules.AreaOfEffect p_module) {
		if(p_module.rect_corners == null || p_module.rect_corners.Length != 2)
			p_module.rect_corners = new Vec2d[2];

		EditorGUILayout.BeginHorizontal();
		GUILayout.Label("Rectangle Corners (Needs to be opposite): ", GUILayout.MinWidth(GlobalEditor.LABEL_MIN_WIDTH));
		EditorGUILayout.BeginVertical();
		for(int i = 0; i < p_module.rect_corners.Length; i++) {
			p_module.rect_corners[i] = new Vec2d(EditorGUILayout.Vector2Field("Corner" + (i + 1), 
													p_module.rect_corners[i].toVector2(), 
													GUILayout.MaxWidth(GlobalEditor.OPTIONS_MAX_WIDTH)));
		}
		EditorGUILayout.EndVertical();
		EditorGUILayout.EndHorizontal();
	}


	/* */
	private void moduleAOE_Triangle(ref Weapons.Modules.AreaOfEffect p_module) {
		if(p_module.tri_corners == null || p_module.tri_corners.Length != 3)
			p_module.tri_corners = new Vec2d[3];

		EditorGUILayout.BeginHorizontal();
		GUILayout.Label("Triangle Corners: ", GUILayout.MinWidth(GlobalEditor.LABEL_MIN_WIDTH));
		EditorGUILayout.BeginVertical();
		for(int i = 0; i < p_module.tri_corners.Length; i++) {
			p_module.tri_corners[i] = new Vec2d(EditorGUILayout.Vector2Field("Corner " + (i + 1),
													p_module.tri_corners[i].toVector2(), 
													GUILayout.MaxWidth(GlobalEditor.OPTIONS_MAX_WIDTH)));
		}
		EditorGUILayout.EndVertical();
		EditorGUILayout.EndHorizontal();
	}


	/* */
	private void moduleMovement(ref Weapons.Modules.Movement p_module) {
		if(!isFoldedOut("Movement Module Settings"))
			return;

		EditorGUILayout.BeginHorizontal();
		GUILayout.Label("OnTrigger Environment (Stop): ", GUILayout.MinWidth(GlobalEditor.LABEL_MIN_WIDTH));
		p_module.ontrigger_stop = EditorGUILayout.Toggle(p_module.ontrigger_stop, GUILayout.MaxWidth(GlobalEditor.OPTIONS_MAX_WIDTH));
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		GUILayout.Label("Force Continuous (If Ticking is Enabled): ", GUILayout.MinWidth(GlobalEditor.LABEL_MIN_WIDTH));
		p_module.force_continuous = EditorGUILayout.Toggle(p_module.force_continuous, GUILayout.MaxWidth(GlobalEditor.OPTIONS_MAX_WIDTH));
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		GUILayout.Label("Travel Speed: ", GUILayout.MinWidth(GlobalEditor.LABEL_MIN_WIDTH));
		p_module.travel_speed = EditorGUILayout.Slider((float)p_module.travel_speed, 0, 60, GUILayout.MaxWidth(GlobalEditor.OPTIONS_MAX_WIDTH));
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		GUILayout.Label("Rotation Speed: ", GUILayout.MinWidth(GlobalEditor.LABEL_MIN_WIDTH));
		p_module.rotation_speed = EditorGUILayout.Slider((float)p_module.rotation_speed, 0, 1440, GUILayout.MaxWidth(GlobalEditor.OPTIONS_MAX_WIDTH));
		EditorGUILayout.EndHorizontal();
	}


	/* */
	private void moduleStatusEffect(ref Weapons.Modules.StatusEffect p_module) {
		if(!isFoldedOut("Status Effect Module Settings"))
			return;
	}


	private bool isFoldedOut(string p_title) {
		if(_foldout_menu.Count <= _foldout_index)
			_foldout_menu.Add(false);

		GUILayout.Space(10);
		bool foldout = _foldout_menu[_foldout_index];
		GlobalEditor.foldout(ref foldout, p_title, EditorStyles.boldLabel);
		_foldout_menu[_foldout_index++] = foldout;
		GUILayout.Space(20);

		if(!foldout)
			return false;

		return true;
	}


	/* */
	private void moduleExtraBehaviour(ref Weapons.Modules.ExtraBehaviour p_module) {
		if(!isFoldedOut("Extra Behaviour Module Settings"))
			return;
	}


	/* */
	private void moduleGraphics(ref Weapons.Modules.Graphics p_module) {
		if(!isFoldedOut("Graphics Module Settings"))
			return;

		EditorGUILayout.BeginHorizontal();
		GUILayout.Label("Expired (Behaviour Time): ", GUILayout.MinWidth(GlobalEditor.LABEL_MIN_WIDTH));
		p_module.expiration_time = (Weapons.Modules.ExpirationBehaviour)EditorGUILayout.EnumPopup(p_module.expiration_time, GUILayout.MaxWidth(GlobalEditor.OPTIONS_MAX_WIDTH));
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		GUILayout.Label("Expired (Environment Trigger): ", GUILayout.MinWidth(GlobalEditor.LABEL_MIN_WIDTH));
		p_module.expiration_environment_trigger = (Weapons.Modules.ExpirationBehaviour)EditorGUILayout.EnumPopup(p_module.expiration_environment_trigger, GUILayout.MaxWidth(GlobalEditor.OPTIONS_MAX_WIDTH));
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		GUILayout.Label("Expired (AreaOfEffect Trigger): ", GUILayout.MinWidth(GlobalEditor.LABEL_MIN_WIDTH));
		p_module.expiration_aoe_trigger = (Weapons.Modules.ExpirationBehaviour)EditorGUILayout.EnumPopup(p_module.expiration_aoe_trigger, GUILayout.MaxWidth(GlobalEditor.OPTIONS_MAX_WIDTH));
		EditorGUILayout.EndHorizontal();

		GUILayout.Space(20);

		EditorGUILayout.BeginHorizontal();
		GlobalEditor.selectFile<GameObject>(ref p_module.prefab_object, GlobalEditor.PREFAB_EXTENSIONS, ref _editor_graphics_gameobject);
		EditorGUILayout.EndHorizontal();
	}


	/* */
	private void moduleTime(ref Weapons.Modules.Time p_module) {
		if(!isFoldedOut("Time Module Settings"))
			return;


		/* */
		EditorGUILayout.BeginHorizontal();
		GUILayout.Label("Use Ticks: ", GUILayout.MinWidth(GlobalEditor.LABEL_MIN_WIDTH));
		p_module.ticking = EditorGUILayout.Toggle(p_module.ticking, GUILayout.MaxWidth(GlobalEditor.OPTIONS_MAX_WIDTH));
		EditorGUILayout.EndHorizontal();

		/* */
		if(p_module.ticking) {

			EditorGUILayout.BeginHorizontal();
			GUILayout.Label("Ticks (Total): ", GUILayout.MinWidth(GlobalEditor.LABEL_MIN_WIDTH));
			p_module.ticks = EditorGUILayout.IntSlider(p_module.ticks, 1, 60, GUILayout.MaxWidth(GlobalEditor.OPTIONS_MAX_WIDTH));
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.BeginHorizontal();
			GUILayout.Label("Tick Interval: ", GUILayout.MinWidth(GlobalEditor.LABEL_MIN_WIDTH));
			p_module.tick_interval = EditorGUILayout.Slider((float)p_module.tick_interval, 0, 60, GUILayout.MaxWidth(GlobalEditor.OPTIONS_MAX_WIDTH));
			EditorGUILayout.EndHorizontal();

		/* */
		} else {

			EditorGUILayout.BeginHorizontal();
			GUILayout.Label("Duration: ", GUILayout.MinWidth(GlobalEditor.LABEL_MIN_WIDTH));
			p_module.duration = EditorGUILayout.Slider((float)p_module.duration, 0, 60, GUILayout.MaxWidth(GlobalEditor.OPTIONS_MAX_WIDTH));
			EditorGUILayout.EndHorizontal();
		}
	}


	/* */
	private void moduleToggle<T>(ref T module, T backup, string p_tag) where T : class, new() {
		GUI.color = module != null ? GlobalEditor.SELECTED_COLOUR : Color.white;
		if(GUILayout.Button(p_tag, GUILayout.ExpandWidth(false))) {
			module = module != null ? null :
								backup != null ? backup : new T();
		}
	}
}
