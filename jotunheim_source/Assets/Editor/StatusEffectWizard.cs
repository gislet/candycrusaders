﻿/***
 * 
 * StatusEffectWizard.cs
 * 
 * This script is an editor tool that allows designers to create and modify objects we call Powerups.
 * These objects can both be negative and positive for the players, it is entirely up to the choices
 * the designer makes. The tool uses Json files to store the created objects, which also allows for
 * external editing.
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEditor;
using UnityEngine;
using System.Collections;
using System.IO;
using LitJson;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System;
using StatusEffects;
using Modules = StatusEffects.Modules;
using System.Reflection;

public class StatusEffectWizard : EditorWindow {


	/* Miscellaneous. */
	private System.DateTime _updatetime;
	private GlobalEditorStruct<StatusEffect> _struct;



	/* */
	[MenuItem("Window/Wizards/StatusEffectWizard")]
	public static void ShowWindow() {
		EditorWindow window = GetWindow(typeof(StatusEffectWizard));
		window.minSize = new Vector2(950, 600);
	}


	/* */
	void OnEnable() {
		if(DatabaseHandler.statuseffects_database == null)
			DatabaseHandler.awake();

		_struct = new GlobalEditorStruct<StatusEffect>("Status Effect", DatabaseHandler.statuseffects_database.directory_path, createNew, deleteEntry, selectEntry, saveDatabase, 
																		basicSettings, toggleModules, editModules);
	}


	/* */
	private void saveDatabase() {
		Globals.saveDatabase(DatabasePrivacy.Private, DatabaseHandler.statuseffects_database.list_private, DatabaseHandler.statuseffects_database.directory_path);
	}


	/* */
	private void updateDatabase() {
		DatabaseHandler.update();
		_updatetime = DatabaseHandler.statuseffects_database.private_writetime;
		selectEntry();
	}


	/* */
	void OnGUI() {
		if(DatabaseHandler.statuseffects_database == null)
			DatabaseHandler.awake();

		if(DatabaseHandler.statuseffects_database.private_writetime.CompareTo(_updatetime) != 0) {
			updateDatabase();			
		}

		try {
			EditorGUILayout.BeginHorizontal(GUILayout.ExpandWidth(true));
			GlobalEditor.displayListArea(ref _struct, DatabaseHandler.statuseffects_database.list_private);
			GlobalEditor.displayEditArea(ref _struct, DatabaseHandler.statuseffects_database.list_private);
			EditorGUILayout.EndHorizontal();
		} catch {
			Repaint();
		}
	}


	/* */
	private void createNew() {
		DatabaseHandler.statuseffects_database.list_private.Add(new StatusEffect());
		saveDatabase();
	}


	/* */
	private void deleteEntry() {
		saveDatabase();
	}


	/* */
	private void selectEntry() {
		if(DatabaseHandler.statuseffects_database.list_private != null && DatabaseHandler.statuseffects_database.list_private.Count > _struct.index && _struct.index >= 0)
			_struct.item = new StatusEffect(DatabaseHandler.statuseffects_database.list_private[_struct.index]);

		//Debug.Log("Current: " + _struct.item.mod_gfx.sprite_primary);
	}


	/* */
	private void toggleModules() {
		moduleToggle(ref _struct.item.mod_gfx, DatabaseHandler.statuseffects_database.list_private[_struct.index].mod_gfx, "Graphics");
		moduleToggle(ref _struct.item.mod_sfx, DatabaseHandler.statuseffects_database.list_private[_struct.index].mod_sfx, "Sound FX");
		moduleToggle(ref _struct.item.mod_time, DatabaseHandler.statuseffects_database.list_private[_struct.index].mod_time, "Time");
		moduleToggle(ref _struct.item.mod_movement, DatabaseHandler.statuseffects_database.list_private[_struct.index].mod_movement, "Movement");
		moduleToggle(ref _struct.item.mod_life, DatabaseHandler.statuseffects_database.list_private[_struct.index].mod_life, "Life");
		moduleToggle(ref _struct.item.mod_weapon, DatabaseHandler.statuseffects_database.list_private[_struct.index].mod_weapon, "Weapon");
	}


	/* */
	private void editModules() {
		/* Displays the Graphics modifying GUI if available. */
		if(_struct.item.mod_gfx != null) {
			moduleGFX();
			GUILayout.Space(20);
		}

		/* Displays the Sound modifying GUI if available. */
		if(_struct.item.mod_sfx != null) {
			moduleSFX();
			GUILayout.Space(20);
		}

		/* Displays the Time modifying GUI if available. */
		if(_struct.item.mod_time != null) {
			moduleTime();
			GUILayout.Space(20);
		}

		/* Displays the Movement modifying GUI if available. */
		if(_struct.item.mod_movement != null) {
			moduleMovement();
			GUILayout.Space(20);
		}

		/* Displays the Life modifying GUI if available. */
		if(_struct.item.mod_life != null) {
			moduleLife();
			GUILayout.Space(20);
		}

		/* Displays the Life modifying GUI if available. */
		if(_struct.item.mod_weapon != null) {
			moduleWeapon();
			GUILayout.Space(20);
		}
	}


	/* */
	private void moduleToggle<T>(ref T module, T backup, string p_tag) where T : class, new() {
		GUI.color = module != null ? GlobalEditor.SELECTED_COLOUR : Color.white;
		if(GUILayout.Button(p_tag, GUILayout.ExpandWidth(false))) {
			module = module != null ? null :
								backup != null ? backup : new T();
		}
	}


	/* */
	private void basicSettings() {
		GlobalEditor.label("Basics Settings", EditorStyles.boldLabel);
		GUILayout.Space(20);


		EditorGUILayout.BeginHorizontal();
		GUILayout.Label("Name: ", GUILayout.MinWidth(GlobalEditor.LABEL_MIN_WIDTH));
		_struct.item.name = GUILayout.TextField(_struct.item.name, GUILayout.MaxWidth(GlobalEditor.OPTIONS_MAX_WIDTH));
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		GUILayout.Label("Unique (Cannot Spawn Randomly, Forces Attachment): ", GUILayout.MinWidth(GlobalEditor.LABEL_MIN_WIDTH));
		_struct.item.unique = EditorGUILayout.Toggle(_struct.item.unique, GUILayout.MaxWidth(GlobalEditor.OPTIONS_MAX_WIDTH));
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		GUILayout.Label("Effect ID: ", GUILayout.MinWidth(GlobalEditor.LABEL_MIN_WIDTH));
		EditorGUILayout.SelectableLabel(_struct.item.ID, GUILayout.MaxWidth(GlobalEditor.OPTIONS_MAX_WIDTH));
		EditorGUILayout.EndHorizontal();
	}


	/* */
	private void moduleTime() {
		GlobalEditor.label("Time Module Settings", EditorStyles.boldLabel);
		GUILayout.Space(20);

		EditorGUILayout.BeginHorizontal();
		GUILayout.Label("Effect Duration: ", GUILayout.MinWidth(GlobalEditor.LABEL_MIN_WIDTH));
		_struct.item.mod_time.duration = EditorGUILayout.Slider((float)_struct.item.mod_time.duration, 0, 30, GUILayout.MaxWidth(GlobalEditor.OPTIONS_MAX_WIDTH));
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		GUILayout.Label("Ignore Timer Refresh: ", GUILayout.MinWidth(GlobalEditor.LABEL_MIN_WIDTH));
		_struct.item.mod_time.ignore_refresh = EditorGUILayout.Toggle(_struct.item.mod_time.ignore_refresh, GUILayout.MaxWidth(GlobalEditor.OPTIONS_MAX_WIDTH));
		EditorGUILayout.EndHorizontal();
	}


	/* */
	private void moduleWeapon() {
		GlobalEditor.label("Weapon Module Settings", EditorStyles.boldLabel);
		GUILayout.Space(20);


		EditorGUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		GUILayout.Label("Select Weapon: ");
		int current = DatabaseHandler.weapons_database.index(_struct.item.mod_weapon.weapon_ID);
		if(current != -1) {
			int change = EditorGUILayout.Popup(current, DatabaseHandler.weapons_database.names);
			if(current != change) {
				_struct.item.mod_weapon.weapon_ID = DatabaseHandler.weapons_database.list_private[change].ID;
			}
		} else if(_struct.item.mod_weapon.weapon_ID == null || _struct.item.mod_weapon.weapon_ID.CompareTo("") == 0) {
			_struct.item.mod_weapon.weapon_ID = DatabaseHandler.weapons_database.list_private[0].ID;
		}
		GUILayout.FlexibleSpace();
		EditorGUILayout.EndHorizontal();


		EditorGUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		GUILayout.Label("Weapon ID: " + _struct.item.mod_weapon.weapon_ID);
		GUILayout.FlexibleSpace();
		EditorGUILayout.EndHorizontal();
	}


	/* */
	private void moduleGFX() {
		GUILayout.Space(10);
		GlobalEditor.label("Graphics Module Settings", EditorStyles.boldLabel);
		GUILayout.Space(20);

		EditorGUILayout.BeginVertical();
		/*label("Sprite:", false);
		Rect rect = GUILayoutUtility.GetRect(PREVIEW_WINDOW_SIZE, PREVIEW_WINDOW_SIZE, GUILayout.MaxHeight(PREVIEW_WINDOW_SIZE), GUILayout.MaxWidth(PREVIEW_WINDOW_SIZE));
		EditorGUI.DrawPreviewTexture(rect, new Texture2D(1, 1));*/
		GlobalEditor.selectFile<Texture2D>(ref _struct.item.mod_gfx.sprite_primary, GlobalEditor.TEXTURE_EXTENSIONS, ref editor);
		EditorGUILayout.EndVertical();

		GUILayout.Space(20);
	}

	private Editor editor;

	/* */
	private void moduleSFX() {
		GUILayout.Space(10);
		GlobalEditor.label("Sound Module Settings", EditorStyles.boldLabel);
		GUILayout.Space(20);

		EditorGUILayout.BeginVertical();

		GlobalEditor.selectFile<object>(ref _struct.item.mod_sfx.sfx_pickup, GlobalEditor.SOUND_EXTENSIONS, ref editor);
		if(_struct.item.mod_sfx.sfx_pickup != null && _struct.item.mod_sfx.sfx_pickup.Length > 0) {
			EditorGUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			if(GUILayout.Button("\u25B6", "box")) {
				PreviewSound.playClip(Resources.Load(_struct.item.mod_sfx.sfx_pickup) as AudioClip);
			}
			if(GUILayout.Button("\u275A\u275A", "box")) {
				PreviewSound.stopClip();
            }
			GUILayout.FlexibleSpace();
			EditorGUILayout.EndHorizontal();
		}

		EditorGUILayout.EndVertical();

		GUILayout.Space(20);
	}


	/* */
	private void moduleMovement() {
		GUILayout.Space(10);
		GlobalEditor.label("Movement Module Settings", EditorStyles.boldLabel);
		GUILayout.Space(20);

		EditorGUILayout.BeginHorizontal();
		GUILayout.Label("Speed (100 default): ", GUILayout.MinWidth(GlobalEditor.LABEL_MIN_WIDTH));
		_struct.item.mod_movement.speed = EditorGUILayout.IntSlider(_struct.item.mod_movement.speed, 0, 200, GUILayout.MaxWidth(GlobalEditor.OPTIONS_MAX_WIDTH));
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		GUILayout.Label("Daze Immunity (Ignores Negative Modifiers): ", GUILayout.MinWidth(GlobalEditor.LABEL_MIN_WIDTH));
		_struct.item.mod_movement.daze_immunity = EditorGUILayout.Toggle(_struct.item.mod_movement.daze_immunity, GUILayout.MaxWidth(GlobalEditor.OPTIONS_MAX_WIDTH));
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		GUILayout.Label("Unavoidable (Ignores Immunities): ", GUILayout.MinWidth(GlobalEditor.LABEL_MIN_WIDTH));
		_struct.item.mod_movement.unavoidable = EditorGUILayout.Toggle(_struct.item.mod_movement.unavoidable, GUILayout.MaxWidth(GlobalEditor.OPTIONS_MAX_WIDTH));
		EditorGUILayout.EndHorizontal();

		GUILayout.Space(20);
	}


	/* */
	private void moduleLife() {
		GUILayout.Space(10);
		GlobalEditor.label("Life Module Settings", EditorStyles.boldLabel);
		GUILayout.Space(20);

		EditorGUILayout.BeginHorizontal();
		GUILayout.Label("Heal Amount: ", GUILayout.MinWidth(GlobalEditor.LABEL_MIN_WIDTH));
		_struct.item.mod_life.heal_amount = EditorGUILayout.IntSlider(_struct.item.mod_life.heal_amount, 0, 10, GUILayout.MaxWidth(GlobalEditor.OPTIONS_MAX_WIDTH));
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		GUILayout.Label("Ticks: ", GUILayout.MinWidth(GlobalEditor.LABEL_MIN_WIDTH));
		_struct.item.mod_life.tick_amount = EditorGUILayout.IntSlider(_struct.item.mod_life.tick_amount, 1, 10, GUILayout.MaxWidth(GlobalEditor.OPTIONS_MAX_WIDTH));
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		GUILayout.Label("Ignore Ticks Refresh: ", GUILayout.MinWidth(GlobalEditor.LABEL_MIN_WIDTH));
		_struct.item.mod_life.ignore_refresh = EditorGUILayout.Toggle(_struct.item.mod_life.ignore_refresh, GUILayout.MaxWidth(GlobalEditor.OPTIONS_MAX_WIDTH));
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		GUILayout.Label("Immortal: ", GUILayout.MinWidth(GlobalEditor.LABEL_MIN_WIDTH));
		_struct.item.mod_life.immortal = EditorGUILayout.Toggle(_struct.item.mod_life.immortal, GUILayout.MaxWidth(GlobalEditor.OPTIONS_MAX_WIDTH));
		EditorGUILayout.EndHorizontal();

		GUILayout.Space(20);
	}	
}





