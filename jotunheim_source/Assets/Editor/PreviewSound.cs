﻿/***
 * 
 * PreviewSound.cs
 * 
 * This is an editor script to allow various editor-guis to preview soundclips. It caches the received soundclip
 * and plays it immediately. Should a call come in to stop a soundclip there's one cached, then that clip will  
 * seize to play should it be currently running.
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Reflection;
using System;


public static class PreviewSound {


	private static Assembly _unityEditorAssembly;
	private static Type _audioUtilClass;
	private static MethodInfo _playClip_method, _stopClips_method;


	/* */
	private static void initialise() {
		_initialsed = true;

		_unityEditorAssembly = typeof(AudioImporter).Assembly;
		_audioUtilClass = _unityEditorAssembly.GetType("UnityEditor.AudioUtil");
		_playClip_method = _audioUtilClass.GetMethod(
			"PlayClip",
			BindingFlags.Static | BindingFlags.Public,
			null,
			new System.Type[] {	typeof(AudioClip) },
			null
		);

		_stopClips_method = _audioUtilClass.GetMethod(
			"StopAllClips",
			BindingFlags.Static | BindingFlags.Public,
			null,
			new System.Type[] { },
			null
		);
	}


	/* */
	public static void playClip(AudioClip p_clip) {
		if(!_initialsed)
			initialise();

		_playClip_method.Invoke(
			null,
			new object[] { p_clip }
		);
	}


	/* */
	public static void stopClip() {
		if(!_initialsed)
			initialise();

		_stopClips_method.Invoke(
			null,
			new object[] { }
		);
	}


	#region Getters & Setters
	private static bool _initialsed { get; set; }
	#endregion
}
