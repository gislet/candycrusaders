﻿/***
 * 
 * GenericListWizard.cs
 * 
 * ...
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEngine;
using System.Collections;
using UnityEditor;
using System;
using System.IO;
using System.Collections.Generic;
using System.Text.RegularExpressions;


public static class GlobalEditor {


	/* Constants. */
	public const int DEFAULT_BUTTON_WIDTH = 75;
	public const int PREVIEW_WINDOW_SIZE = 150;
	public const int OPTIONS_MAX_WIDTH = 300;
	public const int LABEL_MIN_WIDTH = 150;
	public const int LIST_AREA_WIDTH = 250;
	public const int ITEM_COUNT_LABEL_WIDTH = 125;
	public const int REMOVE_ITEM_BUTTON_WIDTH = 25;

	public static readonly string[] TEXTURE_EXTENSIONS = { "png", "jpg" };
	public static readonly string[] SOUND_EXTENSIONS = { "wav", "mp3", "ogg" };
	public static readonly string[] PREFAB_EXTENSIONS = { "prefab" };
	public static readonly string[] MESH_EXTENSIONS = { "fbx" };

	public static readonly Color SELECTED_COLOUR = new Color(0, .5f, 0, .5f);

	private static Editor gameObject_editor;



	/* Presents an array of generic objects as a list that the user can to add, remove and select entries from. */
	public static void displayListArea<T>(ref GlobalEditorStruct<T> p_struct, List<T> p_list) where T : class, IIdentifiable {
		EditorGUILayout.BeginVertical(GUILayout.Width(LIST_AREA_WIDTH));
		EditorGUILayout.Space();


		/* Displays a list of objects you can interact with. */
		p_struct.listarea_SP = EditorGUILayout.BeginScrollView(p_struct.listarea_SP, "box", GUILayout.ExpandHeight(true));
		for(int i = 0; p_list != null && i < p_list.Count; i++) {
			EditorGUILayout.BeginHorizontal();

			/* If the list entry is not targeted for deletion, it will show as normal.*/
			if(!p_struct.delete_confirm.Contains(i)) {
				GUI.color = new Color(1f, 0, 0, .75f);
				if(GUILayout.Button("-", GUILayout.Width(REMOVE_ITEM_BUTTON_WIDTH))) {
					p_struct.delete_confirm.Add(i);
				}

				/* Allows you to select an object. Color of the button indicates whether it is
				 * selected or not. */
				GUI.color = i == p_struct.index ? SELECTED_COLOUR : Color.white;
				if(GUILayout.Button(p_list[i].name, "box", GUILayout.ExpandWidth(true))) {
					p_struct.index = i;
					if(p_struct.action_select != null)
						p_struct.action_select();
				}
				GUI.color = Color.white;
			} else {

				/* Cancels the deletion of a list entry. */
				if(GUILayout.Button("Cancel", GUILayout.ExpandWidth(true))) {
					p_struct.delete_confirm.Remove(i);
				}

				/* Confirms the deletion of a list entry, resets a few values and calls an action tied to
				 * the deletion process. */
				if(GUILayout.Button("Delete", GUILayout.ExpandWidth(true))) {
					p_struct.delete_confirm.Remove(i);
					p_list.RemoveAt(i);
					p_struct.index = -1;
					if(p_struct.action_deletion != null)
						p_struct.action_deletion();
					p_struct.item = null;
					break;
				}
			}

			EditorGUILayout.EndHorizontal();
		}
		EditorGUILayout.EndScrollView();

		/* */
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.HelpBox("Some settings might not work!\n\nNotify a developer if something doesn't seem to work as intended.", MessageType.Warning);
		EditorGUILayout.EndHorizontal();

		/* Allows the user to open the JSON file associated with this database externally using their default text editing application. */
		EditorGUILayout.BeginHorizontal();
		if(GUILayout.Button(string.Format("Open JSON File (Externally)"))) {
			Globals.openFile(p_struct.directory);
		}
		EditorGUILayout.EndHorizontal();


		/* Label indicating how many entries the list has. */
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField(string.Format("{0}s: ", p_struct.name) + (p_list != null ? p_list.Count : 0), GUILayout.Width(ITEM_COUNT_LABEL_WIDTH));
		if(GUILayout.Button(string.Format("New {0}", p_struct.name))) {
			if(p_struct.action_createnew != null)
				p_struct.action_createnew();
		}
		EditorGUILayout.EndHorizontal();


		EditorGUILayout.Space();
		EditorGUILayout.EndVertical();
	}


	/* */
	public static void displayEditArea<T>(ref GlobalEditorStruct<T> p_struct, List<T> p_list) where T : class, IIdentifiable {
		EditorGUILayout.BeginVertical(GUILayout.ExpandWidth(true));
		EditorGUILayout.Space();


		/* */
		p_struct.editarea_SP = EditorGUILayout.BeginScrollView(p_struct.editarea_SP, GUILayout.ExpandHeight(true));
		if(p_struct.index >= 0 && p_struct.item != null) {

			/* */
			if(p_struct.action_basic_settings != null) {
				p_struct.action_basic_settings();
			}
			GUILayout.Space(20);


			/* */
			if(p_struct.action_toggle_modules != null) {
				label("Active Modules", EditorStyles.boldLabel);
				EditorGUILayout.BeginHorizontal();
				GUILayout.FlexibleSpace();

				p_struct.action_toggle_modules();

				GUI.color = Color.white;
				GUILayout.FlexibleSpace();
				EditorGUILayout.EndHorizontal();
				GUILayout.Space(40);
			}


			if(p_struct.action_edit_modules != null) {
				p_struct.action_edit_modules();
            }
		}
		EditorGUILayout.EndScrollView();

		/* */
		basicTools(ref p_struct, p_list);

		EditorGUILayout.Space();
		EditorGUILayout.EndVertical();
	}


	/* */
	public static void label(string p_label, bool p_center = true) {
		label(p_label, EditorStyles.label, p_center);
	}

	public static void label(string p_label, GUIStyle p_style, bool p_center = true, params GUILayoutOption[] p_options) {
		EditorGUILayout.BeginHorizontal();
		if(p_center)
			GUILayout.FlexibleSpace();
		GUILayout.Label(p_label, p_style);
		if(p_center)
			GUILayout.FlexibleSpace();
		EditorGUILayout.EndHorizontal();
	}


	/* */
	public static void foldout(ref bool p_current, string p_label, GUIStyle p_style, bool p_center = true, params GUILayoutOption[] p_options) {
		EditorGUILayout.BeginHorizontal();
		if(p_center)
			GUILayout.FlexibleSpace();
		if(GUILayout.Button(string.Format("{0} {1}", !p_current ? "\u25B6" : "\u25BC", p_label), p_style))
			p_current = !p_current;
		if(p_center)
			GUILayout.FlexibleSpace();
		EditorGUILayout.EndHorizontal();
	}


	/* */
	private static void basicTools<T>(ref GlobalEditorStruct<T> p_struct, List<T> p_list) where T : class, IIdentifiable {
		EditorGUILayout.BeginHorizontal();

		if(GUILayout.Button("CLOSE")) {
			p_struct.index = -1;
		}

		if(GUILayout.Button("REVERT CHANGES")) {
			if(p_struct.index >= 0) {
				if(p_struct.action_select != null)
					p_struct.action_select();
			}
		}

		if(GUILayout.Button("CLOSE & SAVE")) {
			p_list[p_struct.index] = p_struct.item;
			if(p_struct.action_saveDatabase != null)
				p_struct.action_saveDatabase();
			p_struct.index = -1;
		}

		if(GUILayout.Button("SAVE")) {
			p_list[p_struct.index] = p_struct.item;
			if(p_struct.action_saveDatabase != null)
				p_struct.action_saveDatabase();
		}

		EditorGUILayout.EndHorizontal();
	}


	/* */
	public static void selectFile<T>(ref string p_path, string[] p_extensions, ref Editor p_editor) where T : class {
		if(p_path == null)
			p_path = "";

		T resource = Resources.Load(p_path, typeof(T)) as T;

		EditorGUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		EditorGUILayout.BeginVertical();

		if(resource != null) {
			Rect rect = GUILayoutUtility.GetRect(PREVIEW_WINDOW_SIZE, PREVIEW_WINDOW_SIZE, GUILayout.MaxHeight(PREVIEW_WINDOW_SIZE), GUILayout.MaxWidth(PREVIEW_WINDOW_SIZE));

			if(resource.GetType() == typeof(Texture2D)) {
				Texture2D tex = resource as Texture2D;
				tex.alphaIsTransparency = true;
				tex.wrapMode = TextureWrapMode.Repeat;
				EditorGUI.DrawPreviewTexture(rect, tex);
			} else if(resource.GetType() == typeof(GameObject)) {
				if(p_editor == null)
					p_editor = Editor.CreateEditor(resource as GameObject);
				p_editor.OnPreviewGUI(rect, null);
			}			
		}

		if(GUILayout.Button(resource == null ? "None Selected" : p_path, "box", GUILayout.MaxWidth(LABEL_MIN_WIDTH))) {
			string root_directory = Application.dataPath + "/Resources/";
			string extensions = "";
			string rgx_extensions_pattern = "";

			for(int i = 0; i < p_extensions.Length; i++) {
				extensions += p_extensions[i];
				rgx_extensions_pattern += "." + p_extensions[i];

				if(i + 1 < p_extensions.Length) {
					extensions += ",";
					rgx_extensions_pattern += "|";
				}
			}

			rgx_extensions_pattern = @"(" + rgx_extensions_pattern + ")";
			string path = EditorUtility.OpenFilePanel("Load GameObject",
																root_directory,
																extensions);

			/* We want to make sure the path selected is located in the Resources folder, otherwise the objects
			 * selected can't be loaded. */
			if(path != null && path.Length > 0 && new Regex(@"(Assets/Resources/)").IsMatch(path)) {
				path = path.Remove(0, root_directory.Length);
				Regex rgx = new Regex(rgx_extensions_pattern);
				path = rgx.Replace(path, "");
			} else {
				path = p_path;
			}

			p_path = path;
		}

		if(resource != null && GUILayout.Button("Clear")) {
			p_path = "";
		}

		EditorGUILayout.EndVertical();
		GUILayout.FlexibleSpace();
		EditorGUILayout.EndHorizontal();
	}
}


public struct GlobalEditorStruct<T> where T : class, IIdentifiable {

	public string name, directory;
	public int index;
	public T item;
	public Vector2 listarea_SP, editarea_SP;
	public List<int> delete_confirm;
	public Action action_deletion, action_createnew, action_select, action_saveDatabase, action_toggle_modules, action_edit_modules, action_basic_settings;


	public GlobalEditorStruct(string p_name, string p_directory, Action p_createnew = null, Action p_delete = null, Action p_select = null, Action p_saveDatabase = null, 
											Action p_basic_settings = null, Action p_toggle_modules = null, Action p_edit_modules = null) {

		name = p_name;
		directory = p_directory;

		item = null;
		index = -1;
		listarea_SP = new Vector2();
		editarea_SP = new Vector2();
		delete_confirm = new List<int>();

		action_deletion = p_delete;
		action_createnew = p_createnew;
		action_select = p_select;
		action_saveDatabase = p_saveDatabase;
		action_toggle_modules = p_toggle_modules;
		action_edit_modules = p_edit_modules;
		action_basic_settings = p_basic_settings;
    }
}
