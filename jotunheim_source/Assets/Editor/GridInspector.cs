﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(Grid))]
public class GridInspector : Editor {

	public override void OnInspectorGUI() {
		DrawDefaultInspector();

		EditorGUILayout.BeginVertical();

		if(GUILayout.Button("Remake Grid")) {
			Grid grid = (Grid)target;
			grid.create();
		}

		EditorGUILayout.EndVertical();
	}
}
