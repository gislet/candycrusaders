﻿/***
 * 
 * WeaponWizard.cs
 * 
 * This script is an editor tool that allows designers to create and modify objects we call Weapons.
 * These objects allow the players to damage other players or objects in the world. The tool uses Json
 * files to store the created objects, which also allows for external editing.
 * 
 * 
 * Written by Gisle Thorsen
 * 
 ***/


using UnityEditor;
using UnityEngine;
using System.Collections;
using System.IO;
using LitJson;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System;
using Weapons;
using System.Reflection;


public class WeaponWizard : EditorWindow {


	/* Caches. */
	private GlobalEditorStruct<Weapon> _struct;

	/* Miscellaneous. */
	private System.DateTime _updatetime;
	private int _primary_index = -1, _secondary_index = -1;
	private bool _visuals_foldout = false, _primary_foldout = false, _secondary_foldout = false;
	private Editor _editor_right_gameobject = null, _editor_left_gameobject = null, _editor_logo;



	/* */
	[MenuItem("Window/Wizards/WeaponWizard")]
	public static void ShowWindow() {
		EditorWindow window = GetWindow(typeof(WeaponWizard));
		window.minSize = new Vector2(950, 600);
	}


	/* */
	void OnEnable() {
		if(DatabaseHandler.weapons_database == null)
			DatabaseHandler.awake();

		_struct = new GlobalEditorStruct<Weapon>("Weapon", DatabaseHandler.weapons_database.directory_path, createNew, deleteEntry, selectEntry, saveDatabase, basicSettings, null, null);
	}


	/* */
	void OnGUI() {
		if(DatabaseHandler.weapons_database == null)
			DatabaseHandler.awake();

		if(DatabaseHandler.weapons_database.private_writetime.CompareTo(_updatetime) != 0) {
			DatabaseHandler.update();
			_updatetime = DatabaseHandler.weapons_database.private_writetime;
			selectEntry();
			Debug.Log("Weapons update occured - " + DatabaseHandler.weapons_database.list_private[1].right_limb.ToString());
			return;
		}

		try {
			EditorGUILayout.BeginHorizontal(GUILayout.ExpandWidth(true));
			GlobalEditor.displayListArea(ref _struct, DatabaseHandler.weapons_database.list_private);
			GlobalEditor.displayEditArea(ref _struct, DatabaseHandler.weapons_database.list_private);
			EditorGUILayout.EndHorizontal();
		} catch {
			Repaint();
		}
	}


	/* */
	private void saveDatabase() {
		Globals.saveDatabase(DatabasePrivacy.Private, DatabaseHandler.weapons_database.list_private, DatabaseHandler.weapons_database.directory_path);
	}


	/* */
	private void createNew() {
		DatabaseHandler.weapons_database.list_private.Add(new Weapon());
		saveDatabase();
	}


	/* */
	private void deleteEntry() {
		saveDatabase();
	}


	/* */
	private void selectEntry() {
		Debug.Log("Selecting Entry");

		if(DatabaseHandler.weapons_database.list_private != null && DatabaseHandler.weapons_database.list_private.Count > _struct.index && _struct.index >= 0)
			_struct.item = new Weapon(DatabaseHandler.weapons_database.list_private[_struct.index]);

		_primary_index = _secondary_index = -1;

		_editor_left_gameobject = _editor_right_gameobject = null;
    }


	/* */
	private void basicSettings() {
		GlobalEditor.label("Basics Settings", EditorStyles.boldLabel);
		GUILayout.Space(20);


		EditorGUILayout.BeginHorizontal();
		GUILayout.Label("Name: ", GUILayout.MinWidth(GlobalEditor.LABEL_MIN_WIDTH));
		_struct.item.name = GUILayout.TextField(_struct.item.name, GUILayout.MaxWidth(GlobalEditor.OPTIONS_MAX_WIDTH));
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		GUILayout.Label("Effect ID: ", GUILayout.MinWidth(GlobalEditor.LABEL_MIN_WIDTH));
		EditorGUILayout.SelectableLabel(_struct.item.ID, GUILayout.MaxWidth(GlobalEditor.OPTIONS_MAX_WIDTH));
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		GUILayout.Label("Base Damage: ", GUILayout.MinWidth(GlobalEditor.LABEL_MIN_WIDTH));
		_struct.item.base_damage = EditorGUILayout.IntSlider(_struct.item.base_damage, 0, 10, GUILayout.MaxWidth(GlobalEditor.OPTIONS_MAX_WIDTH));
		EditorGUILayout.EndHorizontal();

		GUILayout.Space(40);

		GlobalEditor.foldout(ref _visuals_foldout, "Graphics & Representation", EditorStyles.boldLabel);
		if(_visuals_foldout) {
			EditorGUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			_struct.item.left_limb = (LimbPosition)EditorGUILayout.EnumPopup(_struct.item.left_limb, GUILayout.MaxWidth(GlobalEditor.LABEL_MIN_WIDTH));
			GUILayout.FlexibleSpace();
			_struct.item.right_limb = (LimbPosition)EditorGUILayout.EnumPopup(_struct.item.right_limb, GUILayout.MaxWidth(GlobalEditor.LABEL_MIN_WIDTH));
			GUILayout.FlexibleSpace();
			EditorGUILayout.EndHorizontal();

			GUILayout.Space(20);

			EditorGUILayout.BeginHorizontal();
			GlobalEditor.selectFile<GameObject>(ref _struct.item.left_object, GlobalEditor.PREFAB_EXTENSIONS, ref _editor_left_gameobject);
			GlobalEditor.selectFile<GameObject>(ref _struct.item.right_object, GlobalEditor.PREFAB_EXTENSIONS, ref _editor_right_gameobject);
			EditorGUILayout.EndHorizontal();

			GUILayout.Space(20);

			EditorGUILayout.BeginVertical();
			GlobalEditor.selectFile<Texture2D>(ref _struct.item.sprite, GlobalEditor.TEXTURE_EXTENSIONS, ref _editor_logo);
			EditorGUILayout.EndVertical();
		}

		GUILayout.Space(20);		

		chooseBehaviour();

		GUILayout.Space(20);
	}


	/* Allows the user to select a behaviour for each available method of interaction. */
	private void chooseBehaviour() {
		for(int i = 0; i < 2; i++) {

			/* */
			bool foldout_value = i == 0 ? _primary_foldout : _secondary_foldout;
			GlobalEditor.foldout(ref foldout_value, string.Format("Choose {0} Behaviour", i == 0 ? "Primary" : "Secondary"), EditorStyles.boldLabel);
			if(i == 0)
				_primary_foldout = foldout_value;
			else
				_secondary_foldout = foldout_value;


			EditorGUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();


			/* Attempts to acquire the currently cached behaviour. */
			int index_value = i == 0 ? _primary_index : _secondary_index;
			string ID_value = i == 0 ? _struct.item.primary_behaviour_ID : _struct.item.secondary_behaviour_ID;
			if(DatabaseHandler.behaviour_database != null && DatabaseHandler.behaviour_database.names != null && DatabaseHandler.behaviour_database.names.Length > 0) {
				if(index_value == -1) {
					if(ID_value.CompareTo("") != 0)
						index_value = DatabaseHandler.behaviour_database.index(ID_value);
					else
						index_value = 0;
				}
			}


			if(foldout_value) {

				/* Allows the user to select another behaviour from a dropdown menu if everything is working. */
				EditorGUILayout.BeginHorizontal();
				if(index_value == -1) {
					GlobalEditor.label(string.Format("! Having trouble finding behaviour with ID: {0} !", ID_value));
				} else {
					index_value = EditorGUILayout.Popup(index_value, DatabaseHandler.behaviour_database.names);
					ID_value = DatabaseHandler.behaviour_database.list_all[index_value].ID;
				}
				EditorGUILayout.EndHorizontal();
				GUI.color = Color.white;

				/* Applies the values to the currently focused behaviour. */
				if(i == 0) {
					_primary_index = index_value;
					_struct.item.primary_behaviour_ID = ID_value;
				} else {
					_secondary_index = index_value;
					_struct.item.secondary_behaviour_ID = ID_value;
				}

			/* Letting the user see what the currently selected behaviour is without all the extra fluff. */
			} else {
				if(index_value != -1) {
					GlobalEditor.label(string.Format("{0}, ID: \"{1}\"", 
											DatabaseHandler.behaviour_database.list_all[index_value].name, 
											DatabaseHandler.behaviour_database.list_all[index_value].ID),
											EditorStyles.helpBox);
                }
			}

			GUILayout.FlexibleSpace();
			EditorGUILayout.EndHorizontal();

			if(foldout_value)
				inspectBehaviour(i == 0, i == 0 ? _struct.item.primary_behaviour() : _struct.item.secondary_behaviour());
			GUILayout.Space(20);
		}
	}


	/* Briefs the user of the inner workings of their selected behaviour without having to consult the behaviour editor. */
	private void inspectBehaviour(bool p_main, WeaponBehaviour p_behaviour) {
		if(p_behaviour == null)
			return;

		EditorGUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		
		EditorGUILayout.BeginVertical("box", GUILayout.Width(GlobalEditor.LIST_AREA_WIDTH), GUILayout.ExpandHeight(true), GUILayout.Height(GlobalEditor.LABEL_MIN_WIDTH));
		EditorGUILayout.Space();



		GlobalEditor.label("Basics", EditorStyles.boldLabel);
		GlobalEditor.label("Name: " + p_behaviour.name);
		GlobalEditor.label("ID: " + p_behaviour.ID);
		EditorGUILayout.Space();
		GlobalEditor.label(string.Format("Cooldown (On Usage): {0} seconds", p_behaviour.cooldown.ToString("f1")));
		GlobalEditor.label(string.Format("Damage Modifier: {0}% ({1} Damage)", p_behaviour.damage_modifier, _struct.item.damage(p_main)));
		EditorGUILayout.Space();
		EditorGUILayout.Space();
		GlobalEditor.label("Active Modules", EditorStyles.boldLabel);
		GlobalEditor.label(string.Format("AreaOfEffect ({0})", p_behaviour.mod_areaofeffect != null ? "On" : "Off"));



		EditorGUILayout.Space();
		EditorGUILayout.EndVertical();

		GUILayout.FlexibleSpace();
		EditorGUILayout.EndHorizontal();
	}


	/* */
	private void toggleModules(WeaponBehaviour p_behaviour, WeaponBehaviour p_behaviour_backup) {
		moduleToggle(ref p_behaviour.mod_areaofeffect, p_behaviour_backup.mod_areaofeffect, "AreaOfEffect");
	}


	/* */
	private void moduleToggle<T>(ref T module, T backup, string p_tag) where T : class, new() {
		GUI.color = module != null ? GlobalEditor.SELECTED_COLOUR : Color.white;
		if(GUILayout.Button(p_tag, GUILayout.ExpandWidth(false))) {
			module = module != null ? null :
								backup != null ? backup : new T();
		}
	}
}
